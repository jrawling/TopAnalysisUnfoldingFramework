'''
    Author: Jacob Rawling
    Date:

    Utilizes the TopAnalysis.Unfolding package ( ) to create the migration matrix,
    perform the desired type of unfolding, run some tests, save the ouptut in ROOT
    and Journal ready eps fomrat
'''
from TopAnalysis.Unfolding.Unfolder import Unfolder
from TopAnalysis.Unfolding.IdentityTest import IdentityTest
from TopAnalysis.Unfolding.StressTest import StressTest
from TopAnalysis.Unfolding.ClosureTest import ClosureTest
from TopAnalysis.Unfolding.IterationConvergenceTest import IterationConvergenceTest

from TopAnalysis.FakeEvaluator import FakeEvalautor
from TopAnalysis.Unfolding.Systematic import Systematic
from TopAnalysis.SystematicsManager import SystematicsManager
import numpy as np
import config as c
import ROOT as r
from collections import OrderedDict
from TopAnalysis.Backgrounds import background_samples
from unfolder_configs import load_unfolders, print_unfolder_names
from TopAnalysis.SystematicsManager.all_systs import combinations
import sys 

def print_usage():
    print "USAGE: python2.7 perform_nominal_unfoldering <unfolder_name> <aux_settings> <systematic name 1> <systematic name 2> .."
    print "       where unfolder_name is one of the following: "
    print_unfolder_names()
    print "       and systematics one of the follwoing:"
    print "              all " 
    print "       aux_settings should be one of the following "
    print "             None, pdf, fastsim"
    sys.exit()

def main():
    # Read in the unfolding region
    unfolder_names = None
    if len(sys.argv) < 2:
        print_usage()

    unfolder_names = [sys.argv[1]]
    # aux_settings = sys.argv[2:]
    # if "none" in any(aux_settings.lower()):
    aux_settings = ' '.join(sys.argv[2:])
        # aux_settings = None
    systematics = SystematicsManager.select_systematics(sys.argv[2:])


    from TopAnalysis.Backgrounds import background_samples, background_debug_samples
    debug_mode = 'debug' in aux_settings
    if debug_mode:
        bkg_samples = background_debug_samples  
        additional_samples=c.additional_debug_samples
    else:
        bkg_samples = background_samples
        additional_samples=c.additional_samples
    # Load the unfolder from the configurations we have pre-defined 
    unfolders = load_unfolders(unfolder_names, 
                               additional_samples=additional_samples,
                               background_samples=bkg_samples,
                               aux_settings=aux_settings,
                               load_unfolder=True
                               )
    for unfolder in unfolders:

        # Choose which systematics to run over, configured correctly (hopefully) behind the
        # Scenes.
        # See SysteamaticsManager.load_systematics and Unfolding.Systematic for details
        # on these are managed
        unfolder.systematics = SystematicsManager.load_systematics(unfolder, selected_systematics = systematics)

        # The point of this analysis is to compare NLO highly validated Powheg+Pythia8
        # ATLAS Sim to my personally produced MC jobs

        # So let's hand the tool the additional samples we wish to plot
        # at particleLevel a long with the unfolded result
        unfolder.additional_samples = c.additional_samples
        unfolder.background_samples = background_samples

        # We also want to run the systematics
        unfolder.evaluate_systematics()
        unfolder.evaluate_total_systematic()

        # Now draw reco level things to reassure our selves things have loaded correctly
        unfolder.draw_reco_plots()
        unfolder.draw_nominal_unfolding_plots()

        # Draw the unfolded systematics 
        unfolder.draw_systematics(combinations)
                

        #clean up ROOT stuff
        unfolder.close()

if __name__ == "__main__":
    main()