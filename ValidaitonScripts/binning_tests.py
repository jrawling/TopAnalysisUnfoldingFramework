from __future__ import print_function
import ROOT as r
import array
import numpy as np 


test = r.TH2F("", "", 60, 0, 30, 60, 0, 30)
x_axis_binning = [0,5, 30]
y_axis_binning = [0,5,10,15,20,25 ,30]

test_2 = r.TH2F("", "", len(x_axis_binning) - 1,
               array.array('d', x_axis_binning), len(y_axis_binning) - 1,
               array.array('d', y_axis_binning)
               )
r.gStyle.SetPalette(r.kInvertedDarkBodyRadiator)

for a in xrange(1, 587, 1):
    for b in xrange(0, 587, 1):
        c = (a/100.0)**2
        d = (b/100.0)**2
        test.Fill(c, d) 
        test_2.Fill(c,d)

r.gStyle.SetOptStat(0)
c = r.TCanvas()
test.Draw("COLZ")
print("Test integral: ", test.Integral())
c.Print('a.png')

test_2.Draw("COLZ")
print("Test integral: ", test_2.Integral())
c.Print('a_2.png')


def rebin_2d_hist(hist, x_axis_binning,y_axis_binning):
    
    # Get previous binning for hist
    inital_binning = []

    # Create a new 2D Histogram for the binin
    htemp = r.TH2F("", "", len(x_axis_binning) - 1,
                   array.array('d', x_axis_binning), len(y_axis_binning) - 1,
                   array.array('d', y_axis_binning)
                   )

    # Iterate over the bins of the new histogram
    new_hist = np.zeros(  (len(x_axis_binning)+1, len(y_axis_binning)+1) )
    n_entries = np.zeros( (len(x_axis_binning)+1, len(y_axis_binning)+1) )
    errors = np.zeros(    (len(x_axis_binning)+1, len(y_axis_binning)+1) )

    # 
    for x in range(0,hist.GetXaxis().GetNbins()+1):
        for y in range(0, hist.GetYaxis().GetNbins()+1):
            x_val = hist.GetXaxis().GetBinCenter(x)
            new_x_bin = htemp.GetXaxis().FindBin(x_val)

            y_val = hist.GetYaxis().GetBinCenter(y)
            new_y_bin = htemp.GetYaxis().FindBin(y_val)

            bin_content, bin_error = htemp.GetBinContent(new_x_bin, new_y_bin),\
                                     htemp.GetBinError(new_x_bin, new_y_bin)

            bin_error = (bin_error**2 + hist.GetBinError(x,y)**2 )**0.5
            htemp.SetBinContent(new_x_bin, new_y_bin, bin_content + hist.GetBinContent(x,y))
            htemp.SetBinError(new_x_bin, new_y_bin, bin_error )

    assert htemp.Integral() == hist.Integral(), "ERROR: Integral of input and output histograms are unequal, this implies an invalid rebinning scheme."
    return htemp 

x_axis_binning = [0,5, 30]
y_axis_binning = [0,5,10,15,20,25 ,30]

foo = rebin_2d_hist(test, x_axis_binning, y_axis_binning)
foo.Draw("COLZ")
print("foo integral: ", foo.Integral())
c.Print('b.png')



x_axis_binning = [0,0.1 ,30]
y_axis_binning = [0, 0.1 ,30]
foo = rebin_2d_hist(test, x_axis_binning, y_axis_binning)
foo.Draw("COLZ")
print("foo integral: ", foo.Integral())
c.Print('C.png')