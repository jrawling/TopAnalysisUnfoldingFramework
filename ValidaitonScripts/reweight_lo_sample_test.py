from __future__ import print_function
import argparse
import ROOT as r
import RootHelperFunctions.RootHelperFunctions as rhf
import RootHelperFunctions.StringHelperFunctions as shf
import RootHelperFunctions.DrawingHelperFunctions as dhf
import ATLASStyle.AtlasStyle as AS
import copy
from collections import OrderedDict
import numpy as np
import array
from time import time

# import threading
import os
import psutil
import numpy as np
import json
import logging
import sys
import glob 

class Sample:
    def __init__(self, config_file):
        """
        @Brief a class that managers ntuple outputs grouped together and drawn
        together

        @Parmameters:
        config_file: A JSON file with infomraiton of the sample at hand. 
        """

        # Read in the relveant information from a JSON File 
        logging.info('Reading sample from file %s'%config_file)
        try:
            with open(config_file) as json_data:
                self.config = json.load(json_data)
        except IOError:
            logging.critical('Failed to open sample configuration file %s.'%(config_file))
            sys.exit(-1)

        self.name = self.config["name"]
        self.eval_luminosity = self.config["evaluate_luminosity"]

        self.input_file_names = []
        self.k_factors = []
        self.x_secs = []
        self.data_period = []

        # Iterate over the saved samples and convert the order json dictioanry
        # into the expected format that RHF drawing tools wants 
        for sample in self.config["files"]:
            self.input_file_names.append(glob.glob(sample["folder"]))
            self.k_factors.append(sample["k_factor"])
            self.x_secs.append(sample["x_sec"])
            self.data_period.append(sample["data_period"])

        if self.eval_luminosity:
            self.evaluate_luminosity()
        else:
            self.mc_lumi = []
            for i in self.x_secs:
                self.mc_lumi.append(1.0)

        logging.info('    Reading sample %s'%self.config["name"])

    def evaluate_luminosity(self):
        """
        @Brief read the luminsotiy of each dataset stored in this sample; this
        is evaluated dynamically fromthe sumWeights tree in thefile. 
        """
        self.mc_lumi = []
        for i, file_list in enumerate(self.input_file_names):
            assert len(file_list) != 0, 'ERROR: signal_sample file list is empty!'

            # Evaluate n
            nEvents = 0
            sum_weights_chain = r.TChain("sumWeights")
            for f in file_list:
                sum_weights_chain.Add(f)
            for e in sum_weights_chain:
                nEvents += e.totalEventsWeighted

            # Then evaluating the MC luminosity
            mc_luminosity_nom = nEvents/(self.x_secs[i]*self.k_factors[i])
            self.mc_lumi.append(mc_luminosity_nom)

class ReweightedPlottingTool:
    def __init__(self, config_file, verbosity=logging.DEBUG, recalculate_weights=False):
        """
        @Brief a class that reads in a configuration JSON file with all sample and
        style options pre-defined to produce a set of reweighted plotting tests

        @Parameters
        config_file: Full absolute path to JSON configuraiton file 

        verbosity: logging level as defined  by the python loggin clas, 
        see https://docs.python.org/2/library/logging.html for more information

        recalculate_weights: By default the reweighting procedure is only 
        recalculated if the expect reweighting configruation file is not found. 
        This file is generated automatically by this tool. This flag will force
        the recalculation of the reweighting string. 
        """

        # Set up the logger
        logging.basicConfig(format='PlottingTool:%(levelname)s %(message)s', 
                            level=verbosity)

        # Hide the annoying root messages
        rhf.hide_root_infomessages()

        self.parse_configuration_file(config_file)

        self.load_samples()

        if recalculate_weights:
            logging.warning("Recreating the reweighintg string and plots! ")

        self.macro_name = self.config["reweighting_string_config_folder"]+self.config["name"]+".C"
        self.config["output_folder"] = self.config["output_folder"]+"/"+self.config["name"]

        # Check whether we need to recreate the reweighting string, this
        if np.DataSource().exists(self.macro_name) and \
            not recalculate_weights:
            # attempt to read the weight string from the json file
            logging.info("Reading reweighting tool from file %s "%(self.macro_name))
            r.gROOT.ProcessLine(".L %s"%(self.macro_name))
            self.reweight_string="GetReweightingSF(%s)"%( self.config["float_var"] )

        else:
            # If the config file to store the reweighting string is not 
            # avalaible then create it 
            self.create_reweighting_string()


    def create_reweighting_string(self):
        """
        @Brief draws a histogram of reference_var for both the reference sample 
        and spectator samples. THe ratio of this provides the weights which
        are saved in a string and a selection of cuts based on reference_var
        """
        logging.info("Constructing the reweighting string....")
        logging.info("  Constructing reference histogram")

        # Create the reference histogram 
        self.reference_reweight_hist = rhf.get_histogram_from_sample(
                                            sample=self.reference_sample,
                                            ntuple_name=self.config["reference_tuple"],
                                            variable_name=self.config["reference_var"],
                                            x_axis_binning=self.config["reweighting_var_binning"],
                                            weight=self.config["reference_weight"],
                                            scale_to_lumi=False
                                            )
        rhf.normalize_histogram(self.reference_reweight_hist)
        self.reference_reweight_hist.SetDirectory(0)
        logging.info("  Constructing floating histogram")


        # Create the histogram of the reweighting variable for the floating 
        # sample, i.e numerator of division and thinghat is being reweighting to 
        # match the reference sample 
        self.float_reweight_hist = rhf.get_histogram_from_sample(
                                            self.float_sample,
                                            self.config["float_tuple"],
                                            self.config["float_var"],
                                            self.config["reweighting_var_binning"],
                                            self.config["float_weight"],
                                            scale_to_lumi=False
                                            )
        rhf.normalize_histogram(self.float_reweight_hist)
        self.float_reweight_hist.SetDirectory(0)


        # Create the weighting histogram 
        logging.info("  Constructing weight distribution.")
        self.weight_hist =  self.reference_reweight_hist.Clone("weight_hist")
        self.weight_hist.Divide(self.float_reweight_hist)

        if self.config["apply_smoothing"]: 
            self.unsmoothed_weight_hist =  self.weight_hist.Clone("unsmoothedweight_hist")
            self.weight_hist.Smooth()
        

        # Dump the reweighting string to a a C macro, this allows for arbitrary
        # amounts of binning as we are nolonger restricted by 
        self.reweight_string = "float GetReweightingSF(float x){ \n  float scale=1.0;\n"
        for i, x in enumerate(self.config["reweighting_var_binning"][:-1]):
            if i == 0:
                self.reweight_string += "  if(x < %.2f) scale = %.5f;\n"%(
                        self.config["reweighting_var_binning"][i+1],
                        self.weight_hist.GetBinContent(i+1)
                    )
            elif i == len(self.config["reweighting_var_binning"][:-1])-1:       
                self.reweight_string += "  else scale = %.5f; \n  return scale;\n}"%(
                        self.weight_hist.GetBinContent(i+1)
                    )

            else:
                self.reweight_string += "  else if(x < %.2f) scale = %.5f;\n"%(
                        self.config["reweighting_var_binning"][i+1],
                        self.weight_hist.GetBinContent(i+1)
                    )

        # Dump this string to a macro file 

        try:
            with open(self.macro_name, "w") as write_file:
                write_file.write(self.reweight_string) 
        except IOError:
            logging.critical("Failed to save to file %s"%(self.macro_name))
            sys.exit(-1)

        r.gROOT.ProcessLine(".L %s"%(self.macro_name))
        self.reweight_string="GetReweightingSF(%s)"%( self.config["float_var"] )


        # Create a validation plot to ensure the behaviour is as expected in the
        # reweighting variable 
        if self.config["run_closure_check"]:
            self.run_closure_check()

        # 
        logging.info("Reweighintg saved to %s"%(self.macro_name) )


    def run_closure_check(self):
        """
        @Brief apply the derived reweighting to the floating variable in bins of 
        the reweighting_var and plot the results. We expect perfect agreeement     
        """
        logging.info("Running closure closre check")                
        self.plotting_closure_dictionary = OrderedDict()
        self.plotting_closure_dictionary[self.config["reference_label"]] = [
                     self.reference_reweight_hist,
                     dhf.get_truth_large_legend_stlye_opt(0,is_ratio=False,show_legend=True,line_width=2),
                     dhf.get_truth_large_legend_stlye_opt(0,is_ratio=True,show_legend=True,line_width=2, drawing_options="HIST E1"),
                     None
            ]

        self.float_closure_hist = rhf.get_histogram_from_sample(
                                            self.float_sample,
                                            self.config["float_tuple"],
                                            self.config["float_var"],
                                            self.config["reweighting_var_binning"],
                                            self.config["float_weight"]+"*"+
                                            self.reweight_string,
                                            scale_to_lumi=False,
                                            )
        rhf.normalize_histogram(self.float_closure_hist)
        self.float_reweight_hist.SetDirectory(0)

        # Format this intl our dictionary 
        self.plotting_closure_dictionary[self.float_sample.name] = [
                     self.float_reweight_hist,
                     dhf.get_truth_large_legend_stlye_opt(2,is_ratio=False,show_legend=True,line_width=2),
                     dhf.get_truth_large_legend_stlye_opt(2,is_ratio=True,show_legend=True,line_width=2, drawing_options="HIST E1"),
                     self.config["reference_label"]
            ]
        self.plotting_closure_dictionary[self.float_sample.name+" Reweighted"] = [
                     self.float_closure_hist,
                     dhf.get_truth_large_legend_stlye_opt(3,is_ratio=False,show_legend=True,line_width=2),
                     dhf.get_truth_large_legend_stlye_opt(3,is_ratio=True,show_legend=True,line_width=2, drawing_options="HIST E1"),
                     self.config["reference_label"]
            ]

        self.plot_closure()


    def parse_configuration_file(self, config_file):
        """
        @Brief stores the required info in the configuration file into the class

        @Parameters
        config_file: Full absolute path to JSON configuraiton file 
        """
        logging.info('Reading plotting configuration: %s'%(config_file))

        #
        try:
            with open(config_file) as json_data:
                self.config = json.load(json_data)

                # Log the information see we can keep track of what we've actually
                # loaded.
                for prop in self.config:
                    logging.info('\t%s: \t\t%s'%(prop, str(self.config[prop])))
        except IOError:
            logging.critical('Failed to open configuration file %s.'%(config_file))
            sys.exit(-1)
    
        if isinstance(self.config["reweighting_var_binning"], unicode) or \
           isinstance(self.config["reweighting_var_binning"], str):

            # User handing binning information in ROOT histogram style, format
            # this into a list as epxected by the plotting scrips. 
            binning_info = self.config["reweighting_var_binning"].split(',')
            self.config["reweighting_var_binning"] = list(np.linspace(
                    float(binning_info[1]),
                    float(binning_info[2]),
                    int(binning_info[0]),
                )) 
            # Shout about the evaluated binning cehme 
            logging.info('Binning evaluated as: %s'%(str(self.config["reweighting_var_binning"])))

    def load_samples(self):
        """
        Loads the details of the samples form their configraution files, as expected
        by the initial config file for this. 
        """ 
        self.reference_sample = Sample(
                config_file=self.config["reference_sample"]
            )
        self.float_sample = Sample(
                config_file=self.config["float_sample"]
            )
        
        self.spectator_samples = []
        for sample_config in self.config["spectator_samples"]:
            self.spectator_samples.append(Sample(config_file=sample_config))


    def construct_reweighted_distributions(self):
        """
        """
        logging.info("Constructing the reweighting histograms of interest...")

        #Create a dictionary in the format the ratio plotting support function
        # expects. 
        self.plotting_dictionary = OrderedDict()

        # Create the reference histogram 
        logging.info("  Constructing reference histogram: %s"%(self.reference_sample.name))

        if self.config["reference_tuple"] in self.config["reference_voi"] or \
            self.config["reference_tuple"] in self.config["reference_weight"]:
            friend_name=self.config["reference_tuple"]
        else:
            friend_name=None

        self.reference_reweight_voi_hist = rhf.get_histogram_from_sample(
                                            sample=self.reference_sample,
                                            ntuple_name=self.config["reference_voi_tuple"],
                                            variable_name=self.config["reference_voi"],
                                            x_axis_binning=self.config["variable_of_interest_binning"],
                                            weight=self.config["reference_voi_weight"],
                                            scale_to_lumi=False,
                                            friend_name=friend_name
                                            )
        rhf.normalize_histogram(self.reference_reweight_voi_hist)
        self.reference_reweight_voi_hist.SetDirectory(0)

        # Format and store the histogram 
        self.plotting_dictionary[self.config["reference_label"]] = [
                     self.reference_reweight_voi_hist,
                     dhf.get_truth_large_legend_stlye_opt(0,is_ratio=False,show_legend=True,line_width=2),
                     dhf.get_truth_large_legend_stlye_opt(0,is_ratio=True,show_legend=True,line_width=2),
                     None
            ]



        # Create the histogram of the reweighting variable for the floating sample
        logging.info("  Constructing floating histogram without reweighting: %s"%(self.float_sample.name))
        self.float_reweight_voi_hist = rhf.get_histogram_from_sample(
                                            sample=self.float_sample,
                                            ntuple_name=self.config["float_voi_tuple"],
                                            variable_name=self.config["float_voi"],
                                            x_axis_binning=self.config["variable_of_interest_binning"],
                                            weight=self.config["float_voi_weight"],
                                            scale_to_lumi=False,
                                            friend_name=self.config["float_tuple"]
                                            )
        rhf.normalize_histogram(self.float_reweight_voi_hist)
        self.float_reweight_voi_hist.SetDirectory(0)

        # Format this intl our dictionary 
        self.plotting_dictionary[self.float_sample.name] = [
                     self.float_reweight_voi_hist,
                     dhf.get_truth_large_legend_stlye_opt(2,is_ratio=False,show_legend=True,line_width=2),
                     dhf.get_truth_large_legend_stlye_opt(2,is_ratio=True,show_legend=True,line_width=2, drawing_options="HIST E1"),
                     self.config["reference_label"]
            ]

        # Create the histogram of the reweighting variable for the floating sample
        logging.info("  Constructing floating histogram: %s"%(self.float_sample.name))
        self.float_reweight_voi_hist = rhf.get_histogram_from_sample(
                                            sample=self.float_sample,
                                            ntuple_name=self.config["float_voi_tuple"],
                                            variable_name=self.config["float_voi"],
                                            x_axis_binning=self.config["variable_of_interest_binning"],
                                            weight=self.config["float_voi_weight"]+"*"+
                                            self.reweight_string,
                                            scale_to_lumi=False,
                                            friend_name=self.config["float_tuple"]
                                            )
        rhf.normalize_histogram(self.float_reweight_voi_hist)
        self.float_reweight_voi_hist.SetDirectory(0)

        # Format this intl our dictionary 
        self.plotting_dictionary[self.float_sample.name + " RW"] = [
                     self.float_reweight_voi_hist,
                     dhf.get_truth_large_legend_stlye_opt(3,is_ratio=False,show_legend=True,line_width=2),
                     dhf.get_truth_large_legend_stlye_opt(3,is_ratio=True,show_legend=True,line_width=2, drawing_options="HIST E1"),
                     self.config["reference_label"]
            ]
        self.create_spectator_plots()

    def create_spectator_plots(self):
        """
        @Create distributions of variable of interest for spectator sampels 
        passed to the tool
        """
        for i, sample in enumerate(self.spectator_samples):
            
            logging.info('creating spectator voi plot %s'%(sample.name))

            voi_hist = rhf.get_histogram_from_sample(
                                            sample=sample,
                                            ntuple_name=self.config["float_voi_tuple"],
                                            variable_name=self.config["float_voi"],
                                            x_axis_binning=self.config["variable_of_interest_binning"],
                                            weight=self.config["float_voi_weight"],
                                            scale_to_lumi=False,
                                            friend_name=self.config["float_tuple"]
                                            )
            rhf.normalize_histogram(voi_hist)
            voi_hist.SetDirectory(0)

            # Format this intl our dictionary 
            self.plotting_dictionary[sample.name] = [
                         voi_hist,
                         dhf.get_truth_large_legend_stlye_opt(i+4,is_ratio=False,show_legend=True,line_width=2),
                         dhf.get_truth_large_legend_stlye_opt(i+4,is_ratio=True,show_legend=True,line_width=2),
                         self.config["reference_label"]
                ]

            logging.info('creating spectator reweighted voi plot %s'%(sample.name))
            reweight_voi_hist = rhf.get_histogram_from_sample(
                                            sample=sample,
                                            ntuple_name=self.config["float_voi_tuple"],
                                            variable_name=self.config["float_voi"],
                                            x_axis_binning=self.config["variable_of_interest_binning"],
                                            weight=self.config["float_voi_weight"]+"*"+
                                            self.reweight_string,
                                            scale_to_lumi=False,
                                            friend_name=self.config["float_tuple"]
                                            )
            rhf.normalize_histogram(reweight_voi_hist)
            reweight_voi_hist.SetDirectory(0)

            # Format this intl our dictionary 
            self.plotting_dictionary[sample.name + " RW"] = [
                         reweight_voi_hist,
                         dhf.get_truth_large_legend_stlye_opt(i+5,is_ratio=False,show_legend=True,line_width=2),
                         dhf.get_truth_large_legend_stlye_opt(i+5,is_ratio=True,show_legend=True,line_width=2, drawing_options="HIST E1"),
                         self.config["reference_label"]
                ]

    def plot_distributions(self):
        """
        @Brief save the reweighted variable of interest (voi) histograms to the
        folderr. 
        """

        logging.info("Saving reweighted disitributions to folder: %s"%(self.config["output_folder"]))
        rhf.create_folder(self.config["output_folder"])

        canvas = r.TCanvas(self.config["name"] + "reco_distribution",
                           self.config["name"] + "reco_distribution", 600, 600)
        r.SetOwnership(canvas, 0)

        ratio_histograms = dhf.ratio_plot(canvas,
                                          self.plotting_dictionary,
                                          x_axis_title=self.config['voi_x_axis_title'],
                                          y_axis_title="Normalized number of events",
                                          messages=self.config['messages'],
                                          leg_font_size=0.032,
                                          ratio_y_axis_title="#frac{Prediciton}{%s}"%(
                                                self.config["reference_label"]
                                            ),
                                          )
        canvas.Print(self.config["output_folder"]+"/"+self.config["name"]+".png")
        canvas.Print(self.config["output_folder"]+"/"+self.config["name"]+".eps")

    def plot_closure(self):
        """
        @Brief save the created reweighted histograms to the oflder. 
        """
        logging.info("Saving reweighted closure disitributions to folder: %s"%(self.config["output_folder"]))
        rhf.create_folder(self.config["output_folder"])

        canvas = r.TCanvas(self.config["name"] + "reco_distribution",
                           self.config["name"] + "reco_distribution", 600, 600)
        r.SetOwnership(canvas, 0)

        ratio_histograms = dhf.ratio_plot(canvas,
                                          self.plotting_closure_dictionary,
                                          x_axis_title=self.config['reference_x_axis_label'],
                                          y_axis_title="Normalized number of events",
                                          messages=self.config['messages'],
                                          leg_font_size=0.032,
                                          ratio_y_axis_title="#frac{Prediciton}{%s}"%(
                                                self.config["reference_label"]
                                            ),
                                          )
        # 
        canvas.Print(self.config["output_folder"]+"/closure.png")
        canvas.Print(self.config["output_folder"]+"/closure.eps")
        logging.info("Created %s"%(self.config["output_folder"]+"/closure.png"))
        logging.info("Created %s"%(self.config["output_folder"]+"/closure.eps"))

# Main entry point for this script
# ToDO: move this class over to tools and write sperate running scripts. 
def main():
    parser = argparse.ArgumentParser(
        description='A general configuration based plotting script for NTuples.')

    # We really only want two default 
    parser.add_argument("configuration_file", type=str,
                        help="The plotting configuration file to draw.")
    parser.add_argument("-v", "--verbosity", type=int, choices=[10, 20, 30, 40, 50], default=10,
                        help="Increase output verbosity")
    parser.add_argument("-r", "--recalculate_reweights", action='store_true',
                        help="Force the recalculation of the reweighting string."
                             "WARNING: Will overwrite previous output files if they exist. ")
    # 
    args = parser.parse_args()

    # Map the verbosity configuration options onto
    reweighting_plotting_tool = ReweightedPlottingTool(config_file=args.configuration_file,
                                 verbosity=args.verbosity,
                                 recalculate_weights=args.recalculate_reweights)

    reweighting_plotting_tool.construct_reweighted_distributions()

    reweighting_plotting_tool.plot_distributions()

# 
main()

