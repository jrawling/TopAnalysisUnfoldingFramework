"""
Author: Jacob Rawling <jrawling@cern.ch>
Date: 05/03/2018

A quick script to calcuate the best method of reconstruction the ttbar system
in lepton+jet events. It copmares three methods
    1. Chi2: Minimum Chi2 = sum_{had_top,lep_top,had_W}[ delta_mass(reco,truth)/width ]
    2. LepFirst_Chi2: Minimum Chi2 = sum_{had_top,had_W}[ delta_mass(reco,truth)/width ],
       the leptonic b is chosen to be the closest the W
    3. Ground truth: the pairing that has the closest <mass/angular/energy> resolution
                     w.r.t the partonic system

"""
import ROOT as r
import RootHelperFunctions.RootHelperFunctions as rhf
import ATLASStyle.AtlasStyle as AS
import RootHelperFunctions.StringHelperFunctions as shf
import RootHelperFunctions.DrawingHelperFunctions as dhf
import math
from copy import copy, deepcopy
import array


class Jet:
    """
    Container for TLorentZvectors that also can store the particle ID
    that the fourmomenta represents.
    """
    def __init__(self, Pt, Eta, Phi, E, pdg_id, energy_is_mass=False):
        self.jet = r.TLorentzVector()
        if energy_is_mass:
            self.jet.SetPtEtaPhiM(Pt, Eta, Phi, E)
        else:
            self.jet.SetPtEtaPhiE(Pt, Eta, Phi, E)

        self.id = pdg_id

    def __add__(a, b):
        """
        """
        if isinstance(a, r.TLorentzVector) and isinstance(b, r.TLorentzVector):
            total_p = a+ b
        elif isinstance(a, r.TLorentzVector) and not isinstance(b, r.TLorentzVector):
            total_p = b.jet + a
        elif not isinstance(a, r.TLorentzVector) and isinstance(b, r.TLorentzVector):
            total_p = a.jet + b
        else:
            total_p = a.jet + b.jet
        return Jet(total_p.Pt(),total_p.Eta(), total_p.Phi(), total_p.E(), None)

    def DeltaR(self, jet):
        if isinstance(jet, r.TLorentzVector):
            return jet.DeltaR(self.jet)
        return jet.jet.DeltaR(self.jet)

    def Pt(self):
        return self.jet.Pt()

    def Eta(self):
        return self.jet.Eta()

    def Phi(self):
        return self.jet.Phi()

    def E(self):
        return self.jet.E()

    def M(self):
        return self.jet.M()


class HadronicW:
    def __init__(self, jet_1, jet_2, i, j, light_jets):
        self.W = jet_1+jet_2
        self.i = i
        self.j = j
        self.additional_jets = []
        for index, jet in enumerate(light_jets):
            if index != i and index != j:
                self.additional_jets.append(jet)

        if len(self.additional_jets) == 0:
            print "ERROR: No additional jets found in sample."
            print ""
            print ""
            print ""
            self.leading_jet = None
            return

        self.leading_jet = self.additional_jets[0]
        for jet in self.additional_jets[1:]:
            if jet.Pt() > self.leading_jet.Pt():
                self.leading_jet = jet



class TTBar:
    def __init__(self, hadronic_W, leptonic_b,hadronic_b, leptonic_W, leptonic_W_other, leading_jet):
        self.hadronic_b = hadronic_b
        self.leptonic_b = leptonic_b
        self.hadronic_W = hadronic_W
        self.leptonic_W = leptonic_W
        self.leptonic_W_other = leptonic_W_other

        #
        self.hadronic_top = hadronic_W + self.hadronic_b.jet
        self.leptonic_top = leptonic_W + self.leptonic_b.jet
        self.leading_additional_jet = leading_jet

    def is_leptonic_event(self):
        return self.leptonic_top.DeltaR(self.leading_additional_jet.jet) < self.hadronic_top.DeltaR(self.leading_additional_jet.jet)

    def Theta_lep(self):
        theta_d = self.leptonic_top.M()/self.leptonic_top.E()
        theta = self.leading_additional_jet.DeltaR(self.leptonic_top)#.DeltaR()
        Theta = theta/theta_d
        return Theta

    def Theta_had(self):
        theta_d = self.hadronic_top.M()/self.hadronic_top.E()
        theta = self.leading_additional_jet.DeltaR(self.hadronic_top)
        Theta = theta/theta_d
        return Theta

    def A_lep(self):
        theta_d = self.leptonic_top.M()/self.leptonic_top.E()
        theta = self.leading_additional_jet.DeltaR(self.leptonic_top)
        A = (theta-theta_d)/(theta+theta_d)
        return A

    def A_had(self):
        theta_d = self.hadronic_top.M()/self.hadronic_top.E()
        theta = self.leading_additional_jet.DeltaR(self.hadronic_top)
        A = (theta-theta_d)/(theta+theta_d)
        return A

    def closest_b_to_lep_is_leptonic_b(self,lepton):
        return lepton.DeltaR(self.leptonic_b.jet) < lepton.DeltaR(self.hadronic_b.jet)

    def chi2(self):
        top_width = 1.35e3
        top_mass = 172.44e3
        w_width = 2.085e3
        w_mass = 80.385e3
        return ((self.hadronic_W.M() - w_mass)/w_width)**2 + ((top_mass - self.hadronic_top.M())/top_width)**2 + ((top_mass - self.leptonic_top.M())/top_width)**2


class TTBarSystems:
    def __init__(self,  leptonic_W, lepton, MET):
        self.leptonic_W =  leptonic_W
        self.lepton = lepton
        self.MET = MET
        self.tops = []
        self.calculate_leptonic_W()

    def append(self, hadronic_W, leptonic_b,hadronic_b):
        self.tops.append(TTBar(hadronic_W.W,
                leptonic_b,
                hadronic_b,
                self.leptonic_W,
                self.leptonic_W_other,
                hadronic_W.leading_jet)
        )

    def lowest_chi2(self):
        min_chi2, lowest_top = self.tops[0].chi2(), self.tops[0]
        for top in self.tops[1:]:
            top_chi2 = top.chi2()
            if top_chi2 < min_chi2:
                lowest_top = top
                min_chi2 = top_chi2
        return lowest_top, min_chi2

    def lowest_chi2_of_leptonic_closeby_b(self):
        min_chi2 = 1e23
        for i, top in enumerate(self.tops):
            if not top.closest_b_to_lep_is_leptonic_b(self.lepton):
                continue

            top_chi2 = top.chi2()
            if top_chi2 < min_chi2:
                lowest_top = top
                min_chi2 = top_chi2

        return lowest_top, min_chi2

    def best_angular_res(self, truth_ttbar):
        min_dR, best_top = 1e3, None
        for i, top in enumerate(self.tops):
            dR = truth_ttbar.hadronic_top.DeltaR(top.hadronic_top)
            dR += truth_ttbar.leptonic_top.DeltaR(top.leptonic_top)
            if dR < min_dR:
                best_top = top
                min_dR = dR
        return min_dR, best_top


    def best_physics_objects(self, truth_ttbar):
        min_dR, best_top = 1e3, None

        # For each top we are going to construct the best possible
        # reconstruction,  we still need to permute through the combinations
        # as we meed
        best_tops = deepcopy(self.tops)
        for i,top in enumerate(best_tops):
            # For this top event FORCE each object to be the closest matching one possible
            # print "truth_ttbar.hadronic_W = ", truth_ttbar.hadronic_W
            # print "top.hadronic_W = ", top.hadronic_W
            dR = truth_ttbar.hadronic_W.DeltaR(top.hadronic_W.jet)

            # Shuffle the b's if we've got a far away match
            if truth_ttbar.leptonic_b.jet.DeltaR(top.hadronic_b.jet) < truth_ttbar.leptonic_b.jet.DeltaR(top.leptonic_b.jet):
                top_lep_b = deepcopy(top.leptonic_b)
                top.leptonic_b = top.hadronic_b
                top.hadronic_b = top_lep_b
                top.hadronic_top = top.hadronic_b + top.hadronic_W
                top.leptonic_top = top.leptonic_b + top.leptonic_W

            # Select the very best leptonic W
            if truth_ttbar.leptonic_W.DeltaR(top.leptonic_W_other) < truth_ttbar.leptonic_W.DeltaR(top.leptonic_W):
                top.leptonic_W = top.leptonic_W_other
                top.leptonic_top = top.leptonic_b + top.leptonic_W

            # Chose the W with the best matchin
            dR += truth_ttbar.leptonic_W.DeltaR(top.leptonic_W)
            dR += truth_ttbar.leptonic_b.jet.DeltaR(top.leptonic_b.jet)
            dR += truth_ttbar.hadronic_b.jet.DeltaR(top.hadronic_b.jet)

            if dR < min_dR:
                best_top = top
                min_dR = dR
        return min_dR,best_top

    def calculate_leptonic_W(self):
        """
        Use the constrainnt of the W being on shell to solve for the
        unmeasureable neutrino momentum in the z-direction.

        Follow LHC Top WG recommendiations and chose the solution that has
        largest absolute magnitude.
        """
        KinemEdge = 13.9e9
        mWPDG     = 80.399e3
        mTop      = 173.0e3
        v_pz  = -KinemEdge
        delta = -KinemEdge

        # get the MET x and y components for the reconstruction
        nu_px  = self.MET.Px()
        nu_py  = self.MET.Py()
        met_et =  math.sqrt(nu_px*nu_px + nu_py*nu_py)

        # lepton already made by reco or particle levels:
        l_px, l_py, l_pz = self.lepton.Px(),self.lepton.Py(),self.lepton.Pz()
        l_m, l_E = self.lepton.M(),self.lepton.E()

        mdiff = 0.5 * ( mWPDG*mWPDG - l_m*l_m )
        pT_vl = nu_px*l_px + nu_py*l_py

        a = l_E*l_E - l_pz*l_pz
        b = -2. * l_pz * ( mdiff + pT_vl )
        c = met_et*met_et*l_E*l_E - mdiff*mdiff - pT_vl*pT_vl - 2.*mdiff*pT_vl
        v_pz_2 = -0.5*b/a
        delta = b*b - 4.*a*c

        # Complex solution!
        if delta <= 0.:
            # take only the real part
            v_pz = -0.5*b/a
            v_pz_other = v_pz

        else:
            v_pz_1 = 0.5 * ( -b - math.sqrt(delta) ) / a
            v_pz_2 = 0.5 * ( -b + math.sqrt(delta) ) / a
            v_E_1  = math.sqrt( nu_px*nu_px +nu_py*nu_py + v_pz_1*v_pz_1 )
            v_E_2  = math.sqrt( nu_px*nu_px +nu_py*nu_py + v_pz_2*v_pz_2 )

            # Need to put the correct choice in here
            v_1,v_2 = r.TLorentzVector(),r.TLorentzVector()
            v_1.SetPxPyPzE( nu_px, nu_py, v_pz_1, v_E_1 )
            v_2.SetPxPyPzE( nu_px, nu_py, v_pz_2, v_E_2 )

            if abs(v_pz_2) > abs(v_pz_1):
                v_pz = v_pz_2
                v_pz_other = v_pz_1
            else:
                v_pz = v_pz_1
                v_pz_other = v_pz_2


        v_E  = math.sqrt( nu_px*nu_px +nu_py*nu_py + v_pz*v_pz )
        v_E_other  = math.sqrt( nu_px*nu_px +nu_py*nu_py + v_pz_other*v_pz_other )

        self.MET.SetPxPyPzE( nu_px, nu_py, v_pz, v_E )
        self.MET_other, self.leptonic_W_other = r.TLorentzVector(), r.TLorentzVector()
        self.MET_other.SetPxPyPzE( nu_px, nu_py, v_pz, v_E_other )

        self.leptonic_W = self.MET + self.lepton
        self.leptonic_W_other = self.MET_other + self.lepton


class TTBarReconstructor:
    """
    A class that constructs the permutations and required objects
    per event
    """
    def __init__(self,
                 input_files,
                 truth_tuple="truth",
                 reco_tuple="nominal",
                 partice_tuple="particleLevel",
                 bjet_working_point=""
                 ):
        self.input_files = input_files
        self.truth_tuple = truth_tuple
        self.reco_tuple = reco_tuple
        self.partice_tuple = partice_tuple
        self.bjet_working_point = bjet_working_point
        self.created_histograms = {}
        self.histograms = {}
        self.corr_histograms = {}
        self.save_output = False

        self.create_histograms("Best #chi2")
        self.create_histograms("Best #chi^{2} given leptonic b")
        self.create_histograms("Best Possible")
        self.create_histograms("Particle Level")
        self.create_histograms("Baseline")
        self.create_histograms("LHCTopWG")
        # self.create_histograms("Truth Ws")(
        self.create_histograms("Truth")


        r.gROOT.SetBatch()
    def log(self, *messages):
        """
        Output a message with a sufix indicating this tool created the message

        """
        message = "[TTBAR-REC] - "
        for msg in messages:
            message += str(msg) + " "
        print message

    def process(self):
        """
        Iterate over all events in the files and calculate all possible
        permuations
        """

        # Go file by file, we store them as a list of files per
        # DSID, so its an array of arrays
        for file_list in self.input_files:
            for file in file_list:
                self.log("Processing file", file)
                self.process_file(file)


    def create_histograms(self, name):
        """
        """
        self.histograms[name+"had R_m"]      = r.TH1F(name+"had R_m",name+"had R_m",40,0.5,2)
        self.histograms[name+"had deltaEta"] = r.TH1F(name+"had deltaEta",name+"had deltaEta",40,-3,3)
        self.histograms[name+"had deltaPhi"] = r.TH1F(name+"had deltaPhi",name+"had deltaPhi",40,-3,3)
        self.histograms[name+"had R_pT"]     = r.TH1F(name+"had R_pT",name+"had R_pT",40,0.5,2)

        self.histograms[name+"lep R_m"]      = r.TH1F(name+"lep R_m",name+"lep R_m",40,0.5,2)
        self.histograms[name+"lep deltaEta"] = r.TH1F(name+"lep deltaEta",name+"lep deltaEta",40,-3,3)
        self.histograms[name+"lep deltaPhi"] = r.TH1F(name+"lep deltaPhi",name+"lep deltaPhi",40,-3,3)
        self.histograms[name+"lep R_pT"]     = r.TH1F(name+"lep R_pT",name+"lep R_pT",40,0.5,2)

        self.histograms[name+"is_gluon"]     = r.TH1F(name+"is_gluon",name+"is_gluon",2,0,2)


        self.histograms[name+"lep eta"] = r.TH1F(name+"lep #eta",name+"lep #eta",40,-5,5)
        self.histograms[name+"lep phi"] = r.TH1F(name+"lep #phi",name+"lep #phi",40,-5,5)
        self.histograms[name+"lep p_T"]  = r.TH1F(name+"lep p_T",name+"lep p_T",40,0,500)
        self.histograms[name+"lep M"]    = r.TH1F(name+"lep M",name+"lep M",50,100,250)

        self.histograms[name+"had eta"] = r.TH1F(name+"had #eta",name+"had #eta",40,-5,5)
        self.histograms[name+"had phi"] = r.TH1F(name+"had #phi",name+"had #phi",40,-5,5)
        self.histograms[name+"had p_T"]  = r.TH1F(name+"had p_T",name+"had p_T",40,0,500)
        self.histograms[name+"had M"]    = r.TH1F(name+"had M",name+"had M",50,100,250)

        self.corr_histograms[name+"lep eta"]  = r.TProfile(name+"corr lep #eta",name+"corr lep #eta",20,-5,5, )
        self.corr_histograms[name+"lep phi"]  = r.TProfile(name+"corr lep #phi",name+"corr lep #phi",20,-5,5, )
        x_binning = [20, 40, 60, 80, 100]
        self.corr_histograms[name+"lep pT"]   = r.TProfile(name+"corr lep p_T", name+"corr lep p_T", len(x_binning)-1,array.array('d',x_binning) )
        x_binning = [100, 130, 170, 250, 300, 400, 600]
        self.corr_histograms[name+"lep pT high"]   = r.TProfile(name+"corr lep p_T high", name+"corr lep p_T high", len(x_binning)-1,array.array('d',x_binning) )
        x_binning = [130, 145, 160, 165, 170, 175, 180, 195, 210]
        self.corr_histograms[name+"lep M"]    = r.TProfile(name+"corr lep M",   name+"corr lep M",   len(x_binning)-1,array.array('d',x_binning) )
        self.corr_histograms[name+"lep M fine"]    = r.TProfile(name+"corr lep M fine",   name+"corr had M fine",  30,165, 185 )

        self.corr_histograms[name+"had eta"]  = r.TProfile(name+"corr had #eta",name+"corr had #eta",20,-5,5, )
        self.corr_histograms[name+"had phi"]  = r.TProfile(name+"corr had #phi",name+"corr had #phi",20,-5,5, )
        x_binning = [0,20, 40, 60, 80, 100]
        self.corr_histograms[name+"had pT"]   = r.TProfile(name+"corr had p_T", name+"corr had p_T", len(x_binning)-1,array.array('d',x_binning) )
        x_binning = [100, 130, 170, 250, 300, 400, 600]
        self.corr_histograms[name+"had pT high"]   = r.TProfile(name+"corr had p_T high", name+"corr had p_T high", len(x_binning)-1,array.array('d',x_binning) )
        x_binning = [130, 145, 160, 165, 170, 175, 180, 195, 210]
        self.corr_histograms[name+"had M"]    = r.TProfile(name+"corr had M",   name+"corr had M",   len(x_binning)-1,array.array('d',x_binning) )
        self.corr_histograms[name+"had M fine"]    = r.TProfile(name+"corr had M fine",   name+"corr had M fine",  30,165, 185 )

        x_binning = [0, 1, 2, 5, 10]
        self.corr_histograms[name+"hadTheta"] = r.TProfile(name+"corr hadTheta",   name+"corr hadTheta",   len(x_binning)-1,array.array('d',x_binning) )
        self.corr_histograms[name+"lepTheta"] = r.TProfile(name+"corr lepTheta",   name+"corr lepTheta",   len(x_binning)-1,array.array('d',x_binning) )

        x_binning = [-1,-0.25,0.0,0.25,0.5,0.7,1.0]
        self.corr_histograms[name+"hadA"] = r.TProfile(name+"corr hadA",   name+"corr hadA",   len(x_binning)-1,array.array('d',x_binning) )
        self.corr_histograms[name+"lepA"] = r.TProfile(name+"corr lepA",   name+"corr lepA",   len(x_binning)-1,array.array('d',x_binning) )

    def fill_top_kinematic_hists(self, name, ttbar, truth_ttbar, particle_ttbar):
        """
        """
        if ttbar is None or truth_ttbar.hadronic_top.Pt() < 2 or truth_ttbar.leptonic_top.Pt() < 2:
            return

        if ttbar.hadronic_top.Pt()/truth_ttbar.hadronic_top.Pt() > 100.0:
            print "LARGE HAD R_PT:", ttbar.hadronic_top.Pt(),"/",truth_ttbar.hadronic_top.Pt(),"=",ttbar.hadronic_top.Pt()/truth_ttbar.hadronic_top.Pt(), " eta = ", ttbar.hadronic_top.Eta()
            return

        if ttbar.hadronic_top.M()/ truth_ttbar.hadronic_top.M() > 100.0:
            print "LARGE HAD R_M:", ttbar.hadronic_top.M(),"/",truth_ttbar.hadronic_top.M(),"=", ttbar.hadronic_top.M()/ truth_ttbar.hadronic_top.M(), " eta = ", ttbar.hadronic_top.Eta()
            return

        if ttbar.leptonic_top.Pt()/truth_ttbar.leptonic_top.Pt() > 100.0:
            print "LARGE LEP R_PT:", ttbar.leptonic_top.Pt(),"/",truth_ttbar.leptonic_top.Pt(), "=", ttbar.leptonic_top.Pt()/truth_ttbar.leptonic_top.Pt()," eta = ", ttbar.hadronic_top.Eta()
            return

        if ttbar.leptonic_top.M()/ truth_ttbar.leptonic_top.M() > 100.0:
            print "LARGE LEP R_M:", ttbar.leptonic_top.M(),"/", truth_ttbar.leptonic_top.M(), "=", ttbar.leptonic_top.M()/ truth_ttbar.leptonic_top.M(), " eta = ", ttbar.leptonic_top.Eta(),
            return


        self.histograms[name+"had R_m"].Fill(ttbar.hadronic_top.M()/truth_ttbar.hadronic_top.M())
        self.histograms[name+"had deltaEta"].Fill(ttbar.hadronic_top.Eta() - truth_ttbar.hadronic_top.Eta())
        self.histograms[name+"had deltaPhi"].Fill(ttbar.hadronic_top.Phi() - truth_ttbar.hadronic_top.Phi())
        self.histograms[name+"had R_pT"].Fill(ttbar.hadronic_top.Pt()/truth_ttbar.hadronic_top.Pt())

        self.histograms[name+"lep R_m"].Fill(ttbar.leptonic_top.M()/truth_ttbar.leptonic_top.M())
        self.histograms[name+"lep deltaEta"].Fill(ttbar.leptonic_top.Eta() - truth_ttbar.leptonic_top.Eta())
        self.histograms[name+"lep deltaPhi"].Fill(ttbar.leptonic_top.Phi() - truth_ttbar.leptonic_top.Phi())
        self.histograms[name+"lep R_pT"].Fill(ttbar.leptonic_top.Pt()/truth_ttbar.leptonic_top.Pt())

        self.histograms[name+"lep eta"].Fill(ttbar.leptonic_top.Eta() )
        self.histograms[name+"lep phi"].Fill(ttbar.leptonic_top.Phi() )
        self.histograms[name+"lep p_T"].Fill(ttbar.leptonic_top.Pt() /1e3)
        self.histograms[name+"lep M"].Fill(ttbar.leptonic_top.M()/1e3 )

        self.histograms[name+"had eta"].Fill(ttbar.hadronic_top.Eta() )
        self.histograms[name+"had phi"].Fill(ttbar.hadronic_top.Phi() )
        self.histograms[name+"had p_T"].Fill(ttbar.hadronic_top.Pt()/1e3 )
        self.histograms[name+"had M"].Fill(ttbar.hadronic_top.M()/1e3 )

        self.corr_histograms[name+"had eta"].Fill(truth_ttbar.hadronic_top.Eta(), ttbar.hadronic_top.Eta()- truth_ttbar.hadronic_top.Eta())
        self.corr_histograms[name+"had phi"].Fill(ttbar.hadronic_top.Eta(), ttbar.hadronic_top.Phi()- truth_ttbar.hadronic_top.Phi())
        self.corr_histograms[name+"had pT"].Fill(ttbar.hadronic_top.Pt()/1e3, ttbar.hadronic_top.Pt()/truth_ttbar.hadronic_top.Pt())
        self.corr_histograms[name+"had pT high"].Fill(ttbar.hadronic_top.Pt()/1e3, ttbar.hadronic_top.Pt()/truth_ttbar.hadronic_top.Pt())
        self.corr_histograms[name+"had M"].Fill(ttbar.hadronic_top.M()/1e3,  ttbar.hadronic_top.M()/ truth_ttbar.hadronic_top.M())
        self.corr_histograms[name+"had M fine"].Fill(ttbar.hadronic_top.M()/1e3,  ttbar.hadronic_top.M()/ truth_ttbar.hadronic_top.M())

        self.corr_histograms[name+"lep eta"].Fill(truth_ttbar.leptonic_top.Eta(), ttbar.leptonic_top.Eta() - truth_ttbar.leptonic_top.Eta())
        self.corr_histograms[name+"lep phi"].Fill(truth_ttbar.leptonic_top.Eta(), ttbar.leptonic_top.Phi() - truth_ttbar.leptonic_top.Phi())
        self.corr_histograms[name+"lep pT"].Fill(truth_ttbar.leptonic_top.Pt()/1e3, ttbar.leptonic_top.Pt() / truth_ttbar.leptonic_top.Pt())
        self.corr_histograms[name+"lep pT high"].Fill(truth_ttbar.leptonic_top.Pt()/1e3, ttbar.leptonic_top.Pt() / truth_ttbar.leptonic_top.Pt())
        self.corr_histograms[name+"lep M"].Fill(ttbar.leptonic_top.M()/1e3,   ttbar.leptonic_top.M() / truth_ttbar.leptonic_top.M())

        if particle_ttbar is not None:
            if particle_ttbar.is_leptonic_event():
                self.corr_histograms[name+"lepTheta"].Fill(particle_ttbar.Theta_lep(),
                                                           ttbar.Theta_lep() - particle_ttbar.Theta_lep())
                self.corr_histograms[name+"lepA"].Fill(particle_ttbar.A_lep(),
                                                       ttbar.A_lep() - particle_ttbar.A_lep())
            else:
                self.corr_histograms[name+"hadTheta"].Fill(particle_ttbar.Theta_had(),
                                                           ttbar.Theta_had() - particle_ttbar.Theta_had())
                self.corr_histograms[name+"hadA"].Fill(particle_ttbar.A_had(),
                                                       ttbar.A_had() - particle_ttbar.A_had())
        if isinstance(ttbar.leading_additional_jet, Jet):
            if ttbar.leading_additional_jet.id == 21:
                self.histograms[name+"is_gluon"].Fill(1)
                print name, "id:", ttbar.leading_additional_jet.id
            else:
                self.histograms[name+"is_gluon"].Fill(0)
                print name, "id:", ttbar.leading_additional_jet.id
        # else:
            # Just the truth system that doens't have jtes
            # print "TTBAR SYSTEM ", name, " HAS TLorentzVectors for leadingjet..what's up witht that?"



    def draw_histograms(self, output_folder):
        hists = ["is_gluon",
                 "had R_m", "had deltaEta", "had deltaPhi", "had R_pT",
                 "lep R_m", "lep deltaEta", "lep deltaPhi", "lep R_pT",
                 "lep eta",
                 "lep phi",
                 "lep p_T",
                 "lep M",
                 "had eta",
                 "had phi",
                 "had p_T",
                 "had M",

                 ]

        names = [
            "Best #chi2",
            # "Best #chi^{2} given leptonic b",
            # "Best Possible",
            "Particle Level",
            "Baseline",
            # "LHCTopWG",
            # "Truth"
        ]

        AS.SetAtlasStyle()
        canvas = r.TCanvas("n_efficiencies", "n_efficiencies",800,800)


        for j, hist_type in enumerate(hists):
            plots = {}

            for i,name in enumerate(names):
                if name == "Truth":
                    if "R_" in hist_type or "delta" in hist_type or " M" in hist_type or "gluon" in hist_type:
                        continue

                if "gluon" in hist_type:
                    self.histograms[name+hist_type].Scale(1.0/self.n)

                plots[name] = [self.histograms[name+hist_type] ,dhf.get_simple_style(i)]
                self.histograms[name+hist_type].Write()

            dhf.plot_histogram( canvas,
                plots,
                x_axis_title = hist_type,
                y_axis_title = "NEvents",
                do_atlas_labels = True,
                do_legend = True,
                scaling_mid_point = 0.0,
                force_zero_min=True)

            canvas.Print(output_folder+hist_type+".eps")
            canvas.Print(output_folder+hist_type+".png")

    def draw_corr_histograms(self,output_folder):
        hists = ["lep eta",
                 "lep phi",
                 "lep pT",
                 "lep pT high",
                 "lep M",
                 "lep M fine",
                 "had eta",
                 "had phi",
                 "had pT",
                 "had pT high",
                 "had M",
                 "had M fine",
                 "lepTheta",
                 "hadTheta",
                 "lepA",
                 "hadA",
                 ]
        y_axis_titles = {}
        y_axis_titles["lep eta"] = "#Delta #eta( parton, reco) "
        y_axis_titles["lep phi"] = "#Delta #phi( parton, reco) "
        y_axis_titles["lep pT"]  = "Leptonic top < p_{T}^{reco}/p_{T}^{parton} > "
        y_axis_titles["lep pT high"]  = "Leptonic top < p_{T}^{reco}/p_{T}^{parton} > "
        y_axis_titles["lep M"]   = "Leptonic top < m^{reco}/m^{parton} > "
        y_axis_titles["lep M fine"]   = "Leptonic top < m^{reco}/m^{parton} > "
        y_axis_titles["had eta"] = "#Delta #eta( parton, reco) "
        y_axis_titles["had phi"] = "#Delta #phi( parton, reco) "
        y_axis_titles["had pT"] = "Hadronic top < p_{T}^{parton}/p_{T}^{reco} > "
        y_axis_titles["had pT high"] = "Hadronic top < p_{T}^{parton}/p_{T}^{reco} > "
        y_axis_titles["had M"]   = "Hadronic top < m^{reco}/ m^{parton} > "
        y_axis_titles["had M fine"]   = "Hadronic top < m^{reco}/ m^{parton} > "
        y_axis_titles["lepTheta"] = "Leptonic #Delta #Theta( parton, reco) "
        y_axis_titles["lepA"] = "Leptonic #Delta A( parton, reco) "
        y_axis_titles["hadTheta"] = "Hadronic #Delta #Theta( parton, reco) "
        y_axis_titles["hadA"] = "Hadronic #Delta A( parton, reco) "

        X_axis_titles = {}
        X_axis_titles["lep eta"] = "Partonic top #eta"
        X_axis_titles["lep phi"] = "Partonic top #eta"
        X_axis_titles["lep pT"]  = "Partonic top p_{T} [GeV]"
        X_axis_titles["lep pT high"]  = "Partonic top p_{T} [GeV]"
        X_axis_titles["lep M"]   = "Partonic top m [GeV]"
        X_axis_titles["lep M fine"]   = "Partonic top m [GeV]"
        X_axis_titles["had eta"] = "Partonic top #eta"
        X_axis_titles["had phi"] = "Partonic top #eta"
        X_axis_titles["had pT"]  = "Partonic top p_{T} [GeV]"
        X_axis_titles["had pT high"]  = "Partonic top p_{T} [GeV]"
        X_axis_titles["had M"]   = "Partonic top m [GeV]"
        X_axis_titles["had M fine"]   = "Partonic top m [GeV]"
        X_axis_titles["hadTheta"] = "Particle level hadronic top #Theta"
        X_axis_titles["hadA"] = "Particle level hadronic top A_{theta}"
        X_axis_titles["lepTheta"] = "Particle level leptonic top #Theta"
        X_axis_titles["lepA"] = "Particle level leptonic top A_{theta}"

        names = [
            "Best #chi2",
            # "Best #chi^{2} given leptonic b",
            # "Best Possible",
            # "Particle Level",
            "Baseline",
            # "LHCTopWG",
            "Truth",
        ]

        AS.SetAtlasStyle()
        canvas = r.TCanvas("n_efficiencies", "n_efficiencies",800,800)

        for j, hist_type in enumerate(hists):
            plots = {}

            for i, name in enumerate(names):
                # if "eta" in hist_type or "phi" in hist_type:
                plots[name] = [self.corr_histograms[name+hist_type] ,dhf.get_simple_style(i, "PROF")]
                self.corr_histograms[name+hist_type] = dhf.set_style_options(plots[name][0], plots[name][1])
                self.corr_histograms[name+hist_type].Write()


            dhf.plot_histogram( canvas,
                plots,
                x_axis_title=X_axis_titles[hist_type],
                y_axis_title=y_axis_titles[hist_type],
                do_atlas_labels=True,
                do_legend=True,
                scaling_mid_point=0.0,
                # force_zero_min=True
                )

            canvas.Print(output_folder+hist_type+"_correlation_plots.eps")
            canvas.Print(output_folder+hist_type+"_correlation_plots.png")


    def tbar_is_leptonic(self,reco_event, truth_event):
        # the charge of the lepton is NEGATVIE since
        # tbar > W-b > l-nub

        # Evaluate which lepton's charge to evaluate
        return truth_event.MC_Wdecay1_from_tbar_pdgId >= 10

    def create_truth_top(self, reco_event, truth_event):
        if self.tbar_is_leptonic(reco_event, truth_event):
            # Now we know that the bbar must match to leptonic b-jet
            leptonic_b = Jet(truth_event.MC_b_from_tbar_pt,
                             truth_event.MC_b_from_tbar_eta,
                             truth_event.MC_b_from_tbar_phi,
                             truth_event.MC_b_from_tbar_m,
                             0
                             )

            hadronic_b = Jet(truth_event.MC_b_from_t_pt,
                             truth_event.MC_b_from_t_eta,
                             truth_event.MC_b_from_t_phi,
                             truth_event.MC_b_from_t_m,
                             0
                             )

            leptonic_W = r.TLorentzVector()
            leptonic_W.SetPtEtaPhiM(truth_event.MC_W_from_tbar_pt,
                                    truth_event.MC_W_from_tbar_eta,
                                    truth_event.MC_W_from_tbar_phi,
                                    truth_event.MC_W_from_tbar_m
                                    )

            hadronic_W = r.TLorentzVector()
            hadronic_W.SetPtEtaPhiM(truth_event.MC_W_from_t_pt,
                                    truth_event.MC_W_from_t_eta,
                                    truth_event.MC_W_from_t_phi,
                                    truth_event.MC_W_from_t_m
                                    )

            # Now we know that the bbar must match to leptonic b-jet
            leptonic_t = r.TLorentzVector()
            leptonic_t.SetPtEtaPhiM(truth_event.MC_tbar_afterFSR_pt,
                                    truth_event.MC_tbar_afterFSR_eta,
                                    truth_event.MC_tbar_afterFSR_phi,
                                    truth_event.MC_tbar_afterFSR_m
                                )

            hadronic_t = r.TLorentzVector()
            hadronic_t.SetPtEtaPhiM(truth_event.MC_t_afterFSR_pt,
                                    truth_event.MC_t_afterFSR_eta,
                                    truth_event.MC_t_afterFSR_phi,
                                    truth_event.MC_t_afterFSR_m
                                )


        else:
            # Swap them round if the top is the leptonic
            hadronic_b = Jet(truth_event.MC_b_from_tbar_pt,
                             truth_event.MC_b_from_tbar_eta,
                             truth_event.MC_b_from_tbar_phi,
                             truth_event.MC_b_from_tbar_m,
                             0
                             )
            leptonic_b = Jet(truth_event.MC_b_from_t_pt,
                             truth_event.MC_b_from_t_eta,
                             truth_event.MC_b_from_t_phi,
                             truth_event.MC_b_from_t_m,
                             0
                             )


            hadronic_W = r.TLorentzVector()
            hadronic_W.SetPtEtaPhiM(truth_event.MC_W_from_tbar_pt,
                                    truth_event.MC_W_from_tbar_eta,
                                    truth_event.MC_W_from_tbar_phi,
                                    truth_event.MC_W_from_tbar_m
                                    )

            leptonic_W = r.TLorentzVector()
            leptonic_W.SetPtEtaPhiM(truth_event.MC_W_from_t_pt,
                                    truth_event.MC_W_from_t_eta,
                                    truth_event.MC_W_from_t_phi,
                                    truth_event.MC_W_from_t_m
                                )

            # Swap them round if the top is the leptonic
            hadronic_t = r.TLorentzVector()
            hadronic_t.SetPtEtaPhiM(truth_event.MC_tbar_afterFSR_pt,
                                    truth_event.MC_tbar_afterFSR_eta,
                                    truth_event.MC_tbar_afterFSR_phi,
                                    truth_event.MC_tbar_afterFSR_m
                                )
            leptonic_t = r.TLorentzVector()
            leptonic_t.SetPtEtaPhiM(truth_event.MC_t_afterFSR_pt,
                                    truth_event.MC_t_afterFSR_eta,
                                    truth_event.MC_t_afterFSR_phi,
                                    truth_event.MC_t_afterFSR_m
                                )
        return leptonic_t, leptonic_W, leptonic_b,hadronic_t, hadronic_W, hadronic_b

    def process_file(self, file):
        """

        """
        in_file = rhf.open_file(file)

        reco_tree = in_file.Get(self.reco_tuple)
        truth_tree = in_file.Get(self.truth_tuple)
        particle_tree = in_file.Get(self.partice_tuple)
        reco_tree.BuildIndex('eventNumber')
        particle_tree.BuildIndex('eventNumber')
        truth_tree.BuildIndex('eventNumber')

        max_n = 1e10
        self.n = 0
        # Get the truth tree and the reco tree
        for event in reco_tree:
            # Get the truth events
            t_index = truth_tree.GetEntryNumberWithIndex(event.eventNumber)
            truth_tree.GetEntry(t_index)

            # If the truth event doesn't exist for whatever reason quit
            if t_index == -1:
                continue

            # Get the particle events
            p_index = particle_tree.GetEntryNumberWithIndex(event.eventNumber)
            particle_tree.GetEntry(p_index)

            # If the truth event doesn't exist for whatever reason quit
            if p_index == -1:
                continue

            # Reconstruct the w
            b_jets, light_jets = [], []
            for i in xrange(len(event.jet_pt)):
                jet = Jet(
                     event.jet_pt[i],
                     event.jet_eta[i],
                     event.jet_phi[i],
                     event.jet_e[i],
                     event.jet_truthPartonLabel[i]
                    )
                #
                if ord(event.jet_isbtagged_MV2c10_85[i]) > 0:
                    b_jets.append(jet)
                else:
                    light_jets.append(jet)

            w_pairings = []
            lepton = r.TLorentzVector()
            if len(event.el_pt) > 0:
                lepton.SetPtEtaPhiE(
                        event.el_pt[0],
                        event.el_eta[0],
                        event.el_phi[0],
                        event.el_e[0]
                    )
            else:
                lepton.SetPtEtaPhiE(
                        event.mu_pt[0],
                        event.mu_eta[0],
                        event.mu_phi[0],
                        event.mu_e[0]
                    )
            ttbar_systems = TTBarSystems(
                    event.W_lep,
                    lepton,
                    event.neutrino
                )

            # Evaluate all possible pairings
            for i in xrange(len(light_jets)-1):
                for j in xrange(i+1,len(light_jets)):
                    w_pairings.append(HadronicW(light_jets[i],light_jets[j],i,j,light_jets))


            # Now combine this with all possible b-jets
            for l,w_had in enumerate(w_pairings):
                # For every b-jet store the b-info
                for i in xrange(len(b_jets)-1):
                    for j in xrange(i+1,len(b_jets)):
                        ttbar_systems.append(w_had,
                                             b_jets[i],
                                             b_jets[j])
                        # Flip the b-jets
                        ttbar_systems.append(w_had,
                                             b_jets[j],
                                             b_jets[i])
            if len(ttbar_systems.tops) < 1:
                print "SKIPPING DODGE EVENT!!"
                print "NJets ", len(light_jets)
                print "N bJets ", len(b_jets)
                continue

            # We've got all the permutations so now process this
            best_chi2_ttbar, best_chi2 = ttbar_systems.lowest_chi2()
            best_chi2_lep_ttbar, best_lep_top_chi2 = ttbar_systems.lowest_chi2_of_leptonic_closeby_b()
            #
            leptonic_t, leptonic_W, leptonic_b,hadronic_t, hadronic_W, hadronic_b = self.create_truth_top(
                    event,
                    truth_tree
                )

            truth_ttbar = TTBar(
                hadronic_W, leptonic_b, hadronic_b, leptonic_W,
                leptonic_W, None
                )
            truth_ttbar.hadronic_top = hadronic_t
            truth_ttbar.leptonic_top = leptonic_t

            baseline_ttbar = TTBar(
                event.W_had,
                Jet(event.b_lep.Pt(), event.b_lep.Eta(), event.b_lep.Phi(), event.b_lep.E(), 0),
                Jet(event.b_had.Pt(), event.b_had.Eta(), event.b_had.Phi(), event.b_had.E(), 0),
                event.W_lep,
                event.W_lep,
                Jet(event.gluons[0].Pt(), event.gluons[0].Eta(), event.gluons[0].Phi(), event.gluons[0].E(), event.g_truthPartonLabel[0]),
                )
            baseline_ttbar.hadronic_top = event.top_had
            baseline_ttbar.leptonic_top = event.top_lep

            #
            truth_Ws_ttbar = TTBar(
                hadronic_W,
                best_chi2_ttbar.leptonic_b,
                best_chi2_ttbar.hadronic_b,
                leptonic_W,
                leptonic_W,
                best_chi2_ttbar.leading_additional_jet
                )

            particle_level_ttbar = self.get_particle_level_ttbar(particle_tree)
            if particle_level_ttbar is None:
                continue

            ttbar_LHCTopWG = self.get_LHCTopWG_ttbar(event)
            if ttbar_LHCTopWG is None:
                continue
            # Using truth information evaluate the best we could ever do given the detector resolution
            # with this event
            best_dR, best_angular_ttbar = ttbar_systems.best_physics_objects(truth_ttbar)

            # # compare to ground truth
            self.fill_top_kinematic_hists("Best #chi2", best_chi2_ttbar,
                                          truth_ttbar,
                                          particle_level_ttbar)

            self.fill_top_kinematic_hists("Best #chi^{2} given leptonic b",
                                          best_chi2_lep_ttbar, truth_ttbar,
                                          particle_level_ttbar)

            self.fill_top_kinematic_hists("Best Possible",
                                          best_angular_ttbar, truth_ttbar,
                                          particle_level_ttbar)

            self.fill_top_kinematic_hists("Particle Level",
                                          particle_level_ttbar, truth_ttbar,
                                          particle_level_ttbar)

            self.fill_top_kinematic_hists("Baseline",
                                          baseline_ttbar, truth_ttbar,
                                          particle_level_ttbar)

            self.fill_top_kinematic_hists("LHCTopWG",
                                          ttbar_LHCTopWG, truth_ttbar,
                                          particle_level_ttbar)
            self.fill_top_kinematic_hists("Truth",
                                          truth_ttbar, truth_ttbar,
                                          None)

            # Now we can
            self.n += 1
            if self.n > max_n:
                break

    def save_to(self,output_folder):
        """

        """
        self.save_output = True
        self.out_file = rhf.open_file(output_folder+"reco_histograms.root",
                                      "RECREATE")

    def close(self):
        self.out_file.Close()

    def get_particle_level_ttbar(self, particle_tree):
        """
        Build the ttbar system according to LHC top WG definition.
        """
        # Sort the jets into light and
        b_jets, light_jets = [], []
        for i in xrange(len(particle_tree.jet_pt)):
            jet = Jet(
                 particle_tree.jet_pt[i],
                 particle_tree.jet_eta[i],
                 particle_tree.jet_phi[i],
                 particle_tree.jet_e[i],
                 particle_tree.jet_truthPartonLabel[i]
                )

            #
            if particle_tree.jet_nGhosts_bHadron[i] > 0:
                b_jets.append(jet)
            else:
                light_jets.append(jet)

        if len(b_jets) < 2 or len(light_jets) < 3:
            return None
        lepton = r.TLorentzVector()
        if len(particle_tree.el_pt) > 0:
            lepton.SetPtEtaPhiE(
                    particle_tree.el_pt[0],
                    particle_tree.el_eta[0],
                    particle_tree.el_phi[0],
                    particle_tree.el_e[0]
                )
        else:
            lepton.SetPtEtaPhiE(
                    particle_tree.mu_pt[0],
                    particle_tree.mu_eta[0],
                    particle_tree.mu_phi[0],
                    particle_tree.mu_e[0]
                )
        # Order jets and b-jets by their pT - should be unnecessary
        sorted(b_jets, cmp=lorentzvector_pt)
        sorted(light_jets, cmp=lorentzvector_pt)

        # Define the leptonic W by combining the lepton with the ETmiss and solving for pz assuming the W mass (highest pz from two-fold ambiguity)
        leptonic_W = calculate_leptonic_W(lepton, particle_tree.neutrino)

        # Select the two highest pT b-jets as the b-jets to be used for the
        # pseudo-top-quark definition.
        # Combine the b-jet closest to the lepton with the leptonic W, to form
        # the leptonic pseudo-top-quark.
        leptonic_b = b_jets[0] if lepton.DeltaR(b_jets[0].jet) < lepton.DeltaR(b_jets[1].jet) else b_jets[1]
        hadronic_b = b_jets[0] if lepton.DeltaR(b_jets[0].jet) >= lepton.DeltaR(b_jets[1].jet) else b_jets[1]
        hadronic_W = HadronicW(light_jets[0], light_jets[1], 0, 1, light_jets)

        return TTBar(
                hadronic_W.W,
                leptonic_b,
                hadronic_b,
                leptonic_W,
                leptonic_W,
                hadronic_W.leading_jet
                )

    def get_LHCTopWG_ttbar(self, event):
        """
        Build the ttbar system according to LHC top WG definition.
        """
        # Sort the jets into light and
        b_jets, light_jets = [], []
        for i in xrange(len(event.jet_pt)):
            jet = Jet(
                event.jet_pt[i],
                event.jet_eta[i],
                event.jet_phi[i],
                event.jet_e[i],
                event.jet_truthPartonLabel[i]
                )

            #
            if ord(event.jet_isbtagged_MV2c10_85[i]) > 0:
                b_jets.append(jet)
            else:
                light_jets.append(jet)

        if len(b_jets) < 2 or len(light_jets) < 3:
            return None
        lepton = r.TLorentzVector()
        if len(event.el_pt) > 0:
            lepton.SetPtEtaPhiE(
                    event.el_pt[0],
                    event.el_eta[0],
                    event.el_phi[0],
                    event.el_e[0]
                )
        else:
            lepton.SetPtEtaPhiE(
                    event.mu_pt[0],
                    event.mu_eta[0],
                    event.mu_phi[0],
                    event.mu_e[0]
                )
        # Order jets and b-jets by their pT - should be unnecessary
        sorted(b_jets, cmp=lorentzvector_pt)
        sorted(light_jets, cmp=lorentzvector_pt)

        # Define the leptonic W by combining the lepton with the ETmiss and solving for pz assuming the W mass (highest pz from two-fold ambiguity)
        leptonic_W = calculate_leptonic_W(lepton, event.neutrino)

        # Select the two highest pT b-jets as the b-jets to be used for the
        # pseudo-top-quark definition.
        # Combine the b-jet closest to the lepton with the leptonic W, to form
        # the leptonic pseudo-top-quark.
        leptonic_b = b_jets[0] if lepton.DeltaR(b_jets[0].jet) < lepton.DeltaR(b_jets[1].jet) else b_jets[1]
        hadronic_b = b_jets[0] if lepton.DeltaR(b_jets[0].jet) >= lepton.DeltaR(b_jets[1].jet) else b_jets[1]
        hadronic_W = HadronicW(light_jets[0], light_jets[1], 0, 1, light_jets)

        return TTBar(
                hadronic_W.W, leptonic_b, hadronic_b, leptonic_W,
                leptonic_W, hadronic_W.leading_jet
                )
def lorentzvector_pt(v1, v2):
    return v1.Pt() > v2.Pt()


def calculate_leptonic_W(lepton, MET):
    """
    Use the constrainnt of the W being on shell to solve for the
    unmeasureable neutrino momentum in the z-direction.

    Follow LHC Top WG recommendiations and chose the solution that has
    largest absolute magnitude.
    """
    KinemEdge = 13.9e9
    mWPDG     = 80.399e3
    mTop      = 173.0e3
    v_pz  = -KinemEdge
    delta = -KinemEdge

    # get the MET x and y components for the reconstruction
    nu_px  = MET.Px()
    nu_py  = MET.Py()
    met_et =  math.sqrt(nu_px*nu_px + nu_py*nu_py)

    # lepton already made by reco or particle levels:
    l_px, l_py, l_pz = lepton.Px(),lepton.Py(),lepton.Pz()
    l_m, l_E = lepton.M(),lepton.E()

    mdiff = 0.5 * ( mWPDG*mWPDG - l_m*l_m )
    pT_vl = nu_px*l_px + nu_py*l_py

    a = l_E*l_E - l_pz*l_pz
    b = -2. * l_pz * ( mdiff + pT_vl )
    c = met_et*met_et*l_E*l_E - mdiff*mdiff - pT_vl*pT_vl - 2.*mdiff*pT_vl
    v_pz_2 = -0.5*b/a
    delta = b*b - 4.*a*c

    # Complex solution!
    if delta <= 0.:
        # take only the real part
        v_pz = -0.5*b/a
        v_pz_other = v_pz

    else:
        v_pz_1 = 0.5 * ( -b - math.sqrt(delta) ) / a
        v_pz_2 = 0.5 * ( -b + math.sqrt(delta) ) / a
        v_E_1  = math.sqrt( nu_px*nu_px +nu_py*nu_py + v_pz_1*v_pz_1 )
        v_E_2  = math.sqrt( nu_px*nu_px +nu_py*nu_py + v_pz_2*v_pz_2 )

        # Need to put the correct choice in here
        v_1,v_2 = r.TLorentzVector(),r.TLorentzVector()
        v_1.SetPxPyPzE( nu_px, nu_py, v_pz_1, v_E_1 )
        v_2.SetPxPyPzE( nu_px, nu_py, v_pz_2, v_E_2 )

        if abs(v_pz_2) > abs(v_pz_1):
            v_pz = v_pz_2
            v_pz_other = v_pz_1
        else:
            v_pz = v_pz_1
            v_pz_other = v_pz_2


    v_E = math.sqrt(nu_px*nu_px + nu_py*nu_py + v_pz*v_pz)
    v_E_other = math.sqrt(nu_px*nu_px + nu_py*nu_py + v_pz_other*v_pz_other )

    MET.SetPxPyPzE( nu_px, nu_py, v_pz, v_E )
    MET_other, leptonic_W_other = r.TLorentzVector(), r.TLorentzVector()
    MET_other.SetPxPyPzE( nu_px, nu_py, v_pz, v_E_other )

    leptonic_W = MET + lepton
    leptonic_W_other = MET_other + lepton
    return leptonic_W


if __name__ == "__main__":
    import config as c
    ttbar_folder = '/eos/user/j/jrawling/DeadConeTuples/v0-02-03/user.jrawling.410470.PhPy8EG.DAOD_TOPQ1.e6337_e5984_s3126_r9364_r9315_p3409.18-03-11v3_output.root/'
    # ttbar_folder
    input_mc_tuple = [[
                     ttbar_folder+'user.jrawling.13436959._000001.output.root',
                     ttbar_folder+'user.jrawling.13436959._000002.output.root',
    #                  ttbar_folder+'user.jrawling.13253727._000002.output.root',
    #                  ttbar_folder+'user.jrawling.13253727._000003.output.root',
    #                  ttbar_folder+'user.jrawling.13253727._000004.output.root',
    #                  ttbar_folder+'user.jrawling.13253727._000005.output.root',
    #                  # ttbar_folder+'user.jrawling.13253727._000006.output.root',
    #                  # ttbar_folder+'user.jrawling.13253727._000007.output.root',
    #                  # ttbar_folder+'user.jrawling.13253727._000008.output.root',
    #                  # NB: max is 37
                     ]]
    input_mc_tuple = [[
                     '/afs/cern.ch/user/j/jrawling/workDir/DeadConeAnalysis/AnalysisTop-21/run/output.root',
                     ]]

    ttbar_reconstructor = TTBarReconstructor(input_mc_tuple)
    ttbar_reconstructor.process()

    output_folder = "/afs/cern.ch/user/j/jrawling/myeos/www/pages/plots/Dead-Cone_Analysis/v0-02-02/ttbar_reco_quickTest/"
    ttbar_reconstructor.save_to(output_folder)
    ttbar_reconstructor.draw_corr_histograms(output_folder)
    ttbar_reconstructor.draw_histograms(output_folder)
    ttbar_reconstructor.close()


