"""
    Calculate the measures of the quality of jet assignment when reconstructing the leptonic and hadronci top.

    Calculates the fration of b-jets assigned to t the correct top quark, measures the angular and energy/mass resolution of
    hadonic W and the leptonic/hadronic top
"""
import ATLASStyle.AtlasStyle as AS
import settings.JetAssignmentSettings as c
import RootHelperFunctions.RootHelperFunctions as rhf
import RootHelperFunctions.DrawingHelperFunctions as dhf
import ROOT as r
import copy


class JetAssignmentQualityQuantify:
    def __init__(
        self,
        input_file,
        detector_tuple_name,
        truth_tuple_name,
        output_folder,
        reco_cuts = [""],
        name = "JetAssignmentQualityQuantify",
        evaluate_bjet_matching=True,
        evaluate_top_resolution=True
        ):
        """
            A class that calculates the fraction of correctly matched b jets,
            resolution of top quark kinematcis and resolution of W boson
            kineamtics compared to truth level

            Performs an event loop style analysis since this quite a cumbersome
            procedure with TTree::Draw commands
        """
        self.input_file          = input_file
        self.detector_tuple_name = detector_tuple_name
        self.truth_tuple_name    = truth_tuple_name
        self.output_folder       = output_folder
        rhf.create_folder(self.output_folder)

        # Used in drwaing
        self.reco_cuts           = reco_cuts
        self.weight = "1.0"
        for cut in self.reco_cuts:
            self.weight += "*(" + cut + ")"

        #
        self.name = name
        self.output_file = rhf.open_file(self.output_folder + "/" + self.name + ".root", "RECREATE")

        # Options for evaluation
        self.evaluate_bjet_matching  = evaluate_bjet_matching
        self.evaluate_top_resolution = evaluate_top_resolution


        # Setup root options
        r.gROOT.SetBatch()
        r.TH1.SetDefaultSumw2()
        rhf.hide_root_infomessages()

        self.construct_histograms()
        self.consturct_top_resolution_histograms()

        print "Running with cuts: "
        for cut in self.reco_cuts:
            print "\t",cut
        print ""

    def construct_histograms(self):
        """
            Creates the histograms for:
                fraction of  b-jets assigned correctly to leptonic and haronic top quark
        """
        self.bjet_correct_hist    = r.TH1F(self.name+"_bjet_efficiency","",4,0,4)
        self.bjet_total_hist      = r.TH1F(self.name+"_bjet_efficiency","",4,0,4)
        self.R = 0.3

    def consturct_top_resolution_histograms(self):

        # Ultimately want to exmaine each of these resolution variables as a function of
        # truth eta binnined in truth pT
        self.resolution_histograms = {}

        # Hadronic top resolution histograms
        self.resolution_histograms["top_had_pt_res" ]  = r.TH1F("top_had_pt_res",  ";top #Delta(p_{T}^{truth},p_{T}^{reco}) [GeV]; Normalized Number of Events",30,-350,350)
        self.resolution_histograms["top_had_eta_res"] = r.TH1F("top_had_eta_res",  ";top #Delta(#eta^{truth},#eta^{reco}); Normalized Number of Events",30,-5,5)
        self.resolution_histograms["top_had_phi_res"] = r.TH1F("top_had_phi_res",  ";top #Delta(#phi^{truth},#phi^{reco}); Normalized Number of Events",30,-5,5)
        self.resolution_histograms["top_had_m_res"  ]   = r.TH1F("top_had_m_res",  ";top #Delta(m^{truth},m^{reco}) [GeV]; Normalized Number of Events",30,-200,200)
        self.resolution_had_names = ["top_had_pt_res", "top_had_eta_res", "top_had_phi_res", "top_had_m_res"  ]
        # Leptonic top resolution plots
        self.resolution_histograms["top_lep_pt_res"]  = r.TH1F("top_lep_pt_res", ";top #Delta(p_{T}^{truth},p_{T}^{reco}) [GeV]; Normalized Number of Events",30,-350,350)
        self.resolution_histograms["top_lep_eta_res"] = r.TH1F("top_lep_eta_res",";top #Delta(#eta^{truth},#eta^{reco}); Normalized Number of Events",30,-5,5)
        self.resolution_histograms["top_lep_phi_res"] = r.TH1F("top_lep_phi_res",";top #Delta(#phi^{truth},#phi^{reco}); Normalized Number of Events",30,-5,5)
        self.resolution_histograms["top_lep_m_res"]   = r.TH1F("top_lep_m_res",  ";top #Delta(m^{truth},m^{reco}) [GeV]; Normalized Number of Events",30,-200,200)

    def draw_top_resolution_histograms(self):
        """
            Draw all the plots in the dictionary "resolution_histograms in an ALTAS stlye and to a summary plot
        """

        #Move to the current file to save
        self.output_file.cd()

        AS.SetAtlasStyle()
        canvas = r.TCanvas(self.name + "top_resolution",self.name + "top_resolution",600,600)
        canvas.Print(self.output_folder+ "resolution_summary.pdf[")

        for name in self.resolution_had_names:
            #
            dhf.plot_histogram(canvas,
                        {
                            "Hadronic Top": [rhf.normalize_histogram(self.resolution_histograms[name]),dhf.get_simple_red_style_opts() ],
                            "Leptonic Top": [rhf.normalize_histogram(self.resolution_histograms[name.replace("had","lep")]),dhf.get_simple_blue_style_opts() ]
                        },
                        x_axis_title = self.resolution_histograms[name].GetXaxis().GetTitle(),
                        y_axis_title = self.resolution_histograms[name].GetYaxis().GetTitle(),
                        do_legend = True,
                        do_atlas_labels = True )

            self.resolution_histograms[name].Write()
            #deeply iretating feature of ROOT means to draw the plot style I'd like we have to use 2 draw commands
            res_error_hist = self.resolution_histograms[name].Clone( self.resolution_histograms[name].GetName() + "CloneyMcClone")
            res_error_hist.SetFillColor(r.kRed)
            res_error_hist.SetFillStyle(3004)
            res_error_hist.Draw("E2 SAME")

            res_lep_error_hist = self.resolution_histograms[name.replace("had","lep")].Clone( self.resolution_histograms[name.replace("had","lep")].GetName() + "CloneyMcClone")
            res_lep_error_hist.SetFillColor(r.kBlue)
            res_lep_error_hist.SetFillStyle(3005)
            res_lep_error_hist.Draw("E2 SAME")

            #save as pboth eps and png in order to preserve the alpha value which the eps format does not support
            canvas.Print(self.output_folder+ name +".eps")
            canvas.Print(self.output_folder+ name +".png")
            canvas.Print(self.output_folder+ "resolution_summary.pdf")
        canvas.Print(self.output_folder+ "resolution_summary.pdf]")
        self.output_file.Close()

    def tbar_is_leptonic(self,reco_event, truth_event):
        # the charge of the lepton is NEGATVIE since
        # tbar > W-b > l-nub

        # Evaluate which lepton's charge to evaluate
        if len(reco_event.el_pt) > 0:
            return reco_event.el_charge[0] < 0
        else:
            return reco_event.mu_charge[0] < 0

    def fill_bjet_matching_hist(self, reco_event, truth_event):
        """
            Takes a reco_event and a particle tree and calcualte
            the approriate things
        """

        # Top quark decays to positive charged lepton,
        # tbar goes to negative
        # Support for checking both e+jets or mu+jets channel so shove the
        # boolean logic into a neat function for readablity
        if self.tbar_is_leptonic(reco_event, truth_event):
            # Now we know that the bbar must match to leptonic b-jet
            leptonic_b = r.TLorentzVector()
            leptonic_b.SetPtEtaPhiM(truth_event.MC_b_from_tbar_pt,
                                    truth_event.MC_b_from_tbar_eta,
                                    truth_event.MC_b_from_tbar_phi,
                                    truth_event.MC_b_from_tbar_m
                                )
            hadronic_b = r.TLorentzVector()
            hadronic_b.SetPtEtaPhiM(truth_event.MC_b_from_t_pt,
                                    truth_event.MC_b_from_t_eta,
                                    truth_event.MC_b_from_t_phi,
                                    truth_event.MC_b_from_t_m
                                )

        else:
            # Swap them round if the top is the leptonic
            hadronic_b = r.TLorentzVector()
            hadronic_b.SetPtEtaPhiM(truth_event.MC_b_from_tbar_pt,
                                    truth_event.MC_b_from_tbar_eta,
                                    truth_event.MC_b_from_tbar_phi,
                                    truth_event.MC_b_from_tbar_m
                                )
            leptonic_b = r.TLorentzVector()
            leptonic_b.SetPtEtaPhiM(truth_event.MC_b_from_t_pt,
                                    truth_event.MC_b_from_t_eta,
                                    truth_event.MC_b_from_t_phi,
                                    truth_event.MC_b_from_t_m
                                )

        # Now decide upon what's "correctly matched"
        # if hadronic_b.DeltaR(reco_event.b_had) < min(reco_event.b_had.DeltaR(reco_event.b_lep),3.0):
        if hadronic_b.DeltaR(reco_event.b_had) < self.R:
            self.bjet_correct_hist.Fill(1)
        if leptonic_b.DeltaR(reco_event.b_lep) < self.R:
            self.bjet_correct_hist.Fill(0)

        #
        if leptonic_b.DeltaR(reco_event.b_lep) < self.R or hadronic_b.DeltaR(reco_event.b_lep) < self.R:
            self.bjet_correct_hist.Fill(2)
        if leptonic_b.DeltaR(reco_event.b_had) < self.R or hadronic_b.DeltaR(reco_event.b_had) < self.R:
            self.bjet_correct_hist.Fill(3)


        # Always fill in the "totla histogram"
        self.bjet_total_hist.Fill(1)
        self.bjet_total_hist.Fill(0)
        self.bjet_total_hist.Fill(2)
        self.bjet_total_hist.Fill(3)

    def fill_top_resolution_hists(self,event,truth_event):
        """
            Evaluate the resolution variables for hadronic and leptonic top
            four momentum

            event: a instance of a TTree
            truth_event: a TTree
        """

        # Find the leptonic and hadronic tops at parton level as four vectors
        if self.tbar_is_leptonic(event, truth_event):
            # Now we know that the bbar must match to leptonic b-jet
            leptonic_t = r.TLorentzVector()
            leptonic_t.SetPtEtaPhiM(truth_event.MC_tbar_afterFSR_pt,
                                    truth_event.MC_tbar_afterFSR_eta,
                                    truth_event.MC_tbar_afterFSR_phi,
                                    truth_event.MC_tbar_afterFSR_m
                                )
            hadronic_t = r.TLorentzVector()
            hadronic_t.SetPtEtaPhiM(truth_event.MC_t_afterFSR_pt,
                                    truth_event.MC_t_afterFSR_eta,
                                    truth_event.MC_t_afterFSR_phi,
                                    truth_event.MC_t_afterFSR_m
                                )

        else:
            # Swap them round if the top is the leptonic
            hadronic_t = r.TLorentzVector()
            hadronic_t.SetPtEtaPhiM(truth_event.MC_tbar_afterFSR_pt,
                                    truth_event.MC_tbar_afterFSR_eta,
                                    truth_event.MC_tbar_afterFSR_phi,
                                    truth_event.MC_tbar_afterFSR_m
                                )
            leptonic_t = r.TLorentzVector()
            leptonic_t.SetPtEtaPhiM(truth_event.MC_t_afterFSR_pt,
                                    truth_event.MC_t_afterFSR_eta,
                                    truth_event.MC_t_afterFSR_phi,
                                    truth_event.MC_t_afterFSR_m
                                )
        #
        self.resolution_histograms["top_lep_pt_res"].Fill ( (leptonic_t.Pt()  - event.top_lep.Pt() )/1e3)
        self.resolution_histograms["top_lep_eta_res"].Fill( leptonic_t.Eta() - event.top_lep.Eta() )
        self.resolution_histograms["top_lep_phi_res"].Fill( leptonic_t.Phi() - event.top_lep.Phi() )
        self.resolution_histograms["top_lep_m_res"].Fill  ( (leptonic_t.M()   - event.top_lep.M() )/1e3)
        #
        self.resolution_histograms["top_had_pt_res"].Fill ( (hadronic_t.Pt()  - event.top_had.Pt())/1e3)
        self.resolution_histograms["top_had_eta_res"].Fill( hadronic_t.Eta() - event.top_had.Eta() )
        self.resolution_histograms["top_had_phi_res"].Fill( hadronic_t.Phi() - event.top_had.Phi() )
        self.resolution_histograms["top_had_m_res"].Fill  ( (hadronic_t.M()   - event.top_had.M() )/1e3 )


    def evaluate(self):
        """
            Opens the input tuples and creates the histograms, we perform this at the
            in an event loop style anaylsis in order to minimize calls to costly
            BuildINdex functions
        """
        in_file = rhf.open_file(self.input_file, "READ")

        # Get the tuples and synchronise them across eventNubers
        reco_tree  = in_file.Get(self.detector_tuple_name)
        truth_tree = in_file.Get(self.truth_tuple_name)
        # Very costly :O
        reco_tree.BuildIndex('eventNumber')
        truth_tree.BuildIndex('eventNumber')

        # Construct the weight based upon the reco_cuts:
        weight_formula   = r.TTreeFormula("weight_formula", self.weight, reco_tree)

        for event in reco_tree:
            index = reco_tree.GetEntryNumberWithIndex(event.eventNumber)
            truth_tree.GetEntry(index)

            # Do not bother weith events that fail the cuts
            weight  = weight_formula.EvalInstance()

            if weight < 1.0:
                continue

            #
            if self.evaluate_bjet_matching:
                self.fill_bjet_matching_hist(event,truth_tree)

            if self.evaluate_top_resolution:
                self.fill_top_resolution_hists(event,truth_tree)

        # Clean up the root file
        in_file.Close()
        self.create_bjet_matching_efficiency()
        self.save_bjet_histograms()
        self.draw()

    def save_bjet_histograms(self):
        self.output_file.cd()
        self.bjet_efficiency_hist.Write()
        self.bjet_correct_hist.Write()
        self.bjet_total_hist.Write()

    def create_bjet_matching_efficiency(self):
        """
            Evaluate the efficiency of matching by dividing the correctly matched hist
            by the total histogram

            Note: Errors are binominal here, since the events in the correctly matched hist
            are strictly are subset of those in the total histogram, we therefore have two strongly
            correlated histograms. This is a text book case of using binomial errors

        """
        self.bjet_efficiency_hist = self.bjet_correct_hist.Clone("bjet_matching_efficiency")
        self.bjet_efficiency_hist.Divide(self.bjet_correct_hist,self.bjet_total_hist,1.0,1.0,"b")
        self.bjet_efficiency_hist.Scale(100.0)

    def draw_bjet_matching_fraction(self):
        """
            Nealty draw the hadronic and leptonic b-jet matriching efficiency
        """
        AS.SetAtlasStyle()
        canvas = r.TCanvas(self.name + "bjet_matching_efficiency",self.name + "bjet_matching_efficiency",600,600)
        #
        self.bjet_efficiency_hist.GetXaxis().SetBinLabel(1, "Leptonic b-jets")
        self.bjet_efficiency_hist.GetXaxis().SetBinLabel(2, "Hadronic b-jets")
        self.bjet_efficiency_hist.GetXaxis().SetBinLabel(3, "#splitline{Possible matchs for}{leptonic b-jet}")
        self.bjet_efficiency_hist.GetXaxis().SetBinLabel(4, "#splitline{Possible matchs for}{hadronic b-jet}")

        #
        dhf.plot_histogram(canvas,
                    {
                        "Efficiency": [self.bjet_efficiency_hist,dhf.get_simple_red_style_opts() ]
                    },
                    x_axis_title = "",
                    y_axis_title ="Fraction of correctly matched b-jets",
                    do_legend = False,
                    force_zero_min=True,
                    do_atlas_labels = True )

        #deeply iretating feature of ROOT means to draw the plot style I'd like we have to use 2 draw commands
        bjet_efficiency_error_hist = self.bjet_efficiency_hist.Clone("CloneyMcClone")
        bjet_efficiency_error_hist.SetFillColor(r.kRed)
        bjet_efficiency_error_hist.SetFillStyle(3004)
        bjet_efficiency_error_hist.Draw("E2 SAME")

        #save as pboth eps and png in order to preserve the alpha value which the eps format does not support
        canvas.Print(self.output_folder+"bjet_matching_efficiency.eps")
        canvas.Print(self.output_folder+"bjet_matching_efficiency.png")

    def draw(self):
        """
            Draw the histograms that have been filled
        """

        if self.evaluate_bjet_matching:
            self.draw_bjet_matching_fraction()

        if self.evaluate_top_resolution:
            self.draw_top_resolution_histograms()
        self.output_file.Close()

def main():
    # Load a quality quantifier with standard selection cuts
    reco_cuts = ["(ejets_2015 || ejets_2016 || mujets_2016 || mujets_2016)",
                 "W_lep.M()/1e3 < 85",
                 "W_lep.M()/1e3 > 70",
                 # "Sum$(jet_isbtagged_MV2c10_77)==2"
                ]
    jet_assignment_quality_quanitfier = JetAssignmentQualityQuantify(
            c.input_file,
            "nominal",
            "truth",
            c.output_folder,
            reco_cuts
        )

    # Evalutes the plots
    jet_assignment_quality_quanitfier.evaluate()
    print "Done!"

main()


