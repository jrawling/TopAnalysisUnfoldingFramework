'''
    Author: Jacob Rawling
    Date: 

    Utilizes the TopAnalysis.Unfolding package ( ) to create the migration matrix, 
    perform the desired type of unfolding, run some tests, save the ouptut in ROOT 
    and Journal ready eps fomrat 
'''
from TopAnalysis.Unfolding.Unfolder import Unfolder
from TopAnalysis.Unfolding.Tests import *
from TopAnalysis.Unfolding.StressTest import StressTest
from TopAnalysis.FakeEvaluator import FakeEvalautor
import config as c
import ROOT as r


def main():
    # truth_cuts_resolved = [
    #     "(particleLevel.mujets_particle_trimming_resolved"
    #     " + particleLevel.ejets_particle_trimming_resolved)"
    #     "*(particleLevel.ljet_pt[0]/1e3 > 0)*(particleLevel.g_subjet_pt/1e3 > 20)"
    # ]
    # reco_cuts_resolved = [
    #     "(nominal.ejets_resolved_2015 + nominal.ejets_resolved_2016  +"
    #     "nominal.mujets_resolved_2015 + nominal.mujets_resolved_2016)"
    #     "*(nominal.ljet_pt[0]/1e3 > 0)*(nominal.g_subjet_pt/1e3 > 20)"

    # ]
    # truth_cuts_boosted = [
    #     "(particleLevel.mujets_particle_trimming"
    #     " + particleLevel.ejets_particle_trimming)"
    #     "*(particleLevel.ljet_pt[0]/1e3 > 0)*(particleLevel.g_subjet_pt/1e3 > 20)"
    # ]
    # reco_cuts_boosted = [
    #     "(nominal.ejets_2015 + nominal.ejets_2016 +"
    #     "nominal.mujets_2015 + nominal.mujets_2016)"
    #     "*(nominal.ljet_pt[0]/1e3 > 0)*(nominal.g_subjet_pt/1e3 > 20)"

    # ]
    truth_cuts_resolved = [
        " (particleLevel.mujets_particle"
        " + particleLevel.ejets_particle)"
    ]
    reco_cuts_resolved = [
        "(nominal.ejets_2015 +  nominal.ejets_2016  +"
        "nominal.mujets_2015 + nominal.mujets_2016)"

    ]
    truth_cuts_boosted = [
        "(particleLevel.mujets_particle"
        " + particleLevel.ejets_particle)"
    ]
    reco_cuts_boosted = [
        "(nominal.ejets_2015 + nominal.ejets_2016 +"
        "nominal.mujets_2015 + nominal.mujets_2016)"
    ]

    fake_evaluator = FakeEvalautor(
                c.input_mc_tuple,
                name = "quick_test",
                truth_tree_name = "particleLevel",
                reco_tree_name  = "nominal"  )
    fake_evaluator.labels = ["ttj selection"]
    # fake_evaluator.evaluate_real_and_fake_distributions(c.output_folder+"/resolved/")
    fake_evaluator.construct_efficiency_histograms(truth_level_cuts =truth_cuts_resolved,
                                                   reco_level_cuts = reco_cuts_resolved   )
    fake_evaluator.draw_efficiency_histograms(c.output_folder+"/")
    fake_evaluator.construct_comparitive_cutflows(
        starting_truth_bin = 5,
        starting_reco_bin= 10,
        #resolved selection
        truth_cut_flow_names = [
                                ["ejets_particle",5],
                                ["mujets_particle",5]
                               ] ,
        reco_cut_flow_names= [ 
                               ["mujets_2015",11] ,
                               ["mujets_2016",10],
                               ["ejets_2015",11],
                               ["ejets_2016",10], 
                             ],
        output_folder = c.output_folder + "/"
        )
    fake_evaluator.labels = ["t#bar{t}j selection"]
#    fake_evaluator.evaluate_real_and_fake_distributions(c.output_folder+"/")
    fake_evaluator.construct_efficiency_histograms(truth_level_cuts =truth_cuts_resolved, reco_level_cuts = reco_cuts_resolved   )
    fake_evaluator.draw_efficiency_histograms(c.output_folder+"/")
    # fake_evaluator.construct_comparitive_cutflows(
    #     starting_truth_bin = 5,
    #     starting_reco_bin= 10,
    #     #resolved selection
    #     truth_cut_flow_names = [
    #                             ["ejets_particle_trimming_resolved",5],
    #                             ["mujets_particle_trimming_resolved",5]
    #                            ] ,
    #     reco_cut_flow_names= [ 
    #                            ["mujets_resolved_2015",11] ,
    #                            ["mujets_resolved_2016",10],
    #                            ["ejets_resolved_2015",11],
    #                            ["ejets_resolved_2016",10], 
    #                          ],
    #     output_folder = c.output_folder + "/resolved/"
    #     )


    # fake_evaluator.labels = ["Boosted"]
    # fake_evaluator.evaluate_real_and_fake_distributions(c.output_folder+"/boosted/", "boosted")
    # fake_evaluator.construct_efficiency_histograms(truth_level_cuts =truth_cuts_boosted, reco_level_cuts = reco_cuts_boosted )
    # fake_evaluator.draw_efficiency_histograms(c.output_folder+"/boosted/")
    # fake_evaluator.construct_comparitive_cutflows(
    #     starting_truth_bin = 5,
    #     starting_reco_bin= 10,
    #     #resolved selection
    #     truth_cut_flow_names = [
    #                             ["ejets_particle_trimming",5],
    #                             ["mujets_particle_trimming",5]
    #                            ] ,
    #     reco_cut_flow_names= [ 
    #                            ["mujets_2015",11] ,
    #                            ["mujets_2016",10],
    #                            ["ejets_2015",11],
    #                            ["ejets_2016",10], 
    #                          ],
    #     output_folder = c.output_folder + "boosted/"
    #     )

if __name__ == "__main__":
    main()