from __future__ import print_function
import argparse
import ROOT as r
import RootHelperFunctions.RootHelperFunctions as rhf
import RootHelperFunctions.StringHelperFunctions as shf
import RootHelperFunctions.DrawingHelperFunctions as dhf
import ATLASStyle.AtlasStyle as AS
import copy
from collections import OrderedDict
import numpy as np
import array
from time import time

# import threading
import os
import psutil
import numpy as np
import json
import logging
import sys
import glob 

class Sample:
    def __init__(self, config_file):
        """
        @Brief a class that managers ntuple outputs grouped together and drawn
        together

        @Parmameters:
        config_file: A JSON file with infomraiton of the sample at hand. 
        """

        # Read in the relveant information from a JSON File 
        logging.info('Reading sample from file %s'%config_file)
        try:
            with open(config_file) as json_data:
                self.config = json.load(json_data)
        except IOError:
            logging.critical('Failed to open sample configuration file %s.'%(config_file))
            sys.exit(-1)

        self.name = self.config["name"]
        self.eval_luminosity = self.config["evaluate_luminosity"]

        self.input_file_names = []
        self.k_factors = []
        self.x_secs = []
        self.data_period = []

        # Iterate over the saved samples and convert the order json dictioanry
        # into the expected format that RHF drawing tools wants 
        for sample in self.config["files"]:
            self.input_file_names.append(glob.glob(sample["folder"]))
            self.k_factors.append(sample["k_factor"])
            self.x_secs.append(sample["x_sec"])
            self.data_period.append(sample["data_period"])

        if self.eval_luminosity:
            self.evaluate_luminosity()
        else:
            self.mc_lumi = []
            for i in self.x_secs:
                self.mc_lumi.append(1.0)

        logging.info('    Reading sample %s'%self.config["name"])

    def evaluate_luminosity(self):
        """
        @Brief read the luminsotiy of each dataset stored in this sample; this
        is evaluated dynamically fromthe sumWeights tree in thefile. 
        """
        self.mc_lumi = []
        for i, file_list in enumerate(self.input_file_names):
            assert len(file_list) != 0, 'ERROR: signal_sample file list is empty!'

            # Evaluate n
            nEvents = 0
            sum_weights_chain = r.TChain("sumWeights")
            for f in file_list:
                sum_weights_chain.Add(f)
            for e in sum_weights_chain:
                nEvents += e.totalEventsWeighted

            # Then evaluating the MC luminosity
            mc_luminosity_nom = nEvents/(self.x_secs[i]*self.k_factors[i])
            self.mc_lumi.append(mc_luminosity_nom)

class CorrelationExplorer:
    def __init__(self, config_file, verbosity=logging.DEBUG):
        """
        @Brief a class that reads in a configuration JSON file with all sample and
        style options pre-defined to produce a set of  plotting tests

        @Parameters
        config_file: Full absolute path to JSON configuraiton file 

        verbosity: logging level as defined  by the python loggin clas, 
        see https://docs.python.org/2/library/logging.html for more information
        """

        # Set up the logger
        logging.basicConfig(format='PlottingTool:%(levelname)s %(message)s', 
                            level=verbosity)

        # Hide the annoying root messages
        rhf.hide_root_infomessages()

        self.parse_configuration_file(config_file)

        self.load_samples()

        # All histograms will be stored in a dictionary, keyed by sample name.
        self.correlation_hists = OrderedDict()
        self.config["output_folder"] = self.config["output_folder"]+"/"+self.config["name"]
    def parse_configuration_file(self, config_file):
        """
        @Brief stores the required info in the configuration file into the class

        @Parameters
        config_file: Full absolute path to JSON configuraiton file 
        """
        logging.info('Reading plotting configuration: %s'%(config_file))

        #
        try:
            with open(config_file) as json_data:
                self.config = json.load(json_data)

                # Log the information see we can keep track of what we've actually
                # loaded.
                for prop in self.config:
                    logging.info('\t%s: \t\t%s'%(prop, str(self.config[prop])))
        except IOError:
            logging.critical('Failed to open configuration file %s.'%(config_file))
            sys.exit(-1)

        # We are flexible with the format of the bins, so parse these with a bit
        # more scrutintiy         
        self.config["x_binning"] = self.determine_binning(self.config["x_binning"])
        logging.info('x-axis binning evaluated as: %s'%(str(self.config["x_binning"])))

        self.config["y_binning"] = self.determine_binning(self.config["y_binning"])
        logging.info('y-axis binning evaluated as: %s'%(str(self.config["y_binning"])))


    def determine_binning(self, binning):
        """
        @Brief Parse the possible string binning and generate an evenly sppaced
        list of numbers base upon the input string, assuming it is a standard
        root histogram format, i.e NBins:Min:Max. Has no effect on lists. 

        @PArmaeters
        binning: Either a string or unicode object or a list
        """
        if isinstance(binning, unicode) or \
           isinstance(binning, str):

            # User handing binning information in ROOT histogram style, format
            # this into a list as epxected by the plotting scrips. 
            binning_info = binning.split(',')
            binning = list(np.linspace(
                    float(binning_info[1]),
                    float(binning_info[2]),
                    int(binning_info[0]),
                )) 
        return binning
        

    def load_samples(self):
        """
        Loads the details of the samples form their configraution files, as expected
        by the initial config file for this. 
        """ 

        self.samples = []
        for sample_config in self.config["samples"]:
            self.samples.append(Sample(config_file=sample_config))


    def create_plots(self):
        """
        @Create distributions of variable of interest for spectator sampels 
        passed to the tool
        """
        for i, sample in enumerate(self.samples):
            logging.info('creating correlation for plot %s'%(sample.name))

            self.correlation_hists[sample.name] = rhf.get_2d_histogram_from_sample(
                                            sample=sample,
                                            ntuple_name=self.config["x_tuple"],
                                            variable_name=self.config["y_variable"]+":"+self.config["x_variable"],
                                            x_axis_binning=self.config["x_binning"],
                                            y_axis_binning=self.config["y_binning"],
                                            weight=self.config["weight"],
                                            scale_to_lumi=False,
                                            friend_name=self.config["y_tuple"]
                                            )
            self.correlation_hists[sample.name].SetDirectory(0)

            self.plot_correlation(sample.name)

    def plot_correlation(self, plot_name):
        """
        @Brief save the reweighted variable of interest (voi) histograms to the
        folderr. 
        """

        logging.info("Saving correlation plot for sample %s to folder: %s"%(
            plot_name,
            self.config["output_folder"]))


        rhf.create_folder(self.config["output_folder"])

        canvas = r.TCanvas(self.config["name"],
                           self.config["name"], 600, 600)
        AS.SetAtlasStyle()
        canvas.SetLeftMargin(0.2 )
        canvas.SetRightMargin(0.15 )
        canvas.SetTopMargin(0.13 )
        canvas.SetBottomMargin(0.15 )
        r.SetOwnership(canvas, 0)

        # Basic formatting
        self.correlation_hists[plot_name].GetXaxis().SetTitle(self.config["x_axis_title"])
        self.correlation_hists[plot_name].GetXaxis().SetLabelSize(0.05)
        self.correlation_hists[plot_name].GetXaxis().SetTitleSize(0.05)

        self.correlation_hists[plot_name].GetYaxis().SetTitle(self.config["y_axis_title"])
        self.correlation_hists[plot_name].GetYaxis().SetLabelSize(0.05)
        self.correlation_hists[plot_name].GetYaxis().SetTitleSize(0.05)

        self.correlation_hists[plot_name].GetYaxis().SetLabelSize(0.03)

        r.gStyle.SetPalette(r.kDarkBodyRadiator)
        r.gStyle.SetNumberContours(99)

        self.correlation_hists[plot_name].Draw("COLZ")
        lables = [', '.join([plot_name, 'C = %.3f'%(self.correlation_hists[plot_name].GetCorrelationFactor())]+
                             self.config["messages"])
                 ]
        dhf.draw_atlas_details(labels=lables,  text_size=0.03,y_pos=0.96,
                               x_pos=0.21,dy=0.03)

        canvas.Print(self.config["output_folder"]+"/"+shf.clean_string(plot_name)+"_correlation.png")
        canvas.Print(self.config["output_folder"]+"/"+shf.clean_string(plot_name)+"_correlation.eps")


# Main entry point for this script
# ToDO: move this class over to tools and write sperate running scripts. 
def main():
    parser = argparse.ArgumentParser(
        description='A general configuration based plotting script for NTuples.')

    # We really only want two default 
    parser.add_argument("configuration_file", type=str,
                        help="The plotting configuration file to draw.")
    parser.add_argument("-v", "--verbosity", type=int, choices=[10, 20, 30, 40, 50], default=10,
                        help="Increase output verbosity")
    args = parser.parse_args()

    # Map the verbosity configuration options onto
    correlation_plotting_tool = CorrelationExplorer(config_file=args.configuration_file,
                                 verbosity=args.verbosity)

    correlation_plotting_tool.create_plots()


# 
main()

