
'''
    Author: Jacob Rawling
    Date:

    Utilizes the TopAnalysis.Unfolding package ( ) to create the migration matrix,
    perform the desired type of unfolding, run some tests, save the ouptut in ROOT
    and Journal ready eps fomrat
'''

from TopAnalysis.Unfolding.Systematic import Systematic
from TopAnalysis.SystematicsManager import SystematicsManager
import numpy as np
import config as c
import ROOT as r
import sys
from collections import OrderedDict
from TopAnalysis.Backgrounds import background_samples, ttbar_samples,ttbar_debug_samples,ttV_samples, ttbar_allhad_samples, w_jets_samples
from TopAnalysis.GeneralPlotter import GeneralPlot, GeneralPlotter

# macro_folder = r.gSystem.GetFromPipe("echo $TOP_ANALYSIS_INPUT_PATH")
# for macro_file in c.macros:
#     r.gROOT.ProcessLineSync(".L "+str(macro_folder) +"/macros/"+macro_file)
#     r.gROOT.ProcessLineSync(".x "+str(macro_folder) +"/macros/"+macro_file)
    # r.gROOT.ProcessLine("lead_bjet_pt(0,0)")
def main():
    """
    Runs the general plotter and creates a set of control plots for
    a generic analysis using ROOT NTuples created by AnalysisTop
    """
    tbar_is_leptonic="*(Length$(el_pt) > 0)*(el_charge[0] < 0)"

    cuts = {}
    cuts["signal_region"] = "*(Length$(nominal.jet_pt)==5)" \
                           "*(nominal.g_pt[0]/1e3 >  25)" \
                           "*(nominal.g_pt[0]/nominal.top_lep_pt > 0.05)" \
                           "*(nominal.top_lep_pt/1000 > 100)" \
                           "*(nominal.theta < nominal.theta_had)" \
                           "*(nominal.top_lep_m/1e3 > 150  && nominal.top_lep_m/1e3 < 300)" 

    cuts["inclusive"] = "*(ejets_2015==1 || ejets_2016==1 || mujets_2015==1 || mujets_2016==1)*(Length$(nominal.jet_pt)==5)"#*(mu < 24)" 
    cuts["inclusive_2016"] = "*(ejets_2016==1 || mujets_2016==1)*(Length$(nominal.jet_pt)==5)" 

    
    cuts["e+jets"] = "*(ejets_2015==1 || ejets_2016==1)*(Length$(nominal.jet_pt)==5)"
    cuts["mu+jets"] = "*(mujets_2015==1 || mujets_2016==1)*(Length$(nominal.jet_pt)==5)"

    cuts["control_region"] = "*(nominal.g_pt[0]/1e3 >  25)" \
                             "*(nominal.g_pt[0]/nominal.top_lep_pt > 0.05)" \
                             "*(nominal.top_lep_pt/1000 < 192)" \
                             "*(nominal.top_lep_pt/1000 > 40)" \
                             "*(nominal.theta_d < 0.9)" \
                             "*(nominal.theta < nominal.theta_had)" \
                             "*(nominal.top_lep_m/1e3 > 155  && nominal.top_lep_m/1e3 < 187)" 


    if len(sys.argv) < 3:
        print "ERROR: Usage is <cut_name> <plot_to_plot>"
        print "       Where cut_name is one of:"
        print "                 signal_region"
        print "                 control_region"
        print "                 inclusive"
        print "       And plot_to_plot is one of:"
        plots = ["lep_top_m", "had_top_m", "lep_top_pt", "had_top_pt", "n_jets", "deltar_top_jet", "lep_top_eta", "lep_top_eta", "lep_top_phi", "lep_top_phi", "el_energy", "el_pt", "el_eta", "el_phi", "el_eta", "mu_energy", "mu_pt", "mu_eta", "mu_phi", "W_lep_m", "W_lep_eta", "W_lep_phi", "W_lep_pt", "W_had_m", "W_had_eta", "W_had_phi", "W_had_pt", "b_lep_m", "b_lep_eta", "b_lep_phi", "b_lep_pt", "b_had_m", "b_had_eta", "b_had_phi", "b_had_pt", "n_bjets_85", ]
        for p in plots:
            print "                 ", p 
        sys.exit()
    gp_name=cut_name = sys.argv[1]
    gp_name=gp_name.replace('+','_')
    plot_to_plot = sys.argv[2]
    # fake_weight = "(weight_mm_p[9]*(Length$(mu_pt)==0)+(Length$(mu_pt)==1)*weight_mm_p[16]) "
    fake_weight = "weight_mm_p[4]" #DPhi
    fake_weight = "weight_mm_p[2]" #jetpt
    debug_mode = False
    parameterization = [
        'NS:eta',
        'NS:pt',
        'NS:jetpt',
        'NS:dR',
        'NS:dPhi',
        #
        'NS:dR:dPhi',
        #
        'NS:jetpt:dR',
        'NS:jetpt:dR:dPhi',
        'NS:jetpt:dPhi',

        #
        'NS:pt:jetpt:dR',
        'NS:pt:jetpt:dR:dPhi',
        'NS:pt:jetpt:dPhi',
        'NS:pt:dR',
        'NS:pt:dR:dPhi',
        'NS:pt:dPhi',

        'NS:eta:pt',
        'NS:eta:jetpt',
        'NS:eta:dR',
        'NS:eta:dPhi',

        'NS:eta:pt:jetpt',
        'NS:eta:pt:dPhi',
        'NS:eta:pt:dR',
        'NS:eta:pt:jetpt:dR',
        'NS:eta:pt:jetpt:dPhi',
        'NS:eta:pt:jetpt:dR:dPhi',
        ]
    for param in sys.argv[2:]:
        if 'weight_mm' in param:
            fake_weight = param
            print('SETTING FAKE WEIGHT: ', fake_weight)
            p_index = int(fake_weight.split('[')[1].split(']')[0])
            gp_name = parameterization[p_index].replace(':','_')+'_'+gp_name

        if 'debug' in param.lower():
            debug_mode = True

    print "Creating control plot: "
    print "    Region   : ", cut_name
    print "    Plot     : ", plot_to_plot
    print "   Debug     : ", debug_mode
    print "Fake weight  : ", fake_weight

    # Test settings and store it to my webpage
    for appending_method in [
                            append_pileup_variables,
                            append_deadcone_variables,
                            append_lep_top_variables,
                            # append_lhcwg_lep_top_variables,
                            append_jet_variables,
                            append_el_variables,
                            append_mu_variables,
                            # append_had_top_variables,
                            append_lead_bjet_pt,
                            append_lep_W_variables,
                            append_had_W_variables,
                            append_lep_b_variables,
                            append_had_b_variables,
                            append_jet_multiplicty_variables
                            ]:

        #
        bkg_samples =  [ttbar_samples]+background_samples
        if debug_mode:
            bkg_samples = [ttbar_debug_samples]

        plotting_tool = GeneralPlotter(
            output_folder=c.control_plot_output_folder,
            MC_samples=bkg_samples,
            MC_fake_samples=[ttbar_allhad_samples],
            data_samples=c.data_samples,
            luminosity=c.luminosity,
            cut_weight=cuts[cut_name],
            name=gp_name,
            messages=[cut_name],
            debug_mode=debug_mode,
            data_fake_weight=fake_weight,
            fake_estimation_method='mm'
            )

        appending_method(plotting_tool)
        plotting_tool.create_plots(plot_to_plot)


n_bins = 18
def append_truth_variables(plotting_tool):
    plotting_tool.add_variable(
            GeneralPlot(
                variable="(172.5-top_lep.M())/1e3",
                x_axis_binning=list(np.linspace(-300, 300, 45)),
                cut_weight="",
                name="delta_m",
                x_axis_title="Leptonic top #Delta(reco mass, truth mass) [GeV]",
                y_axis_title="Number of Events",
                log_y=False,
                force_min=1e-3
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="(172.5-lhcwg_top_lep.M())/1e3",
                x_axis_binning=list(np.linspace(-300, 300, 45)),
                cut_weight="",
                name="lhcwg_delta_m",
                x_axis_title="Leptonic top #Delta(reco mass, truth mass) [GeV]",
                y_axis_title="Number of Events",
                log_y=False,
                force_min=1e-3
                )
        )

def append_el_variables(plotting_tool):
    plotting_tool.add_variable(
            GeneralPlot(
                variable="el_e[0]/1e3",
                x_axis_binning=list(np.linspace(27, 220, 15)), 
                cut_weight="",
                name="el_energy",
                x_axis_title="electron energy [GeV]",
                y_axis_title="Number of Events",
                log_y=False,
                force_min=1e-3,
                normalize=False,
                divide_by_bin_width=False

                )
        )

    plotting_tool.add_variable(
            GeneralPlot(
                variable="el_pt[0]/1e3",
                x_axis_binning=list(np.linspace(27, 220, 15)),
                cut_weight="",
                name="el_pt",
                x_axis_title="electron p_{T} [GeV]",
                y_axis_title="Number of Events",
                normalize=False,
                # divide_by_bin_width=False
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="el_eta[0]",
                x_axis_binning=list(np.linspace(-3., 3., 15 )),
                cut_weight="",
                name="el_eta",
                x_axis_title="electron #eta",
                y_axis_title="Number of Events",
                normalize=False,
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="el_phi[0]",
                x_axis_binning=list(np.linspace(-3., 3., 15 )),
                cut_weight="",
                name="el_phi",
                x_axis_title="electron #phi",
                y_axis_title="Number of Events",
                normalize=False,
                )
        )

def append_pileup_variables(plotting_tool):
    plotting_tool.add_variable(
            GeneralPlot(
                variable="mu_original_xAOD/1.03",
                mc_variable="mu",
                x_axis_binning=list(np.linspace(5, 75, 71)),
                cut_weight="",
                name="mu",
                x_axis_title="<#mu>",
                y_axis_title="Number of Events",
                normalize=False,
                divide_by_bin_width=False
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="mu_original_xAOD*1.03",
                mc_variable="mu",
                x_axis_binning=list(np.linspace(5, 75, 71)),
                cut_weight="",
                name="mu_times_103",
                x_axis_title="<#mu>",
                y_axis_title="Number of Events",
                normalize=False,
                divide_by_bin_width=False
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="mu_original_xAOD",
                mc_variable="mu",
                x_axis_binning=list(np.linspace(5, 75, 71)),
                cut_weight="",
                name="mu_MC_v_nom_mu_xAOD",
                x_axis_title="<#mu>",
                y_axis_title="Number of Events",
                normalize=False,
                divide_by_bin_width=False
                )
        )

    plotting_tool.add_variable(
            GeneralPlot(
                variable="1.0",
                mc_variable="weight_mc",
                x_axis_binning=list(np.linspace(-1000, -1, 50)) + [0] + list(np.linspace(1,1000)),
                cut_weight="",
                name="weights",
                x_axis_title="weight mc",
                y_axis_title="Number of Events",
                normalize=False,
                divide_by_bin_width=False
                )
        )

    plotting_tool.add_variable(
            GeneralPlot(
                variable="mu",
                mc_variable="mu",
                x_axis_binning=list(np.linspace(5, 75, 71)),
                cut_weight="",
                name="mu_MC",
                x_axis_title="<#mu>",
                y_axis_title="Number of Events",
                normalize=False,
                divide_by_bin_width=False
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                mc_variable="randomRunNumber > 311481",
                variable="RunNumber",
                x_axis_binning=list(np.linspace(0, 4, 6)),
                cut_weight="",
                name="run_num",
                x_axis_title="RandomRunNumber > 31148",
                y_axis_title="Number of Events",
                normalize=False,
                divide_by_bin_width=False
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="met_met/1e3",
                x_axis_binning=list(np.linspace(25, 300, n_bins)),
                cut_weight="",
                name="met",
                x_axis_title="MET [GeV]",
                y_axis_title="Number of Events",
                normalize=False,
                force_min=0.0,
                divide_by_bin_width=False
                )
        )


def append_deadcone_variables(plotting_tool):
                # ,                 #Detector level variable name that will be unfolded from
    plotting_tool.add_variable(
        GeneralPlot(
            variable="(nominal.theta[0]-nominal.theta_d)/(nominal.theta[0]+nominal.theta_d)",
            x_axis_binning=[-1.0, 0.0508475, 0.322034, 0.457627, 0.59322, 0.661017, 0.728814, 0.79661, 0.864407, 1.0],
            cut_weight="*(theta[0] < theta_had[0])",
            name="A_theta_lep",
            x_axis_title="Leptonic A_{#theta}",
            y_axis_title="Number of Events",
            normalize=False,
            divide_by_bin_width=False
            )
    )
    plotting_tool.add_variable(
        GeneralPlot(
            variable="(nominal.theta_had[0]-nominal.theta_d)/(nominal.theta_had[0]+nominal.theta_d)",
            x_axis_binning=[-1,-0.25,0.0,0.25,0.5,0.7,1.0],
            cut_weight="*(theta[0] > theta_had[0])",
            name="A_theta_had",
            x_axis_title="Hadronic A_{#theta}",
            y_axis_title="Number of Events",
            normalize=False,
            divide_by_bin_width=False
            )
    )
    # plotting_tool.add_variable(
    #     GeneralPlot(
    #         variable="(nominal.theta[0]-nominal.theta_d)/(nominal.theta[0]+nominal.theta_d)",
    #         x_axis_binning=[-1,-0.25,0.0,0.25,0.5,0.7,1.0],
    #         cut_weight="*(theta[0] < theta_had[0])",
    #         name="A_theta_lep",
    #         x_axis_title="Leptonic A_{#theta}",
    #         y_axis_title="Number of Events",
    #         normalize=False,
    #         divide_by_bin_width=False
    #         )
    # )
def append_jet_variables(plotting_tool):
    plotting_tool.add_variable(
            GeneralPlot(
                variable="jet_pt[0]/1e3",
                x_axis_binning=list(np.linspace(25, 220, n_bins)),
                cut_weight="",
                name="lead_jet_pt",
                x_axis_title="Leading jet p_{T} [GeV]",
                y_axis_title="Number of Events",
                normalize=False,
                divide_by_bin_width=False
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="jet_eta[0]",
                x_axis_binning=list(np.linspace(-3., 3., n_bins)),
                cut_weight="",
                name="lead_jet_eta",
                x_axis_title="Leading jet #eta",
                y_axis_title="Number of Events",
                normalize=False,
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="jet_phi[0]",
                x_axis_binning=list(np.linspace(-3., 3., n_bins)),
                cut_weight="",
                name="lead_jet_phi",
                x_axis_title="Leading jet #phi",
                y_axis_title="Number of Events",
                normalize=False,

                )
        )  
    plotting_tool.add_variable(
            GeneralPlot(
                variable="jet_pt[1]/1e3",
                x_axis_binning=list(np.linspace(25, 220, n_bins)),
                cut_weight="",
                name="sublead_jet_pt",
                x_axis_title="Sub-leading jet p_{T} [GeV]",
                y_axis_title="Number of Events",
                normalize=False,
                divide_by_bin_width=False
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="jet_eta[1]",
                x_axis_binning=list(np.linspace(-3., 3., n_bins)),
                cut_weight="",
                name="sublead_jet_eta",
                x_axis_title="Sub-leading jet #eta",
                y_axis_title="Number of Events",
                normalize=False,
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="jet_phi[1]",
                x_axis_binning=list(np.linspace(-3., 3., n_bins)),
                cut_weight="",
                name="sublead_jet_phi",
                x_axis_title="Sub-leading jet #phi",
                y_axis_title="Number of Events",
                normalize=False,

                )
        )  
def append_mu_variables(plotting_tool):
    plotting_tool.add_variable(
            GeneralPlot(
                variable="mu_e[0]/1e3",
                x_axis_binning=list(np.linspace(20, 500, n_bins)),
                cut_weight="",
                name="mu_energy",
                x_axis_title="muon energy [GeV]",
                y_axis_title="Number of Events",
                log_y=False,
                force_min=1e-3,
                normalize=False,
                divide_by_bin_width=False
                )
        )

    plotting_tool.add_variable(
            GeneralPlot(
                variable="mu_pt[0]/1e3",
                x_axis_binning=list(np.linspace(27, 200, n_bins)),
                cut_weight="",
                name="mu_pt",
                x_axis_title="muon p_{T} [GeV]",
                y_axis_title="Number of Events",
                normalize=False,
                divide_by_bin_width=False
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="mu_eta[0]",
                x_axis_binning=list(np.linspace(-3., 3., n_bins)),
                cut_weight="",
                name="mu_eta",
                x_axis_title="muon #eta",
                y_axis_title="Number of Events",
                normalize=False,
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="mu_phi[0]",
                x_axis_binning=list(np.linspace(-3., 3., n_bins)),
                cut_weight="",
                name="mu_phi",
                x_axis_title="muon #phi",
                y_axis_title="Number of Events",
                normalize=False,

                )
        )

def append_lep_top_variables(plotting_tool):
    plotting_tool.add_variable(
            GeneralPlot(
                variable="top_lep_m/1e3",
                x_axis_binning=list(np.linspace(100, 250, n_bins)),#+[270,290,320,350,400],
                cut_weight="",
                name="lep_top_m",
                x_axis_title="Leptonic top Mass [GeV]",
                y_axis_title="Number of Events",
                log_y=False,
                force_min=0,
                divide_by_bin_width=False,  
                # normalize=False,
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="top_had_m/1e3",
                x_axis_binning=list(np.linspace(75, 400, n_bins)),
                cut_weight="",
                name="had_top_m",
                x_axis_title="Hadronic top Mass [GeV]",
                y_axis_title="Number of Events",
                log_y=False,
                force_min=0,
                # normalize=False,
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="top_lep_pt/1e3",
                x_axis_binning=list(np.linspace(0, 500, n_bins)),
                cut_weight="",
                name="lep_top_pt",
                x_axis_title="Leptonic top p_{T} [GeV]",
                y_axis_title="Number of Events",
                log_y=False,
                force_min=0,
                # normalize=False,
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="top_had_pt/1e3",
                x_axis_binning=list(np.linspace(0, 500, n_bins)),
                cut_weight="",
                name="had_top_pt",
                x_axis_title="Hadronic top p_{T} [GeV]",
                y_axis_title="Number of Events",
                log_y=False,
                force_min=0,
                # normalize=False,
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="Length$(jet_pt)",
                x_axis_binning=list(np.linspace(5.5, 10.5, 6)),
                cut_weight="",
                name="n_jets",
                x_axis_title="Number of jets",
                y_axis_title="Number of Events",
                log_y=False,
                force_min=0,
                # normalize=False,
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="theta",
                x_axis_binning=list(np.linspace(0, 10, n_bins)),
                cut_weight="",
                name="deltar_top_jet",
                x_axis_title="#Delta R( leptonic top, radiative jet)",
                y_axis_title="Number of Events",
                log_y=False,
                force_min=0,
                # normalize=False,
                )
        )

    plotting_tool.add_variable(
            GeneralPlot(
                variable="top_lep_eta",
                x_axis_binning=list(np.linspace(-3., 3., n_bins)),
                cut_weight="",
                name="lep_top_eta",
                x_axis_title="Leptonic top #eta",
                y_axis_title="Number of Events",
                force_min=0,
                # normalize=False,
                )
        )
        
    plotting_tool.add_variable(
            GeneralPlot(
                variable="top_had_eta",
                x_axis_binning=list(np.linspace(-3., 3., n_bins)),
                cut_weight="",
                name="had_top_eta",
                x_axis_title="Hadronic top #eta",
                y_axis_title="Number of Events",
                force_min=0,
                # normalize=False,
                )
        )
        

    plotting_tool.add_variable(
            GeneralPlot(
                variable="top_lep_phi",
                x_axis_binning=list(np.linspace(-3., 3., n_bins)),
                cut_weight="",
                name="lep_top_phi",
                x_axis_title="Leptonic top #phi",
                y_axis_title="Number of Events",
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="top_had_phi",
                x_axis_binning=list(np.linspace(-3., 3., n_bins)),
                cut_weight="",
                name="had_top_phi",
                x_axis_title="Hadronic top #phi",
                y_axis_title="Number of Events",
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="(((172.44e3-top_lep_m)/(1.35e3))**2 +((172.44e3-top_had_m)/(1.35e3))**2+((80.385e3-W_had.M())/(2.085e3))**2 )",
                x_axis_binning=list(np.linspace(0, 3500, n_bins)),
                cut_weight="",
                name="chi2",
                x_axis_title="Reconstruction #chi^{2}",
                y_axis_title="Number of Events",
                )
        )
def append_lhcwg_lep_top_variables(plotting_tool):
    plotting_tool.add_variable(
            GeneralPlot(
                variable="lhcwg_top_lep_m/1e3",
                x_axis_binning=list(np.linspace(75, 400, 45)),
                cut_weight="",
                name="lhcwg_lep_top_mass",
                x_axis_title="Leptonic top Mass [GeV]",
                y_axis_title="Number of Events",
                log_y=False,
                force_min=1e-3
                )
        )

    plotting_tool.add_variable(
            GeneralPlot(
                variable="lhcwg_top_lep_pt/1e3",
                x_axis_binning=list(np.linspace(0, 500, n_bins)),
                cut_weight="",
                name="lhcwg_lep_top_pt",
                x_axis_title="Leptonic top p_{T} [GeV]",
                y_axis_title="Number of Events",
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="lhcwg_top_lep_eta",
                x_axis_binning=list(np.linspace(-3., 3., n_bins)),
                cut_weight="",
                name="lhcwg_lep_top_eta",
                x_axis_title="Leptonic top #eta",
                y_axis_title="Number of Events",
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="lhcwg_top_lep_phi",
                x_axis_binning=list(np.linspace(-3., 3., n_bins)),
                cut_weight="",
                name="lhcwg_lep_top_phi",
                x_axis_title="Leptonic top #phi",
                y_axis_title="Number of Events",
                )
        )
def append_had_top_variables(plotting_tool):
    plotting_tool.add_variable(
            GeneralPlot(
                variable="top_had_m/1e3",
                x_axis_binning=list(np.linspace(115, 230, 45)),
                cut_weight="",
                name="had_top_m",
                x_axis_title="Hadronic top Mass [GeV]",
                y_axis_title="Number of Events",
                log_y=False,
                force_min=1e-3
                )
        )

    plotting_tool.add_variable(
            GeneralPlot(
                variable="top_had_pt/1e3",
                x_axis_binning=list(np.linspace(0, 500, n_bins)),
                cut_weight="",
                name="had_top_pt",
                x_axis_title="Hadronic top p_{T} [GeV]",
                y_axis_title="Number of Events",
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="top_had_eta",
                x_axis_binning=list(np.linspace(-3., 3., n_bins)),
                cut_weight="",
                name="had_top_eta",
                x_axis_title="Hadronic top #eta",
                y_axis_title="Number of Events",
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="top_had_phi",
                x_axis_binning=list(np.linspace(-3., 3., n_bins)),
                cut_weight="",
                name="had_top_phi",
                x_axis_title="Hadronic top #phi",
                y_axis_title="Number of Events",
                )
        )


def append_lhcwg_had_top_variables(plotting_tool):
    plotting_tool.add_variable(
            GeneralPlot(
                variable="lhcwg_top_had_m/1e3",
                x_axis_binning=list(np.linspace(115, 230, 45)),
                cut_weight="",
                name="lhcwg_had_top_mass",
                x_axis_title="Hadronic top Mass [GeV]",
                y_axis_title="Number of Events",
                log_y=False,
                force_min=1e-3
                )
        )

    plotting_tool.add_variable(
            GeneralPlot(
                variable="lhcwg_top_had_pt/1e3",
                x_axis_binning=list(np.linspace(0, 500, n_bins)),
                cut_weight="",
                name="lhcwg_had_top_pt",
                x_axis_title="Hadronic top p_{T} [GeV]",
                y_axis_title="Number of Events",
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="lhcwg_top_had_eta",
                x_axis_binning=list(np.linspace(-3., 3., n_bins)),
                cut_weight="",
                name="lhcwg_had_top_eta",
                x_axis_title="Hadronic top #eta",
                y_axis_title="Number of Events",
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="lhcwg_top_had_phi",
                x_axis_binning=list(np.linspace(-3., 3., n_bins)),
                cut_weight="",
                name="lhcwg_had_top_phi",
                x_axis_title="Hadronic top #phi",
                y_axis_title="Number of Events",
                )
        )


def append_lep_W_variables(plotting_tool):
    plotting_tool.add_variable(
            GeneralPlot(
                variable="W_lep.M()/1e3",
                x_axis_binning=list(np.linspace(50, 250, 45)),
                cut_weight="",
                name="W_lep_m",
                x_axis_title="Leptonic W mass [GeV]",
                y_axis_title="Number of Events",
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="W_lep.Eta()",
                x_axis_binning=list(np.linspace(-4.5, 4.5, n_bins)),
                cut_weight="",
                name="W_lep_eta",
                x_axis_title="Leptonic W #eta",
                y_axis_title="Number of Events",
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="W_lep.Phi()",
                x_axis_binning=list(np.linspace(-3.14, 3.14, n_bins)),
                cut_weight="",
                name="W_lep_phi",
                x_axis_title="Leptonic W #phi",
                y_axis_title="Number of Events",
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="W_lep.Pt()/1e3",
                x_axis_binning=list(np.linspace(0, 400, n_bins)),
                cut_weight="",
                name="W_lep_pt",
                x_axis_title="Leptonic W p_{T} [GeV]",
                y_axis_title="Number of Events",
                )
        )


def append_had_W_variables(plotting_tool):
    plotting_tool.add_variable(
            GeneralPlot(
                variable="W_had.M()/1e3",
                x_axis_binning=list(np.linspace(35, 150, 45)),
                cut_weight="",
                name="W_had_m",
                x_axis_title="Hadronic W mass [GeV]",
                y_axis_title="Number of Events",
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="W_had.Eta()",
                x_axis_binning=list(np.linspace(-4.5, 4.5, n_bins)),
                cut_weight="",
                name="W_had_eta",
                x_axis_title="Hadronic W #eta",
                y_axis_title="Number of Events",
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="W_had.Phi()",
                x_axis_binning=list(np.linspace(-3.14, 3.14, n_bins)),
                cut_weight="",
                name="W_had_phi",
                x_axis_title="Hadronic W #phi",
                y_axis_title="Number of Events",
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="W_had.Pt()/1e3",
                x_axis_binning=list(np.linspace(0, 400, n_bins)),
                cut_weight="",
                name="W_had_pt",
                x_axis_title="Leptonic W p_{T} [GeV]",
                y_axis_title="Number of Events",
                )
        )

def append_lep_b_variables(plotting_tool):
    plotting_tool.add_variable(
            GeneralPlot(
                variable="b_lep.M()/1e3",
                x_axis_binning=list(np.linspace(0, 100, 20)),
                cut_weight="",
                name="b_lep_m",
                x_axis_title="Leptonic b-jet mass [GeV]",
                y_axis_title="Number of Events",
                normalize=False,
                force_min=0,
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="b_lep.Eta()",
                x_axis_binning=list(np.linspace(-2.5, 2.5, 20)),
                cut_weight="",
                name="b_lep_eta",
                x_axis_title="Leptonic b-jet #eta",
                y_axis_title="Number of Events",
                normalize=False,
                force_min=0,
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="b_lep.Phi()",
                x_axis_binning=list(np.linspace(-3.14, 3.14, 20)),
                cut_weight="",
                name="b_lep_phi",
                x_axis_title="Leptonic b-jet #phi",
                y_axis_title="Number of Events",
                normalize=False,
                force_min=0,
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="b_lep.Pt()/1e3",
                x_axis_binning=[0,20,30,40,50,70,100,150,250,350,500], 
                cut_weight="",
                name="b_lep_pt",
                x_axis_title="Hadronic b-jet p_{T} [GeV]",
                y_axis_title="Number of Events",
                normalize=False,
                divide_by_bin_width=False,
                force_min=0,
                )
        )
def append_lead_bjet_pt(plotting_tool):
    plotting_tool.add_variable(
            GeneralPlot(
                variable="leading_bjet_pt(jet_pt,jet_isbtagged_MV2c10_85)/1e3",
                x_axis_binning=[0,25,40,50,70,100,150,250,350,500], 
                cut_weight="",
                name="lead_bjet_pt",
                x_axis_title="Leading b-jet p_{T} [GeV]",
                y_axis_title="Number of Events",
                normalize=False,
                divide_by_bin_width=False,
                force_min=0,
                )
        )

def append_had_b_variables(plotting_tool):
    plotting_tool.add_variable(
            GeneralPlot(
                variable="b_had.M()/1e3",
                x_axis_binning=list(np.linspace(0, 100, 20)),
                cut_weight="",
                name="b_had_m",
                x_axis_title="Hadronic b-jet mass [GeV]",
                y_axis_title="Number of Events",
                normalize=False,
                force_min=0,
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="b_had.Eta()",
                x_axis_binning=list(np.linspace(-2.5, 2.5, 20)),
                cut_weight="",
                name="b_had_eta",
                x_axis_title="Hadronic b-jet #eta",
                y_axis_title="Number of Events",
                normalize=False,
                force_min=0,
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="b_had.Phi()",
                x_axis_binning=list(np.linspace(-3.14, 3.14, 20)),
                cut_weight="",
                name="b_had_phi",
                x_axis_title="Hadronic b-jet #phi",
                y_axis_title="Number of Events",
                normalize=False,
                force_min=0,
                )
        )
    plotting_tool.add_variable(
            GeneralPlot(
                variable="b_had.Pt()/1e3",
                x_axis_binning=[0,20,30,40,50,70,100,150,250,350,500], 
                cut_weight="",
                name="b_had_pt",
                x_axis_title="Leptonic b-jet p_{T} [GeV]",
                y_axis_title="Number of Events",
                normalize=False,
                force_min=0,
                divide_by_bin_width=False,
                )
        )


def append_jet_multiplicty_variables(plotting_tool):
    plotting_tool.add_variable(
            GeneralPlot(
                variable="Sum$(jet_isbtagged_MV2c10_85)",
                x_axis_binning=[1.5,2.5,3.5,4.5,5.5],
                cut_weight="",
                name="n_bjets_85",
                x_axis_title="b-Jet Multiplicty [85% WP]",
                y_axis_title="Number of Events",
                normalize=False,
                force_min=0,
                )
        )


if __name__ == "__main__":
    main()
