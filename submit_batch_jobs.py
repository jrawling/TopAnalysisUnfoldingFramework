import config as c 
import sys
import os

if len(sys.argv) < 2:
    print "USAGE: python submit_batch_jobs <plot_type> <region>"
    print "  plot_type is one of: control, unfolding, systematic"
    print "  region is on of: signal,control,hadronic_signal, hadronic_control, inclusive"
    sys.exit()

plot_type = sys.argv[1]
selected_regions = sys.argv[2:]

# Now read in the ptah that the code is stored in. 
INPUT_PATH = os.environ["TOP_ANALYSIS_INPUT_PATH"]
batch_log_outdir = INPUT_PATH+"/logs/batch_job_logs/"
print INPUT_PATH

if plot_type == "control":
    regions = ["inclusive", "e+jets", "mu+jets", "signal_region", "control_region"]
    plots = [ 
              "A_theta_had","A_theta_lep",
              "el_energy", 
              "el_pt", 
              "el_eta", "el_phi", "el_eta", 
              "mu_energy", "mu_pt", "mu_eta", "mu_phi", 
              # "b_lep_eta", "b_lep_phi", "b_lep_pt", 
              # "b_had_eta", "b_had_phi", "b_had_pt", 
              "lead_jet_pt", "lead_jet_eta", "lead_jet_phi",  
              "lep_top_m", 
              "lep_top_eta", "lep_top_phi", "lep_top_pt", 
              "had_top_m", 
              "had_top_eta", "had_top_phi","had_top_pt", 
              "deltar_top_jet", 
              "sublead_jet_pt", "sublead_jet_eta", "sublead_jet_phi",  
              "mu",  
              # "mu_times_103",  
              # "mu_MC",  
              "mu_MC_v_nom_mu_xAOD",
              "weights",
              "chi2",
              "met", 
              "n_bjets_85", 
              "W_had_m",
              "W_had_eta",
              "W_had_phi",
              "W_had_pt",
              "W_lep_m",
              "W_lep_eta",
              "W_lep_phi",
              "W_lep_pt",
              ]#
    for region in selected_regions:
      print "Submitting jobs for region: ", region 
      assert region in regions, "ERROR: selected region not in regions!"
      for i, plot in enumerate(plots): 
          os.system('bsub -q 8nh -J '+region + "_" + plot +' -o "'+
                    batch_log_outdir+region+'_'+plot+'_%J.log" -env "INPUT_PATH= '+INPUT_PATH+', region='+
                    region+', plot_type='+str(plot)+
                    ', all"<' + INPUT_PATH + '/BatchRunning/create_control_plots.sh')

if plot_type == "weight_scan":
    print 'Perfoming weight scan. '
    regions = ["inclusive", "e+jets", "mu+jets", "signal_region", "control_region"]
    plots = [ 
              "A_theta_lep",
              "el_energy", "el_pt", "el_eta", "el_phi", 
              "mu_energy", "mu_pt", "mu_eta", "mu_phi", 
              "lep_top_m", "lep_top_eta", "lep_top_phi", "lep_top_pt", 
              "had_top_m", "had_top_eta", "had_top_phi","had_top_pt", 
              "met", 
              ]#
    weights = ['weight_mm_p['+str(i)+']' for i in xrange(0,25)]
    n_jobs = 0
    for w in weights: 
        print 'Processing weight: ', w
        for region in regions:
          print "    Submitting jobs for region: ", region, ' with weight: ', w
          
          for i, plot in enumerate(plots): 
              print '       Processing plot', plot
              os.system('bsub -q 8nh -J '+region + "_" + plot +' -o "'+
                        batch_log_outdir+region+'_'+plot+'_%J.log" -env "INPUT_PATH= '+INPUT_PATH+', region='+
                        region+', plot_type='+str(plot)+', weight='+str(w)+
                        ', all"<' + INPUT_PATH + '/BatchRunning/create_control_weight_plot.sh')
              n_jobs += 1

    print('Submitted ', n_jobs, ' weigth scan jobs. ')
# The following is for unfolding, where the region definitions are 
# hard coded into unfolder_configs and selected by the firs argument of this
# script. 
regions = ["inclusive", "signal_region", "control_region", 
           "hadronic_control_region", "hadronic_signal_region"]

if plot_type == "nominal":
    for region in selected_regions:
      print "Submitting jobs for region: ", region 
      assert region in regions, "ERROR: selected region not in regions!"

      os.system('bsub -q 8nh -J nom_unfold_'+region +' -o "'+
                batch_log_outdir+'nom_un_'+region+'%J.log" -env "INPUT_PATH= '+INPUT_PATH+', region='+
                region+
                ', all"<' + INPUT_PATH + '/BatchRunning/create_nominal_unfolding.sh')

if plot_type == "pdf":
    for region in selected_regions:
      print "Submitting jobs for region: ", region 
      assert region in regions, "ERROR: selected region not in regions!"

      os.system('bsub -q 8nh -J pdf_'+region +' -o "'+
                batch_log_outdir+'pdf_'+region+'%J.log" -env "INPUT_PATH= '+INPUT_PATH+', region='+
                region+
                ', all"<' + INPUT_PATH + '/BatchRunning/create_pdf_unfolding.sh')


if plot_type == "systematic":
    from TopAnalysis.SystematicsManager import SystematicsManager
    # systematics = SystematicsManager.select_systematics(["all"])
    systematics = SystematicsManager.select_systematics([
    #                                                 "Frag._and_Had.",
    #                                                 # "Scale_Var",
                                                    "Var3Down",
                                                    "FSR Up",  
                                                    "FSR Down",  
                                                    ])
    print(systematics)
    for region in selected_regions:
      print "Submitting jobs for region: ", region 
      assert region in regions, "ERROR: selected region not in regions!"
      for syst in systematics:
        os.system('bsub -q 8nh -J syst_'+region+'_'+syst +' -o "'+
                batch_log_outdir+'syst_'+region+'_'+syst+'%J.log" -env "INPUT_PATH= '+INPUT_PATH+', region='+
                region+', syst='+syst+
                ', all"<' + INPUT_PATH + '/BatchRunning/create_systematic.sh')


if plot_type == "test":

    tests = ["identity","iteration","stress" ]
    # tests = [ "stress" ]
    for region in selected_regions:
      print "Submitting jobs for region: ", region 
      assert region in regions, "ERROR: selected region not in regions!"
      for t in tests:
        os.system('bsub -q 8nh -J test_'+region+'_'+t +' -o "'+
                batch_log_outdir+'test_'+region+'_'+t+'%J.log" -env "INPUT_PATH= '+INPUT_PATH+', region='+
                region+', test='+t+
                ', all"<' + INPUT_PATH + '/BatchRunning/create_test.sh')

print "Submissions complete."
