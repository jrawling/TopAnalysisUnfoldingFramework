"""
    Use an iterative set of pull tests to evaluate the binnig of a variable. 

    Based heavily on the ClosureTest found in TopAnalysis/Unfolding/ClosureTest
"""
import RootHelperFunctions.RootHelperFunctions as rhf
from TopAnalysis.Unfolding.ClosureTest import ClosureTest
import ATLASStyle.AtlasStyle as AS
import numpy as np
import config as c
import ROOT as r
from collections import OrderedDict


class BinOptimizer:

    def __init__(
        self,
        input_mc_tuple,
        truth_level_variable,
        detector_level_variable,
        truth_tuple_name,
        detector_tuple_name,
        inital_binning,
        x_axis_title,
        y_axis_title,
        truth_cuts,
        detector_cuts,
        detector_weight,
        truth_weight,
        output_folder,
        draw_pulls,
        luminosity,
        mc_luminosity,
        verbosity=1,
        n_toys=100,
        max_iterations=30
    ):
        """

        """
        #
        self.current_binning = inital_binning
        self.verbosity = verbosity

        #
        self.input_mc_tuple = input_mc_tuple
        self.truth_level_variable = truth_level_variable
        self.detector_level_variable = detector_level_variable
        self.truth_tuple_name = truth_tuple_name
        self.detector_tuple_name = detector_tuple_name
        self.current_binning = inital_binning
        self.x_axis_title = x_axis_title
        self.y_axis_title = y_axis_title
        self.truth_cuts = truth_cuts
        self.detector_cuts = detector_cuts
        self.detector_weight = detector_weight
        self.truth_weight = truth_weight
        self.draw_pulls = draw_pulls
        self.luminosity = luminosity
        self.mc_luminosity = mc_luminosity
        self.output_folder = output_folder
        self.n_toys = n_toys
        self.max_iterations = max_iterations
        # Create the output folder and root file as necessary
        self.setup_output()

        # Suppress roots annoying messages
        rhf.hide_root_infomessages()

    def log(self, message):
        """
            Create a logger that sm
        """
        if self.verbosity > 0:
            print("[ TA - OPTIMIZE ] - " + str(message))
            self.out_text_file.write(str(message) + "\n")

    def setup_output(self):
        """
            Creates a folder and root file to store the output of this class
        """
        # the folder first
        rhf.create_folder(self.output_folder)

        # the output file
        self.out_file = rhf.open_file(
            self.output_folder + "optimizater.root", "RECREATE")

        # the log file
        self.out_text_file = open(self.output_folder + "log.txt", "w+")

    def close(self):
        """
            Cleanup dangling root objects
        """
        self.out_file.Close()
        self.out_text_file.close()

    def optimize(self):
        """
        """
        converged = False
        iteration = 0
        while converged == False:
            self.log(" Measuring pulls for iteration: " + str(iteration))
            # create the folder to store the results of the pulltest
            current_output_folder = self.output_folder + \
                "ClosureTest_" + str(iteration) + "/"
            rhf.create_folder(current_output_folder)

            # Create a pull test based upon the current binning
            pull_test = ClosureTest(
                name="ClosureTest_" + str(iteration),
                input_mc_tuple=self.input_mc_tuple,
                truth_level_variable=self.truth_level_variable,
                detector_level_variable=self.detector_level_variable,
                truth_tuple_name=self.truth_tuple_name,
                detector_tuple_name=self.detector_tuple_name,
                x_axis_binning=self.current_binning,
                x_axis_title=self.x_axis_title,
                y_axis_title=self.y_axis_title,
                truth_cuts=self.truth_cuts,
                detector_cuts=self.detector_cuts,
                detector_weight=self.detector_weight,
                truth_weight=self.truth_weight,
                draw_pulls=self.draw_pulls,
                luminosity=self.luminosity,
                mc_luminosity=self.mc_luminosity,
                n_toys=self.n_toys

            )
            self.out_file.cd()

            # Run the test to find the pulls
            # this test doesn't actually crae about any of its arugments
            pull_test.run(
                None,  # self.response,
                None,  # self.truth_all_hist,
                None,  # self.reco_all_hist,
                None,  # self.efficiency_plot,
                None,  # self.acceptance_plot,
                "Bayes",  # Method
                None)  # Migration matrix

            # draw this test
            AS.SetAtlasStyle()
            pull_test.draw(output_folder=current_output_folder,
                           x_axis_title=self.x_axis_title)

            # Calculate the new binning based upon the widths and means of the fitted
            # Gaussians to the pull distributions
            pull_width, pull_width_errors = pull_test.pull_rms, pull_test.pull_width_errors,
            pull_mid_points, pull_mean_erros = pull_test.pull_mid_points, pull_test.pull_mean_erros

            #  Evaluate if all widths are within 1 sigma of the errors
            self.log("Examing convergence...")
            converged = True
            for width, error in zip(pull_width, pull_width_errors):
                if(abs(1 - width) > error):
                    converged = False
                    break

            # If we haven't converged then lets recalulate the binning
            if not converged:
                self.current_binning = self.recalculate_binning(
                    pull_width, pull_width_errors)
                iteration += 1

            if iteration >= self.max_iterations:
                self.log(" Unable to converge exiting.")
                break
            self.log("\n\n")

    def recalculate_binning(self, width, errors):
        """

        """
        binning = self.current_binning
        new_binning = [self.current_binning[0]]
        skip_next = False
        for bin in range(0, len(self.current_binning) - 1):
            # if skip_next == True:
                # print "skipping this bin as we need to remove a bin. "
                # skip_next = False
                # continue

            self.log("evaluating bin: " +
                     str(binning[bin]) + "-" + str(binning[bin + 1]))
            self.log("         width: " + str(width[bin]))
            self.log("        errors: " + str(errors[bin]))
            self.log("       width[bin] - errors[bin] = " +
                     str(width[bin] - errors[bin]))

            scaling = 0.9
            if width[bin] - errors[bin] > 1.0 or errors[bin] <= 1e-7:
                self.log("Removing a bin. Current bin is too small!  ")
                continue
                # We think this bin is too small
                # self.log( "Bin too small - needs to be adjusted!")
                # new_binning.append( binning[bin+1]*(1+scaling*(width[bin]-1)))
                # self.log(" Added bin: " + str(new_binning[-1]))

                # Now we need to remove the next bin a long
                # skip_next = True
                # self.log( "Removed this bin." )

            elif width[bin] + errors[bin] < 1.0:
                self.log("Adding additional bins. Current bin is too large!  ")

                # do not allow a scaling that sets the bin lower than the one before it
                # max_scaling = binning[bin+1]/(new_binning[-1]*width[bin]) - 1.0/width[bin]   #Uppor bound
                # min_scaling = binning[bin]/(new_binning[-1]*width[bin])   -
                # 1.0/width[bin]    #Lower bound

                # 1/width[bin] - binning[bin+1]/(width[bin]*new_binning[-1])
                max_scaling = (binning[bin + 1] - new_binning[-1]
                               ) / (width[bin] * binning[bin + 1])
                min_scaling = 0
                # if scaling > max_scaling or scaling < min_scaling:
                scaling = (max_scaling - min_scaling) * 0.75 + min_scaling

                max_bin = binning[bin + 1] * (1 - width[bin] * min_scaling)
                min_bin = binning[bin + 1] * (1 - width[bin] * max_scaling)
                new_bin = binning[bin + 1] * (1 - width[bin] * scaling)

                self.log("  %.3f < Scaling = %.3f < %.3f" %
                         (min_scaling, scaling, max_scaling))
                self.log("  %.3f < bin = %.3f < %.3f" %
                         (min_bin, new_bin, max_bin))
                # We think this bin is too large
                new_binning.append(new_bin)
                self.log("  Added bin: " + str(new_binning[-1]))
                # now we need to add another bin
                new_binning.append(binning[bin + 1])
                self.log("  Added bin: " + str(new_binning[-1]))
                # new_binning.append( binning[bin+2]*(1-width[bin])  )
                # self.log(" Added bin: " + str(new_binning[-1]))

            else:
                # at this point we are happy with this bin
                self.log("  Happy with this bin.")
            self.log("\n\n")

        if new_binning[-1] != binning[-1]:
            new_binning.append(binning[-1])

        self.log(" OLD BINNING: " + str(self.current_binning))
        self.log(" NEW BINNING: " + str(new_binning))
        return new_binning


def main():
    """
        Main entry point for this script. Creates a binning optimizer 
        and calculates the best binning for a set variable
    """
    reco_cuts = ["(nominal.ejets_2015 || nominal.ejets_2016)"]
    truth_cuts = ["(particleLevel.ejets_particle)"]
    from TopAnalysis.Backgrounds import ttbar_samples, ttbar_pdf_samples, ttbar_fastsim_samples, ttbar_debug_samples, ttbar_debug_fastsim_samples
    input_ttbar = ttbar_debug_samples

    optimizer = BinOptimizer(
        input_mc_tuple=input_ttbar,
        truth_level_variable="particleLevel.el_pt[0]/1e3",
        truth_tuple_name="particleLevel",
        detector_level_variable="nominal.el_pt[0]/1e3",
        detector_tuple_name="nominal",
        inital_binning=list(np.linspace(30, 200, 200)),
        x_axis_title="Measured electron p_{T} [GeV]",
        y_axis_title="Lepton electron p_{T} [GeV]",
        truth_cuts=truth_cuts,
        detector_cuts=reco_cuts,
        detector_weight="weight_mc*weight_leptonSF*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup",
        truth_weight="weight_mc",
        draw_pulls=True,
        luminosity=c.luminosity,
        mc_luminosity=c.luminosity,
        output_folder=c.optimization_output_folder
    )
    optimizer.optimize()
    optimizer.close()

if __name__ == "__main__":
    main()
