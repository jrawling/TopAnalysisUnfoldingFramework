"""
    Use an iterative set of pull tests to evaluate the binnig of a variable. 

    Based heavily on the ClosureTest found in TopAnalysis/Unfolding/ClosureTest
"""
from __future__ import print_function 
import RootHelperFunctions.RootHelperFunctions as rhf
from TopAnalysis.Unfolding.ClosureTest import ClosureTest
import ATLASStyle.AtlasStyle as AS
import numpy as np
import config as c
import ROOT as r
from collections import OrderedDict
import sys 

def remove_duplicates_from_list(input_list):
    cleaned_list = []
    for x in input_list:
        if x not in cleaned_list:
            cleaned_list.append(x)
    return cleaned_list 
    

class BinOptimizer:

    def __init__(
        self,
        input_mc_tuple,
        truth_level_variable,
        detector_level_variable,
        truth_tuple_name,
        detector_tuple_name,
        inital_binning,
        x_axis_title,
        y_axis_title,
        truth_cuts,
        detector_cuts,
        detector_weight,
        truth_weight,
        output_folder,
        draw_pulls,
        luminosity,
        mc_luminosity=None,
        verbosity=1,
        n_toys=300,
        max_iterations=30,
        min_n_entries=1e2
        ):
        """

        """
        #
        self.current_binning = inital_binning
        self.verbosity = verbosity
        self.min_n_entries = min_n_entries 
        #
        self.input_mc_tuple = input_mc_tuple
        self.truth_level_variable = truth_level_variable
        self.detector_level_variable = detector_level_variable
        self.truth_tuple_name = truth_tuple_name
        self.detector_tuple_name = detector_tuple_name
        self.current_binning = inital_binning
        self.x_axis_title = x_axis_title
        self.y_axis_title = y_axis_title
        self.truth_cuts = truth_cuts
        self.detector_cuts = detector_cuts
        self.detector_weight = detector_weight
        self.truth_weight = truth_weight
        self.draw_pulls = draw_pulls
        self.luminosity = luminosity
        self.mc_luminosity = mc_luminosity
        self.output_folder = output_folder
        self.n_toys = n_toys
        self.max_iterations = max_iterations
        # Create the output folder and root file as necessary
        self.setup_output()

        # Suppress roots annoying messages
        rhf.hide_root_infomessages()

    def log(self, *msgs):
        """
            Create a logger that sm
        """
        if self.verbosity > 0:
            full_message = "[ TA - OPTIMIZE ] - " 
            for msg in msgs:
                full_message += str(msg)
            print( full_message )
            self.out_text_file.write(str(full_message) + "\n")

    def setup_output(self):
        """
            Creates a folder and root file to store the output of this class
        """
        # the folder first
        rhf.create_folder(self.output_folder)

        # the output file
        self.out_file = rhf.open_file(
            self.output_folder + "optimizater.root", "RECREATE")

        # the log file
        self.out_text_file = open(self.output_folder + "log.txt", "w+")

    def close(self):
        """
            Cleanup dangling root objects
        """
        self.out_file.Close()
        self.out_text_file.close()

    def merge_binning(self, migration_mat,current_reco_hist,iteration, merge_left=True,max_n_merged =1):
        #Iterate over the bins and check that 
        if merge_left: 
            next_binning = [self.current_binning[-1]]
            self.log('Starting binning with ', next_binning)
            b = len(self.current_binning)-1
            n_merged= 0
            while b > 0:
                n_entries = current_reco_hist.GetBinContent(b)
                M_bb =  migration_mat.GetBinContent(b,b)    
                self.log('NEvents %.3g Mbb %.3g '%(n_entries, M_bb))

                # If we satisfy both criteria then append the current bin 
                # and continue to the next one 
                if n_entries >= self.min_n_entries and M_bb > 0.5:
                    self.log('Appending bin: ', self.current_binning[b])
                    next_binning.append(self.current_binning[b])
                    if iteration == 0:
                        print("!!WARNING!!! Bin passed minimum requirements on first iteration, try a coarser initial binning")

                else:
                    b_orig = b 
                    b-=1
                    n_merged= 0
                        
                    # Merge as many bins as required to hit the n_entries 
                    # requirement 
                    n_entries += current_reco_hist.GetBinContent(b)
                    while n_entries < self.min_n_entries and b > 0 and n_merged < max_n_merged:
                        n_merged += 1
                        b-=1 
                        n_entries += current_reco_hist.GetBinContent(b)
                    self.log("Merging bins ",b_orig, " and ", b," ...")
                    self.log('appending bin: ', self.current_binning[b])
                    next_binning.append(self.current_binning[b])

                b-=1 

            next_binning = list(reversed(next_binning))

            # # Shove the last bin on the end as that is fixed in this optimziation
            # # procedure. 
            if next_binning[0] != self.current_binning[0]:
                self.log('appending final bin: ', self.current_binning[0])
                next_binning = [ self.current_binning[0]] + next_binning
            next_binning = remove_duplicates_from_list(next_binning)

        else:  
            next_binning = [self.current_binning[0]]
            self.log('Starting binning with ', next_binning)
            b = 1 
            n_merged= 0
            while b < len(self.current_binning)-1:
                n_entries = current_reco_hist.GetBinContent(b)
                M_bb =  migration_mat.GetBinContent(b,b)    
                self.log('NEvents %.3g Mbb %.3g '%(n_entries, M_bb))

                # If we satisfy both criteria then append the current bin 
                # and continue to the next one 
                if n_entries >= self.min_n_entries and M_bb > 0.5:
                    self.log('appending bin: ', self.current_binning[b])
                    next_binning.append(self.current_binning[b])
                    n_merged= 0
                    if iteration == 0:
                        print("!!WARNING!!! Bin passed minimum requirements on first iteration, try a coarser initial binning")
                else:
                    b_orig = b 
                    b+=1
                        
                    # Merge as many bins as required to hit the n_entries 
                    # requirement 
                    n_entries += current_reco_hist.GetBinContent(b)
                    self.log('     Adding bin with entries: ', current_reco_hist.GetBinContent(b))
                    n_merged= 0
                    while n_entries < self.min_n_entries and b < len(self.current_binning)-1 and n_merged < max_n_merged:
                        b+=1 
                        n_merged += 1
                        self.log('     Adding bin with entries: ', current_reco_hist.GetBinContent(b))
                        n_entries += current_reco_hist.GetBinContent(b)
                    self.log("Merging bins ",b_orig, " and ", b," ...")
                    self.log('appending bin: ', self.current_binning[b])
                    next_binning.append(self.current_binning[b])

                b+=1 

            # # Shove the last bin on the end as that is fixed in this optimziation
            # # procedure. 
            if next_binning[-1] != self.current_binning[-1]:
                self.log('appending final bin: ', self.current_binning[-1])
                next_binning = next_binning +  [ self.current_binning[-1]]  
            next_binning = remove_duplicates_from_list(next_binning)
        return next_binning

    def optimize(self):
        """
        """
        converged = False
        iteration = 0
        self.merge_left = False 
        switched_direction = False 
        max_n_merged= 1
        while not converged:
            self.log(" Measuring pulls for iteration: " + str(iteration))
            # create the folder to store the results of the pulltest
            current_output_folder = self.output_folder + \
                "ClosureTest_" + str(iteration) + "/"
            rhf.create_folder(current_output_folder)

            # Create a pull test based upon the current binning
            pull_test = ClosureTest(
                name="ClosureTest_" + str(iteration),
                input_mc_tuple=self.input_mc_tuple,
                truth_level_variable=self.truth_level_variable,
                detector_level_variable=self.detector_level_variable,
                truth_tuple_name=self.truth_tuple_name,
                detector_tuple_name=self.detector_tuple_name,
                x_axis_binning=self.current_binning,
                x_axis_title=self.x_axis_title,
                y_axis_title=self.y_axis_title,
                truth_cuts=self.truth_cuts,
                detector_cuts=self.detector_cuts,
                detector_weight=self.detector_weight,
                truth_weight=self.truth_weight,
                draw_pulls=self.draw_pulls,
                luminosity=self.luminosity,
                mc_luminosity=self.mc_luminosity,
                n_toys=self.n_toys
            )
            self.out_file.cd()

            # Run the test to find the pulls
            # this test doesn't actually crae about any of its arugments
            pull_test.run(
                None,     # self.response,
                None,     # self.truth_all_hist,
                None,     # self.reco_all_hist,
                None,     # self.efficiency_plot,
                None,     # self.acceptance_plot,
                "Bayes",  # Method
                None)     # Migration matrix

            # draw this test
            AS.SetAtlasStyle()
            pull_test.draw(output_folder=current_output_folder,
                           x_axis_title=self.x_axis_title)

            # Calculate the new binning based upon the widths and means of the fitted
            # Gaussians to the pull distributions
            pull_width, pull_width_errors = pull_test.pull_rms, pull_test.pull_width_errors,
            pull_mid_points, pull_mean_erros = pull_test.pull_mid_points, pull_test.pull_mean_erros

            #  Evaluate if all widths are within 1 sigma of the errors
            current_reco_hist = pull_test.test_detector_hists[0].Clone()
            migration_mat = rhf.normalize_migration_matrix(pull_test.total_migration_matrix.Clone()) 

            next_binning = self.merge_binning(migration_mat, current_reco_hist,iteration,
                                              merge_left=self.merge_left,
                                              max_n_merged=max_n_merged
                                              )
            # After the initial set of merging we switch directions and reduce 
            # the allowed number of merging per bin in one iteration. This is
            # useful to do as bins are larger after each pass, so the merging 3
            # on iteration 10 corresponds to a much larger leap than on iteration
            # 1 
            # self.merge_left = not self.merge_left
            # max_n_merged = 2

            self.log("Previous binning (",len(self.current_binning),"): ",self.current_binning)
            self.log("New binning (",len(next_binning),"):  ", next_binning,'\n')

            # Check if we've hit our exit condition 
            if next_binning == self.current_binning:
                if not switched_direction:
                    self.log('Converged on abinning scheme, verifying no issues with last bin')
                    self.merge_left = not self.merge_left
                    switched_direction = True
                else:
                    self.log('Converged on final binning.')
                    converged = True

            # If not iterate onward
            self.current_binning = next_binning  
            iteration += 1 

        self.log("Converged to binning: ", self.current_binning)

def get_unfolder_cuts(cut):
    reco_cuts  = ["(nominal.ejets_2015 || nominal.ejets_2016 || nominal.mujets_2015 || nominal.mujets_2016)"]
    truth_cuts = ["(particleLevel.ejets_particle || particleLevel.mujets_particle )"]
    if "control_region" in cut:
        reco_cuts.append("(Length$(nominal.jet_pt)==5)")
        reco_cuts.append("(nominal.g_pt[0]/1e3 >  25)")
        reco_cuts.append("(nominal.g_pt[0]/nominal.top_lep_pt > 0.05)")
        reco_cuts.append("(nominal.top_lep_pt/1000 < 192)")
        reco_cuts.append("(nominal.top_lep_pt/1000 > 40)")
        reco_cuts.append("(nominal.theta < nominal.theta_had)")
        reco_cuts.append("(nominal.top_lep_m/1e3 > 155  && nominal.top_lep_m/1e3 < 205)")

        truth_cuts.append("(Length$(particleLevel.jet_pt)==5)")
        truth_cuts.append("(particleLevel.g_pt[0]/1e3 > 25)")
        truth_cuts.append("(particleLevel.g_pt[0]/particleLevel.top_lep_pt > 0.05)")
        truth_cuts.append("(particleLevel.top_lep_pt/1000 < 192)")
        truth_cuts.append("(particleLevel.top_lep_pt/1000 > 40)")
        truth_cuts.append("(particleLevel.top_lep_m/1e3 > 155 && particleLevel.top_lep_m/1e3 < 205)")
        truth_cuts.append("(particleLevel.theta < particleLevel.theta_had)")

    elif  "signal_region" in cut:
        reco_cuts.append("(Length$(nominal.jet_pt)==5)")
        reco_cuts.append("(nominal.g_pt[0]/1e3 >  30)")
        reco_cuts.append("(nominal.g_pt[0]/nominal.top_lep_pt > 0.05)")
        reco_cuts.append("(nominal.top_lep_pt/1000 > 130)")
        reco_cuts.append("(nominal.top_lep_m/1e3 > 150  && nominal.top_lep_m/1e3 < 300)")
        reco_cuts.append("(nominal.theta < nominal.theta_had)")

        truth_cuts.append("(Length$(particleLevel.jet_pt)==5)")
        truth_cuts.append("(particleLevel.g_pt[0]/1e3 > 30)")
        truth_cuts.append("(particleLevel.g_pt[0]/particleLevel.top_lep_pt > 0.05)")
        truth_cuts.append("(particleLevel.top_lep_pt/1000 > 130)")
        truth_cuts.append("(particleLevel.top_lep_m/1e3 > 150 && particleLevel.top_lep_m/1e3 < 300)")
        truth_cuts.append("(particleLevel.theta < particleLevel.theta_had)")

    return reco_cuts, truth_cuts

def main():
    """
        Main entry point for this script. Creates a binning optimizer 
        and calculates the best binning for a set variable
    """
    # Read in the unfolding region
    unfolder_names = None
    if len(sys.argv) >= 1:
        unfolder_names = sys.argv[1]

    # Read in auxillary settings such as if we are running on AFII or if we're
    # doing  
    if len(sys.argv) >= 2:
        aux_settings = ' '.join(sys.argv[2:])
    else:
        aux_settings = ""

    debug_mode = 'debug' in aux_settings
    reco_cuts, truth_cuts = get_unfolder_cuts(unfolder_names,)
    out_folder_name = "/optimized_binning_" + unfolder_names + "/"

    # Load the unfolder from the configurations we have pre-defined 
    from TopAnalysis.Backgrounds import ttbar_samples, ttbar_pdf_samples, ttbar_fastsim_samples, ttbar_debug_samples, ttbar_debug_fastsim_samples
    if debug_mode:
        print('Running optimziation in debug mode! ')
        input_ttbar = ttbar_debug_samples
        out_folder_name = out_folder_name[:-1] + "_debug/"
    else:
        input_ttbar = ttbar_samples

    print('Saving output to: ', c.optimization_output_folder+out_folder_name)
    optimizer = BinOptimizer(
        input_mc_tuple=input_ttbar,
        truth_level_variable="(particleLevel.theta[0]-particleLevel.theta_d)/(particleLevel.theta[0]+particleLevel.theta_d)",
        truth_tuple_name="particleLevel",
        detector_level_variable="(nominal.theta[0]-nominal.theta_d)/(nominal.theta[0]+nominal.theta_d)",    
        detector_tuple_name="nominal",
        inital_binning=list(np.linspace(-1, 1, 90)),
        x_axis_title="Measured Leptonic A_{#theta}",
        y_axis_title="Leptonic A_{#theta}",
        truth_cuts=truth_cuts,
        detector_cuts=reco_cuts,
        detector_weight="weight_mc*weight_leptonSF*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup",
        truth_weight="weight_mc",
        draw_pulls=False,
        luminosity=c.luminosity,
        output_folder=c.optimization_output_folder+out_folder_name
    )

    optimizer.optimize()
    optimizer.close()

if __name__ == "__main__":

    # optimizer = BinOptimizer(
    #     input_mc_tuple=input_ttbar,
    #     truth_level_variable="particleLevel.el_pt[0]/1e3",
    #     truth_tuple_name="particleLevel",
    #     detector_level_variable="nominal.el_pt[0]/1e3",
    #     detector_tuple_name="nominal",
    #     inital_binning=list(np.linspace(30, 200, 500)),
    #     x_axis_title="Measured electron p_{T} [GeV]",
    #     y_axis_title="Lepton electron p_{T} [GeV]",
    #     truth_cuts=truth_cuts,
    #     detector_cuts=reco_cuts,
    #     detector_weight="weight_mc*weight_leptonSF*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup",
    #     truth_weight="weight_mc",
    #     draw_pulls=True,
    #     luminosity=c.luminosity,
    #     mc_luminosity=c.luminosity,
    #     output_folder=c.optimization_output_folder+"/el_pt_binning_opts_7/"
    # )
    main()
