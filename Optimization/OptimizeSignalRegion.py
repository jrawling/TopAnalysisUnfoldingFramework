"""Example of how to use this bayesian optimization package."""

import sys
from bayes_opt import BayesianOptimization
from Unfold import unfolder_wrapper#
import config as c
import pickle 
sys.path.append("./")
import cloudpickle
import numpy as np

t = unfolder_wrapper(cuts=sys.argv[1])

# Lets find the maximum of a simple quadratic function of two variables
# We create the bayes_opt object and pass the function to be maximized
# together with the parameters names and their bounds.
save_path = c.optimization_output_folder+sys.argv[1]+"_bayes_opt.pkl"
if not np.DataSource().exists(save_path):
    bo = BayesianOptimization(lambda x, y, z, g_pt_cut, max_theta_d: t(top_pt=x,
                                                min_top_mass=y,
                                                top_window_width=z,
                                                g_pt_cut=g_pt_cut,
                                                max_theta_d=max_theta_d
                                                ),
                        {
                        'x': (100, 250),  # minimum top_pt
                        'y': (150, 170),  # minimum top_mass
                        'z': (35, 100),     # width of top_mass window
                        'g_pt_cut': (25,40),
                        'max_theta_d': (0.65,1.0)
                        })

    # One of the things we can do with this object is pass points
    # which we want the algorithm to probe. A dictionary with the
    # parameters names and a list of values to include in the search
    # must be given.
    # bo.explore({'x': [150, 200,100], 'y': [165, 155,170], 'z': (35, 50,100), 'g_pt_cut': (25,40,25), 'max_theta_d': (0.65,1.0,0.75) })
    bo.explore({'x': [150], 'y': [165], 'z': [40], 'g_pt_cut': [30], 'max_theta_d': [0.75] })
    
    output = open(save_path, 'wb')
    blob = cloudpickle.dump(bo, output, protocol=2)
    output.close()

    # Once we are satisfied with the initialization conditions
    # we let the algorithm do its magic by calling the maximize()
    # method.
    bo.maximize(init_points=0, n_iter=0, kappa=2)

else:
    print "LOADING BayesianOptimization TOOL: ", save_path
    print "       REGION ", sys.argv[1]
    output = open( save_path, 'rb')
    bo = pickle.load(output)
    output.close()

    if not isinstance(bo, BayesianOptimization):
        print "ERROR: Failed loading tool"
        sys.exit(-1)

    bo.maximize(init_points=0, n_iter=1, kappa=2)

# Finally, we take a look at the final results.
print(bo.res['max'])
print(bo.res['all'])

print("Optimal settings: ")
print(" top_pt:           ", bo.res['max']['max_params']['x'])
print(" min_top_mass:     ", bo.res['max']['max_params']['y'])
print(" top_window_width: ", bo.res['max']['max_params']['z'])
print(" g_pt_min: ", bo.res['max']['max_params']['g_pt_cut'])
print(" max_theta_d: ", bo.res['max']['max_params']['max_theta_d'])

output = open(c.optimization_output_folder+"bayes_opt.pkl", 'wb')
blob = cloudpickle.dump(bo, output, protocol=2)
output.close()

