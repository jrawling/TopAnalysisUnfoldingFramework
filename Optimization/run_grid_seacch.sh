#setup the environment
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
lsetup root

export PATH=/afs/cern.ch/user/j/jrawling/.local/bin:$PATH
export PYTHONPATH=/afs/cern.ch/user/j/jrawling/workDir/DeadConeAnalysis/TopAnalysisUnfolding:/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.10.04-x86_64-slc6-gcc62-opt/lib
cd /afs/cern.ch/user/j/jrawling/workDir/DeadConeAnalysis/TopAnalysisUnfolding/Optimization
source optenv/bin/activate 

echo "PYTHONPATH: ", $PYTHONPATH
echo "region":, $MIN_TOP_PT
echo "region":, $TOP_MASS_WIDTH
echo "region":, $MIN_G_PT
echo "region":, $REGION

### run the command
python /afs/cern.ch/user/j/jrawling/workDir/DeadConeAnalysis/TopAnalysisUnfolding/Optimization/Unfold.py $REGION $MIN_TOP_PT $TOP_MASS_WIDTH $MIN_G_PT