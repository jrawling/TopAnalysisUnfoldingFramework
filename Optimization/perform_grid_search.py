
"""
A script to run the optimization that finds a signal region with maximal shpae difference between signal and background hypotheses 
It is convinient to have a python script to initiatie the jobs themselves 

"""
from __future__ import print_function 
import os
import sys
from bayes_opt import BayesianOptimization
from subprocess import call,Popen
import subprocess
import config as c
import pickle 
import numpy as np
import cloudpickle
import time

def convert_sub_output_to_id(sub_output):
    job_id = sub_output.split('<')[1].split('>')[0]
    return int(job_id)


def job_running(id):
    output = subprocess.check_output('bjobs',shell=True)
    lines = output.split('\n')
    if lines == ['']:
        return False 
    job_ids = [ l.split(' ')[0] for l in lines[1:-1]]
    job_ids = [int(j) for j in job_ids]
    return id in job_ids


output_folder = "/afs/cern.ch/user/j/jrawling/workDir/DeadConeAnalysis/TopAnalysisUnfolding/logs/optimization/"
INPUT_PATH = os.environ["TOP_ANALYSIS_INPUT_PATH"]
# Run for a period of burn in 
region = sys.argv[1]
mass_width_bins = np.linspace(50,150,5)
min_pt_bins = np.linspace(100,200, 11)
min_g_pt_bins = np.linspace(25,45, 5)

i = 0
for width in mass_width_bins:
    for pt in min_pt_bins:
        for g_pt in min_g_pt_bins:
            sub_output = subprocess.check_output('bsub -q 8nh -J bayes_opt_'+region +'_'+str(i)+' -o "'+
                        output_folder+'iteraiton_'+region+'_'+str(i)+'.log" -env "INPUT_PATH= '+INPUT_PATH+', REGION='+
                        region+', MIN_TOP_PT=%.3f, TOP_MASS_WIDTH=%.3f, MIN_G_PT=%.3f'%(pt,width,g_pt) + 
                        ', all"<' + INPUT_PATH + '/Optimization/run_grid_seacch.sh', shell=True)
            i+=1
            job_id = convert_sub_output_to_id(sub_output)
            print("Submitted iteration ", i)
            print("     Region: ", region)
            print("     job_id: ", job_id)
        