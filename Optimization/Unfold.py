'''
    Author: Jacob Rawling
    Date: 

    Utilizes the TopAnalysis.Unfolding package ( ) to create the migration matrix, 
    perform the desired type of unfolding, run some tests, save the ouptut in ROOT 
    and Journal ready eps fomrat 
'''
from TopAnalysis.Unfolding.Unfolder import Unfolder
from TopAnalysis.Unfolding.Systematic import Systematic
from TopAnalysis.SystematicsManager import SystematicsManager
from TopAnalysis.Backgrounds import ttbar_samples, ttbar_fastsim_samples
import numpy as np
import config as c
import ROOT as r
from collections import OrderedDict
import RootHelperFunctions.RootHelperFunctions as rhf
from TopAnalysis.SystematicsManager.all_systs import combinations
from SImpleBinOptimizer import BinOptimizer
import sys

class unfolder_wrapper:
    def __init__(self, cuts):
        """
            Returns an array of unfolders (defined in TopAnalysis), shoved away into a function to keep
            main() clean and readable.
        """
        self.cuts = cuts

    def load_unfolders(self, name, top_pt=200, min_top_mass=160, top_window_width=180, max_theta_d=0.76,
                       g_pt_cut=30, debug_mode=True):

        truth_weight, detector_weight = None, None
        input_mc = c.input_mc_tuple
        from TopAnalysis.Backgrounds import ttbar_samples, ttbar_pdf_samples, ttbar_fastsim_samples,ttbar_debug_samples, ttbar_debug_fastsim_samples
        input_ttbar = ttbar_samples
        additional_samples = c.additional_samples 
        np_background_samples, background_samples=[], []

        reco_cuts  = ["(nominal.ejets_2015 || nominal.ejets_2016 || nominal.mujets_2015 || nominal.mujets_2016)"]
        reco_cuts.append("(Length$(nominal.jet_pt)==5)")
        # reco_cuts.append("(nominal.theta_d > 0.4)")
        reco_cuts.append("(nominal.theta < nominal.theta_had)")
        reco_cuts.append("(nominal.g_pt[0]/1e3 > "+str(g_pt_cut)+")")
        reco_cuts.append("(nominal.g_pt[0]/nominal.top_lep_pt > 0.05)")
        reco_cuts.append("(nominal.top_lep_pt/1000 > "+str(top_pt)+")")   
        reco_cuts.append("(nominal.theta_d < "+str(max_theta_d)+")")

        # NOTE: SEEMS THAT MASS CONTSTRAINTS DON'T HELP TOO MUCH
        reco_cuts.append("(nominal.top_lep_m/1e3 > "+str(min_top_mass)+")*(nominal.top_lep_m/1e3 < "+str(min_top_mass+top_window_width)+")")

        truth_cuts = ["(particleLevel.mujets_particle || particleLevel.ejets_particle)"]
        truth_cuts.append("(Length$(particleLevel.jet_pt)==5)")
        truth_cuts.append("(particleLevel.g_pt[0]/particleLevel.top_lep_pt > 0.05)")

        truth_cuts.append("(particleLevel.g_pt[0]/1e3 > "+str(g_pt_cut)+")")
        truth_cuts.append("(particleLevel.top_lep_pt/1000 > "+str(top_pt)+")")   
        truth_cuts.append("(particleLevel.theta_d < "+str(max_theta_d)+")")
        truth_cuts.append("(particleLevel.theta < particleLevel.theta_had)")
        truth_cuts.append("(particleLevel.top_lep_m/1e3 > "+str(min_top_mass)+")*(particleLevel.top_lep_m/1e3 < "+str(min_top_mass+top_window_width)+")")

        return Unfolder(
                    ttbar_samples,        #MC that we base the unfolding on
                    ttbar_samples.input_file_names,            # data tuple that we want to unfold  - pass the MC since we don't want to optimize based upon data! 
                    c.optimization_output_folder,         #Where all ROOT  and  eps will be put 
                    "(particleLevel.theta[0]-particleLevel.theta_d)/(particleLevel.theta[0]+particleLevel.theta_d)",                 #Truth level variable name that will be unfolded to
                    "(nominal.theta[0]-nominal.theta_d)/(nominal.theta[0]+nominal.theta_d)",                 #Detector level variable name that will be unfolded from
                    [-1,-0.25,0.0,0.25,0.5,0.7,1.0],#,10,15],
                    method = "bayes",        # we're going to use iterative bayesian 
                    truth_tuple_name = "particleLevel",
                    detector_tuple_name = "nominal",
                    data_tuple_is_mc = True, #say that we are in test mode and therefiore the data tuple is infact MC 
                    save_output = True,       # we want everything to be shoved to a ROOT file 
                    x_axis_label = "Measured A_{#theta}",  
                    y_axis_label = "Truth A_{#theta}",
                    unfolded_x_axis_title = "Combined A_{#theta}",
                    unfolded_y_axis_title = "#frac{1}{#sigma_{0}} #frac{d#sigma}{dA_{#theta}}",
                    messages = ["Fiducial phase space", "Signal Region"],
                    truth_cuts = truth_cuts,
                    detector_cuts = reco_cuts,
                    luminosity=c.luminosity,
                    name = name,
                    n_iterations = 8,
                    set_log_y = True,
                    do_bootstrap=False,
                    signal_fastsim_sample=ttbar_fastsim_samples
                   )

    def load_signal_region(self, name,  top_pt=150, min_top_mass=160, top_window_width=80, max_theta_d=0.76,
                       g_pt_cut=30, debug_mode=True):

        truth_weight, detector_weight = None, None
        input_mc = c.input_mc_tuple
        from TopAnalysis.Backgrounds import ttbar_samples, ttbar_pdf_samples, ttbar_fastsim_samples,ttbar_debug_samples, ttbar_debug_fastsim_samples
        input_ttbar = ttbar_samples
        additional_samples = c.additional_samples 
        np_background_samples, background_samples=[], []

        reco_cuts  = ["(nominal.ejets_2015 || nominal.ejets_2016 || nominal.mujets_2015 || nominal.mujets_2016)"]
        reco_cuts.append("(Length$(nominal.jet_pt)==5)")
        reco_cuts.append("(nominal.theta < nominal.theta_had)")
        reco_cuts.append("(nominal.g_pt[0]/1e3 > "+str(g_pt_cut)+")")
        reco_cuts.append("(nominal.g_pt[0]/nominal.top_lep_pt > 0.05)")
        reco_cuts.append("(nominal.top_lep_pt/1000 > "+str(top_pt)+")")   
        reco_cuts.append("(nominal.theta_d < "+str(max_theta_d)+")")

        # NOTE: SEEMS THAT MASS CONTSTRAINTS DON'T HELP TOO MUCH
        reco_cuts.append("(nominal.top_lep_m/1e3 > "+str(min_top_mass)+")*(nominal.top_lep_m/1e3 < "+str(min_top_mass+top_window_width)+")")


        truth_cuts = ["(particleLevel.mujets_particle || particleLevel.ejets_particle)"]
        truth_cuts.append("(Length$(particleLevel.jet_pt)==5)")
        truth_cuts.append("(particleLevel.g_pt[0]/particleLevel.top_lep_pt > 0.05)")
        truth_cuts.append("(particleLevel.g_pt[0]/1e3 > "+str(g_pt_cut)+")")
        truth_cuts.append("(particleLevel.top_lep_pt/1000 > "+str(top_pt)+")")   
        truth_cuts.append("(particleLevel.theta_d < "+str(max_theta_d)+")")
        truth_cuts.append("(particleLevel.theta < particleLevel.theta_had)")

        if debug_mode:
            input_ttbar = ttbar_debug_samples
            ttbar_fastsim_samples = ttbar_debug_fastsim_samples
            additional_samples = c.additional_debug_samples 


        return Unfolder(
                    input_ttbar,        #MC that we base the unfolding on
                    input_ttbar.input_file_names,    # data tuple that we want to unfold  - pass the MC since we don't want to optimize based upon data! 
                    c.optimization_output_folder,    # Where all ROOT  and  eps will be put 
                    "(particleLevel.theta[0]-particleLevel.theta_d)/(particleLevel.theta[0]+particleLevel.theta_d)",                 #Truth level variable name that will be unfolded to
                    "(nominal.theta[0]-nominal.theta_d)/(nominal.theta[0]+nominal.theta_d)",                 #Detector level variable name that will be unfolded from
                    [-1,-0.25,0.0,0.25,0.5,0.7,1.0],
                    method="bayes",        # we're going to use iterative bayesian
                    truth_tuple_name = "particleLevel",
                    detector_tuple_name = "nominal",
                    data_tuple_is_mc = True, # say that we are in test mode and therefiore the data tuple is infact MC
                    save_output = True,       # we want everything to be shoved to a ROOT file
                    x_axis_label="Measured A_{#theta}",
                    y_axis_label="Truth A_{#theta}",
                    unfolded_x_axis_title = "Leptonic A_{#theta}",
                    unfolded_y_axis_title = "#frac{1}{#sigma_{0}} #frac{d#sigma}{dA_{#theta}}",
                    messages=["Fiducial phase space", "Signal region"],
                    truth_cuts=truth_cuts,
                    detector_cuts=reco_cuts,
                    name=name,
                    n_iterations=8,
                    set_log_y=True,
                    verbosity=3,
                    load_unfolder=None,
                    additional_samples=additional_samples,
                    background_samples=background_samples,
                    luminosity=c.luminosity,
                    truth_weight=truth_weight,
                    detector_weight=detector_weight,
                    np_background_samples=np_background_samples,
                    do_bootstrap=False,
                    signal_fastsim_sample=ttbar_fastsim_samples
                   )



    def load_hadronic_signal_region(self, name , top_pt=200, min_top_mass=160, top_window_width=180, max_theta_d=0.76,
                       g_pt_cut=30,debug_mode = True):

        truth_weight, detector_weight = None, None
        input_mc = c.input_mc_tuple
        from TopAnalysis.Backgrounds import ttbar_samples, ttbar_pdf_samples, ttbar_fastsim_samples,ttbar_debug_samples, ttbar_debug_fastsim_samples
        input_ttbar = ttbar_samples
        additional_samples = c.additional_samples 
        np_background_samples, background_samples=[], []
            
        reco_cuts  = ["(nominal.ejets_2015 || nominal.ejets_2016 || nominal.mujets_2015 || nominal.mujets_2016)"]
        reco_cuts.append("(Length$(nominal.jet_pt)==5)")
        reco_cuts.append("(nominal.theta_d_had < "+str(max_theta_d)+")")
        reco_cuts.append("(nominal.theta > nominal.theta_had)")
        reco_cuts.append("(nominal.g_pt[0]/1e3 > "+str(g_pt_cut)+")")
        reco_cuts.append("(nominal.g_pt[0]/nominal.top_had_pt > 0.05)")
        reco_cuts.append("(nominal.top_had_pt/1000 > "+str(top_pt)+")")   
        reco_cuts.append("(nominal.top_had_m/1e3 > "+str(min_top_mass)+")*(nominal.top_had_m/1e3 < "+str(min_top_mass+top_window_width)+")")

        truth_cuts = ["(particleLevel.mujets_particle || particleLevel.ejets_particle)"]
        truth_cuts.append("(Length$(particleLevel.jet_pt)==5)")
        truth_cuts.append("(particleLevel.g_pt[0]/particleLevel.top_had_pt > 0.05)")
        truth_cuts.append("(particleLevel.theta_d_had < "+str(max_theta_d)+")")
        truth_cuts.append("(particleLevel.g_pt[0]/1e3 > "+str(g_pt_cut)+")")
        truth_cuts.append("(particleLevel.top_had_pt/1000 > "+str(top_pt)+")")   
        truth_cuts.append("(particleLevel.theta > particleLevel.theta_had)")
        truth_cuts.append("(particleLevel.top_had_m/1e3 > "+str(min_top_mass)+")*(particleLevel.top_had_m/1e3 < "+str(min_top_mass+top_window_width)+")")

        if debug_mode:
            input_ttbar = ttbar_debug_samples
            ttbar_fastsim_samples = ttbar_debug_fastsim_samples
            additional_samples = c.additional_debug_samples 


        return Unfolder(
                    ttbar_samples,        #MC that we base the unfolding on
                    ttbar_samples.input_file_names,            # data tuple that we want to unfold  - pass the MC since we don't want to optimize based upon data! 
                    c.optimization_output_folder,         #Where all ROOT  and  eps will be put 
                    "(particleLevel.theta_had[0]-particleLevel.theta_d_had)/(particleLevel.theta_had[0]+particleLevel.theta_d_had)",                 #Truth level variable name that will be unfolded to
                    "(nominal.theta_had[0]-nominal.theta_d_had)/(nominal.theta_had[0]+nominal.theta_d_had)",                 #Detector level variable name that will be unfolded from
                    [-1,-0.25,0.0,0.25,0.5,0.7,1.0],#,10,15],
                    method = "bayes",        # we're going to use iterative bayesian 
                    truth_tuple_name = "particleLevel",
                    detector_tuple_name = "nominal",
                    data_tuple_is_mc = True, #say that we are in test mode and therefiore the data tuple is infact MC 
                    save_output = True,       # we want everything to be shoved to a ROOT file 
                    x_axis_label = "Measured A_{#theta}",  
                    y_axis_label = "Truth A_{#theta}",
                    unfolded_x_axis_title = "Hadornic A_{#theta}",
                    unfolded_y_axis_title = "#frac{1}{#sigma_{0}} #frac{d#sigma}{dA_{#theta}}",
                    messages=["Fiducial phase space", "Signal region"],
                    truth_cuts=truth_cuts,
                    detector_cuts=reco_cuts,
                    name=name,
                    n_iterations=8,
                    set_log_y=True,
                    verbosity=3,
                    load_unfolder=None,
                    additional_samples=additional_samples,
                    background_samples=background_samples,
                    luminosity=c.luminosity,
                    truth_weight=truth_weight,
                    detector_weight=detector_weight,
                    np_background_samples=np_background_samples,
                    do_bootstrap=False,
                    signal_fastsim_sample=ttbar_fastsim_samples 
                   )

    def get_unfolder_cuts(self, cut, top_pt=200, min_top_mass=160, top_window_width=180, max_theta_d=0.76,
                       g_pt_cut=30): 
        reco_cuts  = ["(nominal.ejets_2015 || nominal.ejets_2016 || nominal.mujets_2015 || nominal.mujets_2016)"]
        truth_cuts = ["(particleLevel.ejets_particle || particleLevel.mujets_particle )"]

        if "had_control_region" in cut:
            reco_cuts.append("(Length$(nominal.jet_pt)==5)")
            reco_cuts.append("(nominal.g_pt[0]/1e3 > "+str(g_pt_cut)+")")
            reco_cuts.append("(nominal.g_pt[0]/nominal.top_had_pt > 0.05)")
            reco_cuts.append("(nominal.top_had_pt/1000 < "+str(top_pt)+")")
            reco_cuts.append("(nominal.theta > nominal.theta_had)")
            reco_cuts.append("(nominal.top_had_m/1e3 > "+ str(min_top_mass) +"  && nominal.top_had_m/1e3 < "+str(min_top_mass+top_window_width)+")")

            truth_cuts.append("(Length$(particleLevel.jet_pt)==5)")
            truth_cuts.append("(particleLevel.g_pt[0]/1e3 > "+str(g_pt_cut)+")")
            truth_cuts.append("(particleLevel.g_pt[0]/particleLevel.top_had_pt > 0.05)")
            truth_cuts.append("(particleLevel.top_had_pt/1000 < "+str(top_pt)+")")
            truth_cuts.append("(particleLevel.top_had_m/1e3 > "+ str(min_top_mass) +"  && particleLevel.top_had_m/1e3 <"+str(min_top_mass+top_window_width)+")")
            truth_cuts.append("(particleLevel.theta > particleLevel.theta_had)")
        elif "had_signal_region" in cut:
            reco_cuts.append("(Length$(nominal.jet_pt)==5)")
            reco_cuts.append("(nominal.g_pt[0]/1e3 > "+str(g_pt_cut)+")")
            reco_cuts.append("(nominal.g_pt[0]/nominal.top_had_pt > 0.05)")
            reco_cuts.append("(nominal.top_had_pt/1000 > "+str(top_pt)+")")
            reco_cuts.append("(nominal.theta > nominal.theta_had)")
            reco_cuts.append("(nominal.top_had_m/1e3 > "+ str(min_top_mass) +"  && nominal.top_had_m/1e3 < "+str(min_top_mass+top_window_width)+")")

            truth_cuts.append("(Length$(particleLevel.jet_pt)==5)")
            truth_cuts.append("(particleLevel.g_pt[0]/1e3 > "+str(g_pt_cut)+")")
            truth_cuts.append("(particleLevel.g_pt[0]/particleLevel.top_had_pt > 0.05)")
            truth_cuts.append("(particleLevel.top_had_pt/1000 > "+str(top_pt)+")")
            truth_cuts.append("(particleLevel.top_had_m/1e3 > "+ str(min_top_mass) +"  && particleLevel.top_had_m/1e3 <"+str(min_top_mass+top_window_width)+")")
            truth_cuts.append("(particleLevel.theta > particleLevel.theta_had)")
        elif "control_region" in cut:
            reco_cuts.append("(Length$(nominal.jet_pt)==5)")
            reco_cuts.append("(nominal.g_pt[0]/1e3 > "+str(g_pt_cut)+")")
            reco_cuts.append("(nominal.g_pt[0]/nominal.top_lep_pt > 0.05)")
            reco_cuts.append("(nominal.top_lep_pt/1000 < "+str(top_pt)+")")
            reco_cuts.append("(nominal.theta < nominal.theta_had)")
            reco_cuts.append("(nominal.top_lep_m/1e3 > "+ str(min_top_mass) +"  && nominal.top_lep_m/1e3 < "+str(min_top_mass+top_window_width)+")")

            truth_cuts.append("(Length$(particleLevel.jet_pt)==5)")
            truth_cuts.append("(particleLevel.g_pt[0]/1e3 > "+str(g_pt_cut)+")")
            truth_cuts.append("(particleLevel.g_pt[0]/particleLevel.top_lep_pt > 0.05)")
            truth_cuts.append("(particleLevel.top_lep_pt/1000 < "+str(top_pt)+")")
            truth_cuts.append("(particleLevel.top_lep_m/1e3 > "+ str(min_top_mass) +"  && particleLevel.top_lep_m/1e3 <"+str(min_top_mass+top_window_width)+")")
            truth_cuts.append("(particleLevel.theta < particleLevel.theta_had)")
        elif  "signal_region" in cut:
            reco_cuts.append("(Length$(nominal.jet_pt)==5)")
            reco_cuts.append("(nominal.g_pt[0]/1e3 > "+str(g_pt_cut)+")")
            reco_cuts.append("(nominal.g_pt[0]/nominal.top_lep_pt > 0.05)")
            reco_cuts.append("(nominal.top_lep_pt/1000 > "+str(top_pt)+")")
            reco_cuts.append("(nominal.theta < nominal.theta_had)")
            reco_cuts.append("(nominal.top_lep_m/1e3 > "+ str(min_top_mass) +"  && nominal.top_lep_m/1e3 < "+str(min_top_mass+top_window_width)+")")
            truth_cuts.append("(Length$(particleLevel.jet_pt)==5)")
            truth_cuts.append("(particleLevel.g_pt[0]/1e3 > "+str(g_pt_cut)+")")
            truth_cuts.append("(particleLevel.g_pt[0]/particleLevel.top_lep_pt > 0.05)")
            truth_cuts.append("(particleLevel.top_lep_pt/1000 > "+str(top_pt)+")")
            truth_cuts.append("(particleLevel.top_lep_m/1e3 > "+ str(min_top_mass) +"  && particleLevel.top_lep_m/1e3 <"+str(min_top_mass+top_window_width)+")")
            truth_cuts.append("(particleLevel.theta < particleLevel.theta_had)")
        return reco_cuts, truth_cuts


    def get_bin_optimizer(self, name, cuts="signal_region", top_pt=200, min_top_mass=160, top_window_width=180, max_theta_d=0.76,
                       g_pt_cut=30): 
        reco_cuts, truth_cuts = self.get_unfolder_cuts(cuts,
                                                    top_pt,
                                                    min_top_mass,
                                                    top_window_width,
                                                    max_theta_d,
                                                    g_pt_cut )
        from TopAnalysis.Backgrounds import ttbar_samples, ttbar_pdf_samples, ttbar_fastsim_samples, ttbar_debug_samples, ttbar_debug_fastsim_samples
        input_ttbar = ttbar_debug_samples

        if "had" in cuts:
            truth_level_variable="(particleLevel.theta_had[0]-particleLevel.theta_d_had)/(particleLevel.theta_had[0]+particleLevel.theta_d_had)"
            detector_level_variable="(nominal.theta_had[0]-nominal.theta_d_had)/(nominal.theta_had[0]+nominal.theta_d_had)"
        else:
            truth_level_variable="(particleLevel.theta[0]-particleLevel.theta_d)/(particleLevel.theta[0]+particleLevel.theta_d)"
            detector_level_variable="(nominal.theta[0]-nominal.theta_d)/(nominal.theta[0]+nominal.theta_d)"

        rhf.create_folder(c.optimization_output_folder+name+'/binning_opt/')
        optimizer = BinOptimizer(
            input_mc_tuple=input_ttbar,
            truth_level_variable=truth_level_variable,
            detector_level_variable=detector_level_variable,
            truth_tuple_name="particleLevel",
            detector_tuple_name="nominal",
            inital_binning=list(np.linspace(-1, 1, 60)),
            x_axis_title="Measured Leptonic A_{#theta}",
            y_axis_title="Leptonic A_{#theta}",
            truth_cuts=truth_cuts,
            detector_cuts=reco_cuts,
            detector_weight="weight_mc*weight_leptonSF*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup",
            truth_weight="weight_mc",
            draw_pulls=True,
            luminosity=c.lumi_2015+c.lumi_2016+c.lumi_2017,
            output_folder=c.optimization_output_folder+name+'/binning_opt/',
            min_n_entries=1.5e2
        )
        return optimizer

    def __call__(self, top_pt=200, min_top_mass=160, top_window_width=180, max_theta_d=0.76,g_pt_cut=30):    
        # name = "CombinedAsymmetry_test_topPt"+str(top_pt)+"_minTopM"+str(min_top_mass)+"_mTopW"+str(top_window_width)+"_thetaD"+str(int(100.*max_theta_d))
        name = "LepSR_topPt%d_minTopM%d_mTopW%d_thetaD%d_gPT%d" % (
                int(top_pt),
                int(min_top_mass),
                int(top_window_width),
                int(100.0*max_theta_d),
                int(g_pt_cut)
            )
        if "had" in self.cuts:
            name = name.replace("LepSR","HadSR")

        ## 
        bin_optimizer = self.get_bin_optimizer(name,cuts=self.cuts,
                                      top_pt=top_pt,
                                      min_top_mass=min_top_mass,
                                      top_window_width=top_window_width,
                                      max_theta_d=max_theta_d,
                                      g_pt_cut=g_pt_cut)
        bin_optimizer.optimize()

        if "had_signal_region" in self.cuts:
            unfolder = self.load_hadronic_signal_region(name, 
                                       top_pt=top_pt,
                                       min_top_mass=min_top_mass,
                                       top_window_width=top_window_width,
                                       max_theta_d=max_theta_d,
                                       g_pt_cut=g_pt_cut,
                                       debug_mode=False)

        else:
            unfolder = self.load_signal_region(name, 
                                       top_pt=top_pt,
                                       min_top_mass=min_top_mass,
                                       top_window_width=top_window_width,
                                       max_theta_d=max_theta_d,
                                       g_pt_cut=g_pt_cut,
                                       debug_mode=False)
        unfolder.x_axis_binning = bin_optimizer.current_binning

        print "RUNNING WITH SETTINGS:"
        print " top_pt:           ", top_pt
        print " min_top_mass:     ", min_top_mass
        print " top_window_width: ", top_window_width
        print " max_theta_d:      ", max_theta_d
        print " Binning:          ", unfolder.x_axis_binning
        # Choose which systematics to run over, configured correctly (hopefully) behind these
        # Scenes.
        # See SysteamaticsManager.load_systematics and Unfolding.Systematic for details
        # on these are managed
        unfolder.systematics = SystematicsManager.load_systematics(
                                unfolder,
                                selected_systematics=[
                                    "Matrix_Element",
                                    "JER",
                                    "Var3Down",
                                    "Frag._and_Had."
                                ],
                                bootstrap=None)

        # The point of this analysis is to compare NLO highly validated Powheg+Pythia8
        # ATLAS Sim to my personally produced MC jobs
        # So let's hand the tool the additional samples we wish to plot
        # at particleLevel a long with the unfolded result
        unfolder.additional_samples = c.additional_samples
        # unfolder.ratio_plot_name = c.ratio_plot_name

        # first thing is to construct the unfolder
        unfolder.construct()
       
        # Now we actually perform the unfolding, save any plots
        unfolder.evaluate()

        # We also want to run the systematics
        unfolder.evaluate_systematics()
        unfolder.evaluate_total_systematic()

        # Now draw everything, tests, systematics, unfolded data, etc.
        unfolder.evaluate_chi2()


        m,mat =  rhf.normalize_migration_matrix_and_get_array(unfolder.migration_matrix.Clone())
        det_mat = np.linalg.det(mat)

        penalty_prefactor = 1
        for i, M in enumerate(mat):
            for j, m in enumerate(M):
                if i == j and m < 0.5:
                    penalty_prefactor *= 1e-3
        pval = unfolder.p_val['DCon']
        print('DCon :', pval)
        #Set tHe messages 
        unfolder.messages = [ "%d < m_{top}^{lep} < %d"%(int(min_top_mass), int(min_top_mass+top_window_width)), 
                              "p_{T}^{top} > %d GeV "%(int(top_pt)), 
                              "#theta_{d} < %.3f, p_{T}^{g} > %d GeV"%(max_theta_d, int(g_pt_cut )),
                              "p(off|on) = %.5g"%pval
                            ]

        unfolder.double_ratio_sample_names = ["Pythia8 DC off","Pythia8 DC on"]
        unfolder.double_ratio_sample_name = "Pythia8 DC on"

        unfolder.draw_nominal_unfolding_plots()
        unfolder.draw_afii()
        unfolder.draw_systematics(combinations)
        unfolder.draw_tests()
        unfolder.draw_reco_plots() 



        #clean up ROOT stuff 
        unfolder.close()

        with open(c.optimization_output_folder + "/"+name+".txt", "a") as myfile:
            info = "%.5g,%.3f,%.3f,%.3f\n"%(
                            pval,
                            top_pt,
                            g_pt_cut,
                            top_window_width
            )
            myfile.write(info)

        if pval != 0:
            return penalty_prefactor/pval

        return 0.0


if __name__ == "__main__":  
    region="signal_region"
    top_pt=150
    top_window_width=50
    g_pt_cut=25

    if len(sys.argv) > 1:
        region = sys.argv[1] 
        top_pt = float(sys.argv[2])
        top_window_width = float(sys.argv[3])
        g_pt_cut = float(sys.argv[4])

    t = unfolder_wrapper(cuts=region)
    t(top_pt=top_pt,
      min_top_mass=150,
      top_window_width=top_window_width,
      g_pt_cut=g_pt_cut,
      max_theta_d=1.0,
      )
