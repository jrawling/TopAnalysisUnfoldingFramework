import subprocess
import os

# Now read in the ptah that the code is stored in. 
INPUT_PATH = os.environ["TOP_ANALYSIS_INPUT_PATH"]
batch_log_outdir = INPUT_PATH+"/logs/batch_job_logs/"

def convert_sub_output_to_id(sub_output):
    job_id = sub_output.split('<')[1].split('>')[0]
    return int(job_id)


def job_running(id):
    output = subprocess.check_output('bjobs',shell=True)
    lines = output.split('\n')
    if lines == ['']:
        return False 
    job_ids = [ l.split(' ')[0] for l in lines[1:-1]]
    job_ids = [int(j) for j in job_ids]
    return id in job_ids

region = 'foo'
sub_output = subprocess.check_output('bsub -q 8nh -J bayes_opt_'+region +'_'+i+' -o "'+
                batch_log_outdir+'nom_un_'+region+'%J.log" -env "INPUT_PATH= '+INPUT_PATH+', REGION='+
                region+
                ', all"<' + INPUT_PATH + '/Optimization/run_opt.sh', shell=True)

job_id = convert_sub_output_to_id(sub_output)
print "Submitted job ID: ",job_id 
print "job is running:", job_running( job_id)
print "job is running:", job_running( 000000000 )