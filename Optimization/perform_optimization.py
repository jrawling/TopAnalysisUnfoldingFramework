
"""
A script to run the optimization that finds a signal region with maximal shpae difference between signal and background hypotheses 
It is convinient to have a python script to initiatie the jobs themselves 

"""
import os
import sys
from bayes_opt import BayesianOptimization
from subprocess import call,Popen
import config as c
import pickle 
import numpy as np
import cloudpickle


python_script = "/afs/cern.ch/user/j/jrawling/workDir/DeadConeAnalysis/TopAnalysisUnfolding/Optimization/OptimizeSignalRegion.py"
output_folder = "/afs/cern.ch/user/j/jrawling/workDir/DeadConeAnalysis/TopAnalysisUnfolding/logs/optimization/"

# Parameters for optimization procedure
n_burn_in = 70
m = 20

# Create a dictionary of process indexed by their IDs
ps = {}

# Run for a period of burn in 
cut_region = sys.argv[1]
print "Running ", n_burn_in, " burn in iterations"
for i in range(n_burn_in):
    logFile=output_folder +"iteraiton_"+str(i)+".log"
    error_log_file=output_folder +"iteraiton_"+str(i)+".err"
    
    if "had" in cut_region:
        logFile = logFile.replace('iteraiton_','iteraiton_had_')
        error_log_file = error_log_file.replace('iteraiton_','iteraiton_had_')

    # set up log files and the actual script to call 
    args = ["nice","-n","0","python", python_script, cut_region]
    log_file = open(logFile,"wb")
    err_log_file = open(error_log_file,"wb")

    # figure out how to move this into bsub instead of Popen  
    p = Popen(args,stdout=log_file,stderr=err_log_file)
    ps[p.pid] = p
    print "Started job", i

    while len(ps) >= 1:
        try:
            pid, status = os.wait()
            if pid in ps:
                del ps[pid]
                print "Starting next job: " + `i+1`
        except:
            print "ERROR: Failed to wait on job..."
            ps = []

# Keep track fof the absolute number of iterations 
iteration = n_burn_in
# As well as the iteration that proceduded the best result
# We want to at least run the next batch of M searches 
# so set max_iteration to a dummy value that will not break the loop
max_iteration = iteration
while max_iteration >  iteration-m:
    print "Running the next ", m, " sets of optimizations."

    for i in range(m):
        
        logFile=output_folder +"iteraiton_"+str(i)+".log"
        error_log_file=output_folder +"iteraiton_"+str(i)+".err"
    
        if "had" in cut_region:
            logFile = logFile.replace('iteraiton_','iteraiton_had_')
            error_log_file = error_log_file.replace('iteraiton_','iteraiton_had_')

        args = ["nice","-n","0","python", python_script, cut_region]
        log_file = open(logFile,"wb")
        err_log_file = open(error_log_file,"wb")
        # p = Popen(args,stdout=log_file,stderr=err_log_file)
        # ps[p.pid] = p
        print "Started job", i

        while len(ps) >= 1:  
            try:
                pid, status = os.wait()
                if pid in ps:
                    del ps[pid]
                    print "Starting next job: " + `i+1`
            except:
                print "ERROR: Failed to wait on job..."
                ps = []

        iteration+=1


    # read in the optimizer 
    output = open( bayes_opt, 'rb')
    bo = pickle.load(output)
    output.close()

    # get the values 
    p_vals =  bo.res['all']['values']   
    max_iteration = np.argmax(p_vals)

print "Evaluated the best separation given constriaints at iteration ", max_iteration, "/", iteration
print "Quiting."
print "Optimal settings: " 
print " top_pt:           ", bo.res['max']['max_params']['x'] 
print " min_top_mass:     ", bo.res['max']['max_params']['y'] 
print " top_window_width: ", bo.res['max']['max_params']['z'] 
print " g_pt_min: ", bo.res['max']['max_params']['g_pt_cut'] 
print " max_theta_d: ", bo.res['max']['max_params']['max_theta_d'] 
print "----------------------------------------------------------" 
print "p*-val: ", bo.res['max']['max_val']  