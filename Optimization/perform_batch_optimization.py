
"""
A script to run the optimization that finds a signal region with maximal shpae difference between signal and background hypotheses 
It is convinient to have a python script to initiatie the jobs themselves 

"""
from __future__ import print_function 
import os
import sys
from bayes_opt import BayesianOptimization
from subprocess import call,Popen
import subprocess
import config as c
import pickle 
import numpy as np
import cloudpickle
import time

def convert_sub_output_to_id(sub_output):
    job_id = sub_output.split('<')[1].split('>')[0]
    return int(job_id)


def job_running(id):
    output = subprocess.check_output('bjobs',shell=True)
    lines = output.split('\n')
    if lines == ['']:
        return False 
    job_ids = [ l.split(' ')[0] for l in lines[1:-1]]
    job_ids = [int(j) for j in job_ids]
    return id in job_ids


python_script = "/afs/cern.ch/user/j/jrawling/workDir/DeadConeAnalysis/TopAnalysisUnfolding/Optimization/OptimizeSignalRegion.py"
output_folder = "/afs/cern.ch/user/j/jrawling/workDir/DeadConeAnalysis/TopAnalysisUnfolding/logs/optimization/"

# Parameters for optimization procedure
n_burn_in = 70
m = 20

# Create a dictionary of process indexed by their IDs
ps = {}

INPUT_PATH = os.environ["TOP_ANALYSIS_INPUT_PATH"]
# Run for a period of burn in 
region = sys.argv[1]
print("Running ", n_burn_in, " burn in iterations")

for i in range(n_burn_in):
    sub_output = subprocess.check_output('bsub -q 8nh -J bayes_opt_'+region +'_'+str(i)+' -o "'+
                output_folder+'iteraiton_'+region+'_'+str(i)+'.log" -env "INPUT_PATH= '+INPUT_PATH+', REGION='+
                region+
                ', all"<' + INPUT_PATH + '/Optimization/run_opt.sh', shell=True)
    job_id = convert_sub_output_to_id(sub_output)
    print("Submitted iteration ", i )
    print("     Region: ", region)
    print("     job_id: ", job_id)
    # 
    while job_running(job_id):
        # 
        time.sleep(10)


# Keep track fof the absolute number of iterations 
iteration = n_burn_in

# As well as the iteration that proceduded the best result
# We want to at least run the next batch of M searches 
# so set max_iteration to a dummy value that will not break the loop
max_iteration = iteration
while max_iteration >  iteration-m:
    print("Running the next ", m, " sets of optimizations.")

    for i in range(m):
        # 
        sub_output = subprocess.check_output('bsub -q 8nh -J bayes_opt_'+region +'_'+str(n_burn_in+i)+' -o "'+
                    output_folder+'iteraiton_'+region+'_'+str(i+n_burn_in)+'.log" -env "INPUT_PATH= '+INPUT_PATH+', REGION='+
                    region+
                    ', all"<' + INPUT_PATH + '/Optimization/run_opt.sh', shell=True)
        job_id = convert_sub_output_to_id(sub_output)

        print("Submitted iteration ", i+ n_burn_in)
        print("     Region: ", region)
        print("     job_id: ", job_id)
        # 
        # 
        while job_running(job_id):
            # 
            time.sleep(10)



    save_path = c.optimization_output_folder+region+"_bayes_opt.pkl"
    # read in the optimizer 
    output = open( bayes_opt, 'rb')
    bo = pickle.load(output)
    output.close()

    # get the values 
    p_vals =  bo.res['all']['values']   
    max_iteration = np.argmax(p_vals)

print("Evaluated the best separation given constriaints at iteration ", max_iteration, "/", iteration)
print("Quiting.")
print("Optimal settings: " )
print(" top_pt:           ", bo.res['max']['max_params']['x'] )
print(" min_top_mass:     ", bo.res['max']['max_params']['y'] )
print(" top_window_width: ", bo.res['max']['max_params']['z'] )
print(" g_pt_min: ", bo.res['max']['max_params']['g_pt_cut'] )
print(" max_theta_d: ", bo.res['max']['max_params']['max_theta_d'] )
print("----------------------------------------------------------" )
print("p*-val: ", bo.res['max']['max_val']  )