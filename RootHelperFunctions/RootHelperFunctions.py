import ROOT as r
import sys
import array
import os
from math import sqrt
import numpy as np
import time
r.TH1.AddDirectory(r.kFALSE)
r.TH2.AddDirectory(r.kFALSE)

def open_file(file_name, option="READ" ):
    f = r.TFile(file_name,option)
    assert f, ("ERROR: failed to open file: ",file_name)
    return f

def create_folder(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

def grab_bin_centres(hist):
    contents = []
    for i in range(1, hist.GetNbinsX() + 1):
        contents.append(hist.GetBinContent(i))

    return contents

def rebin_2d_hist(hist, x_axis_binning,y_axis_binning):
    
    # Get previous binning for hist
    inital_binning = []

    # Create a new 2D Histogram for the binin
    htemp = r.TH2F("", "", len(x_axis_binning) - 1,
                   array.array('d', x_axis_binning), len(y_axis_binning) - 1,
                   array.array('d', y_axis_binning)
                   )

    # Iterate over the bins of the new histogram
    new_hist = np.zeros(  (len(x_axis_binning)+1, len(y_axis_binning)+1) )
    n_entries = np.zeros( (len(x_axis_binning)+1, len(y_axis_binning)+1) )
    errors = np.zeros(    (len(x_axis_binning)+1, len(y_axis_binning)+1) )

    # 
    for x in range(0,hist.GetXaxis().GetNbins()+1):
        for y in range(0, hist.GetYaxis().GetNbins()+1):
            x_val = hist.GetXaxis().GetBinCenter(x)
            new_x_bin = htemp.GetXaxis().FindBin(x_val)

            y_val = hist.GetYaxis().GetBinCenter(y)
            new_y_bin = htemp.GetYaxis().FindBin(y_val)

            bin_content, bin_error = htemp.GetBinContent(new_x_bin, new_y_bin),\
                                     htemp.GetBinError(new_x_bin, new_y_bin)

            bin_error = (bin_error**2 + hist.GetBinError(x,y)**2 )**0.5
            htemp.SetBinContent(new_x_bin, new_y_bin, bin_content + hist.GetBinContent(x,y))
            htemp.SetBinError(new_x_bin, new_y_bin, bin_error )

    assert htemp.Integral() == hist.Integral(), "ERROR: Integral of input and output histograms are unequal, this implies an invalid rebinning scheme."
    return htemp 
    

def get_largest_abs_bin_content_hist(hists):
    abs_hist = hists[0].Clone()
    contents = [0.0 for i in range(1, abs_hist.GetNbinsX() + 1) ]


    for hist in hists:
        for bin_index in range(1, hist.GetNbinsX() + 1):
            contents[bin_index-1]  = max([contents[bin_index-1],hist.GetBinContent(bin_index)], key=abs)

    for i, content in enumerate(contents):
        abs_hist.SetBinContent(i+1, content)

    return abs_hist


def get_2d_histogram_from_sample(sample,
                     ntuple_name,
                     variable_name,
                     x_axis_binning,
                     y_axis_binning,
                     weight,
                     scale_to_lumi=False,
                     lumi=1.0,
                     friend_name = None,
                     index_variable = None,
                     draw_options = "",
                     hist_name    = "htemp",
                     allowed_branches=None,
                     allowed_friend_branches=None,
                     debug_mode=False,
                     # allowed_branches = ["runNumber","eventNumber", "mc_generator_weights", "weight_mc", "weight_leptonSF", "weight_jvt", "weight_bTagSF_MV2c10_85", "weight_pileup", "weight_pileup*weight_jvt_UP", "weight_pileup*weight_jvt_UP", "weight_pileup*weight_jvt_DOWN", "weight_leptonSF_EL_SF_Trigger_UP", "weight_leptonSF_EL_SF_Trigger_DOWN", "weight_leptonSF_EL_SF_Reco_UP", "weight_leptonSF_EL_SF_Reco_DOWN", "weight_leptonSF_EL_SF_ID_UP", "weight_leptonSF_EL_SF_ID_DOWN", "weight_leptonSF_EL_SF_Isol_UP", "weight_leptonSF_EL_SF_Isol_DOWN", "weight_leptonSF_MU_SF_Trigger_STAT_UP", "weight_leptonSF_MU_SF_Trigger_STAT_DOWN", "weight_leptonSF_MU_SF_ID_STAT_UP", "weight_leptonSF_MU_SF_ID_STAT_DOWN", "weight_leptonSF_MU_SF_Isol_STAT_UP", "weight_leptonSF_MU_SF_Isol_STAT_DOWN", "weight_leptonSF_MU_SF_Trigger_SYST_UP", "weight_leptonSF_MU_SF_Trigger_SYST_DOWN", "weight_leptonSF_MU_SF_ID_SYST_UP", "weight_leptonSF_MU_SF_ID_SYST_DOWN", "weight_leptonSF_MU_SF_Isol_SYST_UP", "weight_leptonSF_MU_SF_Isol_SYST_DOWN", "weight_pileup_UP", "weight_pileup_DOWN", "weight_bTagSF_MV2c10_85_eigenvars_B_up", "weight_bTagSF_MV2c10_85_eigenvars_B_down", "weight_bTagSF_MV2c10_85_eigenvars_C_up", "weight_bTagSF_MV2c10_85_eigenvars_C_down", "weight_bTagSF_MV2c10_85_eigenvars_Light_up", "weight_bTagSF_MV2c10_85_eigenvars_Light_down", "weight_bTagSF_MV2c10_85_extrapolation_up", "weight_bTagSF_MV2c10_85_extrapolation_down", "weight_bTagSF_MV2c10_85_extrapolation_from_charm_up", "weight_bTagSF_MV2c10_85_extrapolation_from_charm_down", "jet_pt", "mujets_2015", "mujets_2016", "g_pt", "ejets_2015","ejets_2016","top_lep_pt","top_had_pt","theta_d","theta_d_had","theta","theta_had","top_lep_m","top_had_m",], 
                     # allowed_friend_branches = ["runNumber","eventNumber", "mc_generator_weights", "weight_mc", "weight_leptonSF", "weight_jvt", "weight_bTagSF_MV2c10_85", "weight_pileup", "weight_pileup*weight_jvt_UP", "weight_pileup*weight_jvt_UP", "weight_pileup*weight_jvt_DOWN", "weight_leptonSF_EL_SF_Trigger_UP", "weight_leptonSF_EL_SF_Trigger_DOWN", "weight_leptonSF_EL_SF_Reco_UP", "weight_leptonSF_EL_SF_Reco_DOWN", "weight_leptonSF_EL_SF_ID_UP", "weight_leptonSF_EL_SF_ID_DOWN", "weight_leptonSF_EL_SF_Isol_UP", "weight_leptonSF_EL_SF_Isol_DOWN", "weight_leptonSF_MU_SF_Trigger_STAT_UP", "weight_leptonSF_MU_SF_Trigger_STAT_DOWN", "weight_leptonSF_MU_SF_ID_STAT_UP", "weight_leptonSF_MU_SF_ID_STAT_DOWN", "weight_leptonSF_MU_SF_Isol_STAT_UP", "weight_leptonSF_MU_SF_Isol_STAT_DOWN", "weight_leptonSF_MU_SF_Trigger_SYST_UP", "weight_leptonSF_MU_SF_Trigger_SYST_DOWN", "weight_leptonSF_MU_SF_ID_SYST_UP", "weight_leptonSF_MU_SF_ID_SYST_DOWN", "weight_leptonSF_MU_SF_Isol_SYST_UP", "weight_leptonSF_MU_SF_Isol_SYST_DOWN", "weight_pileup_UP", "weight_pileup_DOWN", "weight_bTagSF_MV2c10_85_eigenvars_B_up", "weight_bTagSF_MV2c10_85_eigenvars_B_down", "weight_bTagSF_MV2c10_85_eigenvars_C_up", "weight_bTagSF_MV2c10_85_eigenvars_C_down", "weight_bTagSF_MV2c10_85_eigenvars_Light_up", "weight_bTagSF_MV2c10_85_eigenvars_Light_down", "weight_bTagSF_MV2c10_85_extrapolation_up", "weight_bTagSF_MV2c10_85_extrapolation_down", "weight_bTagSF_MV2c10_85_extrapolation_from_charm_up", "weight_bTagSF_MV2c10_85_extrapolation_from_charm_down", "jet_pt", "mujets_2015", "mujets_2016", "g_pt", "ejets_2015","ejets_2016","top_lep_pt","top_had_pt","theta_d","theta_d_had","theta","theta_had","top_lep_m","top_had_m",], 

                     ):


    r.gROOT.SetBatch()
    r.TH1.SetDefaultSumw2()

    # Construct the required the binning 
    variable_binning_x = not isinstance(x_axis_binning,str)
    variable_binning_y = not isinstance(y_axis_binning,str)

    if not variable_binning_x:
        x_axis_binning = [float(s) for s in x_axis_binning.split(',')]
        x_axis_binning = np.linspace(x_axis_binning[1],x_axis_binning[2],x_axis_binning[0]).tolist()
    if not variable_binning_y:
        y_axis_binning = [float(s) for s in y_axis_binning.split(',')]
        y_axis_binning = np.linspace(y_axis_binning[1],y_axis_binning[2],y_axis_binning[0]).tolist()

    for l, file_list in enumerate(sample.input_file_names):

        # Give all histograms a unique name if they're left as a default 
        if hist_name == "htemp":
            hist_name = "htemp_"+str(time.time())

        chain = r.TChain(ntuple_name)
        for n, file in enumerate(file_list):
            if debug_mode and n > 2:
                continue
                
            if file[-5:] != ".root":
                continue

            # Support both a list of files and an invidiual file
            chain.Add(file)

            # Check to see if the user is attempting to access variables from a friended tree
            if friend_name != None:
                friend_chain = r.TChain(friend_name)
                friend_chain.Add(file)
                if allowed_friend_branches is not None:
                    friend_chain.SetBranchStatus("*",0)
                    for b in allowed_friend_branches:
                        friend_chain.SetBranchStatus(b,1)


                nominal_index = r.TTreeIndex(chain,"(runNumber<<13)","eventNumber")
                chain.SetTreeIndex(nominal_index)
                truth_index = r.TTreeIndex(friend_chain,"(runNumber<<13)","eventNumber")
                friend_chain.SetTreeIndex(truth_index)
                chain.AddFriend(friend_chain)

        # chain.Draw(variable_name, weight, draw_options)
        r.TH2.AddDirectory(r.kTRUE)
        htemp = r.TH2F(hist_name,"",len(x_axis_binning)-1,array.array('d',x_axis_binning),
                                    len(y_axis_binning)-1,array.array('d',y_axis_binning))
        chain.Draw(variable_name+">>"+htemp.GetName()+"", weight, "E")
        try:
            htemp = r.gPad.GetPrimitive(hist_name)
        except ReferenceError:
            print "ERROR: Failed to draw histogram!!! "
            print "     hist_name: ", hist_name
            htemp = r.TH2F(hist_name,"",len(x_axis_binning)-1,array.array('d',x_axis_binning),len(y_axis_binning)-1,array.array('d',y_axis_binning))
            
        # if not isinstance(htemp, r.TH1):
        #     htemp = r.TH2F(hist_name,"",len(x_axis_binning)-1,array.array('d',x_axis_binning),len(y_axis_binning)-1,array.array('d',y_axis_binning))
        r.TH2.AddDirectory(r.kFALSE)
        htemp.SetDirectory(0)

        # # By default we prevent histograms being stored in local directories
        # # Instead they are drawn to the current gPad, so we can retrieve them
        # # from there.... ( fuck ROOT, why are you like this!? )
        # graph = r.gPad.GetPrimitive("Graph")
        # for i in range(graph.GetN()):
        #     htemp.Fill(graph.GetX()[i], graph.GetY()[i])

        chain.Reset()
        if friend_name != None:
            friend_chain.Reset()    
            del friend_chain
        # graph.Delete()
        # del graph
        del chain

        #let's the user rescale
        if scale_to_lumi:
            print("Intgeral before scaling: ", htemp.Integral())
            print "RESCALING 2D HISTOGRAM TO LUMI"
            print "   Data Lumi: ", lumi
            print "   sample Lumi: ", sample.mc_lumi[l]

            htemp.Scale(lumi/sample.mc_lumi[l])
            print("Intgeral after scaling: ", htemp.Integral())

        if l==0:
            total_hist = htemp.Clone('total_hist_'+str(time.time()))
        else:
            total_hist.Add(htemp)


    #retrive the histogram from ROOT and free it from this instance in memory
    # assert isinstance(htemp,r.TH2F) or isinstance(htemp,r.TProfile) ,( "ERROR: Failed to open get histogram with variable expression",variable_name," from files", file_list," is type ",type(htemp))
    total_hist.SetDirectory(0)

    return htemp
    
def calculate_covariance_matrix(uncert, corr=None):
    """
    Takes a list of uncertainties for each bin and returns the covariance matrix 
    """

    if corr is None:
        corr = np.diag([1.0 for u in uncert])
        
    cov_mat = []
    for i,C in enumerate(corr):
        row = []
        for j,c in enumerate(C):
            row.append(uncert[i]*uncert[j]*corr[i,j] )
            # if i == j and np.abs(row[-1]) < 1e-4:
            #     print("PROBLEMATIC DIAGONAL ELEMENT: ", row[-1])
            #     row[-1] = 1e-3
        cov_mat.append(row)
    return np.vstack(tuple(cov_mat))
def get_bin_by_bin_average(hist_list, bin_hist_name=None):
    """
    Average over al ist of TH1 Histograms and produce a histogram whose
    entries are the average of all entries in the list for each bin. 

    Paramaters
    ---------
    hist_list: A list of histograms. 

    Returns
    -------
    average: a histogram whose bin contents are the average of all bins in the 
             list.
    bin_spreads: a list of histograms, one for each bin, showing the spread 
                 of entries inhe list
    """
    average = hist_list[0].Clone(hist_list[0].GetName()+"_average")
    stat_uncert = hist_list[0].Clone(hist_list[0].GetName()+"_average")
    bin_spreads = []

    # Createa  name for the bin_spread histograms if one has not been set 
    # by the user.
    if bin_hist_name is None:
        bin_hist_name = hist_list[0].GetName()      

    for i in range(1, hist_list[0].GetNbinsX() + 1):
        hist_entries = []
        for hist in hist_list[1:]:
            hist_entries.append(hist.GetBinContent(i))

        bin_spreads.append(r.TH1F("bin_"+str(i)+"_"+bin_hist_name, "", 30, 
                                   np.min(hist_entries), np.max(hist_entries)))
        for x in hist_entries:
            bin_spreads[-1].Fill(x)

        mean =  np.mean(hist_entries)
        std  =  np.std(hist_entries)
        # print(" For bin ", i , " of averaging ", bin_hist_name, " mean = ", mean, " std = ", std )
        average.SetBinContent(i, mean)
        average.SetBinError(i, std) 
        
        stat_uncert.SetBinError(i,0)
        stat_uncert.SetBinContent(i,std)

    return average, stat_uncert, bin_spreads



def extract_error_at_unity(hist,y_centre=1.0):
    x,y,exl,eyl,exh,eyh = array.array('d'),array.array('d'),array.array('d'),array.array('d'),array.array('d'),array.array('d')
    for i in xrange(hist.GetSize()):
        x.append(hist.GetBinCenter(i))
        y.append(y_centre)
        exl.append(hist.GetBinWidth(i)/2.0)
        exh.append(hist.GetBinWidth(i)/2.0)
        eyh.append(hist.GetBinError(i))
        eyl.append(hist.GetBinError(i))

    graph = r.TGraphAsymmErrors(len(x),x,y,exl,exh,eyl,eyh)
    return graph

def convert_up_down_uncert_to_asymmerrors(up_hist,down_hist,y_centre=1.0):
    x,y,exl,eyl,exh,eyh = array.array('d'),array.array('d'),array.array('d'),array.array('d'),array.array('d'),array.array('d')
    for i in xrange(up_hist.GetSize()):
        x.append(up_hist.GetBinCenter(i))
        y.append(y_centre)
        exl.append(up_hist.GetBinWidth(i)/2.0)
        exh.append(up_hist.GetBinWidth(i)/2.0)
        eyh.append(up_hist.GetBinContent(i))
        eyl.append(down_hist.GetBinContent(i))

    graph = r.TGraphAsymmErrors(len(x),x,y,exl,exh,eyl,eyh)
    return graph

def get_sorted_stack(stack_name, hist_list):
    """
    Takes a list of histograms and returns a sorted stack
    such that those with the largest integral are appear at the
    top
    """
    hist_list.sort()
    hist_stack = r.THStack(stack_name,"")
    for hist in hist_list:
        hist_stack.Add(hist)
    return hist_stack

def normalize_migration_matrix_and_get_array(migration_matrix):
    """
        Normalize a migration matrix such that each row
        adds up to unity.
    """
    n_cols = migration_matrix.GetXaxis().GetNbins()
    matrix = np.zeros(shape=(migration_matrix.GetNbinsX(),migration_matrix.GetNbinsX()))

    for j in range(1,migration_matrix.GetYaxis().GetNbins()+1):
         norm = migration_matrix.Integral(0,n_cols,j,j)

         #protectiong against zero particle level events in a row
         if norm == 0:
             norm = 1.0

         for i in range(1,migration_matrix.GetXaxis().GetNbins()+1):
             migration = migration_matrix.GetBinContent(i,j)
             migration *= 1.0/norm
             migration_matrix.SetBinContent(i,j,migration)
             matrix[i-1,j-1] = migration

    return migration_matrix, matrix

def poisson_fluctuate(hist,random_generator):
    """
        Go bin by bin and fluctuate the contents

        hist: TH1F of bins
        random_generator: TRandom3
    """
    new_hist = hist.Clone()
    for i in range(1, hist.GetNbinsX() + 1):
        if not hist.GetBinContent(i) == 0:
            new_hist.SetBinContent(i, random_generator.Poisson(hist.GetBinContent(i)))
    return new_hist

# Draws a 1D histogram histogram using TTree::Daw
def get_histogram_from_sample(sample, ntuple_name, variable_name,x_axis_binning,weight,
                  scale_to_lumi=True, 
                  lumi=1.0,
                  draw_options = "e", 
                  hist_name = "htemp", 
                  friend_name = None,
                  index_variable = None, 
                  allowed_branches=None,
                  allowed_friend_branches=None,
                  debug_mode=False,
                 ):
    """
        Parameters:
            file_name_list: either a list of input files, in which case a TChain will be used to draw from
                            all files in the list, OR a string. If a string then a TTRee will be retrieved
                            and drawn from

            ntuple_name: self evident.
            variable_name: the string that will be drawn in the TTree::Draw command
            weight: weighting factor for each event in the draw command.
            scale:  A factor to change the histograms scale by
            hist_name: the ROOT name of the histogram generated
            friend_name: if set a friend tree is assumed to exist in the file, it will be read nad added as a friend
                         and thus accessible in TTree:Draw commands
            index_variable: a string for the index variable that will be built and used to sync the friend tree and the default ntuple
            allowed_branches: A list of allowed branches that will have their 
            status set to 1, if none then SetBranchStatus is not invoked
            allowed_friend_branches: A list of allowed branches that will have their 
            status set to 1, if none then SetBranchStatus is not invoked
        return values:
            A TH1F histogram that has been freed from root memory managemnt (i.e SetDirectory(0))

    """

    r.gROOT.SetBatch()
    r.TH1.SetDefaultSumw2()


    if scale_to_lumi:
        print "RESCALING 1D HISTOGRAM TO LUMI"
        print "   Data Lumi: ", lumi

        for l in range(len(sample.mc_lumi)):
            print "   sample Lumi: ", sample.mc_lumi[l]
    for l, file_list in enumerate(sample.input_file_names):
        if  "htemp" in hist_name:
            hist_name = "htemp_"+str(time.time())
        

        # First setup the TChains 
        chain = r.TChain(ntuple_name)

        # Check to see if the user is attempting to access variables from a friended tree
        if friend_name != None:
            # assert index_variable != None,"ERROR: index_variable must be set when using a friend tree"
            friend_chain = r.TChain(friend_name)
        # Create the chain 

        for n, file in enumerate(file_list):
            if file[-5:] != ".root":
                print "WARNING (get_histogram): File didn't end in .root: "
                print "    ", file
                continue
            if debug_mode and n > 2:
                continue 
                
            if file_list != []:
                chain.Add(file)
                if friend_name != None:
                    friend_chain.Add(file)


            if friend_name != None:
                nominal_index = r.TTreeIndex(chain,"(runNumber<<13)","eventNumber")
                chain.SetTreeIndex(nominal_index)
                truth_index = r.TTreeIndex(friend_chain,"(runNumber<<13)","eventNumber")
                friend_chain.SetTreeIndex(truth_index)
                chain.AddFriend(friend_chain)

        # Construct the actual histograms we are going to draw to 
        if isinstance(x_axis_binning,str) == True:
            x_axis_binning = [float(s) for s in x_axis_binning.split(',')]
            x_axis_binning = np.linspace(x_axis_binning[1],x_axis_binning[2],x_axis_binning[0]).tolist()

        r.TH1.AddDirectory(r.kTRUE)
        htemp = r.TH1F(hist_name,"",len(x_axis_binning)-1,array.array('d',x_axis_binning))
        chain.Draw(variable_name+">>"+htemp.GetName()+"", weight, "E")
        try:
            htemp = r.gPad.GetPrimitive(hist_name)
        except ReferenceError:
            print "ERROR: Failed to draw histogram!!! "
            print "     hist_name: ", hist_name
            htemp = r.TH1F(hist_name,"",len(x_axis_binning)-1,array.array('d',x_axis_binning))
            
        if not isinstance(htemp, r.TH1):
            htemp = r.TH1F(hist_name,"",len(x_axis_binning)-1,array.array('d',x_axis_binning))
        r.TH1.AddDirectory(r.kFALSE)
        htemp.SetDirectory(0)

        if scale_to_lumi:
            print("Intgeral before scaling: ", htemp.Integral())
            print "RESCALING 1D HISTOGRAM TO LUMI"
            print "   Data Lumi: ", lumi
            print "   sample Lumi: ", sample.mc_lumi[l]

            htemp.Scale(lumi/sample.mc_lumi[l])
            print("Intgeral after scaling: ", htemp.Integral())

        if l == 0:
            total = htemp
        else:
            total.Add(htemp)


        chain.Reset()
        del chain
    
        if friend_name != None:
            friend_chain.Reset()    
            del friend_chain

    return total
    
#
# Draws a 1D histogram histogram using TTree::Daw
def get_histogram(file_name_list,ntuple_name, variable_name,x_axis_binning,weight,scale = 1.0, draw_options = "e", hist_name = "htemp", friend_name = None,
                  index_variable = None, 
                  allowed_branches=None,
                  allowed_friend_branches=None,
                  debug_mode=False,
                  # allowed_branches = [ "mc_generator_weights", "weight_mc", "weight_leptonSF", "weight_jvt", "weight_bTagSF_MV2c10_85", "weight_pileup", "weight_pileup*weight_jvt_UP", "weight_pileup*weight_jvt_UP",   "weight_pileup*weight_jvt_DOWN", "weight_leptonSF_EL_SF_Trigger_UP",   "weight_leptonSF_EL_SF_Trigger_DOWN",   "weight_leptonSF_EL_SF_Reco_UP",   "weight_leptonSF_EL_SF_Reco_DOWN",   "weight_leptonSF_EL_SF_ID_UP",   "weight_leptonSF_EL_SF_ID_DOWN",   "weight_leptonSF_EL_SF_Isol_UP",   "weight_leptonSF_EL_SF_Isol_DOWN",   "weight_leptonSF_MU_SF_Trigger_STAT_UP",   "weight_leptonSF_MU_SF_Trigger_STAT_DOWN",   "weight_leptonSF_MU_SF_ID_STAT_UP",   "weight_leptonSF_MU_SF_ID_STAT_DOWN",   "weight_leptonSF_MU_SF_Isol_STAT_UP",   "weight_leptonSF_MU_SF_Isol_STAT_DOWN",   "weight_leptonSF_MU_SF_Trigger_SYST_UP",   "weight_leptonSF_MU_SF_Trigger_SYST_DOWN",   "weight_leptonSF_MU_SF_ID_SYST_UP",   "weight_leptonSF_MU_SF_ID_SYST_DOWN",   "weight_leptonSF_MU_SF_Isol_SYST_UP",   "weight_leptonSF_MU_SF_Isol_SYST_DOWN",   "weight_pileup_UP",   "weight_pileup_DOWN", "weight_bTagSF_MV2c10_85_eigenvars_B_up",   "weight_bTagSF_MV2c10_85_eigenvars_B_down",   "weight_bTagSF_MV2c10_85_eigenvars_C_up",   "weight_bTagSF_MV2c10_85_eigenvars_C_down",   "weight_bTagSF_MV2c10_85_eigenvars_Light_up",   "weight_bTagSF_MV2c10_85_eigenvars_Light_down", "weight_bTagSF_MV2c10_85_extrapolation_up",   "weight_bTagSF_MV2c10_85_extrapolation_down",   "weight_bTagSF_MV2c10_85_extrapolation_from_charm_up",   "weight_bTagSF_MV2c10_85_extrapolation_from_charm_down", "jet_pt", "mujets_2015", "mujets_2016", "g_pt", "ejets_2015" "ejets_2016" "top_lep_pt" "top_had_pt" "theta_d" "theta_d_Had" "theta" "theta_had" "top_lep_m" "top_had_m" ], 

                  # allowed_friend_branches = [ "mc_generator_weights", "weight_mc", "weight_leptonSF", "weight_jvt", "weight_bTagSF_MV2c10_85", "weight_pileup", "weight_pileup*weight_jvt_UP", "weight_pileup*weight_jvt_UP",   "weight_pileup*weight_jvt_DOWN", "weight_leptonSF_EL_SF_Trigger_UP",   "weight_leptonSF_EL_SF_Trigger_DOWN",   "weight_leptonSF_EL_SF_Reco_UP",   "weight_leptonSF_EL_SF_Reco_DOWN",   "weight_leptonSF_EL_SF_ID_UP",   "weight_leptonSF_EL_SF_ID_DOWN",   "weight_leptonSF_EL_SF_Isol_UP",   "weight_leptonSF_EL_SF_Isol_DOWN",   "weight_leptonSF_MU_SF_Trigger_STAT_UP",   "weight_leptonSF_MU_SF_Trigger_STAT_DOWN",   "weight_leptonSF_MU_SF_ID_STAT_UP",   "weight_leptonSF_MU_SF_ID_STAT_DOWN",   "weight_leptonSF_MU_SF_Isol_STAT_UP",   "weight_leptonSF_MU_SF_Isol_STAT_DOWN",   "weight_leptonSF_MU_SF_Trigger_SYST_UP",   "weight_leptonSF_MU_SF_Trigger_SYST_DOWN",   "weight_leptonSF_MU_SF_ID_SYST_UP",   "weight_leptonSF_MU_SF_ID_SYST_DOWN",   "weight_leptonSF_MU_SF_Isol_SYST_UP",   "weight_leptonSF_MU_SF_Isol_SYST_DOWN",   "weight_pileup_UP",   "weight_pileup_DOWN", "weight_bTagSF_MV2c10_85_eigenvars_B_up",   "weight_bTagSF_MV2c10_85_eigenvars_B_down",   "weight_bTagSF_MV2c10_85_eigenvars_C_up",   "weight_bTagSF_MV2c10_85_eigenvars_C_down",   "weight_bTagSF_MV2c10_85_eigenvars_Light_up",   "weight_bTagSF_MV2c10_85_eigenvars_Light_down", "weight_bTagSF_MV2c10_85_extrapolation_up",   "weight_bTagSF_MV2c10_85_extrapolation_down",   "weight_bTagSF_MV2c10_85_extrapolation_from_charm_up",   "weight_bTagSF_MV2c10_85_extrapolation_from_charm_down", "jet_pt", "mujets_2015", "mujets_2016", "g_pt", "ejets_2015" "ejets_2016" "top_lep_pt" "top_had_pt" "theta_d" "theta_d_Had" "theta" "theta_had" "top_lep_m" "top_had_m" ]
                 ):
    """
        Parameters:
            file_name_list: either a list of input files, in which case a TChain will be used to draw from
                            all files in the list, OR a string. If a string then a TTRee will be retrieved
                            and drawn from

            ntuple_name: self evident.
            variable_name: the string that will be drawn in the TTree::Draw command
            weight: weighting factor for each event in the draw command.
            scale:  A factor to change the histograms scale by
            hist_name: the ROOT name of the histogram generated
            friend_name: if set a friend tree is assumed to exist in the file, it will be read nad added as a friend
                         and thus accessible in TTree:Draw commands
            index_variable: a string for the index variable that will be built and used to sync the friend tree and the default ntuple
            allowed_branches: A list of allowed branches that will have their 
            status set to 1, if none then SetBranchStatus is not invoked
            allowed_friend_branches: A list of allowed branches that will have their 
            status set to 1, if none then SetBranchStatus is not invoked
        return values:
            A TH1F histogram that has been freed from root memory managemnt (i.e SetDirectory(0))

    """

    if hist_name == "htemp":
        hist_name = "htemp_"+str(time.time())
        

    r.gROOT.SetBatch()
    chain = r.TChain(ntuple_name)

    # Support both a list of files and an invidiual file
    if isinstance(file_name_list, list):
        for file_list in file_name_list:
            if file_list == []:
                continue 

            for n, file in enumerate(file_list):
                if file[-5:] != ".root":
                    print "WARNING (get_histogram): File didn't end in .root: "
                    print "    ", file
                    continue
                if debug_mode and n > 2:
                    continue 
                    
                if file_list != []:
                    chain.Add(file)
    else:
        chain.Add(file_name_list)
    r.TH1.SetDefaultSumw2()

    if allowed_branches is not None:
        chain.SetBranchStatus("*",0)
        for b in allowed_branches:
            chain.SetBranchStatus(b,1)



    # Check to see if the user is attempting to access variables from a friended tree
    if friend_name != None:
        # assert index_variable != None,"ERROR: index_variable must be set when using a friend tree"
        friend_chain = r.TChain(friend_name)

        if isinstance(file_name_list, list):
            for file_list in file_name_list:
                if file_list == []:
                    continue 

                for n, file in enumerate(file_list):
                    if file[-5:] != ".root":
                        continue
                    if debug_mode and n > 2:
                        continue 
                    if file_list != []:
                        friend_chain.Add(file)
        else:
            friend_chain.Add(file_name_list)

        # friend_chain.BuildIndex(index_variable)
        # chain.BuildIndex(index_variable)
        if allowed_friend_branches is not None:
            friend_chain.SetBranchStatus("*",0)
            for b in allowed_friend_branches:
                friend_chain.SetBranchStatus(b,1)

        nominal_index = r.TTreeIndex(chain,"(runNumber<<13)","eventNumber")
        chain.SetTreeIndex(nominal_index)
        truth_index = r.TTreeIndex(friend_chain,"(runNumber<<13)","eventNumber")
        friend_chain.SetTreeIndex(truth_index)
        chain.AddFriend(friend_chain)
        # friend_chain.AddFriend(chain)

    if isinstance(x_axis_binning,str) == True:
        x_axis_binning = [float(s) for s in x_axis_binning.split(',')]
        x_axis_binning = np.linspace(x_axis_binning[1],x_axis_binning[2],x_axis_binning[0]).tolist()

    r.TH1.AddDirectory(r.kTRUE)
    htemp = r.TH1F(hist_name,"",len(x_axis_binning)-1,array.array('d',x_axis_binning))
    chain.Draw(variable_name+">>"+htemp.GetName()+"", weight, "E")
    try:
        htemp = r.gPad.GetPrimitive(hist_name)
    except ReferenceError:
        print "ERROR: Failed to draw histogram!!! "
        print "     hist_name: ", hist_name
        print "     file_list[0][0]: ",  file_name_list[0][0]
        htemp = r.TH1F(hist_name,"",len(x_axis_binning)-1,array.array('d',x_axis_binning))
        
    if not isinstance(htemp, r.TH1):
        htemp = r.TH1F(hist_name,"",len(x_axis_binning)-1,array.array('d',x_axis_binning))
    r.TH1.AddDirectory(r.kFALSE)
    htemp.SetDirectory(0)
    # print(r.gPad.ls())
    # By default we prevent histograms being stored in local directories
    # Instead they are drawn to the current gPad, so we can retrieve them
    # from there.... ( fuck ROOT, why are you like this!? )

    #let's the user rescale
    htemp.Scale(scale)


    chain.Reset()
    del chain
    
    if friend_name != None:
        friend_chain.Reset()    
        del friend_chain

    return htemp
def extract_stat_error(data_hist):
    """
    Returns a histogram of value unity and error size sqrt(N)/N where N is the 
    number of events in a bin in the data_hists
    """
    stat_error = clear_histogram(data_hist.Clone())
    for i in xrange(1,stat_error.GetNbinsX()+1):
        stat_error.SetBinContent(i,1.0)
        try:
            N = data_hist.GetBinContent(i)
            err = np.sqrt(N)/N
        except: 
            err = 0 
        stat_error.SetBinError(i,  err )

    # 
    return stat_error

# Create some poisson weights 
bs_weights = []
def get_bootstrapped_distribution(file_name_list,
        ntuple_name, 
        variable_name, 
        x_axis_binning,
        weight, 
        bootstrap_weight,
        n_toys,
        scale = 1.0, 
        draw_options = "e", 
        hist_name = "htemp",
        debug_mode=False,
        ):
    """

    Parameters:
        file_name_list: either a list of input files, in which case a TChain will be used to draw from
                        all files in the list, OR a string. If a string then a TTRee will be retrieved
                        and drawn from

        ntuple_name: self evident.
        variable_name: the string that will be drawn in the TTree::Draw command
        weight: weighting factor for each event in the draw command.
        scale:  A factor to change the histograms scale by
        hist_name: the ROOT name of the histogram generated
        friend_name: if set a friend tree is assumed to exist in the file, it will be read nad added as a friend
                     and thus accessible in TTree:Draw commands
        index_variable: a string for the index variable that will be built and used to sync the friend tree and the default ntuple

    return values:
        A list of TH1F histograms that are the bootstrapped result
    """
    if hist_name == "htemp":
        hist_name = "htemp_"+str(time.time())
    # Setup the chains
    r.gROOT.SetBatch()
    chain = r.TChain(ntuple_name)

    # Support both a list of files and an invidiual file
    if not isinstance(file_name_list, list):
        file_name_list = [[file_name_list]]
    r.TH1.SetDefaultSumw2()


    weight_form_string = weight
    # Construct the histograms  
    nominal = r.TH1F(hist_name,"",len(x_axis_binning)-1,array.array('d',x_axis_binning))
    nominal.SetDirectory(0)
    
    bs_hists = []
    for i in xrange(n_toys):
        bs_hists.append(r.TH1F(hist_name+"_toy_"+str(i),"",len(x_axis_binning)-1,array.array('d',x_axis_binning)) )
        bs_hists[-1].SetDirectory(0)
        
    for i, file_list in enumerate(file_name_list):
        for j, file in enumerate(file_list):

            if debug_mode and j > 10:
                continue

            if file[-5:] != ".root":
                continue

            f = open_file(file)

            tree = f.Get(ntuple_name)
            # Construct the weight based upon the reco_cuts:
            weight_formula   = r.TTreeFormula("weight_formula"+str(i)+"_"+str(j), weight_form_string, tree)
            var_formula   = r.TTreeFormula("var_formula"+str(i)+"_"+str(j), variable_name, tree)
            bs_weight_formula = r.TTreeFormula("bs_weight_formula"+str(i)+"_"+str(j), bootstrap_weight, tree)
            for event in tree:
                # Do not bother weith events that fail the cuts
                weight  = weight_formula.EvalInstance(0)
                var = var_formula.EvalInstance(0)
                bs_weight_formula.GetNdata()

                if var == 0.0:
                    continue

                if weight == 0.0 or weight != weight:
                    continue

                # if weight == 0.0 or (weight > -1e-5 and weight < 1e-5):
                #     continue

                # Get the bootstrap weights 
                bs_weight =[] 
                for i in xrange(n_toys):
                    bs_weight.append(float(bs_weight_formula.EvalInstance(i)))

                nominal.Fill(var, weight)
                for i, hist in enumerate(bs_hists):
                    hist.Fill(var, weight*bs_weight[i])

            f.Close()

    return nominal, bs_hists

def convert_list_of_hists_to_heatmap(list_of_hists, x_axis_binning, n_bins = None):
    """
    """
    if n_bins == None:
        n_bins = len(list_of_hists)*3

    min_y, max_y = {},{}
    for bin in xrange(1,len(x_axis_binning)+1):
            min_y[bin] = 99999999
            max_y[bin] = -99999999

    for hist in list_of_hists:
        for bin in xrange(1,hist.GetNbinsX()+1):
            min_y[bin] = min(hist.GetBinContent(bin),min_y[bin])
            max_y[bin] = max(hist.GetBinContent(bin),max_y[bin])

    abs_min_y, abs_max_y = 99999999,-99999999
    for bin in xrange(1,hist.GetNbinsX()+1):
            abs_min_y = min(min_y[bin],abs_min_y)
            abs_max_y = max(max_y[bin],abs_max_y)

    abs_min_y *= 0.75
    abs_max_y *= 1.25
    heatmap = r.TH2F(list_of_hists[-1].GetName() + "_heatmap","",len(x_axis_binning)-1,array.array('d',x_axis_binning),
                                                                 n_bins,abs_min_y,abs_max_y)
    for hist in list_of_hists:
        for bin in xrange(1,hist.GetNbinsX()+1):
            heatmap.Fill( x_axis_binning[bin-1], hist.GetBinContent(bin) )

    heatmap.SetDirectory(0)
    return heatmap

def get_histogram_with_chains(chain,variable_name,x_axis_binning,weight,scale = 1.0, draw_options = "e", hist_name = "htemp" ):
    r.gROOT.SetBatch()
    if hist_name == "htemp":
        hist_name = "htemp_"+str(time.time())
        
    if isinstance(x_axis_binning,list) == True:
        htemp = r.TH1F(hist_name,"",len(x_axis_binning)-1,array.array('d',x_axis_binning))
        chain.Draw(variable_name+">>"+hist_name+"", weight, draw_options)
    else:
        chain.Draw(variable_name+">>"+hist_name+"("+x_axis_binning+")", weight, draw_options)

    #retrive the histogram from ROOT and free it from this instance in memory
    htemp = r.gDirectory.Get(hist_name)
    assert isinstance(htemp,r.TH1F),( "ERROR: Failed to open get histogram with variable expression",variable," from files", file_list," is type ",type(htemp))
    htemp.SetDirectory(0)

    #let's the user rescale
    htemp.Scale(scale)
    return htemp

#
# Draws a 2D histogram histogram using TTree::Daw
#
# Note: Suggest always setting histname to avoid battling ROOTs memory management system
def get_2d_histogram(file_name_list,
                     ntuple_name,
                     variable_name,
                     x_axis_binning,
                     y_axis_binning,
                     weight,
                     scale = 1.0,
                     friend_name = None,
                     index_variable = None,
                     draw_options = "",
                     hist_name    = "htemp",
                     allowed_branches=None,
                     allowed_friend_branches=None,
                     debug_mode=False,
                     # allowed_branches = ["runNumber","eventNumber", "mc_generator_weights", "weight_mc", "weight_leptonSF", "weight_jvt", "weight_bTagSF_MV2c10_85", "weight_pileup", "weight_pileup*weight_jvt_UP", "weight_pileup*weight_jvt_UP", "weight_pileup*weight_jvt_DOWN", "weight_leptonSF_EL_SF_Trigger_UP", "weight_leptonSF_EL_SF_Trigger_DOWN", "weight_leptonSF_EL_SF_Reco_UP", "weight_leptonSF_EL_SF_Reco_DOWN", "weight_leptonSF_EL_SF_ID_UP", "weight_leptonSF_EL_SF_ID_DOWN", "weight_leptonSF_EL_SF_Isol_UP", "weight_leptonSF_EL_SF_Isol_DOWN", "weight_leptonSF_MU_SF_Trigger_STAT_UP", "weight_leptonSF_MU_SF_Trigger_STAT_DOWN", "weight_leptonSF_MU_SF_ID_STAT_UP", "weight_leptonSF_MU_SF_ID_STAT_DOWN", "weight_leptonSF_MU_SF_Isol_STAT_UP", "weight_leptonSF_MU_SF_Isol_STAT_DOWN", "weight_leptonSF_MU_SF_Trigger_SYST_UP", "weight_leptonSF_MU_SF_Trigger_SYST_DOWN", "weight_leptonSF_MU_SF_ID_SYST_UP", "weight_leptonSF_MU_SF_ID_SYST_DOWN", "weight_leptonSF_MU_SF_Isol_SYST_UP", "weight_leptonSF_MU_SF_Isol_SYST_DOWN", "weight_pileup_UP", "weight_pileup_DOWN", "weight_bTagSF_MV2c10_85_eigenvars_B_up", "weight_bTagSF_MV2c10_85_eigenvars_B_down", "weight_bTagSF_MV2c10_85_eigenvars_C_up", "weight_bTagSF_MV2c10_85_eigenvars_C_down", "weight_bTagSF_MV2c10_85_eigenvars_Light_up", "weight_bTagSF_MV2c10_85_eigenvars_Light_down", "weight_bTagSF_MV2c10_85_extrapolation_up", "weight_bTagSF_MV2c10_85_extrapolation_down", "weight_bTagSF_MV2c10_85_extrapolation_from_charm_up", "weight_bTagSF_MV2c10_85_extrapolation_from_charm_down", "jet_pt", "mujets_2015", "mujets_2016", "g_pt", "ejets_2015","ejets_2016","top_lep_pt","top_had_pt","theta_d","theta_d_had","theta","theta_had","top_lep_m","top_had_m",], 
                     # allowed_friend_branches = ["runNumber","eventNumber", "mc_generator_weights", "weight_mc", "weight_leptonSF", "weight_jvt", "weight_bTagSF_MV2c10_85", "weight_pileup", "weight_pileup*weight_jvt_UP", "weight_pileup*weight_jvt_UP", "weight_pileup*weight_jvt_DOWN", "weight_leptonSF_EL_SF_Trigger_UP", "weight_leptonSF_EL_SF_Trigger_DOWN", "weight_leptonSF_EL_SF_Reco_UP", "weight_leptonSF_EL_SF_Reco_DOWN", "weight_leptonSF_EL_SF_ID_UP", "weight_leptonSF_EL_SF_ID_DOWN", "weight_leptonSF_EL_SF_Isol_UP", "weight_leptonSF_EL_SF_Isol_DOWN", "weight_leptonSF_MU_SF_Trigger_STAT_UP", "weight_leptonSF_MU_SF_Trigger_STAT_DOWN", "weight_leptonSF_MU_SF_ID_STAT_UP", "weight_leptonSF_MU_SF_ID_STAT_DOWN", "weight_leptonSF_MU_SF_Isol_STAT_UP", "weight_leptonSF_MU_SF_Isol_STAT_DOWN", "weight_leptonSF_MU_SF_Trigger_SYST_UP", "weight_leptonSF_MU_SF_Trigger_SYST_DOWN", "weight_leptonSF_MU_SF_ID_SYST_UP", "weight_leptonSF_MU_SF_ID_SYST_DOWN", "weight_leptonSF_MU_SF_Isol_SYST_UP", "weight_leptonSF_MU_SF_Isol_SYST_DOWN", "weight_pileup_UP", "weight_pileup_DOWN", "weight_bTagSF_MV2c10_85_eigenvars_B_up", "weight_bTagSF_MV2c10_85_eigenvars_B_down", "weight_bTagSF_MV2c10_85_eigenvars_C_up", "weight_bTagSF_MV2c10_85_eigenvars_C_down", "weight_bTagSF_MV2c10_85_eigenvars_Light_up", "weight_bTagSF_MV2c10_85_eigenvars_Light_down", "weight_bTagSF_MV2c10_85_extrapolation_up", "weight_bTagSF_MV2c10_85_extrapolation_down", "weight_bTagSF_MV2c10_85_extrapolation_from_charm_up", "weight_bTagSF_MV2c10_85_extrapolation_from_charm_down", "jet_pt", "mujets_2015", "mujets_2016", "g_pt", "ejets_2015","ejets_2016","top_lep_pt","top_had_pt","theta_d","theta_d_had","theta","theta_had","top_lep_m","top_had_m",], 

                     ):
    # Give all histograms a unique name if they're left as a default 
    if hist_name == "htemp":
        hist_name = "htemp_"+str(time.time())
 
    r.gROOT.SetBatch()
    r.TH1.SetDefaultSumw2()

    if not isinstance(file_name_list, list):
        file_name_list = [[file_name_list]]        

    # Construct the required the binning 
    variable_binning_x = not isinstance(x_axis_binning,str)
    variable_binning_y = not isinstance(y_axis_binning,str)
    weight += "*(" + index_variable + " == " + friend_name + "." + index_variable + ")"
    if not variable_binning_x:
        x_axis_binning = [float(s) for s in x_axis_binning.split(',')]
        x_axis_binning = np.linspace(x_axis_binning[1],x_axis_binning[2],x_axis_binning[0]).tolist()
    if not variable_binning_y:
        y_axis_binning = [float(s) for s in y_axis_binning.split(',')]
        y_axis_binning = np.linspace(y_axis_binning[1],y_axis_binning[2],y_axis_binning[0]).tolist()

    htemp = r.TH2F(hist_name,"",len(x_axis_binning)-1,array.array('d',x_axis_binning),len(y_axis_binning)-1,array.array('d',y_axis_binning))
    for file_list in file_name_list:
        for n, file in enumerate(file_list):
            if debug_mode and n > 2:
                continue
                
            if file[-5:] != ".root":
                continue
            chain = r.TChain(ntuple_name)

            # Support both a list of files and an invidiual file
            chain.Add(file)
            if allowed_branches is not None:
                chain.SetBranchStatus("*",0)
                for b in allowed_branches:
                    chain.SetBranchStatus(b,1)

            # Check to see if the user is attempting to access variables from a friended tree
            if friend_name != None:
                friend_chain = r.TChain(friend_name)
                friend_chain.Add(file)
                if allowed_friend_branches is not None:
                    friend_chain.SetBranchStatus("*",0)
                    for b in allowed_friend_branches:
                        friend_chain.SetBranchStatus(b,1)


                nominal_index = r.TTreeIndex(chain,"(runNumber<<13)","eventNumber")
                chain.SetTreeIndex(nominal_index)
                truth_index = r.TTreeIndex(friend_chain,"(runNumber<<13)","eventNumber")
                friend_chain.SetTreeIndex(truth_index)
                chain.AddFriend(friend_chain)

            chain.Draw(variable_name, weight, draw_options)

            # By default we prevent histograms being stored in local directories
            # Instead they are drawn to the current gPad, so we can retrieve them
            # from there.... ( fuck ROOT, why are you like this!? )
            graph = r.gPad.GetPrimitive("Graph")
            for i in range(graph.GetN()):
                htemp.Fill(graph.GetX()[i], graph.GetY()[i])

            chain.Reset()
            if friend_name != None:
                friend_chain.Reset()    
                del friend_chain
            graph.Delete()
            del graph
            del chain

    # Set the name             
    htemp.SetName(hist_name)

    #retrive the histogram from ROOT and free it from this instance in memory
    # assert isinstance(htemp,r.TH2F) or isinstance(htemp,r.TProfile) ,( "ERROR: Failed to open get histogram with variable expression",variable_name," from files", file_list," is type ",type(htemp))
    htemp.SetDirectory(0)

    #let's the user rescale
    htemp.Scale(scale)
    return htemp


# #
# # Draws a 2D histogram histogram using TTree::Daw
# #
# # Note: Suggest always setting histname to avoid battling ROOTs memory management system
# def get_2d_histogram(file_name_list,
#                      ntuple_name,
#                      variable_name,
#                      x_axis_binning,
#                      y_axis_binning,
#                      weight,
#                      scale = 1.0,
#                      friend_name = None,
#                      index_variable = None,
#                      draw_options = "",
#                      hist_name    = "htemp"):
#     if hist_name == "htemp":
#         hist_name = "htemp_"+str(time.time())
        

#     r.gROOT.SetBatch()

#     chain = r.TChain(ntuple_name)

#     # Support both a list of files and an invidiual file
#     if isinstance(file_name_list, list):
#         for file_list in file_name_list:
#             for file in file_list:
#                 chain.Add(file)
#     else:
#         chain.Add(file_name_list)
#     r.TH1.SetDefaultSumw2()

#     # Check to see if the user is attempting to access variables from a friended tree
#     if friend_name != None:
#         assert index_variable != None,"ERROR: index_variable must be set when using a friend tree"

#         friend_chain = r.TChain(friend_name)
#         if isinstance(file_name_list, list):
#             for file_list in file_name_list:
#                 for file in file_list:
#                     friend_chain.Add(file)
#         else:
#             friend_chain.Add(file_name_list)

#         nominal_index = r.TTreeIndex(chain,"(runNumber<<13)","eventNumber")
#         chain.SetTreeIndex(nominal_index)
#         truth_index = r.TTreeIndex(friend_chain,"(runNumber<<13)","eventNumber")
#         friend_chain.SetTreeIndex(truth_index)
#         # friend_chain.BuildIndex(index_variable)
#         # chain.BuildIndex(index_variable)
#         chain.AddFriend(friend_chain)

#     #are we handing lists as binning or text ?
#     variable_binning_x = not isinstance(x_axis_binning,str)
#     variable_binning_y = not isinstance(y_axis_binning,str)
#     weight += "*(" + index_variable + " == " + friend_name + "." + index_variable + ")"

#     if not variable_binning_x:
#         x_axis_binning = [float(s) for s in x_axis_binning.split(',')]
#         x_axis_binning = np.linspace(x_axis_binning[1],x_axis_binning[2],x_axis_binning[0]).tolist()
#         # print "CONVERTED X-AXIS BINNING: ", x_axis_binning

#     if not variable_binning_y:
#         y_axis_binning = [float(s) for s in y_axis_binning.split(',')]
#         y_axis_binning = np.linspace(y_axis_binning[1],y_axis_binning[2],y_axis_binning[0]).tolist()
#         # print "CONVERTED Y-AXIS BINNING: ", y_axis_binning

#     # print "[get_histogram() ]  X-AXIS BINNING: ", x_axis_binning
#     # print "[get_histogram() ]  Y-AXIS BINNING: ", y_axis_binning

#     htemp = r.TH2F(hist_name,"",len(x_axis_binning)-1,array.array('d',x_axis_binning),len(y_axis_binning)-1,array.array('d',y_axis_binning))
#     chain.Draw(variable_name, weight, draw_options)

#     # By default we prevent histograms being stored in local directories
#     # Instead they are drawn to the current gPad, so we can retrieve them
#     # from there.... ( fuck ROOT, why are you like this!? )
#     graph = r.gPad.GetPrimitive("Graph")
#     for i in range(graph.GetN()):
#         htemp.Fill(graph.GetX()[i], graph.GetY()[i])
#     htemp.SetName(hist_name)

#     #retrive the histogram from ROOT and free it from this instance in memory
#     # assert isinstance(htemp,r.TH2F) or isinstance(htemp,r.TProfile) ,( "ERROR: Failed to open get histogram with variable expression",variable_name," from files", file_list," is type ",type(htemp))
#     htemp.SetDirectory(0)

#     #let's the user rescale
#     htemp.Scale(scale)

#     del graph

#     chain.Reset()
#     del chain

#     if friend_name != None:
#         friend_chain.Reset()    
#         del friend_chain


#     return htemp

def retrive_hist(file_name,hist_name):
    ftemp    = open_file(file_name)
    if not isinstance(ftemp,r.TFile):
        print "ERROR: Failed to open file", file_name
        exit(-1)

    htemp = ftemp.Get(hist_name)
    if not isinstance(htemp,r.TH1F) and not isinstance(htemp, r.TH1D) and not isinstance(htemp, r.TH2D) and not isinstance(htemp, r.TH2F) and not isinstance(htemp, r.TGraph) and not isinstance(htemp, r.TGraphAsymmErrors) and not isinstance(htemp, r.THStack) and not isinstance(htemp, r.RooUnfoldBayes) and not isinstance(htemp, r.RooUnfoldResponse) :
        print "ERROR: Couldn't get hist: ", hist_name
        print "\t IS of type: ", type(htemp)
        print "\t From file", file_name
        print "\t Exiting..."
        exit(-1)

    # Prevent python deleting the loaded obects when they exit scope.
    if not isinstance(htemp,r.THStack) and not isinstance(htemp, r.RooUnfoldBayes)  and not isinstance(htemp, r.RooUnfoldResponse) and not isinstance(htemp, r.TGraphAsymmErrors) and not isinstance(htemp,  r.TGraph):
        htemp.SetDirectory(0)
    else:
        r.SetOwnership(htemp,0)

    ftemp.Close()
    return htemp

def normalize_histogram(hist, correct_uncert = False, divide_by_bin_width=False):
    integral = hist.Integral()
    if integral == 0:
        integral = 1
    hist.Scale(1.0/integral)

    if divide_by_bin_width:
        hist.Scale(1.0, "width")

    if correct_uncert:
        for i in xrange(1,hist.GetSize()):
            # hist.SetBinContent(i, hist.GetBinContent(i)/integral)
            hist.SetBinError(i, (hist.GetBinError(i)/integral ) )

    return hist

def extract_errors_to_hist(hist):
    """
        Takes the  uncertainty on each bin and sets them as the bin content
        of the error_hist
    """
    error_hist = hist.Clone()
    error_hist.SetDirectory(0)

    for i in xrange(1, hist.GetSize()):
        error_hist.SetBinContent(i,hist.GetBinError(i))
        error_hist.SetBinError(i,0)

    return error_hist

def normalize_migration_matrix(migration_matrix):
    n_cols = migration_matrix.GetXaxis().GetNbins()

    for j in range(1,migration_matrix.GetYaxis().GetNbins()+1):
         norm = migration_matrix.Integral(0,n_cols,j,j)

         #protectiong against zero particle level events in a row
         if norm == 0:
             norm = 1.0

         for i in range(1,migration_matrix.GetXaxis().GetNbins()+1):
             migration = migration_matrix.GetBinContent(i,j)
             migration *= 1.0/norm
             migration_matrix.SetBinContent(i,j,migration)
    return migration_matrix

def normalize_correlation_matrix(cov_matrix):
    n_cols = cov_matrix.GetXaxis().GetNbins()

    for j in range(1,cov_matrix.GetYaxis().GetNbins()+1):
         for i in range(1,cov_matrix.GetXaxis().GetNbins()+1):
            # Correlation_ij = Covariance_ij/sqrt(Covariance_ii*Covariance_jj)
             norm = np.sqrt(cov_matrix.GetBinContent(j,j)*cov_matrix.GetBinContent(i,i))
             #protectiong against zero particle level events in a row
             if norm == 0:
                 norm = 1.0
             corr = cov_matrix.GetBinContent(i,j)
             corr *= 1.0/norm
             cov_matrix.SetBinContent(i,j,corr)
    return cov_matrix

def times_by_bin_center(hist):
    hist = hist.Clone()
    hist.SetDirectory(0)
    for i in xrange(0,hist.GetSize()):
        hist.SetBinContent(i,hist.GetBinContent(i)*hist.GetBinCenter(i))
    return hist

def remove_errors(hist):
    hist = hist.Clone()
    hist.SetDirectory(0)
    for i in xrange(0,hist.GetSize()):
        hist.SetBinError(i,0)
    return hist

def hide_root_infomessages():
    r.gErrorIgnoreLevel = 1001#r.kPrint
    r.gROOT.SetBatch()

def evaluate_ratio_histogram(numerator_hist,denonimator_hist):
    ratio_hist = numerator_hist.Clone("ratio_"+numerator_hist.GetName()+"_"+str(time.time()))
    ratio_hist.Divide(denonimator_hist)
    ratio_hist.SetDirectory(0)
    return ratio_hist

def shift_hist_yaxis(hist, amount):
    shifted_hist = hist.Clone()
    for i in xrange(1,shifted_hist.GetSize()):
        shifted_hist.SetBinContent(i,shifted_hist.GetBinContent(i)+amount)
    shifted_hist.SetDirectory(0)
    return shifted_hist

def bin_by_bin_divide_histogram(numerator_hist,denonimator_hist):
    ratio_hist = numerator_hist.Clone("ratio_"+numerator_hist.GetName())
    for i in xrange(1, numerator_hist.GetSize()):
        try:
            new_content = numerator_hist.GetBinContent(i)/denonimator_hist.GetBinContent(i)
        except ZeroDivisionError:
            new_content = 0.0

        ratio_hist.SetBinContent(i,new_content )

    ratio_hist.SetDirectory(0)
    return ratio_hist

def GetQuantiles(hist,quants):
    """
        takes a LIST of quantiles and returns an array of the corresponding x coordinates
        of the input histogram hist
    """
    quants = array.array('d', [ quant for quant in quants])
    q = array.array('d', [0.0]*len(quants))
    hist.GetQuantiles(len(quants), q, quants)
    return q

def combine_bincenters_in_quadrature(hists):
    combination = hists[0].Clone()

    for i in xrange(1,combination.GetSize()):
        total = 0.0
        for h in hists:
          total += h.GetBinContent(i)**2

        total = sqrt(total)
        combination.SetBinContent(i,total)
        combination.SetBinError(i,0)
    combination.SetDirectory(0)
    return combination

def clear_histogram(hist):
    for i in xrange(0,hist.GetSize()):
        hist.SetBinContent(i,0)
        hist.SetBinError(i,0)
    return hist

def apply_stress(hist,stress):
    """
        Stresses a distribution, such that the first bin is increased by a factor 1, and the last by 1 + stress
    """
    stressed_hist = hist.Clone()
    stressed_hist.SetDirectory(0)

    n_bins = hist.GetSize()
    for i in xrange(1,n_bins):
        stressed_hist.SetBinContent(i, hist.GetBinContent(i)*(1.0 + stress*float(i)/float(n_bins) ))

    return stressed_hist

def evaluate_ratio_histograms(histograms, additional_ratio_graphs = {},eval_index=3 ):
    '''
        brief: turns a dictionary of histograms and key for the denominator histogram into a dictionary of ratio plots

        histograms: an dictionary of tuples such that
                    {
                     histogram_name: (histogram, style_options, ratio_style_options, ratio_hist_string),
                     histogram_2_name: (histogram_2, style_option_2, ratio_style_options_2, ratio_hist_string),
                    }
                    style_otion is an instance of the above class StyleOptions, name is a string and histogram is a TH1F
                    ratio_hist_string is the name of the histogram to divide this one by, if it's None then the histogram is not added to the returned histograms

        evaluate_ratio_histograms: an dictionary of tuples that are not to be processed. Instead they are passed for inclusion of the max_y,min_y limit
                                   Of the format:
                                    {
                                     histogram_name: (histogram, style_options, ratio_style_options, ratio_hist_string),
                                     histogram_2_name: (histogram_2, style_option_2, ratio_style_options_2, ratio_hist_string),
                                    }
                                    style_otion is an instance of the above class StyleOptions, name is a string and histogram is a TH1F
                                    ratio_hist_string is the name of the histogram to divide this one by, if it's None then the histogram is not added to the returned histograms

    '''
    ratio_hists = {}

    max_y,min_y = -1e5, 1e5
    for name in histograms:
        #we don't ever want to evaluate this straight line at y=1.
        denominator_hist_name =  histograms[name][eval_index]
        if denominator_hist_name == None:
            continue

        #ensure we've given sensible parameter to this function
        assert denominator_hist_name in histograms, "ERROR: Denominator histogram '"+denominator_hist_name+"'' not in input dictioanry."
        denominator_hist = histograms[denominator_hist_name][0]

        numerator_hist    = histograms[name][0]
        ratio_style_opts  = histograms[name][2]

        ratio_hists[name] = []
        if not isinstance(numerator_hist,r.THStack):
            r_plot = evaluate_ratio_histogram(numerator_hist,denominator_hist)
        else:
            continue
        r_plot.SetDirectory(0)

        max_y = max(r_plot.GetMaximum(),max_y)
        min_y = min(r_plot.GetMinimum(),min_y)

        ratio_hists[name].append( r_plot)
        ratio_hists[name].append( ratio_style_opts)

    for name in additional_ratio_graphs:
        for i in xrange(1,additional_ratio_graphs[name][0].GetN()-1):
            max_y = max(1+additional_ratio_graphs[name][0].GetErrorYhigh(i) , max_y)
            min_y = min(1-additional_ratio_graphs[name][0].GetErrorYlow(i) , min_y)

    return ratio_hists#, max_y,min_y

def shift_bins(hist, shift_index):
    """
        brief: sequentially moves the all the bins and lables from shift index to 1

        params:
            - hist: TH1 histograms
            - shift_index: Starting index of bin that shall be moved.
    """

    #create acopy and free it from ROOT's memory managemnet
    hist = hist.Clone()
    hist.SetDirectory(0)

    #
    for i in xrange(1,hist.GetSize()-1):
        if i + shift_index <= hist.GetSize()-1:
            # we want to move bins if possible
            new_label   = hist.GetXaxis().GetBinLabel(i+shift_index)
            new_content = hist.GetBinContent(i+shift_index)

            hist.GetXaxis().SetBinLabel(i, new_label)
            hist.SetBinContent(i, new_content)
        else:
            #otherwise, just remove the lable and set the content to zero
            hist.SetBinContent(i,0)
            hist.GetXaxis().SetBinLabel(i,"")

    return hist

