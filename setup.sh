echo "export ML_TOP_ANALYSIS_PATH=$PWD" >> ~/.bash_profile
export TOP_ANALYSIS_PATH=$PWD
echo "export PYTHON_PATH=$ML_TOP_ANALYSIS_PATH:$PYTHON_PATH" >> ~/.bash_profile
export PYTHON_PATH=$TOP_ANALYSIS_PATH:$PYTHON_PATH


if [[ ! -v ROOT_HELPER_FUNCS_PATH ]]; then
        echo "CHECKING OUT ROOT HELPER FUNCTION DEPENDENCY!"
        git clone https://github.com/JacobRawling/RootHelperFunctions.git RootHelperFunctions
        source RootHelperFunctions/setup.sh
fi 

## Should exist 
if [[ ! -v ATLAS_STYLE_PATH ]]; then
	echo "CHECKING OUT ATLAS STYLE DEPENDENCY!" 
	git clone https://github.com/JacobRawling/ATLASStyle.git ATLASStyle
	source ATLASStyle/setup.sh 
fi
