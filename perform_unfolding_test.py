'''
    Author: Jacob Rawling
    Date:

    Utilizes the TopAnalysis.Unfolding package ( ) to create the migration matrix,
    perform the desired type of unfolding, run some tests, save the ouptut in ROOT
    and Journal ready eps fomrat
'''
from TopAnalysis.Unfolding.Unfolder import Unfolder
from TopAnalysis.Unfolding.IdentityTest import IdentityTest
from TopAnalysis.Unfolding.StressTest import StressTest
from TopAnalysis.Unfolding.ClosureTest import ClosureTest
from TopAnalysis.Unfolding.IterationConvergenceTest import IterationConvergenceTest

from TopAnalysis.FakeEvaluator import FakeEvalautor
from TopAnalysis.Unfolding.Systematic import Systematic
from TopAnalysis.SystematicsManager import SystematicsManager
import numpy as np
import config as c
import ROOT as r
from collections import OrderedDict
from TopAnalysis.Backgrounds import background_samples
from unfolder_configs import load_unfolders, print_unfolder_names
import sys 

def print_usage():
    print "USAGE: python2.7 perform_nominal_unfoldering <unfolder_name> <test 1> <(optional) test 2> ....."
    print "       where unfolder_name is one of the following: "
    print_unfolder_names()
    print "       and the test one of the following:"
    print "              all " 
    print "              identity " 
    print "              stress " 
    print "              iteration " 

    sys.exit()

def main():
    # Read in the unfolding region
    # Read in the unfolding region
    unfolder_names = None
    if len(sys.argv) >= 1:
        unfolder_names = sys.argv[1]

    # Read in auxillary settings such as if we are running on AFII or if we're
    # doing  
    if len(sys.argv) >= 2:
        aux_settings = ' '.join(sys.argv[2:])
        tests = sys.argv[2:]
    else:
        aux_settings = ""

    debug_mode = 'debug' in aux_settings


    # Load the unfolder from the configurations we have pre-defined 
    from TopAnalysis.Backgrounds import background_samples, background_debug_samples
    bkg_samples = background_debug_samples if debug_mode else background_samples
    unfolders = load_unfolders([unfolder_names], 
                               additional_samples=c.additional_samples,
                               background_samples=bkg_samples,
                               aux_settings=aux_settings,
                               load_unfolder=True
                               )
    if unfolders == []:
        print "USAGE: python2.7 perform_nominal_unfoldering <unfolder_name 1> (optional_Settings) ..."
        print "       where unfolder_name is one of the following: "
        print_unfolder_names()
        sys.exit()

    for unfolder in unfolders:
        # Choose which systematics to run over, configured correctly (hopefully) behind the
        # Scenes.
        # See SysteamaticsManager.load_systematics and Unfolding.Systematic for details
        # on these are managed
        unfolder.systematics = SystematicsManager.load_systematics(unfolder, selected_systematics = [])
        unfolder = get_tests(tests, unfolder )


        # and check that everything behaviours how it should
        unfolder.run_tests()
        unfolder.draw_tests()

                


def get_tests(test_list, unfolder):
    """
    """
    unfolder.log("TESTS: ", test_list)

    # Load all relevant tests - starting with an identity
    if any("identity" in test for test in test_list) or \
        any("all" in test for test in test_list):
        unfolder.add_test( IdentityTest(name = "IdentityTest") )

    # upward and downward variation in shape of various sizes
    # performed by an event-by-event reweighting based on the truth level variable
    if any("stress" in test for test in test_list) or \
    any("all" in test for test in test_list):
        unfolder.add_test( StressTest(
                                # [-0.15,0.15,-0.25,0.25,-0.35,0.35],# -1.0,1.0],
                                [-0.05,0.05,-0.1,0.1,-0.15,0.15],# -1.0,1.0],
                                name = "Stress_Test",
                                input_mc_tuple   = unfolder.input_mc_tuple,
                                truth_tuple_name = unfolder.truth_tuple_name,
                                truth_level_variable    = unfolder.truth_level_variable,
                                detector_tuple_name     = unfolder.detector_tuple_name,
                                detector_level_variable = unfolder.detector_level_variable,
                                x_axis_binning   = unfolder.x_axis_binning,
                                truth_cuts       = unfolder.truth_cuts,
                                detector_cuts    = unfolder.detector_cuts,
                                detector_weight  = unfolder.detector_weight,
                                truth_weight     = unfolder.truth_weight,
                                reweighting_variable = unfolder.truth_level_variable,
                                n_iterations     = unfolder.n_iterations
                                ) )

    # upward and downward variation in shape,
    # performed by an event-by-event reweighting based on the truth level variable
    if any("iteration" in test for test in test_list) or \
        any("all" in test for test in test_list):
        unfolder.add_test( IterationConvergenceTest(
                                name = "IterationConvergenceTest",
                                input_mc_tuple   = unfolder.input_mc_tuple,
                                truth_tuple_name = unfolder.truth_tuple_name,
                                truth_level_variable    = unfolder.truth_level_variable,
                                detector_tuple_name     = unfolder.detector_tuple_name,
                                detector_level_variable = unfolder.detector_level_variable,
                                x_axis_binning   = unfolder.x_axis_binning,
                                x_axis_title     = unfolder.x_axis_label,
                                y_axis_title     = unfolder.y_axis_label,
                                truth_cuts       = unfolder.truth_cuts,
                                detector_cuts    = unfolder.detector_cuts,
                                detector_weight  = unfolder.detector_weight,
                                truth_weight     = unfolder.truth_weight,
                                ) )


    # upward and downward variation in shape,
    # performed by an event-by-event reweighting based on the truth level variable
    if any("closure" in test for test in test_list):
        unfolder.add_test( ClosureTest(
                                name = "ClosureTest",
                                input_mc_tuple   = unfolder.input_mc_tuple,
                                truth_tuple_name = unfolder.truth_tuple_name,
                                truth_level_variable    = unfolder.truth_level_variable,
                                detector_tuple_name     = unfolder.detector_tuple_name,
                                detector_level_variable = unfolder.detector_level_variable,
                                x_axis_binning   = unfolder.x_axis_binning,
                                # x_axis_binning   = unfolder.x_axis_binning,
                                x_axis_title     = unfolder.x_axis_label,
                                y_axis_title     = unfolder.y_axis_label,
                                truth_cuts       = unfolder.truth_cuts,
                                detector_cuts    = unfolder.detector_cuts,
                                detector_weight  = unfolder.detector_weight,
                                truth_weight     = unfolder.truth_weight,
                                draw_pulls       = True,
                                luminosity       = c.luminosity,
                                mc_luminosity    = unfolder.mc_luminosity_nom
                                ) )
    return unfolder

if __name__ == "__main__":
    main()