import os
from subprocess import call,Popen
import datetime
from TopAnalysis.SystematicsManager import SystematicsManager
import sys
python_script = "/afs/hep.man.ac.uk/u/jrawling/DeadConeAnalysis/TopAnalysisUnfoldingFramework/run_systematic.py"
selection = sys.argv[1]
output_folder = "/afs/hep.man.ac.uk/u/jrawling/DeadConeAnalysis/TopAnalysisUnfoldingFramework/logs/"

current_time = datetime.datetime.now().time()
print current_time.isoformat()
ps = {}

###### Set the number to be submitted at a time and how many jobs are allowed to . simultaneously ####
maxSimulataneousJobs = 10
start_syst = 0#62# 67
systematics = SystematicsManager.select_systematics(["all"])
# systematics = [ "JER_Noise",  "JER_CrossCalib",  "JER_NP0_Up",  "JER_NP0_Down",  "JER_NP1_Up",  "JER_NP2_Up",  "JER_NP2_Down",  "JER_NP3_Up",  "JER_NP3_Down",  "JER_NP4_Up",  "JER_NP4_Down",  "JER_NP5_Up",  "JER_NP6_Up",  "JER_NP6_Down",  "JER_NP7_Up",  "JER_NP7_Down",  "JER_NP8_Up",  "JER_NP8_Down",  ]
# print systematics

i = 0
for syst in systematics:	
    if i < start_syst:
	   print "Skipping ", syst
	   i+=1
	   continue 
    #if i > 72:
    #    break 
        
    logFile=output_folder +syst+".log"
    error_log_file=output_folder +syst+".err"
    # print "Logfile: " + logFile
    args = ["nice","-n","0","python", python_script, selection, "fastsim",syst]
    print ' '.join(args)
    log_file = open(logFile,"wb")
    err_log_file = open(error_log_file,"wb")
    p = Popen(args,stdout=log_file,stderr=err_log_file)
    ps[p.pid] = p
    print "Started job", i
    i += 1 
	    
    while len(ps) >= maxSimulataneousJobs:
        try:
            pid, status = os.wait()
        except OSError:
            print "OSERROR thrown!! "
            maxSimulataneousJobs += 1 
	    break 

        if pid in ps:
            del ps[pid]
            print "Starting next job: " + `i+1`

current_time = datetime.datetime.now().time()
print current_time.isoformat()
print "done"
