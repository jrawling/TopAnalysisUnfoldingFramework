"""
A quick set of scripts to strip a NTuple leaving only selected variables
"""
from __future__ import print_function
import argparse
import os
import re
from collections import OrderedDict
import ROOT as r
import subprocess
import sys
import time
import tempfile
import shutil
import glob
from array import array
import RootHelperFunctions.DrawingHelperFunctions as dhf
import RootHelperFunctions.RootHelperFunctions as rhf
from termcolor import cprint
import ATLASStyle.AtlasStyle as AS
import numpy as np 

# 
data_files = [[
    '/afs/cern.ch/user/j/jrawling/deadcone_topq/slimmed_v0-03-02/data_fake_estimatino_examination/2015_test.root',
    '/afs/cern.ch/user/j/jrawling/deadcone_topq/slimmed_2/data/data16_13TeV_faketest.root',
    '/afs/cern.ch/user/j/jrawling/deadcone_topq/slimmed_2/data/data17_13TeV_v0_03_03_faketest.root'
    ]]

# 
cuts = {}
cuts["inclusive"] = "(ejets_2015==1 || ejets_2016==1 || mujets_2015==1 || mujets_2016==1)*(Length$(nominal_Loose.jet_pt)==5)"
cuts["control_region"] = cuts["inclusive"]+"*(nominal_Loose.g_pt[0]/1e3 >  25)" \
                         "*(nominal_Loose.g_pt[0]/nominal_Loose.top_lep_pt > 0.05)" \
                         "*(nominal_Loose.top_lep_pt/1000 < 100)" \
                         "*(nominal_Loose.top_lep_pt/1000 > 40)" \
                         "*(nominal_Loose.theta < nominal_Loose.theta_had)" \
                         "*(nominal_Loose.top_lep_m/1e3 > 150  && nominal_Loose.top_lep_m/1e3 < 300)" 
cuts["signal_region"] = cuts["inclusive"]+"*(Length$(nominal_Loose.jet_pt)==5)" \
                           "*(nominal_Loose.g_pt[0]/1e3 >  25)" \
                           "*(nominal_Loose.g_pt[0]/nominal_Loose.top_lep_pt > 0.05)" \
                           "*(nominal_Loose.top_lep_pt/1000 > 100)" \
                           "*(nominal_Loose.theta < nominal_Loose.theta_had)" \
                           "*(nominal_Loose.top_lep_m/1e3 > 150  && nominal_Loose.top_lep_m/1e3 < 300)" 
# 
cuts["ejets"] = "(ejets_2015==1 || ejets_2016==1)*(Length$(nominal_Loose.jet_pt)==5)"
cuts["mujets"] = "( mujets_2015==1 || mujets_2016==1)*(Length$(nominal_Loose.jet_pt)==5)"

#
cuts["ejets_control_region"] = cuts["ejets"]+"*(nominal_Loose.g_pt[0]/1e3 >  25)" \
                         "*(nominal_Loose.g_pt[0]/nominal_Loose.top_lep_pt > 0.05)" \
                         "*(nominal_Loose.top_lep_pt/1000 < 100)" \
                         "*(nominal_Loose.top_lep_pt/1000 > 40)" \
                         "*(nominal_Loose.theta < nominal_Loose.theta_had)" \
                         "*(nominal_Loose.top_lep_m/1e3 > 150  && nominal_Loose.top_lep_m/1e3 < 300)" 
cuts["ejets_signal_region"] = cuts["ejets"]+"*(Length$(nominal_Loose.jet_pt)==5)" \
                           "*(nominal_Loose.g_pt[0]/1e3 >  25)" \
                           "*(nominal_Loose.g_pt[0]/nominal_Loose.top_lep_pt > 0.05)" \
                           "*(nominal_Loose.top_lep_pt/1000 > 100)" \
                           "*(nominal_Loose.theta < nominal_Loose.theta_had)" \
                           "*(nominal_Loose.top_lep_m/1e3 > 150  && nominal_Loose.top_lep_m/1e3 < 300)" 

# 
cuts["mujets_control_region"] = cuts["mujets"]+"*(nominal_Loose.g_pt[0]/1e3 >  25)" \
                         "*(nominal_Loose.g_pt[0]/nominal_Loose.top_lep_pt > 0.05)" \
                         "*(nominal_Loose.top_lep_pt/1000 < 100)" \
                         "*(nominal_Loose.top_lep_pt/1000 > 40)" \
                         "*(nominal_Loose.theta < nominal_Loose.theta_had)" \
                         "*(nominal_Loose.top_lep_m/1e3 > 150  && nominal_Loose.top_lep_m/1e3 < 300)" 
cuts["mujets_signal_region"] = cuts["mujets"]+"*(Length$(nominal_Loose.jet_pt)==5)" \
                           "*(nominal_Loose.g_pt[0]/1e3 >  25)" \
                           "*(nominal_Loose.g_pt[0]/nominal_Loose.top_lep_pt > 0.05)" \
                           "*(nominal_Loose.top_lep_pt/1000 > 100)" \
                           "*(nominal_Loose.theta < nominal_Loose.theta_had)" \
                           "*(nominal_Loose.top_lep_m/1e3 > 150  && nominal_Loose.top_lep_m/1e3 < 300)" 
# 
def well_behaved_real_efficiency(param, index, cut, cut_name):
    real_eff = rhf.get_histogram(data_files,
                ntuple_name="nominal_Loose",
                variable_name="e_r["+str(index)+"]",    
                x_axis_binning="30,0,3.0",
                weight="(e_r["+str(index)+"] >= 1.0)*"+cut,
                hist_name=param+"_"+cut_name)
    return ( real_eff.GetEntries() == 0.0)

def positive_definite_a_theta_distribution(param, index, cut,cut_name):
    a_theta_binning = [-1.0, 0.0508475, 0.322034, 0.457627, 0.59322, 0.661017, 0.728814, 0.79661, 0.864407, 1.0]

    a_theta_hist = rhf.get_histogram(data_files, 
                ntuple_name="nominal_Loose",
                variable_name="(nominal_Loose.theta[0]-nominal_Loose.theta_d)/(nominal_Loose.theta[0]+nominal_Loose.theta_d)",
                x_axis_binning=a_theta_binning,
                weight="weight_mm_p["+str(index)+"]*"+cut)

    for i in xrange(1, a_theta_hist.GetNbinsX()+1):
        if a_theta_hist.GetBinContent(i) < 0:
            print("\tParameterization failed: ", param)
            print("\tproblematic A_theta histogram for cut region ", cut_name)
            print("\tBin ", a_theta_hist.GetBinLowEdge(i), " - ", a_theta_hist.GetBinLowEdge(i+1), " has entries: ", a_theta_hist.GetBinContent(i))
            return False
    return True


def positive_lepton_kinematics(param, index, cut,cut_name):
    hist_info = [ 
        ("el_pt[0]/1e3", list(np.linspace(27,500,3))),
        ("el_phi[0]", list(np.linspace(-3.14,3.14,3))),
    ]

    
    for var, binning in hist_info: 
        hist = rhf.get_histogram(data_files, 
                ntuple_name="nominal_Loose",
                variable_name=var,
                x_axis_binning=binning,
                weight="weight_mm_p["+str(index)+"]*"+cut)

        for i in xrange(1, hist.GetNbinsX()+1):
            if hist.GetBinContent(i) < 0:
                print("\tParameterization failed: ", param)
                print("\tproblematic ", var, " histogram for cut region ", cut_name)
                print("\tBin ", hist.GetBinLowEdge(i-1), " - ", hist.GetBinLowEdge(i), " has entries: ", hist.GetBinContent(i))
                return False
    return True


def main():
    parameterizations = [
        'NS:eta',
        'NS:pt',
        'NS:jetpt',
        'NS:dR',
        'NS:dPhi',
        'NS:dR:dPhi',
        'NS:jetpt:dR',
        'NS:jetpt:dR:dPhi',
        'NS:jetpt:dPhi',
        'NS:pt:jetpt:dR',
        'NS:pt:jetpt:dR:dPhi',
        'NS:pt:jetpt:dPhi',
        'NS:pt:dR',
        'NS:pt:dR:dPhi',
        'NS:pt:dPhi',
        'NS:eta:pt',
        'NS:eta:jetpt',
        'NS:eta:dR',
        'NS:eta:dPhi',
        'NS:eta:pt:jetpt',
        'NS:eta:pt:dPhi',
        'NS:eta:pt:dR',
        'NS:eta:pt:jetpt:dR',
        'NS:eta:pt:jetpt:dPhi',
        'NS:eta:pt:jetpt:dR:dPhi',
    ]
    valid_parameteriations = []
    # selected_cuts = ["ejets", "ejets_control_region", "ejets_signal_region"]
    selected_cuts = ["mujets", "mujets_control_region", "mujets_signal_region"]

    for i, p in enumerate(parameterizations):
        print("\nExmamining parameterizaiton: ", p)

        # Check that all real efficiencies are < 1.0 as we expect 
        skip = False
        for cut in selected_cuts:
            if skip:
                continue
            if not well_behaved_real_efficiency(p, i, cuts[cut], cut):
                print("\tParameterization:", p, ' has unphysical real efficiencies in region:', cut )
                skip = True

        if skip:
            continue

        # # Evaluate the key variables behavour 
        skip = False
        for cut in selected_cuts:
            # If we've already failed this criteria then don't bother to continue
            # time consuming evalautions 
            if skip:
                continue 
            
            if not positive_definite_a_theta_distribution(p,i, cuts[cut], cut):
                print("\tParameterization:", p, ' has negative estimations in Atheta for region:', cut )
                skip = True

        # Skip this paramaterization 
        if skip:
            continue


        # # Evaluate the key variables behavour 
        # skip = False
        # for cut in cuts:
        #     # If we've already failed this criteria then don't bother to continue
        #     # time consuming evalautions 
        #     if skip:
        #         continue 
            
        #     if not positive_lepton_kinematics(p,i, cuts[cut], cut):
        #         print("\tParameterization:", p, ' has negative estimations in lepton kinematics for region:', cut )
        #         skip = True

        # # Skip this paramaterization 
        # if skip:
        #     continue
        
        valid_parameteriations.append(p)

    print("Found the following valida parameterizations: ")
    for v in valid_parameteriations:
        print("\t", v)

main()
