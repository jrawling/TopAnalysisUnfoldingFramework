"""
I have tried to make it so all things you might want to change are near the
top now, the obvious candidates are the list of branches to remove at l.48-56,
the selections at l.59-60, the variable editing classes at l.63 and the files
sent to the grid at l.138-139

The way it is checked in it does a RootCore submission as it needs this for the
IFAEReweighting, the RootCore setup must have IFAEReweighting and
NNLOReweighting in it I use /scratch3/bsowden/AnalysisTopQuickStrip for this,
feel free to use the same one. This can be reverted to the old submission by
changing the flag on l.315 (sorry for it being so deep) - Ben Sowden
"""

import argparse
import os
import re
import ROOT
import subprocess
import sys
import time
import tempfile
import shutil
import glob

def stripFiles(outname, files, treeNum = False, inSelection = "") :
  if len(files) == 0:
    print "No files found matching arguments"
    sys.exit(1)

  try :
    os.makedirs(os.path.dirname(outname))
  except os.error :
    pass

  # Branches to remove
  removeBranches = ['ljet_pt', 'ljet_eta', 'ljet_phi', 'ljet_e', 'ljet_m',
                    'ljet_sd12', 'ljet_sd23', 'ljet_tau21', 'ljet_tau32',
                    'ljet_tau21_wta', 'ljet_tau32_wta', 'ljet_D2', 'ljet_C2',
                    'ljet_topTag', 'ljet_bosonTag', 'ljet_topTag_loose',
                    'ljet_bosonTag_loose', 'ljet_topTagN', 'ljet_topTagN_loose',
                    'ljet_bosonTagN', 'ljet_bosonTagN_loose',
                    'ClassifHPLUS*',
                    'NBFricoNN4in_ljets', 'NBFricoNN3ex_ljets',
                    'ClassifBDTOutput*']
  allowed_branches = ['weight_*','eventNumber', 'runNumber', 'randomRunNumber',
                      'el_pt','el_eta','el_phi', 'el_e', 'el_charge', 'el_pt','el_true_isPrompt',
                      'mu_pt','mu_eta','mu_phi', 'mu_e', 'mu_charge', 'mu_pt','mu_true_isPrompt',
                      'met_met', 'mu',
                      'jet_pt','jet_eta','jet_phi', 'jet_isbtagged_MV2c10_85','jet_isbtagged_MV2c10_77',
                      'jet_e',
                      'g_pt',
                      'top_lep_pt','top_lep_eta','top_lep_phi', 'top_lep_m',
                      'top_had_pt','top_had_eta','top_had_phi', 'top_had_m',
                      'theta', 'theta_d', 'theta_had', 'theta_d_had',
                      'W_lep', 'W_had', 'neutrino',
                      'mujets_*','ejets_*',
                      'mc_generator_weights'
                       ]
  # Selections
  selections = [lambda event : len(event.jet_eta) == 5]

  outfile = ROOT.TFile(outname,'RECREATE')
  #outfile.SetCompressionLevel(1)

  trees = getListOfTrees(files)
  for itree,tree in enumerate(trees):

    # Strictly skip these trees 
    if tree in ["truth"]:
        continue 
 

    if type(treeNum) is int and itree != treeNum : continue
    print "-> Processing",tree," - ",itree+1,"out of",len(trees)
    outfile.cd()
    inchain = ROOT.TChain(tree)
    for file in files:
      inchain.Add(file)

    # Protection if tree isn't in file, probably nicer to somehow iterate the file - TODO
    if inchain.GetEntries() == 0:
      print "No entries for tree",tree,"so proceed to next"
      continue
    
    # if tree in ["sumWeights", "PDFsumWeights","AnalysisTracking","particleLevel"]:
    if tree in ["sumWeights", "PDFsumWeights","AnalysisTracking"]:
      tree = inchain.CloneTree() # All events
      tree.Write("",ROOT.TObject.kOverwrite) # Only write one header
      continue

    # Drop all physics variables and only turn on the ones we care about
    inchain.SetBranchStatus("*",0)
    for b in allowed_branches:
      inchain.SetBranchStatus(b,1)

    
    # Remove branches
    branchList = inchain.GetListOfBranches()
    branches = []
    for branch in branchList : branches.append(branch.GetName())
    for branchName in removeBranches :
      for branch in branches :
        if re.search(branchName, branch) :
          inchain.SetBranchStatus(branchName, 0)
          break
    tree = inchain.CloneTree(0)
    # Set the maximum file size to 200GB before it forces a new file to be opened
    tree.SetMaxTreeSize(200000000000)
    totalEvents = inchain.GetEntries()
    totalEventsPer = totalEvents/100
    if totalEventsPer == 0 :
      print "Fewer than 100 events, percentage completion unreliable"
      totalEventsPer = 1
    print "-",totalEvents,"found in tree"

    for i,event in enumerate(inchain):
      if i%(totalEventsPer*10) == 0:
        print "-",i/totalEventsPer,"% done"
      # Check selection(s)
      if any([not selection(event) for selection in selections]) : continue

      tree.Fill()
    outfile = tree.GetCurrentFile()
    outfile.Write()

def create_folder(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

def chunks(l, n):
    n = max(1, n)
    return (l[i:i+n] for i in xrange(0, len(l), n)) 

def getListOfTrees(files) :
  trees = {}
  for fileLocation in files :
    file = ROOT.TFile.Open(fileLocation)
    for mkey in file.GetListOfKeys():
      if issubclass(getattr(ROOT,mkey.GetClassName()),ROOT.TTree) and mkey.GetName() != 'truth' :
        trees[mkey.GetName()] = True

  return sorted(trees.keys())

if __name__ == "__main__":
  in_folder = sys.argv[1] 
  if in_folder[-8:] != "/*.root" and in_folder[-12:] != ".output.root":
    in_folder += "/*"

  out_folder = sys.argv[2]
  chunksize = int(sys.argv[3])
  n_iter = int(sys.argv[4])

  print "\n\n\n\n"
  print "Converting all files in:", in_folder
  print "Dumping into folder: ", out_folder

  # # create the out_folder 
  create_folder(out_folder)

  files= glob.glob(in_folder)
  cleaned_files = []
  for f in files:
    if  f[-5] != ".part":
      cleaned_files.append(f)

  n_chunks = 0 
  for n_c, chunk in enumerate(chunks(cleaned_files, chunksize)):
    n_chunks += 1 
    if n_c == n_iter:
      stripFiles(out_folder+"/out_"+str(n_iter)+".root", chunk, treeNum = False, inSelection = "")
  print "Processed iteration: ", n_iter, " out of ", n_chunks 
