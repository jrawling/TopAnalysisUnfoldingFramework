from __future__ import print_function
import ROOT as r 
import argparse
import os
import re
from collections import OrderedDict
import ROOT as r
import subprocess
import sys
import time
import tempfile
import shutil
import glob
from array import array
import RootHelperFunctions.DrawingHelperFunctions as dhf
import RootHelperFunctions.RootHelperFunctions as rhf
from termcolor import cprint
import ATLASStyle.AtlasStyle as AS
import config as c 
import numpy as np 

class FakeEstimationExaminer:
    def __init__(self,
        output_folder,
        variable,
        x_axis_binning,
        cut_weight,
        fake_tuple_name='nominal_Loose',
        data_files=[[ '/eos/atlas/atlascerngroupdisk/phys-top/deadcone/slimmed_v0-03-02/data/data15_13TeV.root', ] ],
        verbosity=3,
        name = ''
        ):
        self.parameterizations = {'NS:eta': 0, 'NS:eta:pt': 15, 'NS:eta:pt:jetpt': 19, 'NS:dR:dPhi': 5, 'NS:jetpt:dR:dPhi': 7, 'NS:pt:dPhi': 14, 'NS:dR': 3, 'NS:pt:dR': 12, 'NS:jetpt:dPhi': 8, 'NS:eta:jetpt': 16, 'NS:pt:jetpt:dR': 9, 'NS:eta:pt:jetpt:dPhi': 23, 'NS:eta:pt:jetpt:dR:dPhi': 24, 'NS:eta:pt:jetpt:dR': 22, 'NS:pt:jetpt:dPhi': 11, 'NS:pt:jetpt:dR:dPhi': 10, 'NS:pt:dR:dPhi': 13, 'NS:jetpt:dR': 6, 'NS:jetpt': 2, 'NS:dPhi': 4, 'NS:eta:dR': 17, 'NS:eta:pt:dPhi': 20, 'NS:eta:dPhi': 18, 'NS:pt': 1, 'NS:eta:pt:dR': 21}
        # self.parameterizations = {'NS:eta': 0, 'NS:eta:pt': 15, 'NS:eta:pt:jetpt': 19, 'NS:dR:dPhi': 5, 'NS:jetpt:dR:dPhi': 7,}# 'NS:pt:dPhi': 14, 'NS:dR': 3, 'NS:pt:dR': 12, 'NS:jetpt:dPhi': 8, 'NS:eta:jetpt': 16, 'NS:pt:jetpt:dR': 9, 'NS:eta:pt:jetpt:dPhi': 23, 'NS:eta:pt:jetpt:dR:dPhi': 24, 'NS:eta:pt:jetpt:dR': 22, 'NS:pt:jetpt:dPhi': 11, 'NS:pt:jetpt:dR:dPhi': 10, 'NS:pt:dR:dPhi': 13, 'NS:jetpt:dR': 6, 'NS:jetpt': 2, 'NS:dPhi': 4, 'NS:eta:dR': 17, 'NS:eta:pt:dPhi': 20, 'NS:eta:dPhi': 18, 'NS:pt': 1, 'NS:eta:pt:dR': 21}

        # Inputs
        self.data_files = data_files

        # x-bins
        self.output_folder = output_folder +'/'+name+'/'
        rhf.create_folder(self.output_folder)
        self.fake_tuple_name = fake_tuple_name
        self.variable = variable
        self.x_axis_binning = x_axis_binning
        self.cut_weight = cut_weight
        self.verbosity = verbosity

    def create_fake_plot(self, parameterization):
        fake_weight = "weight_mm_q[%d]"%self.parameterizations[parameterization]
        self.log(' Getting fake weight: ',fake_weight)
        return rhf.get_histogram(
                file_name_list = self.data_files,
                ntuple_name = self.fake_tuple_name,
                variable_name = fake_weight,#self.variable,
                x_axis_binning = self.x_axis_binning,
                weight = fake_weight +
                self.cut_weight,
                hist_name="fake_est_" + parameterization,
            )

    def construct_fake_estimates(self):
        self.fake_estimate, self.fake_estimate_plotting_dict  = {}, OrderedDict()

        # For each paramaterization create a distribution of the variable we care
        # about 
        for i, p in enumerate(self.parameterizations):
            self.fake_estimate[p] = self.create_fake_plot(p).Clone()
            self.fake_estimate[p].SetDirectory(0)

            # It's useful to also create a plotting dictionary to display these 
            # things 
            self.fake_estimate_plotting_dict[p] = [
                    self.fake_estimate[p],
                    dhf.get_line_opts(r.TColor.GetColor(0.2, float(i)/len(self.parameterizations) , 0.2 ))
                ]


    def construct_bin_varaition(self):
        # For each bin of the signal variable find the distribution of the fake
        # estimates 
        self.fake_estimate_spreads, self.bin_by_bin_spread = [], []

        # Iterate over the chosen binning to constr
        for i in xrange(1,len(self.x_axis_binning)):
            self.fake_estimate_spreads.append([])

            for j, p in enumerate(self.parameterizations):
                self.fake_estimate_spreads[-1].append( self.fake_estimate[p].GetBinContent(i))
            

            # Create a histogram that shows the spread of the fake estimates
            self.bin_by_bin_spread.append(r.TH1F("bin_"+str(i),"bin_"+str(i),
                    20,
                    np.min(self.fake_estimate_spreads[-1]),
                    np.max(self.fake_estimate_spreads[-1]),
                    ))
            self.bin_by_bin_spread[-1].SetDirectory(0)

            # Now fill with each fake estimate 
            for f in self.fake_estimate_spreads[-1]:
                self.bin_by_bin_spread[-1].Fill(f)
            range_string = "%.3f < A_{#theta} #leq %.3f"%(self.x_axis_binning[i-1], self.x_axis_binning[i] )
            self.log(range_string, ': ', self.fake_estimate_spreads[-1] )

    def log(self, *args):
        if self.verbosity == 0:
            return

        total_message = "[ FAKER ] - "
        for msg in args:
            total_message += " " + str(msg)
        print(total_message)

    def draw_fake_estimates(self):
        # First draw the key variable 
        rhf.create_folder(self.output_folder+"bin_by_bin_spread/")
        AS.SetAtlasStyle()
        canvas = r.TCanvas("fake_estimate_bin_by_binspread",
                           "fake_estimate_bin_by_binspread",
                           800,600) 
        r.SetOwnership(canvas, 0)
        AS.SetAtlasStyle()
        dhf.plot_histogram(canvas,
                    self.fake_estimate_plotting_dict,
                    x_axis_title="Leptonic A_{#theta}",
                    y_axis_title="Number of events",
                    do_atlas_labels = True
                    )

        canvas.Print(self.output_folder+"spread_hists.eps")
        canvas.Print(self.output_folder+"spread_hists.png")
    def draw_bin_by_bin_variation(self):

        rhf.create_folder(self.output_folder+"bin_by_bin_spread/")
        rhf.create_folder(self.output_folder+"bin_by_bin_spread/")
        AS.SetAtlasStyle()
        canvas = r.TCanvas("fake_estimate_bin_by_binspread",
                           "fake_estimate_bin_by_binspread",
                           800,600) 
        r.SetOwnership(canvas, 0)
        AS.SetAtlasStyle()


        for i in xrange(1,len(self.x_axis_binning)):
            range_string = "%.3f < A_{#theta} #leq %.3f"%(self.x_axis_binning[i-1], self.x_axis_binning[i] )

            mu  = np.mean(self.fake_estimate_spreads[i-1])
            std = np.std(self.fake_estimate_spreads[i-1])
            fit_string = "#mu #pm #sigma = %.3f #pm %.3f"%(mu,std)

            dhf.data_style_options.x_label_size   = 0.045
            dhf.data_style_options.x_title_size   = 0.045
            dhf.data_style_options.x_title_offset   = 1.45
            dhf.data_style_options.legend_options   = None
            dhf.plot_histogram(canvas,
                   {'bin_%d'%(i): [ self.bin_by_bin_spread[i-1], dhf.data_style_options]},
                   x_axis_title="Fake estimate prediction",
                   y_axis_title="Number of parameterizations",
                   do_atlas_labels = True,
                   labels = [range_string, fit_string]
                   )

            canvas.Print(self.output_folder+"bin_by_bin_spread/fake_estimate_spread_%d.eps"%i)
            canvas.Print(self.output_folder+"bin_by_bin_spread/fake_estimate_spread_%d.png"%i)

cuts = {}
cuts["ejets_signal_region"] = "*(ejets_2015 || ejets_2016)" 
cuts["mujets_signal_region"] = "*(mujets_2015 || mujets_2016)" 
sig_region = "*(Length$(nominal_Loose.jet_pt)==5)" \
             "*(nominal_Loose.g_pt[0]/1e3 >  25)" \
             "*(nominal_Loose.g_pt[0]/nominal_Loose.top_lep_pt > 0.05)" \
             "*(nominal_Loose.top_lep_pt/1000 > 110)" \
             "*(nominal_Loose.theta < nominal_Loose.theta_had)" \
             "*(nominal_Loose.top_lep_m/1e3 > 150  && nominal_Loose.top_lep_m/1e3 < 300)" 

cuts["ejets_signal_region"] += sig_region
cuts["mujets_signal_region"] += sig_region

data_files=[['/eos/atlas/atlascerngroupdisk/phys-top/deadcone/slimmed_v0-03-02/data/data15_13TeV.root',
             '/eos/atlas/atlascerngroupdisk/phys-top/deadcone/slimmed_v0-03-02/data/data16_13TeV.root',
             '/eos/atlas/atlascerngroupdisk/phys-top/deadcone/slimmed_v0-03-02/data/data17_13TeV.root',
            ]]   
data_files=[['/eos/atlas/atlascerngroupdisk/phys-top/deadcone/slimmed_2/data/data15_13TeV_v0_03_03_test.root',
            ]]   

for cut_name in  ["ejets_signal_region","mujets_signal_region"]:
    fake_estimator = FakeEstimationExaminer(
        output_folder=c.fake_estimation_output_folder,
        cut_weight=cuts[cut_name],
        variable="(nominal_Loose.theta[0]-nominal_Loose.theta_d)/(nominal_Loose.theta[0]+nominal_Loose.theta_d)",
        x_axis_binning=[-1.0, 0.0508475, 0.322034, 0.457627, 0.59322, 0.661017, 0.728814, 0.79661, 0.864407, 1.0],
        data_files=data_files,
        name=cut_name
        )

    fake_estimator.construct_fake_estimates()
    fake_estimator.construct_bin_varaition()
    fake_estimator.draw_fake_estimates()
    fake_estimator.draw_bin_by_bin_variation()

