#setup the environment
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
current_dri=$PWD
echo 'current_dri ', $current_dri
cd /afs/cern.ch/work/j/jrawling/public/TopFkaesTest/
source /afs/cern.ch/work/j/jrawling/public/TopFkaesTest/setup_slim.sh
cd $current_dri

echo "INPUT_PATH: ", $INPUT_PATH
echo "PYTHONPATH: ", $PYTHONPATH
echo "in_path:", $in_path 
echo "out_path: ", $out_path
echo "name", $name
echo "plot_dump_folder", $plot_dump_folder

### run the command
# python $INPUT_PATH/helper_scripts/quickstrip_data.py $in_path $out_path $name $plot_dump_folder