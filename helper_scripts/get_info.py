#!/usr/bin/env python
import TopExamples.analysis
import MC15c_TOPQ1
import MC15c_TOPQ1_dc_samples
import MC15_TOPQ1_systematics
import os
import sys
import subprocess

directory = '/eos/user/j/jrawling/DeadConeTuples/v0-00-20/'
# inputs = [
#         "mc15_13TeV.410011.PowhegPythiaEvtGen_P2012_singletop_tchan_lept_top.merge.DAOD_TOPQ1.e3824_s2608_s2183_r7725_r7676_p3138",
#         "mc15_13TeV.410012.PowhegPythiaEvtGen_P2012_singletop_tchan_lept_antitop.merge.DAOD_TOPQ1.e3824_s2608_s2183_r7725_r7676_p3138",
#         "mc15_13TeV.410013.PowhegPythiaEvtGen_P2012_Wt_inclusive_top.merge.DAOD_TOPQ1.e3753_s2608_s2183_r7725_r7676_p3138",
#         "mc15_13TeV.410014.PowhegPythiaEvtGen_P2012_Wt_inclusive_antitop.merge.DAOD_TOPQ1.e3753_s2608_s2183_r7725_r7676_p3138",
#         "mc15_13TeV.410025.PowhegPythiaEvtGen_P2012_SingleTopSchan_noAllHad_top.merge.DAOD_TOPQ1.e3998_s2608_s2183_r7725_r7676_p3138",
#         "mc15_13TeV.410026.PowhegPythiaEvtGen_P2012_SingleTopSchan_noAllHad_antitop.merge.DAOD_TOPQ1.e3998_s2608_s2183_r7725_r7676_p3138",
# ]
names = [    
   'TOPQ1_singleTop_PowPy6', 
   'TOPQ1_ttV',
   'TOPQ1_Wjets_Sherpa221',
   'TOPQ1_Zjets_Sherpa221',
   'TOPQ1_Diboson_Sherpa',
   "TOPQ1_HardScatter",
   "TOPQ1_Fragmentation_Hadronization",
   "TOPQ1_rad_high",
   "TOPQ1_rad_low",
   #"TRUTH1_ttbar_dc_off",
   #"TRUTH1_ttbar_dc_off_410501",
   #"TRUTH1_ttbar_dc_on",
   "TOPQ1_ttbar_PowPy8",
    ]

sample_names = {}
sample_names["TOPQ1_HardScatter"] = ["ttbar_modelling_sample"]
sample_names["TOPQ1_Fragmentation_Hadronization"] = ["ttbar_frag_var_sample"]
sample_names["TOPQ1_rad_high"] = ["ttabar_rad_high_sample"]
sample_names["TOPQ1_rad_low"] = ["ttabar_rad_low_sample"]
sample_names["TOPQ1_Zjets_Sherpa221"] = ["z_jets_samples"]
sample_names["TOPQ1_Wjets_Sherpa221"] = ["w_jets_samples"]
sample_names["TOPQ1_singleTop_PowPy6"] = ["singletop_sample"]
sample_names["TOPQ1_Diboson_Sherpa"] = ["diboson_sample"]
sample_names["TOPQ1_ttV"] = ["ttV_sample"]
sample_names["TRUTH1_ttbar_dc_off"] = ["ttbar_dc_off_sample"]
sample_names["TRUTH1_ttbar_dc_off_410501"] = ["ttbar_powhegpythia_dc_off"]
sample_names["TRUTH1_ttbar_dc_on"] = ["ttbar_dc_on_sample"]
sample_names["TOPQ1_ttbar_PowPy8"] = ["ttbar_sample"]

filename = 'XSection-MC15-13TeV.data'

for sample in TopExamples.grid.Samples(names):
    print "RUNNING ON SAMPLE: ", sample.name 


    inputs = sample.datasets
    k_list,x_sec_list,path_list = [],[],[]

    for in_container in inputs:
        id = int(in_container.split('.')[1])
        # print "PROCESSING ID: ", id

        tdp = TopExamples.analysis.TopDataPreparation(os.getenv('ROOTCOREBIN') + '/data/TopDataPreparation/' + filename)

        # print 'Using', tdp.getFilename()
        # print ''
        k_list.append(tdp.getKfactor(id))
        x_sec_list.append(tdp.getRawXsection(id))
        
        p = subprocess.Popen("ls " + directory + "*"+str(id)+"*/*", stdout=subprocess.PIPE, shell=True)
        (output, err) = p.communicate()
        chain_list = output.split('\n')

        try:
            chain_list.remove('')
        except:
            pass 
        path_list.append( chain_list )
        if tdp.hasID(id):
            pass
        #     print 'ID: %s' % id
        #     print '  Cross-section: %f (%f * %f)' % (tdp.getXsection(id), tdp.getRawXsection(id), tdp.getKfactor(id))
        #     print '  Shower       : %s' % tdp.getShower(id)
        else:
            print 'ID: %s not available' % id

    print ( sample_names[sample.name][0] + '.input_file_names = ' + str(path_list))
    print ( sample_names[sample.name][0] + '.k_factors = ' + str(k_list))
    print ( sample_names[sample.name][0] + '.x_secs = ' + str(x_sec_list))

    print ('\n\nValdiation: n_k_factors = '  + str(len(k_list)) + ' n_x_secs = ' + str(len(x_sec_list) ) + ' n_chains: ' + str(len(path_list)) + '\n\n')
    # for path in path_list:
    #     print str(chain) + "\n\n"
    print ( "\n")
    print ( "\n")
    print ( "\n")
