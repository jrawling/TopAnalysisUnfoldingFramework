"""
A quick set of scripts to strip a NTuple leaving only selected variables
"""
from __future__ import print_function
import argparse
import os
import re
from collections import OrderedDict
import ROOT as r
import subprocess
import sys
import time
import tempfile
import shutil
import glob
from array import array
import RootHelperFunctions.DrawingHelperFunctions as dhf
import RootHelperFunctions.RootHelperFunctions as rhf
from termcolor import cprint
import ATLASStyle.AtlasStyle as AS


def lep_met_dphi(event):

    if len(event.mu_eta) == 1:
        lep_phi = event.mu_phi[0]
    else:
        lep_phi = event.el_phi[0]
    # calculate delta phi in the range -pi,+pi
    delta_phi = abs(lep_phi - event.met_phi)
    if delta_phi > r.TMath.Pi():
        delta_phi = 2 * r.TMath.Pi() - delta_phi

    return delta_phi


def min_deltaR_lep_jet(event):

    lep = r.TLorentzVector()
    if len(event.mu_eta) == 1:
        lep.SetPtEtaPhiE(
            event.mu_pt[0] / 1e3,
            event.mu_eta[0],
            event.mu_phi[0],
            event.mu_e[0] / 1e3,
        )
    else:
        lep.SetPtEtaPhiE(
            event.el_pt[0] / 1e3,
            event.el_eta[0],
            event.el_phi[0],
            event.el_e[0] / 1e3,
        )

    min_dR_lj = 9.0e9
    jet_p4 = r.TLorentzVector()
    for i in xrange(len(event.jet_pt)):
        jet_p4.SetPtEtaPhiE(
            event.jet_pt[i] / 1e3,
            event.jet_eta[i],
            event.jet_phi[i],
            event.jet_e[i] / 1e3,
        )
        min_dR_lj = min(min_dR_lj, jet_p4.DeltaR(lep))

    return min_dR_lj


def is_lepton_tight(event):
    # WARNING!!! TESTING THIS REMOVE!!
    # if len(event.mu_eta)==1:
        # return bool(event.mu_isTight[0])
    # return bool(event.el_isTight[0])

    if len(event.mu_eta) == 1:
        return array('c', event.mu_isTight[0])[0] == '\x01'
    return array('c', event.el_isTight[0])[0] == '\x01'

def get_average_real_rate(fake_estimator, p, fake_selection):
    # Ignore the initial NS
    params_to_average = [ 'NS:' + param for param in p.split(':')[1:]]
    avg = 1.0
    for param in params_to_average:
        avg *= fake_estimator[fake_selection+'_'+param].GetRealEff()

    avg**(1.0/len(params_to_average))
    return avg 

def get_average_fake_rate(fake_estimator, p, fake_selection):
    # Ignore the initial NS
    params_to_average = [ 'NS:' + param for param in p.split(':')[1:]]
    avg = 1.0
    for param in params_to_average:
        avg *= fake_estimator[fake_selection+'_'+param].GetFakeEff()

    avg**(1.0/len(params_to_average))
    return avg 

def eval_trigger(event):

    is_data15 = event.runNumber <= 284484
    is_data16 = event.runNumber <= 311481 and event.runNumber > 284484
    is_data17 = event.runNumber >= 325713
    is_el = len(event.el_eta) == 1
    # Straight up don't get this bit of the code.... stolen from Tomas Dado
    trigger = 0
    if is_el:
        if is_data15:
            if event.HLT_e24_lhmedium_L1EM20VH and event.el_trigMatch_HLT_e24_lhmedium_L1EM20VH and event.el_pt[0] <= 61 * 1e3:
                trigger |= 0x1 << 0
            if ((event.HLT_e60_lhmedium and event.el_trigMatch_HLT_e60_lhmedium) or (event.HLT_e120_lhloose and event.el_trigMatch_HLT_e120_lhloose)) and event.el_pt[0] > 61 * 1e3:
                trigger |= 0x1 << 1
        elif is_data16 or is_data17:
            if (event.HLT_e26_lhtight_nod0_ivarloose and event.el_trigMatch_HLT_e26_lhtight_nod0_ivarloose and event.el_pt[0] <= 61 * 1e3):
                trigger |= 0x1 << 0
            if ((event.HLT_e60_lhmedium_nod0 and event.el_trigMatch_HLT_e60_lhmedium_nod0) or (event.HLT_e140_lhloose_nod0 and event.el_trigMatch_HLT_e140_lhloose_nod0)) and event.el_pt[0] > 61 * 1e3:
                trigger |= 0x1 << 1
    else:
        if is_data15:
            if(event.HLT_mu20_iloose_L1MU15 and event.mu_trigMatch_HLT_mu20_iloose_L1MU15 and event.mu_pt[0] <= 51 * 1e3):
                trigger |= 0x1 << 0
            if(event.HLT_mu50 and event.mu_trigMatch_HLT_mu50 and event.mu_pt[0] > 51 * 1e3):
                trigger |= 0x1 << 1
        elif is_data16 or is_data17:
            if(event.HLT_mu26_ivarmedium and event.mu_trigMatch_HLT_mu26_ivarmedium and event.mu_pt[0] <= 51 * 1e3):
                trigger |= 0x1 << 0
            if(event.HLT_mu50 and event.mu_trigMatch_HLT_mu50 and event.mu_pt[0] > 51 * 1e3):
                trigger |= 0x1 << 1
    return trigger


def construct_fakesweights_string(event):
    selection = ''
    if len(event.mu_eta) == 1:
        selection += 'mu_'
    else:
        selection += 'e_'

    is_data15 = event.runNumber <= 284484
    is_data16 = event.runNumber <= 311481 and event.runNumber > 284484
    is_data17 = event.runNumber >= 325713

    if is_data15:
        selection += '2015'
    elif is_data16:
        selection += '2016'
    elif is_data17:
        selection += '2017'

    return selection


def get_MMLepton(event):
    lepton = r.MMLepton()
    if len(event.mu_pt) == 1:
        lepton.pt = event.mu_pt[0] / 1e3      # pt of the lepton
        # eta of the lepton, use the cluster eta for electrons!
        lepton.eta = r.TMath.Abs(event.mu_eta[0])
        lepton.d0signif = event.mu_d0sig[0]
    else:
        lepton.pt = event.el_pt[0] / 1e3      # pt of the lepton
        # eta of the lepton, use the cluster eta for electrons!
        lepton.eta = r.TMath.Abs(event.el_eta[0])
        lepton.d0signif = event.el_d0sig[0]

    # distance between the lepton and the closest jeat
    lepton.dR = min_deltaR_lep_jet(event)
    lepton.dPhi = lep_met_dphi(event)   # deltaPhi between lepton and ETmiss
    # lepton.trigger  = eval_trigger(event)   # Passed trigger or not
    return lepton


def stripFiles(outname, files, treeNum=False, inSelection="",
               hist_folder='/eos/user/j/jrawling//www/pages/plots/Dead-Cone_Analysis/v0-03-01/fake_weights/'
               ):
    if len(files) == 0:
        print("No files found matching arguments")
        sys.exit(1)

    try:
        os.makedirs(os.path.dirname(outname))
    except os.error:
        pass

    # Branches to remove
    removeBranches = ['ljet_pt', 'ljet_eta', 'ljet_phi', 'ljet_e', 'ljet_m',
                      'ljet_sd12', 'ljet_sd23', 'ljet_tau21', 'ljet_tau32',
                      'ljet_tau21_wta', 'ljet_tau32_wta', 'ljet_D2', 'ljet_C2',
                      'ljet_topTag', 'ljet_bosonTag', 'ljet_topTag_loose',
                      'ljet_bosonTag_loose', 'ljet_topTagN', 'ljet_topTagN_loose',
                      'ljet_bosonTagN', 'ljet_bosonTagN_loose',
                      'ClassifHPLUS*',
                      'NBFricoNN4in_ljets', 'NBFricoNN3ex_ljets',
                      'ClassifBDTOutput*']
    allowed_branches = ['weight_*', 'eventNumber', 'runNumber',  # 'randomRunNumber',
                        'el_pt', 'el_eta', 'el_phi', 'el_e', 'el_charge', 'el_pt',
                        'el_isTight', 'mu_isTight',
                        'el_d0sig', 'mu_d0sig',
                        'mu_pt', 'mu_eta', 'mu_phi', 'mu_e', 'mu_charge', 'mu_pt',
                        'met_met', 'mu','mu_original_xAOD',
                        'jet_pt', 'jet_eta', 'jet_phi', 'jet_e', 'jet_isbtagged_MV2c10_85','jet_isbtagged_MV2c10_77',
                        'g_pt',
                        'top_lep_pt', 'top_lep_eta', 'top_lep_phi', 'top_lep_m',
                        'top_had_pt', 'top_had_eta', 'top_had_phi', 'top_had_m',
                        'theta', 'theta_d', 'theta_had', 'theta_d_had',
                        'W_lep', 'W_had', 'neutrino',
                        'mujets_*', 'ejets_*',
                        # 'HLT_e24_lhmedium_L1EM20VH', 'el_trigMatch_HLT_e24_lhmedium_L1EM20VH', 'HLT_e60_lhmedium', 'el_trigMatch_HLT_e60_lhmedium', 'HLT_e120_lhloose', 'el_trigMatch_HLT_e120_lhloose', 'HLT_e26_lhtight_nod0_ivarloose', 'el_trigMatch_HLT_e26_lhtight_nod0_ivarloose', 'HLT_e60_lhmedium_nod0', 'el_trigMatch_HLT_e60_lhmedium_nod0', 'HLT_e140_lhloose_nod0', 'el_trigMatch_HLT_e140_lhloose_nod0', 'HLT_mu20_iloose_L1MU15', 'mu_trigMatch_HLT_mu20_iloose_L1MU15', 'HLT_mu50', 'mu_trigMatch_HLT_mu50', 'HLT_mu26_ivarmedium', 'mu_trigMatch_HLT_mu26_ivarmedium', 'HLT_mu50', 'mu_trigMatch_HLT_mu50',
                        ]
    # Selections
    selections = [lambda event: len(event.jet_eta) == 5]
    import ROOT as r
    outfile = r.TFile(outname, 'RECREATE')
    # outfile.SetCompressionLevel(1)

    # eta:pt:jetpt:sumet:dR:dRpt:dPhi:d0sig:nbjets
    parameterization = [
        'NS:eta',
        'NS:pt',
        'NS:jetpt',
        'NS:dR',
        'NS:dPhi',
        #
        'NS:dR:dPhi',
        #
        'NS:jetpt:dR',
        'NS:jetpt:dR:dPhi',
        'NS:jetpt:dPhi',

        #
        'NS:pt:jetpt:dR',
        'NS:pt:jetpt:dR:dPhi',
        'NS:pt:jetpt:dPhi',
        'NS:pt:dR',
        'NS:pt:dR:dPhi',
        'NS:pt:dPhi',

        'NS:eta:pt',
        'NS:eta:jetpt',
        'NS:eta:dR',
        'NS:eta:dPhi',

        'NS:eta:pt:jetpt',
        'NS:eta:pt:dPhi',
        'NS:eta:pt:dR',
        'NS:eta:pt:jetpt:dR',
        'NS:eta:pt:jetpt:dPhi',
        'NS:eta:pt:jetpt:dR:dPhi',

        # 'NS:eta:dR',
        # 'NS:eta:dR:dPhi',
        # 'NS:eta:dPhi',
        # 'NS:eta:jetpt:dPhi',
        # 'NS:dPhi',
        # 'NS:jetpt',
        # 'NS:pt',
        # 'NS:jetpt:dR',
        # 'NS:pt:jetpt',
        # 'NS:jetpt:pt',
        # 'NS:eta:jetpt:dR',
        # 'NS:5j:jetpt:pt',
        # '5j:jetpt:pt',
        # 'jetpt:pt',
        # 'jetpt:pt:5j',
        # 'NB/NS:eta:pt:jetpt:dR:dPhi',
        # 'NB/NS:eta:pt:jetpt:dR'
    ]
    fake_estimator = {
        'e_2015': r.FakesWeights(),
        'e_2016': r.FakesWeights(),
        'e_2017': r.FakesWeights(),
        'mu_2015': r.FakesWeights(),
        'mu_2016': r.FakesWeights(),
        'mu_2017': r.FakesWeights(),

    }
    for p in parameterization:
        fake_estimator['e_2016_' + p] = r.FakesWeights()
        fake_estimator['mu_2016_' + p] = r.FakesWeights()
        fake_estimator['e_2017_' + p] = r.FakesWeights()
        fake_estimator['mu_2017_' + p] = r.FakesWeights()
        fake_estimator['e_2015_' + p] = r.FakesWeights()
        fake_estimator['mu_2015_' + p] = r.FakesWeights()

    for fake_name in fake_estimator:
        fake_estimator[fake_name].SetDataPath(
            "/afs/cern.ch/work/j/jmellent/public/Rel21_efficiencies_20180702/")
        r.SetOwnership(fake_estimator[fake_name], 0)
        fake_estimator[fake_name].SetDebug(True)

    # Order MUST be any from these IN THIS ORDER:
    # ,<EL EFF. NAME>:<PARAMETRIZATION>,<EL EFF. NAME>:<PARAMETRIZATION>
    fake_estimator['e_2015'].SetupWeighter(
        r.FakesWeights.EJETS,"2015:NS:eta:jetpt", "2015:NS:eta:jetpt")
    fake_estimator['e_2016'].SetupWeighter(
        r.FakesWeights.EJETS, "2016:NS:eta::dPhi", "2016:NS:eta:pt:dPhi")
    fake_estimator['e_2017'].SetupWeighter(
        r.FakesWeights.EJETS, "2017:NS:eta:pt:dPhi", "2017:NS:eta:pt:dPhi")
    fake_estimator['mu_2015'].SetupWeighter(
        r.FakesWeights.MUJETS, "2015:NS:pt:jetpt:dR", "2015:NS:pt:jetpt:dR")
    fake_estimator['mu_2016'].SetupWeighter(
        r.FakesWeights.MUJETS, "2016:NS:pt:jetpt:dR", "2015:NS:pt:jetpt:dR")
    fake_estimator['mu_2017'].SetupWeighter(
        r.FakesWeights.MUJETS, "2017:NS:pt:jetpt:dR", "2015:NS:pt:jetpt:dR")

    weight_dists = {}
    for p in parameterization:
        fake_estimator[
            'e_2016_' + p].SetupWeighter(r.FakesWeights.EJETS, "2016:" + p, "2016:" + p)
        fake_estimator[
            'mu_2016_' + p].SetupWeighter(r.FakesWeights.MUJETS, "2016:" + p, "2016:" + p)
        fake_estimator[
            'e_2017_' + p].SetupWeighter(r.FakesWeights.EJETS, "2017:" + p, "2017:" + p)
        fake_estimator[
            'mu_2017_' + p].SetupWeighter(r.FakesWeights.MUJETS, "2017:" + p, "2017:" + p)
        fake_estimator[
            'e_2015_' + p].SetupWeighter(r.FakesWeights.EJETS, "2015:" + p, "2015:" + p)
        fake_estimator[
            'mu_2015_' + p].SetupWeighter(r.FakesWeights.MUJETS, "2015:" + p, "2015:" + p)

        weight_dists[
            'e_' + p] = r.TH1F("weight_el_2016_" + p, "weight_el_2016_" + p, 60, -1, 1)
        weight_dists[
            'mu_' + p] = r.TH1F("weight_mu_2016_" + p, "weight_mu_2016_" + p, 60, -1, 1)
        weight_dists[
            'e_met_' + p] = r.TH1F("met_weight_el_2017_" + p, "met_weight_el_2017_" + p, 60, 0, 600)
        weight_dists[
            'mu_met_' + p] = r.TH1F("met_weight_mu_2017_" + p, "met_weight_mu_2017_" + p, 60, 0, 600)

        weight_dists[
            'e_A_' + p] = r.TH1F("A_THETA_SR_el_2017_" + p, "A_THETA_SR_el_2017_" + p, 5, -1, 1)
        weight_dists[
            'mu_A_' + p] = r.TH1F("A_THETA_SR_mu_2017_" + p, "A_THETA_SR_mu_2017_" + p, 5, -1, 1)


        weight_dists['e_n_tight'] = 0
        weight_dists['e_n_loose'] = 0
        weight_dists['mu_n_tight'] = 0
        weight_dists['mu_n_loose'] = 0
        weight_dists['el_n_dodge' + p] = 0
        weight_dists['mu_n_dodge' + p] = 0

        r.SetOwnership(fake_estimator['e_2016_' + p], 0)
        r.SetOwnership(fake_estimator['mu_2016_' + p], 0)
        r.SetOwnership(fake_estimator['e_2017_' + p], 0)
        r.SetOwnership(fake_estimator['mu_2017_' + p], 0)
        r.SetOwnership(weight_dists['e_met_' + p], 0)
        r.SetOwnership(weight_dists['mu_met_' + p], 0)
        r.SetOwnership(weight_dists['e_' + p], 0)
        r.SetOwnership(weight_dists['mu_' + p], 0)

    correlations = ['eta', 'pt', 'jetpt', 'dR', 'dPhi', ]
    for lep in ['e_2015', 'e_2016', 'e_2017', 'mu_2015', 'mu_2016', 'mu_2017']:
        for i, c in enumerate(correlations[:-1]):
            for C in correlations[i + 1:]:
                weight_dists[lep + '_corr_' + c + '_' + C] = r.TH2F(
                    'corr_' + c + '_' + C, 'corr_' + c + '_' + C, 10, 0.84, 0.94, 10, 0.84, 0.94)
                r.SetOwnership(weight_dists[lep + '_corr_' + c + '_' + C], 0)

    trees = getListOfTrees(files)
    for itree, tree in enumerate(trees):
        tuple_name = tree

        if type(treeNum) is int and itree != treeNum:
            continue
        print("-> Processing", tree, " - ", itree + 1, "out of", len(trees))
        outfile.cd()
        inchain = r.TChain(tree)
        for file in files:
            inchain.Add(file)

        # Protection if tree isn't in file, probably nicer to somehow iterate
        # the file - TODO
        if inchain.GetEntries() == 0:
            print("No entries for tree", tree, "so proceed to next")
            continue

        if tree in ["sumWeights", "PDFsumWeights", "AnalysisTracking", "particleLevel"]:
            tree = inchain.CloneTree()  # All events
            tree.Write("", r.TObject.kOverwrite)  # Only write one header
            continue

        # Drop all physics variables and only turn on the ones we care about
        inchain.SetBranchStatus("*", 0)
        for b in allowed_branches:
            inchain.SetBranchStatus(b, 1)

        # Remove branches
        branchList = inchain.GetListOfBranches()
        branches = []
        for branch in branchList:
            branches.append(branch.GetName())
        for branchName in removeBranches:
            for branch in branches:
                if re.search(branchName, branch):
                    inchain.SetBranchStatus(branchName, 0)
                    break

        tree = inchain.CloneTree(0)
        # Set the maximum file size to 200GB before it forces a new file to be
        # opened
        tree.SetMaxTreeSize(200000000000)
        totalEvents = inchain.GetEntries()
        totalEventsPer = totalEvents / 100

        if tuple_name == "nominal_Loose":
            w = array('f', [0.])
            tree.Branch('weight_mm', w, 'weight_mm/F')

            n = array( 'i', [ 0 ] )
            n[0] = len(parameterization)
            w_p = array('f', n[0]*[0.])
            w_q = array('f', n[0]*[0.])
            tree.Branch( 'n_weights', n, 'n_weights/I' )
            tree.Branch('weight_mm_p', w_p, 'weight_mm_p[n_weights]/F')
            tree.Branch('weight_mm_q', w_q, 'weight_mm_q[n_weights]/F')
        else:
            continue 
        if totalEventsPer == 0:
            print("Fewer than 100 events, percentage completion unreliable")
            totalEventsPer = 1
        print("-", totalEvents, "found in tree")

        for i, event in enumerate(inchain):
            if i % (totalEventsPer * 10) == 0:
                print("-", i / totalEventsPer, "% done")
            # Check selection(s)
            if any([not selection(event) for selection in selections]):
                continue

            # Wrking
            if tuple_name == "nominal_Loose":
                # First let's select the correct FakesWeights tool depending
                # on year and electron/muon
                fake_selection = construct_fakesweights_string(event)
                pass_sr_cuts = (event.ejets_2015 or event.ejets_2016 or event.mujets_2015 or event.mujets_2016) and len(event.jet_pt)==5  and event.g_pt[0]/1e3 >  40  and event.g_pt[0]/event.top_lep_pt > 0.05  and event.top_lep_pt/1000 > 192  and event.theta_d < 0.9  and event.theta < event.theta_had  and event.top_lep_m/1e3 > 155  and event.top_lep_m/1e3 < 205  

                # Count n btagged jets
                n_btag = 0
                for b_tag in event.jet_isbtagged_MV2c10_85:
                    if bool(b_tag):
                        n_btag += 1

                # Create the MatrixMethod Event information
                mm_event = r.MMEvent()
                mm_event.njets = event.jet_pt.size()  # jet multiplicity
                mm_event.ntag = n_btag  # b-jet multiplicity
                mm_event.jetpt = event.jet_pt[0] / 1e3

                # We are parameterizing by deltaR so evaluate that for this
                # event
                lepton = get_MMLepton(event)
                is_tight = is_lepton_tight(event)
                if len(event.mu_eta) == 0:
                    if is_tight:
                        weight_dists['mu_n_tight'] += 1
                    weight_dists['mu_n_loose'] += 1
                else:
                    if is_tight:
                        weight_dists['e_n_tight'] += 1
                    weight_dists['e_n_loose'] += 1

                # Now we can actually set the tool with this information and
                # save the event weight
                fake_estimator[fake_selection].SetLepton(mm_event, lepton)
                w[0] = fake_estimator[
                    fake_selection].GetFakesWeightLJets(is_tight)
                # R = fake_estimator[fake_selection].GetRealEff()
                # F = fake_estimator[fake_selection].GetFakeEff()
                # P = 1. if is_tight else 0. 
                # print('R: ', R, ' type: ', type(R))
                # print('F: ', F, ' type: ', type(F))
                # print('P: ', P, ' type: ', type(P))
                # w_estimate = (F/(R-F))*(R-P)

                # if w_estimate != w[0]:
                #     print('WARNING: Fake weight seems a lil dodge. ')
                #     print('Tool gives:  ', w[0])
                #     print('I find    :  ', w_estimate)
                for K,p in enumerate(parameterization):
                    fake_estimator[fake_selection + '_' +
                                   p].SetLepton(mm_event, lepton)
                    trial_weight = fake_estimator[
                        fake_selection + '_' + p].GetFakesWeightLJets(is_tight)
                    w_p[K] = trial_weight 

                    # Evaluate these things again but using the geometric mean 
                    R = get_average_real_rate( fake_estimator, p, fake_selection) 
                    F = get_average_fake_rate( fake_estimator, p, fake_selection) 
                    P = 1. if is_tight else 0. 
                    w_q[K] = (F/(R-F))*(R-P)


                    if fake_estimator[fake_selection + '_' + p].GetRealEff() > 1:
                        if len(event.mu_pt) == 0:
                            weight_dists['el_n_dodge' + p] += 1
                        else:
                            weight_dists['mu_n_dodge' + p] += 1

                    if event.ejets_2016 or event.ejets_2015:
                        weight_dists['e_' + p].Fill(trial_weight)
                        weight_dists['e_met_' +
                                     p].Fill(event.met_met / 1e3, trial_weight)


                    elif event.mujets_2016 or event.mujets_2015:
                        weight_dists['mu_' + p].Fill(trial_weight)
                        weight_dists['mu_met_' +
                                     p].Fill(event.met_met / 1e3, trial_weight)

                     
                    if pass_sr_cuts:
                        a = (event.theta[0]-event.theta_d)/(event.theta[0]+event.theta_d)
                        if len(event.mu_pt) == 0:
                            weight_dists['e_A_' + p].Fill(a, trial_weight)
                        else:          
                            weight_dists['mu_A_' + p].Fill(a, trial_weight)   
                    # print('trail weihgt = ',trial_weight, ' for p=',p)
                for i, c in enumerate(correlations[:-1]):
                    for C in correlations[i + 1:]:
                        weight_dists[fake_selection + '_corr_' + c + '_' + C].Fill(
                            fake_estimator[fake_selection +
                                           '_NS:' + c].GetRealEff(),
                            fake_estimator[fake_selection +
                                           '_NS:' + C].GetRealEff(),
                        )



                # print(' ')
                # print('Selecrtion      : ', fake_selection)
                # print('mm_event.njets: ',mm_event.njets)
                # print('mm_event.ntag : ',mm_event.ntag )
                # print('mm_event.jetpt: ',mm_event.jetpt)
                # print('lepton.pt    :', lepton.pt)
                # print('lepton.eta   :', lepton.eta)
                # print('lepton.dR    :', lepton.dR )
                # print('lepton.dPhi  :', lepton.dPhi)
                # print("weight_mm (", 'el' if len(event.el_pt)==1 else 'mu', ') = ', w[0] )

            tree.Fill()
        #
        outfile = tree.GetCurrentFile()
        outfile.Write()

        if tuple_name == 'nominal_Loose':
            for p in parameterization:
                r.gROOT.SetBatch()
                can = r.TCanvas()
                AS.SetAtlasStyle()

                #
                plots = OrderedDict()
                plots['e+jets'] = [weight_dists['e_' + p],
                                   dhf.get_truth_large_legend_stlye_opt(1,   False)]
                plots['mu+jets'] = [weight_dists['mu_' + p],
                                    dhf.get_truth_large_legend_stlye_opt(3,   False)]
                rhf.hide_root_infomessages()
                dhf.plot_histogram(can,
                                   plots,
                                   x_axis_title="Weight",
                                   y_axis_title="NEvents ",
                                   do_atlas_labels=True,
                                   labels=[p,
                                           "mean #mu: " +
                                           str(weight_dists[
                                               'mu_' + p].GetMean()),
                                           "mean el: " + str(weight_dists['e_' + p].GetMean())]
                                   )
                can.Print(hist_folder + '/weight_' +
                          p.replace(':', '_').replace('/', '') + '.png')
                #
                plots = OrderedDict()
                plots['e+jets'] = [weight_dists['e_met_' + p],
                                   dhf.get_truth_large_legend_stlye_opt(1,   False)]
                plots['mu+jets'] = [weight_dists['mu_met_' + p],
                                    dhf.get_truth_large_legend_stlye_opt(3,   False)]
                rhf.hide_root_infomessages()
                dhf.plot_histogram(can,
                                   plots,
                                   x_axis_title="MET [GeV]",
                                   y_axis_title="NEvents ",
                                   do_atlas_labels=True,
                                   labels=[p],
                                   )
                can.Print(hist_folder + '/met_' +
                          p.replace(':', '_').replace('/', '') + '.png')
 #
                plots = OrderedDict()
                plots['e+jets'] = [weight_dists['e_A_' + p],
                                   dhf.get_truth_large_legend_stlye_opt(1,   False)]
                plots['mu+jets'] = [weight_dists['mu_A_' + p],
                                    dhf.get_truth_large_legend_stlye_opt(3,   False)]
                rhf.hide_root_infomessages()
                dhf.plot_histogram(can,
                                   plots,
                                   x_axis_title="A_{#theta}",
                                   y_axis_title="NEvents ",
                                   do_atlas_labels=True,
                                   labels=[p, 'Signal Region'],
                                   )
                can.Print(hist_folder + '/atheta_' +
                          p.replace(':', '_').replace('/', '') + '.png')


                print('el n_tight/n_loose: ', weight_dists['mu_n_tight'], '/', weight_dists[
                      'mu_n_loose'], '=', float(weight_dists['mu_n_tight']) / float(weight_dists['mu_n_loose']))
                print('mu n_tight/n_loose: ', weight_dists['e_n_tight'], '/', weight_dists[
                      'e_n_loose'], '=', float(weight_dists['e_n_tight']) / float(weight_dists['e_n_loose']))

                if weight_dists['mu_' + p].GetMean() > 0:
                    cprint('MUON WEIGHTS HAS ON AVERAGE MORE POS. : ' +
                           str(p), 'blue', 'on_green')
                    print('    Mean:', weight_dists['mu_' + p].GetMean())
                else:
                    cprint('MUON WEIGHTS HAS ON AVERAGE LESS POS. : ' +
                           str(p), 'yellow', 'on_red')
                    print('    Mean:', weight_dists['mu_' + p].GetMean())
                if weight_dists['e_' + p].GetMean() > 0:
                    cprint('ELECTRON WEIGHTS HAS ON AVERAGE MORE POS. : ' +
                           str(p), 'blue', 'on_green')
                    print('    Mean:', weight_dists['e_' + p].GetMean())
                else:
                    cprint('ELECTRON WEIGHTS HAS ON AVERAGE LESS POS. : ' +
                           str(p), 'yellow', 'on_red')
                    print('    Mean:', weight_dists['e_' + p].GetMean())
            #
            for p in parameterization:
                mu_dodge = float(
                    weight_dists['mu_n_dodge' + p]) / float(weight_dists['mu_n_loose'])
                el_dodge = float(
                    weight_dists['el_n_dodge' + p]) / float(weight_dists['e_n_loose'])

                if mu_dodge > 0:
                    cprint(' PARAM ' + p + '  FOUND A DODGY AMOUNT OF MUONS :  ' + str(mu_dodge) + ' and n_dodge = ' + str(weight_dists['mu_n_dodge' + p]), 'yellow', 'on_red')
                else:
                    cprint(' PARAM ' + p +
                           '  FOUND ZERO DODGY MOUN EFFS', 'blue', 'on_green')
                if el_dodge > 0:
                    cprint(' PARAM ' + p + '  FOUND A DODGY AMOUNT OF ELECTORns :  ' + str(el_dodge) + ' and n_dodge = ' + str(weight_dists['el_n_dodge' + p]),  'yellow', 'on_red')
                else:
                    cprint(
                        ' PARAM ' + p + '  FOUND ZERO DODGY ELECTORns EFFS', 'blue', 'on_green')

            canvas = r.TCanvas("migration_matrix_canvas",
                               "migration_matrix_canvas",
                               800,
                               600)
            r.gROOT.SetBatch()

            for lep in ['e_2015',  'mu_2015']:
                for i, c in enumerate(correlations[:-1]):
                    for C in correlations[i + 1:]:
                        r.gROOT.SetBatch()
                        weight_dists[lep + '_corr_' + c + '_' +
                                     C].GetXaxis().SetTitle('Real efficiency ' + c)
                        weight_dists[lep + '_corr_' + c + '_' +
                                     C].GetYaxis().SetTitle('Real efficiency ' + C)
                        weight_dists[lep + '_corr_' + c + '_' + C].Scale(1.0/100.0)
                        dhf.draw_migration_matrix(weight_dists[lep + '_corr_' + c + '_' + C],
                                                  canvas)
                        canvas.Print(hist_folder + '/' + lep +
                                     '_corr_' + c + '_' + C + ".eps")
                        canvas.Print(hist_folder + '/' + lep +
                                     '_corr_' + c + '_' + C + ".png")


def create_folder(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


def chunks(l, n):
    n = max(1, n)
    return (l[i:i + n] for i in xrange(0, len(l), n))


def getListOfTrees(files):
    trees = {}
    for fileLocation in files:
        file = r.TFile.Open(fileLocation)
        for mkey in file.GetListOfKeys():
            if issubclass(getattr(r, mkey.GetClassName()), r.TTree) and mkey.GetName() != 'truth':
                trees[mkey.GetName()] = True

    return sorted(trees.keys())

if __name__ == "__main__":
    # python helper_scripts/quickstrip_data.py ~/deadcone_topq/data/data16_13TeV_v3_0_02.root ~/deadcone_topq/slimmed_2/data/ data16_13TeV_v3_0_02_test.root ~/myeos/www/pages/plots/Dead-Cone_Analysis/v0-03-01/fake_weights_2016/
    # python helper_scripts/quickstrip_data.py
    # ~/deadcone_topq/data/data17_13TeV.root ~/deadcone_topq/slimmed_2/data/
    # data17_13TeV_v3_0_02_test.root
    # ~/myeos/www/pages/plots/Dead-Cone_Analysis/v0-03-01/fake_weights_2017/
    in_folder = sys.argv[1]
    # if in_folder[-8:] != "/*.root" and in_folder[-12:] != ".output.root" and in_folder[-5] != '.root':
    in_folder += "/*"

    out_folder = sys.argv[2]
    name = sys.argv[3]

    print("\n\n\n\n")
    print("Converting all files in:", in_folder)
    print("Dumping into folder: ", out_folder + '/' + name)

    # # create the out_folder
    create_folder(out_folder)

    files= glob.glob(in_folder)
    cleaned_files = []
    for f in files:
        if  f[-5] != ".part":
            cleaned_files.append(f)
    files = [in_folder]
    stripFiles(out_folder + "/" + name, cleaned_files, treeNum=False, inSelection="",
               hist_folder=sys.argv[4])
