import sys
import os
import glob 
import subprocess
import math 

# Now read in the ptah that the code is stored in. 
INPUT_PATH = os.environ["TOP_ANALYSIS_INPUT_PATH"]
batch_log_outdir = INPUT_PATH+"/logs/slimming_logs/"

data_folders = {
              'data15_13TeV.root': ["/eos/atlas/atlascerngroupdisk/phys-top/deadcone/data/user.jrawling.AllYear.physics_Main.DAOD_TOPQ1.grp15_v01_p3388.data.18-07-25v3_output_root/",
                                    "/eos/user/j/jrawling/www/pages/plots/Dead-Cone_Analysis/v0-03-02/fake_weights_2015"
                                    ],
              'data16_13TeV.root': ["/eos/atlas/atlascerngroupdisk/phys-top/deadcone/data/user.jrawling.AllYear.physics_Main.DAOD_TOPQ1.grp16_v01_p3388.data.18-07-25v3_output_root/",
                                    "/eos/user/j/jrawling/www/pages/plots/Dead-Cone_Analysis/v0-03-02/fake_weights_2016"
                                    ],
              'data17_13TeV.root': ["/eos/atlas/atlascerngroupdisk/phys-top/deadcone/data/user.jrawling.AllYear.physics_Main.DAOD_TOPQ1.grp17_v01_p3388_p3402.data.18-07-25v3_output_root/",
                                    "/eos/user/j/jrawling/www/pages/plots/Dead-Cone_Analysis/v0-03-02/fake_weights_2017"
                                    ]
             }

"""
python helper_scripts/quickstrip_data.py /eos/atlas/atlascerngroupdisk/phys-top/deadcone/data/user.jrawling.AllYear.physics_Main.DAOD_TOPQ1.grp15_v01_p3388.data.18-07-25v3_output_root/ /eos/atlas/atlascerngroupdisk/phys-top/deadcone/slimmed_v3_02_03/data/ data15_13TeV.root /eos/user/j/jrawling/www/pages/plots/Dead-Cone_Analysis/v0-03-02/fake_weights_2015
python helper_scripts/quickstrip_data.py /eos/atlas/atlascerngroupdisk/phys-top/deadcone/data/user.jrawling.AllYear.physics_Main.DAOD_TOPQ1.grp16_v01_p3388.data.18-07-25v3_output_root/ /eos/atlas/atlascerngroupdisk/phys-top/deadcone/slimmed_v3_02_03/data/ data16_13TeV.root /eos/user/j/jrawling/www/pages/plots/Dead-Cone_Analysis/v0-03-02/fake_weights_2016
python helper_scripts/quickstrip_data.py /eos/atlas/atlascerngroupdisk/phys-top/deadcone/data/user.jrawling.AllYear.physics_Main.DAOD_TOPQ1.grp17_v01_p3388_p3402.data.18-07-25v3_output_root/ /eos/atlas/atlascerngroupdisk/phys-top/deadcone/slimmed_v3_02_03/data/ data17_13TeV.root /eos/user/j/jrawling/www/pages/plots/Dead-Cone_Analysis/v0-03-02/fake_weights_2017
"""


outpath = '/eos/atlas/atlascerngroupdisk/phys-top/deadcone/slimmed_v3_02_03/data/'
n_jobs = 0
for name in data_folders:
    in_folder=data_folders[name][0]
    plot_dump_folder=data_folders[name][0]

    print("Running on: ")
    print("         in_path:" + in_folder)
    print("         outpath:" + outpath)
    print("            name:" + name)
    print("plot_dump_folder:" + plot_dump_folder)
    print("      ")

    os.system('bsub -q 8nh -J data_'+name+' -o "'+
          batch_log_outdir+'/data_slim_%J.log" -env "INPUT_PATH= '+INPUT_PATH+
          ', in_path='+str(in_folder) + ', out_path='+str(outpath)+
          ', name='+str(name) + ', plot_dump_folder='+str(plot_dump_folder) +
          ', all"<' + INPUT_PATH + '/helper_scripts/covert_data_tree.sh'
          )

print n_jobs, " submissions complete."

