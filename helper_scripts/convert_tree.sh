#setup the environment
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
lsetup root

echo "INPUT_PATH: ", $INPUT_PATH
echo "PYTHONPATH: ", $PYTHONPATH
echo "in_path:", $in_path 
echo "out_path: ", $out_path
echo "chunksize", $chunksize
echo "n_iter", $n_iter

### run the command
python $INPUT_PATH/helper_scripts/quickstrip.py $in_path $out_path $chunksize $n_iter
