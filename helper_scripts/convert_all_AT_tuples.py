import sys
import os
import glob 
import subprocess
import math 

# Now read in the ptah that the code is stored in. 
INPUT_PATH = os.environ["TOP_ANALYSIS_INPUT_PATH"]
batch_log_outdir='/eos/atlas/atlascerngroupdisk/phys-top/deadcone/logs/'

in_folders = [
              # "/eos/atlas/atlascerngroupdisk/phys-top/deadcone/mc16a/",
              "/eos/atlas/atlascerngroupdisk/phys-top/deadcone/mc16d/",
              # "/eos/atlas/atlascerngroupdisk/phys-top/deadcone/mc16c/",
              # "/eos/atlas/atlascerngroupdisk/phys-top/deadcone/truth/",
              # "/eos/atlas/atlascerngroupdisk/phys-top /deadcone/AFII/",
             ]

def chunks(l, n):
    n = max(1, n)
    return (l[i:i+n] for i in xrange(0, len(l), n)) 

def du(path):
    """disk usage in human readable format (e.g. '2,1GB')"""
    return subprocess.check_output(['du','-s', path]).split()[0].decode('utf-8')

n_jobs = 0
for in_path in in_folders:
    dsids = [ dsid_glob for dsid_glob in 
               glob.glob(in_path + "*")
            ]

    for dsid_path in ['/eos/atlas/atlascerngroupdisk/phys-top/deadcone/mc16d/user.jrawling.410470.PhPy8EG.DAOD_TOPQ1.e6337_e5984_s3126_r10201_r10210_p3409.mc16d.18-08-10_output_root']:

        if dsid_path[-11:] != 'output_root':
            continue 

        outpath = dsid_path.replace('deadcone/','deadcone/slimmed_v0-03-02/')           
        dsid = dsid_path.split('.')[2]
        folder_size = int(du(dsid_path))
        max_size =  12000.  # IN MB
        # Rough estimate of total merged output size is 7.5% of originl
        # Cap this to 2GB to get the maximium number of chunks we require in 
        # the folder 
        chunksize = int(math.ceil( (max_size)/(0.005*folder_size/1024) ))

        files= glob.glob(dsid_path+"/*") 
        cleaned_files = []
        for f in files:
            if  f[-5] != ".part":
                cleaned_files.append(f)
        n_files = len(cleaned_files)
        n_subjobs =  int(math.ceil( float(n_files)/float(chunksize) ) )

        # if n_subjobs>=3:
        # # # if '410470' in dsid_path:
        print("Running on: ")
        print("      in_path:" + dsid_path)
        print(".     foldersize: " + str(folder_size/1024) + " MB") 
        print("      chunksize:" + str(chunksize ))
        print("      n_files:" + str(n_files))
        print("      n_subjobs:" + str(n_subjobs))
        print("      outpath:" + outpath)
        print("      ")
        
        
        for n_iter in xrange(n_subjobs):
            os.system('bsub -q 8nh -outdir '+batch_log_outdir+' -J dsid_'+dsid+' -o "'+
                     batch_log_outdir+'/%J.log" -env "INPUT_PATH= '+INPUT_PATH+
                     ', in_path='+str(dsid_path) + ', out_path='+str(outpath)+
                     ', chunksize='+str(chunksize) + ', n_iter='+str(n_iter) +
                     ', all"<' + INPUT_PATH + '/helper_scripts/convert_tree.sh'
                     )
            n_jobs += 1

print n_jobs, " submissions complete."
