'''
    Author: Jacob Rawling
    Date:

    Utilizes the TopAnalysis.Unfolding package ( ) to create the migration matrix,
    perform the desired type of unfolding, run some tests, save the ouptut in ROOT
    and Journal ready eps fomrat
'''
from TopAnalysis.Unfolding.Unfolder import Unfolder
from TopAnalysis.Unfolding.IdentityTest import IdentityTest
from TopAnalysis.Unfolding.StressTest import StressTest
from TopAnalysis.Unfolding.ClosureTest import ClosureTest
# from TopAnalysis.Unfolding.PullTest import PullTest
from TopAnalysis.Unfolding.IterationConvergenceTest import IterationConvergenceTest

from TopAnalysis.FakeEvaluator import FakeEvalautor
from TopAnalysis.Unfolding.Systematic import Systematic
from TopAnalysis.SystematicsManager import SystematicsManager
import numpy as np
import config as c
import ROOT as r
from collections import OrderedDict
from TopAnalysis.Backgrounds import background_samples


def main():
    unfolders = load_unfolders()
    for unfolder in unfolders:

        # Choose which systematics to run over, configured correctly (hopefully) behind the
        # Scenes.
        # See SysteamaticsManager.load_systematics and Unfolding.Systematic for details
        # on these are managed
        unfolder.systematics = SystematicsManager.load_systematics(unfolder, selected_systematics = [
                                    "JVT",
                                    "Lepton",
                                    "Pile Up",
                                    "B-Tag Flavour",
                                    "JES Flavour",
                                    # # # The following Requires AnalysisTop to run with systematics turned on
                                    "MET",
                                    "JER",
                                    "b-JES",
                                    "JES Modelling",
                                    "JES #eta-intercal",
                                    "JES Pile-up",
                                    "JES Flavour",
                                    "JES Punchthrough",
                                    "JES Single Part.",
                                    "JES Mixed",
                                    "JES Stat."
                                    # # The Following requires additional samples to be ran over
                                    # "Modelling"
                                    # # These are based on variations of the background sizes
                                    # ""
                                ])

        # The point of this analysis is to compare NLO highly validated Powheg+Pythia8
        # ATLAS Sim to my personally produced MC jobs
        # So let's hand the tool the additional samples we wish to plot
        # at particleLevel a long with the unfolded result
        # unfolder.additional_samples        = c.additional_samples
        # unfolder.ratio_plot_name           = c.ratio_plot_name
        unfolder.background_samples        = background_samples

        # Load all relevant tests - starting with an identity
        # unfolder.add_test( IdentityTest(name = "IdentityTest") )

        # upward and downward variation in shape of various sizes
        # performed by an event-by-event reweighting based on the truth level variable
        # unfolder.add_test( StressTest(
        #                             # [-0.15,0.15,-0.25,0.25,-0.35,0.35],# -1.0,1.0],
        #                             [-0.05,0.05,-0.1,0.1,-0.15,0.15],# -1.0,1.0],
        #                             name = "Stress_Test",
        #                             input_mc_tuple   = unfolder.input_mc_tuple,
        #                             truth_tuple_name = unfolder.truth_tuple_name,
        #                             truth_level_variable    = unfolder.truth_level_variable,
        #                             detector_tuple_name     = unfolder.detector_tuple_name,
        #                             detector_level_variable = unfolder.detector_level_variable,
        #                             x_axis_binning   = unfolder.x_axis_binning,
        #                             truth_cuts       = unfolder.truth_cuts,
        #                             detector_cuts    = unfolder.detector_cuts,
        #                             detector_weight  = unfolder.detector_weight,
        #                             truth_weight     = unfolder.truth_weight,
        #                             reweighting_variable = unfolder.truth_level_variable,
        #                             n_iterations     = unfolder.n_iterations
        #                             ) )

        # upward and downward variation in shape,
        # performed by an event-by-event reweighting based on the truth level variable
        # unfolder.add_test( IterationConvergenceTest(
        #                             name = "IterationConvergenceTest",
        #                             input_mc_tuple   = unfolder.input_mc_tuple,
        #                             truth_tuple_name = unfolder.truth_tuple_name,
        #                             truth_level_variable    = unfolder.truth_level_variable,
        #                             detector_tuple_name     = unfolder.detector_tuple_name,
        #                             detector_level_variable = unfolder.detector_level_variable,
        #                             x_axis_binning   = unfolder.x_axis_binning,
        #                             x_axis_title     = unfolder.x_axis_label,
        #                             y_axis_title     = unfolder.y_axis_label,
        #                             truth_cuts       = unfolder.truth_cuts,
        #                             detector_cuts    = unfolder.detector_cuts,
        #                             detector_weight  = unfolder.detector_weight,
        #                             truth_weight     = unfolder.truth_weight,
        #                             ) )


        # upward and downward variation in shape,
        # performed by an event-by-event reweighting based on the truth level variable
        # unfolder.add_test( ClosureTest(
        #                             name = "ClosureTest_80fb_binning_fourth_test",
        #                             input_mc_tuple   = unfolder.input_mc_tuple,
        #                             truth_tuple_name = unfolder.truth_tuple_name,
        #                             truth_level_variable    = unfolder.truth_level_variable,
        #                             detector_tuple_name     = unfolder.detector_tuple_name,
        #                             detector_level_variable = unfolder.detector_level_variable,
        #                             x_axis_binning   = [-1,0.0,0.5,1.0],
        #                             # x_axis_binning   = [-1,0.0,0.4,1.0],
        #                             # x_axis_binning   = [-1,0.0,0.25,0.4,0.7,1.0],
        #                             # x_axis_binning   = [-1,0.0,0.2,0.35,0.5,0.7,0.85,1.0],
        #                             x_axis_title     = unfolder.x_axis_label,
        #                             y_axis_title     = unfolder.y_axis_label,
        #                             truth_cuts       = unfolder.truth_cuts,
        #                             detector_cuts    = unfolder.detector_cuts,
        #                             detector_weight  = unfolder.detector_weight,
        #                             truth_weight     = unfolder.truth_weight,
        #                             draw_pulls       = True,
        #                             luminosity       = c.luminosity,
        #                             mc_luminosity    = unfolder.mc_luminosity_nom
        #                             ) )

        # # upward and downward variation in shape,
        # # performed by an event-by-event reweighting based on the truth level variable
        # unfolder.add_test( ClosureTest(
        #                             name = "ClosureTest_80fb_finebins",
        #                             input_mc_tuple   = unfolder.input_mc_tuple,
        #                             truth_tuple_name = unfolder.truth_tuple_name,
        #                             truth_level_variable    = unfolder.truth_level_variable,
        #                             detector_tuple_name     = unfolder.detector_tuple_name,
        #                             detector_level_variable = unfolder.detector_level_variable,
        #                             x_axis_binning   = np.linspace(-1,1,15),
        #                             x_axis_title     = unfolder.x_axis_label,
        #                             y_axis_title     = unfolder.y_axis_label,
        #                             truth_cuts       = unfolder.truth_cuts,
        #                             detector_cuts    = unfolder.detector_cuts,
        #                             detector_weight  = unfolder.detector_weight,
        #                             truth_weight     = unfolder.truth_weight,
        #                             draw_pulls       = True,
        #                             luminosity       = c.luminosity,
        #                             mc_luminosity    = unfolder.mc_luminosity_nom
        #                             ) )

        #first thing is to construct the unfolder
        unfolder.construct()

        # Now we actually perform the unfolding, save any plots
        unfolder.evaluate()

        # We also want to run the systematics
        unfolder.evaluate_systematics()

        # and check that everything behaviours how it should
        unfolder.run_tests()

        # Now draw everything, tests, systematics, unfolded data, etc.
        unfolder.draw_reco_plots()
        unfolder.draw()

        #clean up ROOT stuff
        unfolder.close()

def load_mu_pt():
    reco_cuts  = ["(nominal.mujets_2015 || nominal.mujets_2016 )"]
    reco_cuts.append("(Length$(nominal.jet_pt)==5)")

    truth_cuts = ["(particleLevel.mujets_particle)"]
    truth_cuts.append("(Length$(particleLevel.jet_pt)==5)")

    return Unfolder(
                c.input_mc_tuple,        #MC that we base the unfolding on
                c.input_data_tuple,      #data tuple that we want to unfold
                c.output_folder,         #Where all ROOT  and  eps will be put
                "particleLevel.mu_pt[0]/1e3",                 #Truth level variable name that will be unfolded to
                "nominal.mu_pt[0]/1e3",                 #Detector level variable name that will be unfolded from
                # A better binning that achieves a reasonable result of the pull test
                [30,50,75,100,140,180,225,300],
                method="bayes",        # we're going to use iterative bayesian
                truth_tuple_name = "particleLevel",
                detector_tuple_name = "nominal",
                data_tuple_is_mc = True, # say that we are in test mode and therefiore the data tuple is infact MC
                save_output = True,       # we want everything to be shoved to a ROOT file
                x_axis_label = "Measured leading muon p_{T} [GeV]",
                y_axis_label = "Truth leading muon p_{T} [GeV]",
                unfolded_x_axis_title = "leading muon p_{T} [GeV]",
                unfolded_y_axis_title = "#frac{1}{#sigma_{0}} #frac{d#sigma}{p_{T,lepton}}",
                messages=["Fiducial phase space"],
                truth_cuts=truth_cuts,
                detector_cuts=reco_cuts,
                name="mu_pt",
                n_iterations=7,
                set_log_y=True
               )

def load_el_phi():
    reco_cuts  = ["(nominal.ejets_2015 || nominal.ejets_2016 )"]
    reco_cuts.append("(Length$(nominal.jet_pt)==5)")
    # reco_cuts.append("(nominal.g_pt[0]/1e3 >  35)")
    # reco_cuts.append("(nominal.g_pt[0]/nominal.top_lep_pt > 0.05)")
    # reco_cuts.append("(nominal.top_lep_pt/1000 < 150)")
    # reco_cuts.append("(nominal.theta_d < 0.75)")
    # # reco_cuts.append("(nominal.theta_d > 0.25)")
    # reco_cuts.append("(nominal.theta[0] < 2.75)")
    # reco_cuts.append("(nominal.theta < nominal.theta_had)")
    # reco_cuts.append("(nominal.top_lep_m/1e3 > 157  && nominal.top_lep_m/1e3 < 212)")

    # NOTE: SEEMS THAT MASS CONTSTRAINTS DON'T HELP TOO MUCH
    truth_cuts = ["(particleLevel.ejets_particle)"]
    truth_cuts.append("(Length$(particleLevel.jet_pt)==5)")
    # truth_cuts.append("(particleLevel.g_pt[0]/1e3 > 30)")
    # truth_cuts.append("(particleLevel.g_pt[0]/particleLevel.top_lep_pt > 0.05)")
    # truth_cuts.append("(particleLevel.top_lep_pt/1000 < 150)")
    # truth_cuts.append("(particleLevel.theta_d < 0.75)")
    # # truth_cuts.append("(particleLevel.theta_d > 0.5)")
    # truth_cuts.append("(particleLevel.theta[0] < 2.75)")
    # truth_cuts.append("(particleLevel.top_lep_m/1e3 > 157 && particleLevel.top_lep_m/1e3 < 212)")
    # truth_cuts.append("(particleLevel.theta < particleLevel.theta_had)")
    import numpy as np

    return Unfolder(c.input_mc_tuple,        #MC that we base the unfolding on
                c.input_data_tuple,      #data tuple that we want to unfold
                c.output_folder,         #Where all ROOT  and  eps will be put
                "particleLevel.el_phi/3.14159",   #Truth level variable name that will be unfolded to
                "nominal.el_phi/3.14159",         #Detector level variable name that will be unfolded from
                np.linspace(-1,1,10).tolist(), 
                method = "bayes",        # we're going to use iterative bayesian
                truth_tuple_name = "particleLevel",
                detector_tuple_name = "nominal",
                data_tuple_is_mc = True, #say that we are in test mode and therefiore the data tuple is infact MC
                save_output = True,       # we want everything to be shoved to a ROOT file
                x_axis_label = "Measured leading electron |#phi/#pi|",
                y_axis_label = "Truth leading electron |#phi/#pi|",
                unfolded_x_axis_title = "leading electron |#phi/#pi|",
                unfolded_y_axis_title = "#frac{1}{#sigma_{0}} #frac{d#sigma}{d|#phi/#pi|}",
                messages = ["Fiducial phase space", "Control Region"],
                truth_cuts = truth_cuts,
                detector_cuts = reco_cuts,
                name = "el_phi",
                n_iterations = 8,
                set_log_y = True
               )

def load_control_region():
    reco_cuts  = ["(nominal.ejets_2015 || nominal.ejets_2016 || nominal.mujets_2015 || nominal.mujets_2016)"]
    reco_cuts.append("(Length$(nominal.jet_pt)==5)")
    reco_cuts.append("(nominal.g_pt[0]/1e3 >  25)")
    reco_cuts.append("(nominal.g_pt[0]/nominal.top_lep_pt > 0.035)")
    # reco_cuts.append("(nominal.top_lep_pt/1000 > 127)")
    reco_cuts.append("(nominal.top_lep_pt/1000 < 150)")
    reco_cuts.append("(nominal.top_lep_pt/1000 > 75)")
    # reco_cuts.append("(nominal.theta_d < 0.75)")
    # reco_cuts.append("(nominal.theta_d > 0.25)")
    # reco_cuts.append("(nominal.theta[0] < 2.75)")
    reco_cuts.append("(nominal.theta < nominal.theta_had)")
    # reco_cuts.append("(nominal.top_lep_m/1e3 > 157  && nominal.top_lep_m/1e3 < 212)")
    reco_cuts.append("(nominal.top_lep_m/1e3 > 155  && nominal.top_lep_m/1e3 < 205)")

    # NOTE: SEEMS THAT MASS CONTSTRAINTS DON'T HELP TOO MUCH
    truth_cuts = ["(particleLevel.mujets_particle || particleLevel.ejets_particle)"]
    truth_cuts.append("(Length$(particleLevel.jet_pt)==5)")
    truth_cuts.append("(particleLevel.g_pt[0]/1e3 > 25)")
    truth_cuts.append("(particleLevel.g_pt[0]/particleLevel.top_lep_pt > 0.035)")
    # truth_cuts.append("(particleLevel.top_lep_pt/1000 > 127)")
    truth_cuts.append("(particleLevel.top_lep_pt/1000 < 150)")
    truth_cuts.append("(particleLevel.top_lep_pt/1000 > 75)")
    # truth_cuts.append("(particleLevel.theta_d < 0.75)")
    # truth_cuts.append("(particleLevel.theta_d > 0.5)")
    # truth_cuts.append("(particleLevel.theta[0] < 2.75)")
    # truth_cuts.append("(particleLevel.top_lep_m/1e3 > 157 && particleLevel.top_lep_m/1e3 < 212)")
    truth_cuts.append("(particleLevel.top_lep_m/1e3 > 155 && particleLevel.top_lep_m/1e3 < 205)")
    truth_cuts.append("(particleLevel.theta < particleLevel.theta_had)")
    return Unfolder(c.input_mc_tuple,        #MC that we base the unfolding on
                c.input_data_tuple,      #data tuple that we want to unfold
                c.output_folder,         #Where all ROOT  and  eps will be put
                "(particleLevel.theta[0]-particleLevel.theta_d)/(particleLevel.theta[0]+particleLevel.theta_d)",                 #Truth level variable name that will be unfolded to
                "(nominal.theta[0]-nominal.theta_d)/(nominal.theta[0]+nominal.theta_d)",                 #Detector level variable name that will be unfolded from
                [-1,-0.25,0.0,0.25,0.5,0.7,0.95],#,10,15],
                method = "bayes",        # we're going to use iterative bayesian
                truth_tuple_name = "particleLevel",
                detector_tuple_name = "nominal",
                data_tuple_is_mc = True, #say that we are in test mode and therefiore the data tuple is infact MC
                save_output = True,       # we want everything to be shoved to a ROOT file
                x_axis_label = "Measured A_{#theta}",
                y_axis_label = "Truth A_{#theta}",
                unfolded_x_axis_title = "Leptonic A_{#theta}",
                unfolded_y_axis_title = "#frac{1}{#sigma_{0}} #frac{d#sigma}{dA_{#theta}}",
                messages = ["Fiducial phase space", "Control Region"],
                truth_cuts = truth_cuts,
                detector_cuts = reco_cuts,
                name = "Leptonic_Asymmetry_CR",
                n_iterations = 8,
                set_log_y = True
               )

def load_signal_region():
    reco_cuts  = ["(nominal.ejets_2015 || nominal.ejets_2016 || nominal.mujets_2015 || nominal.mujets_2016)"]
    reco_cuts.append("(Length$(nominal.jet_pt)==5)")
    reco_cuts.append("(nominal.g_pt[0]/1e3 >  35)")
    reco_cuts.append("(nominal.g_pt[0]/nominal.top_lep_pt > 0.035)")
    # reco_cuts.append("(nominal.top_lep_pt/1000 > 127)")
    reco_cuts.append("(nominal.top_lep_pt/1000 > 150)")
    reco_cuts.append("(nominal.theta_d < 0.75)")
    # reco_cuts.append("(nominal.theta_d > 0.25)")
    # reco_cuts.append("(nominal.theta[0] < 2.75)")
    reco_cuts.append("(nominal.theta < nominal.theta_had)")
    # reco_cuts.append("(nominal.top_lep_m/1e3 > 157  && nominal.top_lep_m/1e3 < 212)")
    reco_cuts.append("(nominal.top_lep_m/1e3 > 155  && nominal.top_lep_m/1e3 < 205)")

    # NOTE: SEEMS THAT MASS CONTSTRAINTS DON'T HELP TOO MUCH
    truth_cuts = ["(particleLevel.mujets_particle || particleLevel.ejets_particle)"]
    truth_cuts.append("(Length$(particleLevel.jet_pt)==5)")
    truth_cuts.append("(particleLevel.g_pt[0]/1e3 > 30)")
    truth_cuts.append("(particleLevel.g_pt[0]/particleLevel.top_lep_pt > 0.035)")
    # truth_cuts.append("(particleLevel.top_lep_pt/1000 > 127)")
    truth_cuts.append("(particleLevel.top_lep_pt/1000 > 150)")
    truth_cuts.append("(particleLevel.theta_d < 0.75)")
    # truth_cuts.append("(particleLevel.theta_d > 0.5)")
    # truth_cuts.append("(particleLevel.theta[0] < 2.75)")
    # truth_cuts.append("(particleLevel.top_lep_m/1e3 > 157 && particleLevel.top_lep_m/1e3 < 212)")
    truth_cuts.append("(particleLevel.top_lep_m/1e3 > 155 && particleLevel.top_lep_m/1e3 < 205)")
    truth_cuts.append("(particleLevel.theta < particleLevel.theta_had)")
    # return Unfolder(c.input_mc_tuple,        #MC that we base the unfolding on
    #             c.input_data_tuple,      #data tuple that we want to unfold
    #             c.output_folder,         #Where all ROOT  and  eps will be put
    #             "(particleLevel.theta[0]/particleLevel.theta_d)**2",#"(particleLevel.theta[0]-particleLevel.theta_d)/(particleLevel.theta[0]+particleLevel.theta_d)",                 #Truth level variable name that will be unfolded to
    #             "(nominal.theta[0]/nominal.theta_d)**2",#"(nominal.theta[0]-nominal.theta_d)/(nominal.theta[0]+nominal.theta_d)",                 #Detector level variable name that will be unfolded from
    #             [0,1,2,5,12],
    #             method = "bayes",        # we're going to use iterative bayesian
    #             truth_tuple_name = "particleLevel",
    #             detector_tuple_name = "nominal",
    #             data_tuple_is_mc = True, #say that we are in test mode and therefiore the data tuple is infact MC
    #             save_output = True,       # we want everything to be shoved to a ROOT file
    #             x_axis_label = "Measured #Theta_{d}",
    #             y_axis_label = "Truth  #Theta_{d}",
    #             unfolded_x_axis_title = "Leptonic  #Theta_{d}",
    #             unfolded_y_axis_title = "#frac{1}{#sigma_{0}} #frac{d#sigma}{d#Theta_{d}}",
    #             messages = ["Fiducial phase space", "Signal Region"],
    #             truth_cuts = truth_cuts,
    #             detector_cuts = reco_cuts,
    #             name = "chain_test",
    #             # name = "hadd_test",
    #             n_iterations = 8,
    #             set_log_y = True
    #             )
    return Unfolder(c.input_mc_tuple,        #MC that we base the unfolding on
                c.input_data_tuple,      #data tuple that we want to unfold
                c.output_folder,         #Where all ROOT  and  eps will be put
                "(particleLevel.theta[0]-particleLevel.theta_d)/(particleLevel.theta[0]+particleLevel.theta_d)",                 #Truth level variable name that will be unfolded to
                "(nominal.theta[0]-nominal.theta_d)/(nominal.theta[0]+nominal.theta_d)",                 #Detector level variable name that will be unfolded from
                [-1,-0.25,0.0,0.25,0.5,0.7,1.0],#,10,15],
                method="bayes",        # we're going to use iterative bayesian
                truth_tuple_name = "particleLevel",
                detector_tuple_name = "nominal",
                data_tuple_is_mc = True, # say that we are in test mode and therefiore the data tuple is infact MC
                save_output = True,       # we want everything to be shoved to a ROOT file
                x_axis_label="Measured A_{#theta}",
                y_axis_label="Truth A_{#theta}",
                unfolded_x_axis_title = "Leptonic A_{#theta}",
                unfolded_y_axis_title = "#frac{1}{#sigma_{0}} #frac{d#sigma}{dA_{#theta}}",
                messages=["Fiducial phase space", "Control Region"],
                truth_cuts=truth_cuts,
                detector_cuts=reco_cuts,
                name="SR",
                n_iterations=7,
                set_log_y=True
               )


def load_unfolders():
    """
        Returns an array of unfolders (defined in TopAnalysis), shoved away into a function to keep
        main() clean and readable.
    """
    unfolders  = []
    unfolders.append(load_signal_region())
    # unfolders.append(load_control_region())
    # unfolders.append(load_el_phi())

    # unfolders.append(load_mu_pt())
    return unfolders


    #   'y': 170.0, 'x': 259.7090765293783, 'z': 118.97016827297826}}
    reco_cuts  = ["(nominal.ejets_2015 || nominal.ejets_2016 || nominal.mujets_2015 || nominal.mujets_2016)"]
    # reco_cuts.append("(Length$(nominal.jet_pt)==5)")
    # reco_cuts.append("(nominal.theta_d > 0.4)")
    # reco_cuts.append("(nominal.theta < nominal.theta_had)")
    reco_cuts.append("(nominal.g_pt[0]/nominal.top_lep_pt > 0.035)")
    reco_cuts.append("(nominal.top_lep_pt/1000 > 150)")
    reco_cuts.append("(nominal.theta_d < 0.75)")

    # NOTE: SEEMS THAT MASS CONTSTRAINTS DON'T HELP TOO MUCH
    reco_cuts.append("(nominal.top_lep_m/1e3 > 172)*(nominal.top_lep_m/1e3 < 182)")
    # reco_cuts.append("(nominal.top_lep_m/1e3 > 157)*(nominal.top_lep_m/1e3 < 187)")

    truth_cuts = ["(particleLevel.mujets_particle || particleLevel.ejets_particle)"]
    # truth_cuts.append("(Length$(particleLevel.jet_pt)==5)")
    # truth_cuts.append("(particleLevel.theta_d > 0.4)")
    truth_cuts.append("(particleLevel.g_pt[0]/particleLevel.top_lep_pt > 0.035)")
    truth_cuts.append("(particleLevel.top_lep_pt/1000 > 150)")
    truth_cuts.append("(particleLevel.theta_d < 0.75)")
    # truth_cuts.append("(particleLevel.theta < particleLevel.theta_had)")
    truth_cuts.append("(particleLevel.top_lep_m/1e3 > 172)*(particleLevel.top_lep_m/1e3 < 182)")
    # truth_cuts.append("(particleLevel.top_lep_m/1e3 > 157)*(particleLevel.top_lep_m/1e3 < 187)")

    # unfolders.append(Unfolder(c.input_mc_tuple,        #MC that we base the unfolding on
    #             c.input_data_tuple,      #data tuple that we want to unfold
    #             c.output_folder,         #Where all ROOT  and  eps will be put
    #             "(particleLevel.theta[0]-particleLevel.theta_d)/(particleLevel.theta[0]+particleLevel.theta_d)",                 #Truth level variable name that will be unfolded to
    #             "(nominal.theta[0]-nominal.theta_d)/(nominal.theta[0]+nominal.theta_d)",                 #Detector level variable name that will be unfolded from
    #             [-1,-0.25,0.0,0.25,0.5,0.7,0.9],#,10,15],
    #             method = "bayes",        # we're going to use iterative bayesian
    #             truth_tuple_name = "particleLevel",
    #             detector_tuple_name = "nominal",
    #             data_tuple_is_mc = True, #say that we are in test mode and therefiore the data tuple is infact MC
    #             save_output = True,       # we want everything to be shoved to a ROOT file
    #             x_axis_label = "Measured A_{#theta}",
    #             y_axis_label = "Truth A_{#theta}",
    #             unfolded_x_axis_title = "Combined A_{#theta}",
    #             unfolded_y_axis_title = "#frac{1}{#sigma_{0}} #frac{d#sigma}{dA_{#theta}}",
    #             messages = ["Fiducial phase space", "Signal Region"],
    #             truth_cuts = truth_cuts,
    #             detector_cuts = reco_cuts,
    #             name = "CombinedAsymmetry_test",
    #             n_iterations = 8,
    #             set_log_y = True
    #            ))


    # unfolders.append(Unfolder(c.input_mc_tuple,        #MC that we base the unfolding on
               #  c.input_data_tuple,      #data tuple that we want to unfold
               #  c.output_folder,         #Where all ROOT  and  eps will be put
               #  "particleLevel.theta[0]",                 #Truth level variable name that will be unfolded to
               #  "nominal.theta[0]",                 #Detector level variable name that will be unfolded from
               #  [0.0,0.5,1.0,1.5,2.5,4.0,5.5],#,10,15],
               #  method = "bayes",        # we're going to use iterative bayesian
               #  truth_tuple_name = "particleLevel",
               #  detector_tuple_name = "nominal",
               #  data_tuple_is_mc = True, #say that we are in test mode and therefiore the data tuple is infact MC
               #  save_output = True,       # we want everything to be shoved to a ROOT file
           #  x_axis_label = "Measured #theta",
               #  y_axis_label = "Truth #theta",
               #  unfolded_x_axis_title = "#theta",
               #  unfolded_y_axis_title = "#frac{1}{#sigma_{0}} #frac{d#sigma}{d#theta}",
               #  messages = ["Fiducial phase space", "Leptonic Top"],
               #  truth_cuts = truth_cuts,
               #  detector_cuts = reco_cuts,
               #  name = "small_theta_SR"
               # ))

    # unfolders.append(Unfolder(c.input_mc_tuple,        #MC that we base the unfolding on
    #             c.input_data_tuple,      #data tuple that we want to unfold
    #             c.output_folder,         #Where all ROOT  and  eps will be put
    #             "particleLevel.theta_d",                 #Truth level variable name that will be unfolded to
    #             "nominal.theta_d",                 #Detector level variable name that will be unfolded from
    #             [0.0,0.25,0.5,0.7,0.9,1.0],#,10,15],
    #             method = "bayes",        # we're going to use iterative bayesian
    #             truth_tuple_name = "particleLevel",
    #             detector_tuple_name = "nominal",
    #             data_tuple_is_mc = True, #say that we are in test mode and therefiore the data tuple is infact MC
    #             save_output = True,       # we want everything to be shoved to a ROOT file
    #             x_axis_label = "Measured #theta_{D}",
    #             y_axis_label = "Truth #theta_{D}",
    #             unfolded_x_axis_title = "#theta_{D}",
    #             unfolded_y_axis_title = "#frac{1}{#sigma_{0}} #frac{d#sigma}{d#theta_{D}}}",
    #             messages = ["Fiducial phase space", "Leptonic Top"],
    #             truth_cuts = truth_cuts,
    #             detector_cuts = reco_cuts,
    #             name = "theta_d_SR"
    #            ))

    reco_cuts  = ["(nominal.ejets_2015 || nominal.ejets_2016 || nominal.mujets_2015 || nominal.mujets_2016)"]
    # reco_cuts.append("(Length$(nominal.jet_pt)==5)")
    # reco_cuts.append("(nominal.theta_d > 0.4)")
    reco_cuts.append("(nominal.theta > nominal.theta_had)")
    reco_cuts.append("(nominal.g_pt[0]/nominal.top_had_pt > 0.035)")
    reco_cuts.append("(nominal.theta_d_had < 0.75)")
    # reco_cuts.append("(nominal.top_had_m/1e3 > 172)*(nominal.top_had_m/1e3 < 182)")
    reco_cuts.append("(nominal.top_had_m/1e3 > 157)*(nominal.top_had_m/1e3 < 187)")
    reco_cuts.append("(nominal.top_had_pt/1000 > 250)")

    truth_cuts = ["(particleLevel.mujets_particle || particleLevel.ejets_particle)"]
    # truth_cuts.append("(Length$(particleLevel.jet_pt)==5)")
    # truth_cuts.append("(particleLevel.theta_d > 0.4)")
    truth_cuts.append("(particleLevel.g_pt[0]/particleLevel.top_had_pt > 0.035)")
    truth_cuts.append("(particleLevel.top_had_pt/1000 > 250)")
    truth_cuts.append("(particleLevel.theta_d_had < 0.75)")
    truth_cuts.append("(particleLevel.theta > particleLevel.theta_had)")
    # truth_cuts.append("(particleLevel.top_had_m/1e3 > 172)*(particleLevel.top_had_m/1e3 < 182)")
    truth_cuts.append("(particleLevel.top_had_m/1e3 > 167)*(particleLevel.top_had_m/1e3 < 187)")

    # unfolders.append(Unfolder(c.input_mc_tuple,        #MC that we base the unfolding on
    #         c.input_data_tuple,      #data tuple that we want to unfold
    #         c.output_folder,         #Where all ROOT  and  eps will be put
    #         "(particleLevel.theta_had[0]-particleLevel.theta_d_had)/(particleLevel.theta_had[0]+particleLevel.theta_d_had)",                 #Truth level variable name that will be unfolded to
    #         "(nominal.theta_had[0]-nominal.theta_d_had)/(nominal.theta_had[0]+nominal.theta_d_had)",                 #Detector level variable name that will be unfolded from
    #         [-1,-0.25,0.0,0.25,0.5,0.7,0.9],#,10,15],
    #         method = "bayes",        # we're going to use iterative bayesian
    #         truth_tuple_name = "particleLevel",
    #         detector_tuple_name = "nominal",
    #         data_tuple_is_mc = True, #say that we are in test mode and therefiore the data tuple is infact MC
    #         save_output = True,       # we want everything to be shoved to a ROOT file
    #         x_axis_label = "Measured A_{#theta}",
    #         y_axis_label = "Truth A_{#theta}",
    #         unfolded_x_axis_title = "Hadronic A_{#theta}",
    #         unfolded_y_axis_title = "#frac{1}{#sigma_{0}} #frac{d#sigma}{dA_{#theta}}",
    #         messages = ["Fiducial phase space", "Signal Region"],
    #         truth_cuts = truth_cuts,
    #         detector_cuts = reco_cuts,
    #         name = "Had_Asymmetry_test",
    #         n_iterations = 8
    #        ))

    # Define nice easy cuts for this validation (i.e easy to unfold) distribution
    reco_cuts  = ["(nominal.ejets_2015 || nominal.ejets_2016)"]
    reco_cuts.append("(Length$(nominal.jet_pt)==5)")

    truth_cuts = ["(particleLevel.ejets_particle)"]
    truth_cuts.append("(Length$(particleLevel.jet_pt)==5)")


    import numpy as np
    # n_bins = 7
    unfolders.append(Unfolder(c.input_mc_tuple,        #MC that we base the unfolding on
                    c.input_data_tuple,      #data tuple that we want to unfold
                    c.output_folder,         #Where all ROOT  and  eps will be put
                    "particleLevel.el_phi[0]/TMath::Pi()",                 #Truth level variable name that will be unfolded to
                    "nominal.el_phi[0]/TMath::Pi()",                 #Detector level variable name that will be unfolded from
                    np.linspace(-1,1,10),#[50,75,100,140,180,225,250,300],
                    method = "bayes",        # we're going to use iterative bayesian
                    truth_tuple_name = "particleLevel",
                    detector_tuple_name = "nominal",
                    data_tuple_is_mc = True, #say that we are in test mode and therefiore the data tuple is infact MC
                    save_output = True,       # we want everything to be shoved to a ROOT file
                    x_axis_label = "Measured leading electron |#phi/#pi|",
                    y_axis_label = "Truth leading electron |#phi/#pi|",
                    unfolded_x_axis_title = "leading electron |#phi/#pi|",
                    unfolded_y_axis_title = "#frac{1}{#sigma_{0}} #frac{d#sigma}{d|#phi/#pi|}",
                    truth_cuts = truth_cuts,
                    detector_cuts = reco_cuts,
                    messages = ["Fiducial phase space"],
                    luminosity = c.luminosity,
                    name = "leading_el_phi"
                   ))

    # unfolders.append(Unfolder(c.input_mc_tuple,        #MC that we base the unfolding on
    #                 c.input_data_tuple,      #data tuple that we want to unfold
    #                 c.output_folder,         #Where all ROOT  and  eps will be put
    #                 "particleLevel.top_lep_m/1e3",                 #Truth level variable name that will be unfolded to
    #                 "nominal.top_lep_m/1e3",                 #Detector level variable name that will be unfolded from
    #                 # A better binning that achieves a reasonable result of the pull test
    #                 np.linspace(100,270,15).tolist(),#[50,75,100,140,180,225,250,300],
    #                 method = "bayes",        # we're going to use iterative bayesian
    #                 truth_tuple_name = "particleLevel",
    #                 detector_tuple_name = "nominal",
    #                 data_tuple_is_mc = True, #say that we are in test mode and therefiore the data tuple is infact MC
    #                 save_output = True,       # we want everything to be shoved to a ROOT file
    #                 x_axis_label = "Measured leptonic top mass [GeV]",
    #                 y_axis_label = "Truth leptonic top mass [GeV]",
    #                 unfolded_x_axis_title = "leptonic top mass [GeV]",
    #                 unfolded_y_axis_title = "#frac{1}{#sigma_{0}} #frac{d#sigma}{m^{lepton}}",
    #                 truth_cuts = truth_cuts,
    #                 detector_cuts = reco_cuts,
    #                 messages = ["Fiducial phase space", "Inclusive Selection"],
    #                 luminosity = c.luminosity,
    #                 name = "leptonic_top_mass"
    #                ))
    # unfolders.append(Unfolder(c.input_mc_tuple,        #MC that we base the unfolding on
    #                 c.input_data_tuple,      #data tuple that we want to unfold
    #                 c.output_folder,         #Where all ROOT  and  eps will be put
    #                 "particleLevel.top_had_m/1e3",               #Truth level variable name that will be unfolded to
    #                 "nominal.top_had_m/1e3",               #Detector level variable name that will be unfolded from
    #                  np.linspace(135,205,15).tolist(),#[-1.0 + x*(1.0 - -1.0)/n_bins for x in range(n_bins+1)],
    #                 method = "bayes",        # we're going to use iterative bayesian
    #                 truth_tuple_name = "particleLevel",
    #                 detector_tuple_name = "nominal",
    #                 data_tuple_is_mc = True, #say that we are in test mode and therefiore the data tuple is infact MC
    #                 save_output = True,       # we want everything to be shoved to a ROOT file
    #                 x_axis_label = "Measured hadronic top mass [GeV]",
    #                 y_axis_label = "Truth hadronic top mass [GeV]",
    #                 unfolded_x_axis_title = "hadronic top mass [GeV]",
    #                 unfolded_y_axis_title = "#frac{1}{#sigma_{0}} #frac{d#sigma}{m^{hadronic}}",
    #                 truth_cuts = truth_cuts,
    #                 detector_cuts = reco_cuts,
    #                 messages = ["Fiducial phase space", "Inclusive Selection"],
    #                 luminosity = c.luminosity,
    #                 name = "hadronic_top_mass"
    #                ))

    # unfolders.append(Unfolder(c.input_mc_tuple,        #MC that we base the unfolding on
                   #  c.input_data_tuple,      #data tuple that we want to unfold
                   #  c.output_folder,         #Where all ROOT  and  eps will be put
                   #  "particleLevel.top_lep_pt/1e3",                 #Truth level variable name that will be unfolded to
                   #  "nominal.top_lep_pt/1e3",                 #Detector level variable name that will be unfolded from
                   #  # A better binning that achieves a reasonable result of the pull test
                   #  np.linspace(0,270,15).tolist(),#[50,75,100,140,180,225,250,300],
                   #  method = "bayes",        # we're going to use iterative bayesian
                   #  truth_tuple_name = "particleLevel",
                   #  detector_tuple_name = "nominal",
                   #  data_tuple_is_mc = True, #say that we are in test mode and therefiore the data tuple is infact MC
                   #  save_output = True,       # we want everything to be shoved to a ROOT file
                   #  x_axis_label = "Measured leptonic top p_{T} [GeV]",
                   #  y_axis_label = "Truth leptonic top p_{T} [GeV]",
                   #  unfolded_x_axis_title = "leptonic top p_{T} [GeV]",
                   #  unfolded_y_axis_title = "#frac{1}{#sigma_{0}} #frac{d#sigma}{p_{T}^{lepton}}",
                   #  truth_cuts = truth_cuts,
                   #  detector_cuts = reco_cuts,
                   #  messages = ["Fiducial phase space", "Inclusive Selection"],
                   #  luminosity = c.luminosity,
                   #  name = "leptonic_top_pt"
                   # ))
    # unfolders.append(Unfolder(c.input_mc_tuple,        #MC that we base the unfolding on
    #                 c.input_data_tuple,      #data tuple that we want to unfold
    #                 c.output_folder,         #Where all ROOT  and  eps will be put
    #                 "particleLevel.top_had_pt/1e3",               #Truth level variable name that will be unfolded to
    #                 "nominal.top_had_pt/1e3",               #Detector level variable name that will be unfolded from
    #                  np.linspace(0,270,15).tolist(),#[-1.0 + x*(1.0 - -1.0)/n_bins for x in range(n_bins+1)],
    #                 method = "bayes",        # we're going to use iterative bayesian
    #                 truth_tuple_name = "particleLevel",
    #                 detector_tuple_name = "nominal",
    #                 data_tuple_is_mc = True, #say that we are in test mode and therefiore the data tuple is infact MC
    #                 save_output = True,       # we want everything to be shoved to a ROOT file
    #                 x_axis_label = "Measured hadronic top p_{T} [GeV]",
    #                 y_axis_label = "Truth hadronic top p_{T} [GeV]",
    #                 unfolded_x_axis_title = "hadronic top p_{T} [GeV]",
    #                 unfolded_y_axis_title = "#frac{1}{#sigma_{0}} #frac{d#sigma}{p_{T}^{hadronic}}",
    #                 truth_cuts = truth_cuts,
    #                 detector_cuts = reco_cuts,
    #                 messages = ["Fiducial phase space", "Inclusive Selection"],
    #                 luminosity = c.luminosity,
    #                 name = "hadronic_top_pt"
    #                ))
    # unfolders.append(Unfolder(c.input_mc_tuple,        #MC that we base the unfolding on
    #                 c.input_data_tuple,      #data tuple that we want to unfold
    #                 c.output_folder,         #Where all ROOT  and  eps will be put
    #                 "particleLevel.theta_d",               #Truth level variable name that will be unfolded to
    #                 "nominal.theta_d",               #Detector level variable name that will be unfolded from
    #                 # A better binning that achieves a reasonable result of the pull test
    #                 np.linspace(0.01,1,10).tolist(),#[-1.0 + x*(1.0 - -1.0)/n_bins for x in range(n_bins+1)],
    #                 method = "bayes",        # we're going to use iterative bayesian
    #                 truth_tuple_name = "particleLevel",
    #                 detector_tuple_name = "nominal",
    #                 data_tuple_is_mc = True, #say that we are in test mode and therefiore the data tuple is infact MC
    #                 save_output = True,       # we want everything to be shoved to a ROOT file
    #                 x_axis_label = "Measured leptonic #theta_{d} = m^{top}/E^{top} [GeV]",
    #                 y_axis_label = "Truth leptonic #theta_{d} = m^{top}/E^{top} [GeV]",
    #                 unfolded_x_axis_title = "leptonic #theta_{d} = m^{top}/E^{top} [GeV]",
    #                 unfolded_y_axis_title = "#frac{1}{#sigma_{0}} #frac{d#sigma}{#theta^{lepton}_{d}}",
    #                 truth_cuts = truth_cuts,
    #                 detector_cuts = reco_cuts,
    #                 messages = ["Fiducial phase space", "Inclusive Selection"],
    #                 luminosity = c.luminosity,
    #                 name = "leptonic_theta_d"
                   # ))
    # unfolders.append(Unfolder(c.input_mc_tuple,        #MC that we base the unfolding on
    #                 c.input_data_tuple,      #data tuple that we want to unfold
    #                 c.output_folder,         #Where all ROOT  and  eps will be put
    #                 "particleLevel.theta_d_had",               #Truth level variable name that will be unfolded to
    #                 "nominal.theta_d_had",               #Detector level variable name that will be unfolded from
    #                  np.linspace(0,1,15).tolist(),#[-1.0 + x*(1.0 - -1.0)/n_bins for x in range(n_bins+1)],
    #                 method = "bayes",        # we're going to use iterative bayesian
    #                 truth_tuple_name = "particleLevel",
    #                 detector_tuple_name = "nominal",
    #                 data_tuple_is_mc = True, #say that we are in test mode and therefiore the data tuple is infact MC
    #                 save_output = True,       # we want everything to be shoved to a ROOT file
    #                 x_axis_label = "Measured hadronic #theta_{d} = m^{top}/E^{top} [GeV]",
    #                 y_axis_label = "Truth hadronic #theta_{d} = m^{top}/E^{top} [GeV]",
    #                 unfolded_x_axis_title = "hadronic #theta_{d} = m^{top}/E^{top} [GeV]",
    #                 unfolded_y_axis_title = "#frac{1}{#sigma_{0}} #frac{d#sigma}{#theta^{hadronic}_{d}}",
    #                 truth_cuts = truth_cuts,
    #                 detector_cuts = reco_cuts,
    #                 messages = ["Fiducial phase space", "Inclusive Selection"],
    #                 luminosity = c.luminosity,
    #                 name = "hadronic_theta_d"
    #                ))
    return unfolders



if __name__ == "__main__":
    main()