#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""Evaluate the differences between events at truth and reconstruction level.

This package provides a set of tools to estimate the number of reco events that do 
not pass truth level cuts, but do pass reco level and vice versa.

"""

import ROOT as r
import RootHelperFunctions.RootHelperFunctions as rhf 
import RootHelperFunctions.StringHelperFunctions as shf 
import RootHelperFunctions.DrawingHelperFunctions as dhf 
import ATLASStyle.AtlasStyle as AS
from copy import deepcopy
import array 
import numpy as np 

class FakeEvalautor:
    def __init__( self, input_file, name = "blah",
                truth_tree_name = "particleLevel",
                reco_tree_name  = "nominal",
                labels = [""] ):
        self.name = name
        self.event_index = "eventNumber"
        self.input_file = input_file
        self.truth_tree_name = truth_tree_name
        self.reco_tree_name = reco_tree_name
        self.labels = labels,
        self.set_style_options()

    def set_style_options(self):
        self.style_options  = dhf.StyleOptions(
                                   draw_options = "HIST",
                                   line_color   = r.kAzure,
                                   line_style   = 1,
                                   line_width   = 3,
                                   fill_color   = r.kWhite,
                                   fill_style   = 0,
                                   marker_color = r.kBlack,
                                   marker_style = 0,
                                   marker_size  = 0,
                                   y_label_size = 0.035,
                                   x_label_size = 0.035,
                                   x_title_size = 0.035,
                                   y_title_size = 0.035,
                                   x_title_offset =  1.3,
                                   y_title_offset =  1.85,
                                   )

        #deep copy the above options as a place holder for the time being
        self.reco_cutflow_style_options  = deepcopy(self.style_options)
        self.reco_cutflow_style_options.fill_color = r.kAzure
        self.reco_cutflow_style_options.fill_style = 3004
        self.reco_cutflow_style_options.legend_options = "f"
        self.reco_cutflow_style_options.x_axis_label_offset = 0.005
        self.reco_cutflow_style_options.draw_options = "HIST"
        self.reco_cutflow_style_options.x_axis_label_color = r.kAzure
        self.reco_cutflow_style_options.x_label_size = 0.0185


        self.truth_cutflow_style_options = deepcopy(self.style_options)
        self.truth_cutflow_style_options.fill_color = r.kRed
        self.truth_cutflow_style_options.line_color = r.kRed
        self.truth_cutflow_style_options.fill_style = 3005
        self.truth_cutflow_style_options.x_axis_label_offset = 0.005#*20
        self.truth_cutflow_style_options.legend_options = "f"
        self.truth_cutflow_style_options.draw_options = "HIST"
        self.truth_cutflow_style_options.x_axis_label_color = r.kRed
        self.truth_cutflow_style_options.x_label_size = 0.0185


    def construct_efficiency_histograms(self,                                   
                                    truth_level_cuts = [],
                                    reco_level_cuts = [],
                                    mc_weight = "weight_mc",
                                    ):
        """
            Build the histogram that presents useful information when estimating fakes. 

            params: 
                input_file: full path to input file
                truth_level_cuts: list of cuts applied at truth level
                reco_level_cuts: list of cuts applied to particle levle

            Construct histograms that has 2 bins:
                epsilon^real = N^(reco & truth)/N^(truth) 
                epsilon^fake = N^(reco & !truth)/N^(truth)
        """

        #construct the cuts 
        truth_weight = "weight_mc"
        for cut in truth_level_cuts:
            truth_weight += "*("+cut+")"

        #construct the cuts 
        reco_weight = "weight_mc"
        for cut in reco_level_cuts:
            reco_weight += "*("+cut+")"

        #Grab histograms from the files that are only filled with the type of events we care about
        self.n_reco_hist = rhf.get_histogram(self.input_file,self.reco_tree_name, "0", [0,1,2], reco_weight, hist_name = self.name + "_n_reco_hist")
        self.n_reco = self.n_reco_hist.GetEntries()

        self.n_truth_hist = rhf.get_histogram(self.input_file,self.truth_tree_name, "0", [0,1,2], truth_weight, hist_name = self.name + "_n_truth_hist")
        self.n_truth = self.n_truth_hist.GetEntries()
 
        #need to add friend detailsfor the more complicated 
        self.n_reco_truth_hist = rhf.get_histogram(
                                                self.input_file,
                                                self.truth_tree_name, 
                                                "0", 
                                                [0,1,2], 
                                                reco_weight+"*("+truth_weight+")", 
                                                hist_name = self.name + "_n_reco_truth_hist",
                                                friend_name = self.reco_tree_name,
                                                index_variable = "eventNumber")
        self.n_reco_truth_hist = rhf.get_histogram(
                                                self.input_file,
                                                self.reco_tree_name , 
                                                "0", 
                                                [0,1,2], 
                                                reco_weight+"*("+truth_weight+")", 
                                                hist_name = self.name + "_n_reco_truth_hist",
                                                friend_name = self.truth_tree_name,
                                                index_variable = "eventNumber")
        self.n_reco_truth = self.n_reco_truth_hist.GetEntries()
        self.n_reco_not_truth = self.n_reco - self.n_reco_truth

        #setup the histogram
        self.efficiency_histogram = r.TH1F("efficiency_histogram","efficiency_histogram",3,0,3)

        self.efficiency_histogram.GetXaxis().SetBinLabel(1,"N^{reco}/N^{truth}")
        self.efficiency_histogram.SetBinContent(1,100.0*self.n_reco /self.n_truth)

        self.efficiency_histogram.GetXaxis().SetBinLabel  (2,"#epsilon^{  real} = N^{reco & truth}/N^{reco}")
        try:
            self.efficiency_histogram.SetBinContent(2,100.0*self.n_reco_truth /self.n_reco)
        except:
            self.efficiency_histogram.SetBinContent(3,-1.0)

        self.efficiency_histogram.GetXaxis().SetBinLabel  (3,"#epsilon^{  fake} = N^{reco & !truth}/N^{reco}")
        try:
            self.efficiency_histogram.SetBinContent(3,100.0*self.n_reco_not_truth /self.n_reco)
        except:
            self.efficiency_histogram.SetBinContent(3,-1.0)

        self.efficiency_histogram.SetDirectory(0)     # Remove ROOT ownership of the hist 

        #shove the results to the terminal also
        self.print_efficiencies()

        return self.efficiency_histogram

    def print_efficiencies(self):
        print "n_reco:            " + str(self.n_reco)
        print "n_truth:           " + str(self.n_truth)
        print "n_reco_truth:      " + str(self.n_reco_truth)
        print "n_reco_not_truth:  " + str(self.n_reco_not_truth)
        print "n^reco/n^truth =   " + str(100.0*self.n_reco /self.n_truth)
        print "e^(real)       =   " + str(100.0*self.n_reco_truth /self.n_truth)
        print "e^(fakes)      =   " + str(100.0*self.n_reco_not_truth /self.n_truth)

    def close(self):
        #clean up 
        self.in_file.Close()
    def evaluate_real_and_fake_distributions(self, output_folder, selection_mode = "resolved"):
        """
            Could not figure out a way to draw the fake using TTree:Draw so instead doing it by iteration over the trees 
        """
        rhf.create_folder(output_folder)
        in_file = rhf.open_file(self.input_file, "READ")

        #set up the histograms in this very cumbersome manner :( 
        self.fake_hists = {}
        self.real_hists = {}
        self.real_fake_x_axis = {}

        #
        binning = [i for i in np.arange(200,500,10)]#[100,120,140,160,190,220,280,350,450,800]
        self.fake_hists["ljet_pt"] = r.TH1F(self.name+"ljet_pt_fakes",self.name+"ljet_pt_fakes", len(binning)-1, array.array('d',binning) )
        self.real_hists["ljet_pt"] = r.TH1F(self.name+"ljet_pt_reals",self.name+"ljet_pt_reals", len(binning)-1, array.array('d',binning) )
        self.real_fake_x_axis["ljet_pt"] = "leading fat jet p_{T} [GeV]"
        #   
        binning = [i for i in np.arange(0,500,10)]#[100,120,140,160,190,220,280,350,450,800]
        self.fake_hists["ljet_m"] = r.TH1F(self.name+"ljet_m_fakes",self.name+"ljet_m_fakes", len(binning)-1, array.array('d',binning) )
        self.real_hists["ljet_m"] = r.TH1F(self.name+"ljet_m_reals",self.name+"ljet_m_reals", len(binning)-1, array.array('d',binning) )
        self.real_fake_x_axis["ljet_m"] = "leading fat jet mass[GeV]"
        #
        binning = [i for i in np.arange(0,500,30)]#[100,120,140,160,190,220,280,350,450,800]
        self.fake_hists["top_lep_pt"] = r.TH1F(self.name+"top_lep_pt_fakes",self.name+"top_lep_pt_fakes", len(binning)-1, array.array('d',binning) )
        self.real_hists["top_lep_pt"] = r.TH1F(self.name+"top_lep_pt_reals",self.name+"top_lep_pt_reals", len(binning)-1, array.array('d',binning) )
        self.real_fake_x_axis["top_lep_pt"] = "leptonic top p_{T} [GeV]"

        binning = [i for i in np.arange(110,500,15)]#[100,120,140,160,190,220,280,350,450,800]
        self.fake_hists["top_lep_m"] = r.TH1F(self.name+"top_lep_m_fakes",self.name+"top_lep_m_fakes", len(binning)-1, array.array('d',binning) )
        self.real_hists["top_lep_m"] = r.TH1F(self.name+"top_lep_m_reals",self.name+"top_lep_m_reals", len(binning)-1, array.array('d',binning) )
        self.real_fake_x_axis["top_lep_m"] = "leptonic top mass [GeV]"
        #
        binning = [i for i in np.arange(0,300,10)]#[100,120,140,160,190,220,280,350,450,800]
        self.fake_hists["b_subjet_pt"] = r.TH1F(self.name+"b_subjet_pt_fakes",self.name+"b_subjet_pt_fakes", len(binning)-1, array.array('d',binning) )
        self.real_hists["b_subjet_pt"] = r.TH1F(self.name+"b_subjet_pt_reals",self.name+"b_subjet_pt_reals", len(binning)-1, array.array('d',binning) )
        self.real_fake_x_axis["b_subjet_pt"] = "b subjet p_{T} [GeV]"
        #
        binning = [i for i in np.arange(0,300,10)]#[100,120,140,160,190,220,280,350,450,800]
        self.fake_hists["g_subjet_pt"] = r.TH1F(self.name+"g_subjet_pt_fakes",self.name+"g_subjet_pt_fakes", len(binning)-1, array.array('d',binning) )
        self.real_hists["g_subjet_pt"] = r.TH1F(self.name+"g_subjet_pt_reals",self.name+"g_subjet_pt_reals", len(binning)-1, array.array('d',binning) )
        self.real_fake_x_axis["g_subjet_pt"] = "g subjet p_{T} [GeV]"
        #
        binning = [i for i in np.arange(0,1.5,0.02)]#[100,120,140,160,190,220,280,350,450,800]
        self.fake_hists["dR_bj_fj"] = r.TH1F(self.name+"dR_bj_fj_fakes",self.name+"dR_bj_fj_fakes", len(binning)-1, array.array('d',binning) )
        self.real_hists["dR_bj_fj"] = r.TH1F(self.name+"dR_bj_fj_reals",self.name+"dR_bj_fj_reals", len(binning)-1, array.array('d',binning) )
        self.real_fake_x_axis["dR_bj_fj"] = "#Delta R(leptonic fat b-jet, R=0.4 b-tagged jet)"
        #
        binning = [i for i in np.arange(0,1.5,0.02)]#[100,120,140,160,190,220,280,350,450,800]
        self.fake_hists["dR_bprong_fj"] = r.TH1F(self.name+"dR_bprong_fj_fakes",self.name+"dR_bprong_fj_fakes", len(binning)-1, array.array('d',binning) )
        self.real_hists["dR_bprong_fj"] = r.TH1F(self.name+"dR_bprong_fj_reals",self.name+"dR_bprong_fj_reals", len(binning)-1, array.array('d',binning) )
        self.real_fake_x_axis["dR_bprong_fj"] = "#Delta R(leptonic fat b-jet,b subprong)"

        binning = [i for i in np.arange(0,4.5,0.02)]#[100,120,140,160,190,220,280,350,450,800]
        self.fake_hists["dr_ljet_particleReco"] = r.TH1F(self.name+"dr_ljet_particleReco_fakes",self.name+"dr_ljet_particleReco_fakes", len(binning)-1, array.array('d',binning) )
        self.real_hists["dr_ljet_particleReco"] = r.TH1F(self.name+"dr_ljet_particleReco_reals",self.name+"dr_ljet_particleReco_reals", len(binning)-1, array.array('d',binning) )
        self.real_fake_x_axis["dr_ljet_particleReco"] = "min(#Delta R(b-fatjet^{reco},b-fatjet^{particle}))"


        binning = [i for i in np.arange(0,350,15)]#[100,120,140,160,190,220,280,350,450,800]
        self.fake_hists["bfatjet_pt"] = r.TH1F(self.name+"bfatjet_pt_fakes",self.name+"bfatjet_pt_fakes", len(binning)-1, array.array('d',binning) )
        self.real_hists["bfatjet_pt"] = r.TH1F(self.name+"bfatjet_pt_reals",self.name+"bfatjet_pt_reals", len(binning)-1, array.array('d',binning) )
        self.real_fake_x_axis["bfatjet_pt"] = "b fatjet p_{T} [GeV]"


        binning = [i for i in np.arange(0,4.5,0.02)]#[100,120,140,160,190,220,280,350,450,800]
        self.fake_hists["bfatjet_eta"] = r.TH1F(self.name+"bfatjet_eta_fakes",self.name+"bfatjet_eta_fakes", len(binning)-1, array.array('d',binning) )
        self.real_hists["bfatjet_eta"] = r.TH1F(self.name+"bfatjet_eta_reals",self.name+"bfatjet_eta_reals", len(binning)-1, array.array('d',binning) )
        self.real_fake_x_axis["bfatjet_eta"] = "b fatjet |#eta|"

        binning = [i for i in np.arange(0,4.5,0.02)]#[100,120,140,160,190,220,280,350,450,800]
        self.fake_hists["dr_bjet_particleReco"] = r.TH1F(self.name+"dr_bjet_particleReco_fakes",self.name+"dr_bjet_particleReco_fakes", len(binning)-1, array.array('d',binning) )
        self.real_hists["dr_bjet_particleReco"] = r.TH1F(self.name+"dr_bjet_particleReco_reals",self.name+"dr_bjet_particleReco_reals", len(binning)-1, array.array('d',binning) )
        self.real_fake_x_axis["dr_bjet_particleReco"] = "min(#Delta R(ljet^{reco},ljet^{particle}))"

        binning = [i for i in np.arange(0,5,0.1)]#[100,120,140,160,190,220,280,350,450,800]
        self.fake_hists["nljets_reco_truth"] = r.TH1F(self.name+"nljets_reco_truth_fakes",self.name+"nljets_reco_truth_fakes", len(binning)-1, array.array('d',binning) )
        self.real_hists["nljets_reco_truth"] = r.TH1F(self.name+"nljets_reco_truth_reals",self.name+"nljets_reco_truth_reals", len(binning)-1, array.array('d',binning) )
        self.real_fake_x_axis["nljets_reco_truth"] = "n_{ljet}^{reco}/n_{ljet}^{truth}"
        #
        binning = [i for i in np.arange(0,4,1)]#[100,120,140,160,190,220,280,350,450,800]
        self.fake_hists["njets"] = r.TH1F(self.name+"nljets_fakes",self.name+"nljets_fakes", len(binning)-1, array.array('d',binning) )
        self.real_hists["njets"] = r.TH1F(self.name+"nljets_reals",self.name+"nljets_reals", len(binning)-1, array.array('d',binning) )
        self.real_fake_x_axis["njets"] = "n_{ljet}^{reco}"
        binning = [i for i in np.arange(0,9,1)]#[100,120,140,160,190,220,280,350,450,800]
        self.fake_hists["njets_truth"] = r.TH1F(self.name+"njets_truth_fakes",self.name+"njets_truth_fakes", len(binning)-1, array.array('d',binning) )
        self.real_hists["njets_truth"] = r.TH1F(self.name+"njets_truth_reals",self.name+"njets_truth_reals", len(binning)-1, array.array('d',binning) )
        self.real_fake_x_axis["njets_truth"] = "n_{ljet}^{truth}"
        #
        #
        #get the input tuples   
        reco_tree     = in_file.Get(self.reco_tree_name)
        particle_tree = in_file.Get(self.truth_tree_name)
        reco_tree.BuildIndex('eventNumber')
        particle_tree.BuildIndex('eventNumber')

        for event in reco_tree:
            #we don't care about events that don't pass reco selection
            pass_resolved_reco = ( False                 
                        or event.ejets_resolved_2015 or event.ejets_resolved_2016 
                        or event.mujets_resolved_2015 or event.mujets_resolved_2016 
                        )
            pass_boosted_reco = ( False 
                        or event.ejets_2015 or event.ejets_2016 
                        or event.mujets_2015 or event.mujets_2016
                        )
            if selection_mode == "resolved":
                pass_reco = pass_resolved_reco
            elif selection_mode == "boosted":
                pass_reco = pass_boosted_reco
            else:
                pass_reco = pass_resolved_reco or pass_boosted_reco
            pass_reco = pass_reco and (event.g_subjet_pt/1e3 >= 25)




            #try and read the particle level entry 
            p_index = particle_tree.GetEntryNumberWithIndex(event.eventNumber)
            particle_tree.GetEntry(p_index)

            if p_index == -1:
                #if the event is not in the particle level tree then it's got to be a fake
                is_fake = True
            else:          
                #check if it's a fake or not            
                is_real_resolved = (
                        False 
                        or particle_tree.ejets_particle_trimming_resolved  
                        or particle_tree.mujets_particle_trimming_resolved  
                        )  and particle_tree.g_subjet_pt/1e3 >= 20 
                is_real_boosted = (
                        False 
                        or particle_tree.ejets_particle_trimming  
                        or particle_tree.mujets_particle_trimming  
                        ) and particle_tree.g_subjet_pt/1e3 >= 20 

                if selection_mode == "resolved":
                    is_fake = not is_real_resolved
                elif selection_mode == "boosted":
                    is_fake = not is_real_boosted
                else: 
                    is_fake = not( is_real_boosted or is_real_resolved ) 
           

            #now identify which of the largeR jets is the leptonic-b
            ljet_index = 0
            if len(event.ljet_pt) > 0:
                if  event.ljet_tagged_80[0] and len(event.ljet_pt)>1:
                    ljet_index = 1
                ljet_p4 = r.TLorentzVector()
                ljet_p4.SetPtEtaPhiM(event.ljet_pt[ljet_index],event.ljet_eta[ljet_index], event.ljet_phi[ljet_index],event.ljet_m[ljet_index])
                bjet_p4 = event.skinny_bjet

            else:
                ljet_index = -1

            #we actually only care about events that 
            if not is_fake and ljet_index >=0:
                particle_ljet_p4s = []

                min_deltaR = 100000
                for i in range(len(particle_tree.ljet_pt)):
                    particle_ljet_p4s.append(r.TLorentzVector())
                    particle_ljet_p4s[i].SetPtEtaPhiE(  particle_tree.ljet_pt[i],particle_tree.ljet_eta[i], particle_tree.ljet_phi[i],particle_tree.ljet_e[i])
                    if min_deltaR > particle_ljet_p4s[i].DeltaR(ljet_p4):
                        particle_ljet_p4 = particle_ljet_p4s[i]
                
                particle_bjet_p4 = particle_tree.skinny_bjet

                if pass_reco:
                    # self.real_hists["dr_ljet_particleReco"].Fill(  particle_ljet_p4.DeltaR(ljet_p4), event.weight_mc)#
                    self.real_hists["dr_bjet_particleReco"].Fill(  particle_bjet_p4.DeltaR(bjet_p4), event.weight_mc)#
                    self.real_hists["nljets_reco_truth"].Fill( float(len(event.ljet_pt))/float(len(particle_tree.ljet_pt)) )
                    self.real_hists["dr_ljet_particleReco"].Fill(  particle_tree.bfatjet.DeltaR(event.bfatjet), event.weight_mc)

                else:
                    # print "truth only:" 
                    # print particle_ljet_p4.DeltaR(ljet_p4)
                    self.fake_hists["dr_ljet_particleReco"].Fill(  particle_tree.bfatjet.DeltaR(event.bfatjet), event.weight_mc)

                    self.fake_hists["dr_bjet_particleReco"].Fill(  particle_bjet_p4.DeltaR(bjet_p4), event.weight_mc)#
                    # self.fake_hists["dr_ljet_particleReco"].Fill(  particle_ljet_p4.DeltaR(ljet_p4), event.weight_mc)
                    # self.fake_hists["dr_ljet_particleReco"].Fill(  particle_tree.bfatjet.DeltaR(event.bfatjet), event.weight_mc)
                    self.fake_hists["nljets_reco_truth"].Fill( float(len(event.ljet_pt))/float(len(particle_tree.ljet_pt)) )

          

            if not pass_reco:
                continue

            if is_fake:
                self.fake_hists["ljet_pt"].Fill(  event.ljet_pt[0]/1e3, event.weight_mc)
                self.fake_hists["ljet_m"].Fill(  event.ljet_m[0]/1e3, event.weight_mc)
                self.fake_hists["top_lep_pt"].Fill(  event.top_lep_pt/1e3, event.weight_mc)
                self.fake_hists["top_lep_m"].Fill(  event.top_lep_m/1e3, event.weight_mc)
                self.fake_hists["b_subjet_pt"].Fill(  event.b_subjet_pt/1e3, event.weight_mc)
                self.fake_hists["g_subjet_pt"].Fill(  event.g_subjet_pt/1e3, event.weight_mc)
                self.fake_hists["njets"].Fill(len(event.ljet_pt) )
                # self.fake_hists["njets_truth"].Fill(len(particle_tree.ljet_pt) )
                #now identify which of the largeR jets is the leptonic-b
                self.fake_hists["dR_bj_fj"].Fill(  ljet_p4.DeltaR(event.skinny_bjet), event.weight_mc)
                self.fake_hists["bfatjet_pt"].Fill( event.bfatjet.Pt(), event.weight_mc)
                self.fake_hists["bfatjet_eta"].Fill( event.bfatjet.Eta(), event.weight_mc)

                b_sjet_p4 = r.TLorentzVector()
                b_sjet_p4.SetPtEtaPhiE(event.b_subjet_pt,event.b_subjet_eta,event. b_subjet_phi,event.b_subjet_e)
                self.fake_hists["dR_bprong_fj"].Fill(  ljet_p4.DeltaR(b_sjet_p4), event.weight_mc)
            else:
                self.real_hists["ljet_pt"].Fill(  event.ljet_pt[0]/1e3, event.weight_mc)
                self.real_hists["ljet_m"].Fill(  event.ljet_m[0]/1e3, event.weight_mc)
                self.real_hists["top_lep_pt"].Fill(  event.top_lep_pt/1e3, event.weight_mc)
                self.real_hists["top_lep_m"].Fill(  event.top_lep_m/1e3, event.weight_mc)
                self.real_hists["b_subjet_pt"].Fill(  event.b_subjet_pt/1e3, event.weight_mc)
                self.real_hists["g_subjet_pt"].Fill(  event.g_subjet_pt/1e3, event.weight_mc)

                self.real_hists["bfatjet_pt"].Fill( event.bfatjet.Pt()/1e3, event.weight_mc)

                self.real_hists["bfatjet_eta"].Fill( event.bfatjet.Eta(), event.weight_mc)
            #now identify which of the largeR jets is the leptonic-b
                self.real_hists["dR_bj_fj"].Fill(  ljet_p4.DeltaR(event.skinny_bjet), event.weight_mc)
                self.real_hists["njets"].Fill(len(event.ljet_pt) )
                self.real_hists["njets_truth"].Fill(len(particle_tree.ljet_pt) )

                b_sjet_p4 = r.TLorentzVector()
                b_sjet_p4.SetPtEtaPhiE(event.b_subjet_pt,event.b_subjet_eta,event. b_subjet_phi,event.b_subjet_e)
                self.real_hists["dR_bprong_fj"].Fill(  ljet_p4.DeltaR(b_sjet_p4), event.weight_mc)


        #divide by width and free from ROOTs icy fingers
        for name in self.real_hists:
            self.fake_hists[name].Scale(1.0,"width")
            self.real_hists[name].Scale(1.0,"width")
            self.fake_hists[name].SetDirectory(0)
            self.real_hists[name].SetDirectory(0)

        in_file.Close() 
        self.draw_real_and_fake_distributions(output_folder)
        
    
    def draw_real_and_fake_distributions(self,output_folder):
        """
            Plots the reco distrubtion of leading largeR jet, with a breakdown
            between total and fake components. 

            Fake is defined as "pass reco but not truth"

            This function should be called internally as part of 'evaluat_real_and_fake_distributions'
        """
        AS.SetAtlasStyle()
        canvas = r.TCanvas("n_efficiencies", "n_efficiencies",800,800)

        #set up the style options 
        fake_options = dhf.StyleOptions(
                                    draw_options = "HIST",
                                    line_color   = r.kRed,
                                    line_style   = 1,
                                    line_width   = 0,
                                    legend_options = "f",
                                    fill_color   = r.kRed,
                                    fill_style   = 1001,
                                    marker_color = r.kBlack,
                                    marker_style = 0,
                                    marker_size  = 0,

                                    y_label_size = 0.035,
                                    x_label_size = 0.035,
                                    x_title_size = 0.035,
                                    y_title_size = 0.035,
                                    x_title_offset =  1.3,
                                    y_title_offset =  1.85,
                                    )
        fake_options.fill_style = 3004
        fake_options.line_width = 3

        ##
        real_options = deepcopy(fake_options  )
        real_options.line_color  = r.kBlack
        real_options.line_width  = 3
        real_options.fill_color  = r.kBlack
        real_options.fill_style  = 0
        real_options.fill_style  = 3005
        real_options.legend_options  = "f"

        for name in self.real_hists:
            real_hist = self.real_hists[name]
            fake_hist = self.fake_hists[name]

            try:
                real_hist.Scale(1.0/real_hist.Integral())
            except:
                pass
            try:
                fake_hist.Scale(1.0/fake_hist.Integral())
            except:
                pass

            fake_name = "Fake events"
            if name == "dr_bjet_particleReco" or name == "dr_ljet_particleReco" or name == "nljets_reco_truth":
            # or name == "bfatjet_pt" or name == "bfatjet_eta" :
                fake_name = "Truth and !Reco"
                
            dhf.plot_histogram( canvas, 
                    {
                       "Real events": [real_hist, real_options ], 
                       fake_name: [fake_hist, fake_options ] 
                    },
                    x_axis_title = self.real_fake_x_axis[name],
                    y_axis_title = "Normalized number of events",
                    labels = self.labels,
                    do_atlas_labels = True)
            canvas.Print(output_folder+"fake_real_"+name+".eps")

    def draw_efficiency_histograms(self, output_folder):
        """
            Draw the builthistogram that presents useful information when estimating fakes. 
            Requires construct_efficiency_histograms to be called. 

            params: 
                output_folder: full file path that the canvas will be saved to. 

            Construct histograms that has 3 bins:
                N^reco / N^truth
                epsilon^real = N^(reco & truth)/N^(truth) 
                epsilon^fake = N^(reco & !truth)/N^(truth)
        """
        rhf.create_folder(output_folder)

        
        AS.SetAtlasStyle()
        canvas = r.TCanvas("n_efficiencies", "n_efficiencies",800,800)
        # canvas.SetRightMargin(0.02)
        # canvas.SetLeftMargin(0.05)
        
        dhf.plot_histogram( canvas, 
                {  "eff":  [ self.efficiency_histogram, self.style_options ]  },
                x_axis_title = "",
                y_axis_title = "Fractional number of events [%]",
                labels = self.labels,
                do_atlas_labels = True,
                do_legend = False)
        canvas.Print(output_folder+"n_efficiencies.eps")


    def add_isFake(self):
        #ugly non-TTRee commands :( 

        pass

    def construct_comparitive_cutflows(self,
        starting_truth_bin, 
        starting_reco_bin,
        truth_cut_flow_names, 
        reco_cut_flow_names,
        output_folder
        ):
        rhf.create_folder(output_folder)

        # Open up the input_file and get the truth and reco level cut flows 
        in_file = rhf.open_file(self.input_file, "READ")

        reco_hists, truth_hists = {},{}
        index = 0

        print "Adding Truth histograms: "
        #hopefully the user has provided a set of cutflows where it makes sense to add them 
        for hist_tuple in truth_cut_flow_names:
            #rename the tuple to be a bit clearer
            name = hist_tuple[0]
            shift_index = hist_tuple[1]
            print "\t",name," shifting: ", shift_index

            #safely read inthe histogram
            truth_hists[name] = in_file.Get(name + "/cutflow_particle_level")
            assert type(truth_hists[name]) == r.TH1D, "ERROR: Failed to find histogram ( is type " + str(type(truth_hists[name])) + name + " from input file: " +  self.input_file 

            #normalize and move the bin to the approraite point
            truth_hists[name].Scale(100.0/truth_hists[name].GetBinContent(1))
            truth_hists[name] = rhf.shift_bins(truth_hists[name],  shift_index)

            if index == 0:
                total_truth_cutflow =   truth_hists[name].Clone(self.name + "total_truth_cutflow")
            else:
                for i in xrange(1,total_truth_cutflow.GetSize()-2):
                    print "\t\tAdding bin: ", total_truth_cutflow.GetXaxis().GetBinLabel(i), " to ", truth_hists[name].GetXaxis().GetBinLabel(i)

                total_truth_cutflow.Add( truth_hists[name] )
            index += 1
            print ""
            print ""

        print "Adding reco histograms: "
        index = 0
        for hist_tuple in reco_cut_flow_names:
            #rename the tuple to be a bit clearer
            name = hist_tuple[0]
            shift_index = hist_tuple[1]
            print "\t",name," shifting: ", shift_index

            reco_hists[name] = in_file.Get(name + "/cutflow")
            assert type(reco_hists[name]) == r.TH1D, "ERROR: Failed to find histogram " +  name +  " from input file: " + self.input_file

            #normalize and move the bin to the approraite point            
            reco_hists[name].Scale(100.0/reco_hists[name].GetBinContent(1))
            reco_hists[name] = rhf.shift_bins(reco_hists[name],  shift_index)

            if index == 0:
                total_reco_cutflow =   reco_hists[name].Clone(self.name + "total_reco_cutflow")
            else:
                for i in xrange(1,total_reco_cutflow.GetSize()-2):
                    print "\t\tAdding bin: ", total_reco_cutflow.GetXaxis().GetBinLabel(i), " to ", reco_hists[name].GetXaxis().GetBinLabel(i)

                total_reco_cutflow.Add( reco_hists[name].Clone() )
            index+=1
            print ""
            print ""

        #Now Set to the same xrange so we don't have a bunch of white space to the right
        total_reco_cutflow. GetXaxis().SetRangeUser(1, total_truth_cutflow.GetSize()-starting_truth_bin-3)
        total_truth_cutflow.GetXaxis().SetRangeUser(1, total_truth_cutflow.GetSize()-starting_truth_bin-3)

        #move the created and formated histograms to the class and free them from ROOT's 
        #icy grip
        self.total_reco_cutflow = total_reco_cutflow
        self.total_reco_cutflow.SetDirectory(0)

        self.total_truth_cutflow = total_truth_cutflow
        self.total_truth_cutflow.SetDirectory(0)

        self.total_efficiency_hist  = rhf.bin_by_bin_divide_histogram( total_reco_cutflow,total_truth_cutflow )
        self.total_efficiency_hist.GetXaxis().SetRangeUser(1, total_truth_cutflow.GetSize()-starting_truth_bin-3)
        self.total_efficiency_hist = self.total_efficiency_hist

        self.draw_comparitive_cutflows(output_folder)
        self.draw_efficiency_flow(output_folder)


    def draw_efficiency_flow(self,output_folder):
        # Plot the comparison using style optinos defined in the constructor 
        AS.SetAtlasStyle()
        canvas = r.TCanvas("comparitive_cutflow", "comparitive_cutflow",800,800)
        canvas.SetBottomMargin(0.2)

        ##draw the cinoarutuve histogram first 
        self.total_efficiency_hist.SetMaximum(1.0)
        self.total_efficiency_hist.SetMinimum(.0)
        dhf.plot_histogram( canvas, 
                {  
                   "Reco":  [ self.total_efficiency_hist, self.reco_cutflow_style_options ], 
                },
                x_axis_title = "",
                y_axis_title = "N^{reco}/N^{truth}",
                labels = self.labels,
                do_atlas_labels = True,
                do_legend = False)
        canvas.Print(output_folder+self.name+"_comparitive_efficiency.eps")

    def draw_comparitive_cutflows(self,
        output_folder
        ):
         # Plot the comparison using style optinos defined in the constructor 
        AS.SetAtlasStyle()
        canvas = r.TCanvas("comparitive_cutflow", "comparitive_cutflow",800,800)
        canvas.SetBottomMargin(0.2)

   
        dhf.plot_histogram( canvas, 
                {  
                   "Truth":  [ self.total_truth_cutflow, self.truth_cutflow_style_options],
                   "Reco":   [ self.total_reco_cutflow, self.reco_cutflow_style_options], 
                },
                x_axis_title = "",
                y_axis_title = "Fractional number of events [%]",
                do_atlas_labels = True,
                do_legend = True)
                
        canvas.Print(output_folder+self.name+"_comparitive_cutflow.eps")

        dhf.plot_histogram( canvas, 
                {  
                   # "Truth":  [ total_truth_cutflow, self.truth_cutflow_style_options ],
                   "Reco":  [ self.total_reco_cutflow, self.reco_cutflow_style_options ], 
                },
                x_axis_title = "",
                y_axis_title = "Fractional number of events [%]",
                do_atlas_labels = True,
                do_legend = True)
   

        canvas.Print(output_folder+self.name+"_comparitive_reco_cutflow.eps")



        dhf.plot_histogram( canvas, 
                {  
                   "Truth":  [ self.total_truth_cutflow, self.truth_cutflow_style_options ],
                   # "Reco":  [ total_reco_cutflow, self.reco_cutflow_style_options ], 
                },
                x_axis_title = "",
                y_axis_title = "Fractional number of events [%]",
                do_atlas_labels = True,
                do_legend = True)
   

        canvas.Print(output_folder+self.name+"_comparitive_truth_cutflow.eps")





