import math 
import numpy as np

def statistical_likelihood(parameters, data, template_1, template_2):
    """
    The likelihood function for a generic template fit 
    to be called by scipy.optimize.minimize. This 
    assumes the data is some unknown mix of two templates
    and tries to evaluate a parameter, mu, that corresponds to
    the fraction of template_1. 
    Thus each bin has content:
        mu*T + (1-mu)*t
    where T is the number of events coming from events of type
    template 1, and t is events from template 2. 
    
    @Parameters
    -----------
    parameters: tuple of paramters that is passed by minimize, 
    			the first element is mu
    data: Bin contents of the data histogram
    template_1: Bin Contents of template_1 
    template_2: Bin Contents of template_2 
    """
    
    # Relabel things so they're a bit more readable
    mu = parameters[0]

    # Iterate over the points  and evaluate the Likelihood, L
    L = 0
    
    for i, d in enumerate(data):
        T, t = template_1[i], template_2[i]
        predicted  =   (mu * T) + ((1.0 - mu) * t) 
        
        if d <= 0:
            continue

        if predicted < 0:
            return 9999999
        # Log likelihood of a poisson 
        L += (predicted - d*math.log(predicted) )
        L += (d*math.log(d) - d ) # Stirlings for the fatorial

    # Return the liklihood
    return L         

def normalized_likelihood(parameters, data, template_1, template_2):
    """
    The likelihood function for a generic template fit 
    to be called by scipy.optimize.minimize. This 
    assumes the data is some unknown mix of two templates
    and tries to evaluate a parameter, mu, that corresponds to
    the fraction of template_1. 
    Thus each bin has content:
        mu*T + (1-mu)*t
    where T is the number of events coming from events of type
    template 1, and t is events from template 2. 
    
    @Parameters
    -----------
    parameters: tuple of paramters that is passed by minimize
    data: Bin contents of the data histogram
    template_1: Bin Contents of template_1 
    template_2: Bin Contents of template_2 
    """
    
    # Relabel things so they're a bit more readable
    mu = parameters[0]
    
    # Iterate over the points  and evaluate the Likelihood, L
    L = 0 

    N_data, N_pred = 0,0 
    for i, d in enumerate(data):
        T, t = template_1[i], template_2[i]
        predicted  =  (mu * T) + ((1.0 - mu) * t) 
        N_pred += predicted
        N_data += d 
    
    for i, d in enumerate(data):
        T, t = template_1[i], template_2[i]
        predicted  =  (mu * T) + ((1.0 - mu) * t) 
        
        if d <= 0:
            continue

        if predicted < 0:
            return 9999999
    
        L += (predicted - d*math.log(predicted) )
        L += (d*math.log(d) - d )
    
    likelihoods_simple.append(L)
    fitting_mus_simple.append(mu)
    # Return the liklihood
    return L         