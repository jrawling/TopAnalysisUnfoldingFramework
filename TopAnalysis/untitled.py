{
    "name": "t#bar{t}"
    
    "files": 
    [
        { 
            "folder": '/eos/atlas/atlascerngroupdisk/phys-top/deadcone/slimmed_v0-03-02/mc16a/user.jrawling.410470.PhPy8EG.5DAOD_TOPQ1.e6337_e5984_s3126_r9364_r9315_p3409.mc16a.18-07-25v3_output_root/out_13*.root,
            "k_factor": 1.1398
            "x_sec": 385.87
            "data_period": "mc16a"
        },

        { 
            "folder": '/eos/atlas/atlascerngroupdisk/phys-top/deadcone/slimmed_v0-03-02/mc16a/user.jrawling.410470.PhPy8EG.5DAOD_TOPQ1.e6337_e5984_s3126_r9364_r9315_p3409.mc16a.18-07-25v3_output_root/out_13*.root,
            "k_factor": 1.1398
            "x_sec": 385.87
            "data_period": "mc16d"
        }
    ]    
}
