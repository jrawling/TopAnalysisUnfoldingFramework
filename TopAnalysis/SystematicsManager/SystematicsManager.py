'''
    Author: Jacob Rawling <jrawling@cern.ch>

    A simple way to load all of the desired systematic varations based on weights and ntuples for analysisTop output files

    Usage:
        for a given unfolder (see TopAnalysis/Unfolder ) simply assign the systmatics as
        unfolder.systematics = load_systematics(unfolder, ["JVT","Lepton","Pile Up"])


'''

from collections import OrderedDict
from TopAnalysis.Unfolding.Systematic import Systematic
from all_systs import all_systs
from all_systs_grouped import all_systs_grouped

def select_systematics( selected_systematics = ["JVT", "Lepton", "Pile Up", "Flavour", "MET", "JES"]  ):
    all_possible_systs =  []
    for syst_grouping in all_systs:
        for syst in syst_grouping:
            all_possible_systs.append(syst['name'].replace(" ", "_").replace("#", ""))
    
    print("ALL POSSIBLE:")
    print(all_possible_systs)
    
    if any("all" in sys for sys in selected_systematics):
        return all_possible_systs

    for syst in selected_systematics:
        if not(syst in sys for sys in all_possible_systs):
            throw("UNKNOWN Systematic: " + str(syst))
    return selected_systematics

def load_systematics(unfolder, selected_systematics = ["JVT", "Lepton", "Pile Up", "Flavour", "MET", "JES"],bootstrap=1000,grouped=False):
    """
    Return the selected systematics 
    """
    looping_systs = all_systs_grouped if grouped else all_systs
    systematics = OrderedDict()
    for syst_grouping in looping_systs:
        for syst in syst_grouping:
            systematics[syst['name'].replace(" ", "_").replace("#", "")] =  Systematic(
                                 [ syst ] ,
                                 unfolder.input_mc_tuple, 
                                 unfolder.x_axis_binning, 
                                 unfolder.detector_level_variable,
                                 cuts=unfolder.detector_cuts,
                                 truth_cut_weight=unfolder.truth_cut_weight,
                                 truth_variable=unfolder.truth_level_variable,
                                 truth_tuple=unfolder.truth_tuple_name,
                                 truth_weight=unfolder.truth_weight,
                                 signal_sample=unfolder.signal_sample, 
                                 output_folder = unfolder.output_folder,
                                 n_iterations = unfolder.n_iterations,
                                 name=syst['name'].replace(" ", "_").replace("#", ""),
                                 bootstrap=None,
                                 luminosity=unfolder.luminosity
                                 )

    if isinstance(selected_systematics,str):
        if "all" in selected_systematics :
            return systematics
            
    final_systematics = OrderedDict()
    for syst in selected_systematics:
        try:
            final_systematics[syst] = systematics[syst]
        except:
            print("FAILED TO READ SYSTEMATIC: ", syst)

    return final_systematics
