"""
A summary of all systematic variations used in this analysed stored in a dictioanry with following information:
    - Weight string for tttree draw 
    - name for plotting and tables
    - directionality of the systematic
    - Name of tree 
    - Combination name 
    - Signal samppl
    - Background sample 
    - Full sim or fast sim response to use  
"""
from TopAnalysis.Backgrounds import Sample
import RootHelperFunctions.DrawingHelperFunctions as dhf
from TopAnalysis.Backgrounds import Sample, ttbar_pdf_samples, ttbar_frag_had_samples,ttbar_fastsim_samples,ttbar_pdf_debug_samples,ttbar_martix_el_var_samples

combinations = [
    "Jet", "Flavour", "Pile-up", "Lepton", "MET", "Backgrounds", "Modelling"
    # "Jet", "Lepton", "Flavour", "MET", "JER", "Pile-up", "Modelling"
    ]
#Systematics JET_JER_NOISE_FORWARD__1up,JET_JER_CROSS_CALIB_FORWARD__1up,JET_JER_NP0__1up,JET_JER_NP0__1down,JET_JER_NP1__1up,JET_JER_NP1__1down,JET_JER_NP2__1up,JET_JER_NP2__1down,JET_JER_NP3__1up,JET_JER_NP3__1down,JET_JER_NP4__1up,JET_JER_NP4__1down,JET_JER_NP5__1up,JET_JER_NP5__1down,JET_JER_NP6__1up,JET_JER_NP6__1down,JET_JER_NP7__1up,JET_JER_NP7__1down,JET_JER_NP8__1up,JET_JER_NP8__1down

all_systs_grouped = [ 
    [{  
    'vary_background': 'Fakes',
    'n_background_variations': 4,
    'name': 'Fakes',
    'weight': "weight_mc*weight_leptonSF*weight_bTagSF_MV2c10_85*weight_pileup*weight_jvt",
    'tree': 'nominal',
    'direction':'up',
    'sig_samples':  None,
    'bkg_samples':  None,
    'combination': 'Backgrounds'
    }],
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Frag. and Had.",  
        'direction':"asymm",  
        'tree':"nominal",  
        'sig_samples':  ttbar_frag_had_samples,
        'bkg_samples':  None,
        'combination':"Modelling",
        'is_afii': True
        }
    ],    
   
    [{
    'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
    'name':"Fastsim_closure",  
    'direction':"asymm",  
    'tree':"nominal",  
    'sig_samples':  ttbar_fastsim_samples,
    'bkg_samples':  None,
    'combination':"Modelling",
    'is_afii': True
    }],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Matrix Element",  
        'direction':"asymm",  
        'tree':"nominal",  
        'sig_samples':  ttbar_martix_el_var_samples,
        'bkg_samples':  None,
        'combination':"Modelling",
        'is_afii': True
        }
    ],    
    # [
    #     {
    #     'weight':"mc_generator_weights[6]*mc_generator_weights[194]*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
    #     'name':"Var3Down",  
    #     'direction':"asymm",  
    #     'tree':"nominal",  
    #     'sig_samples':  ttbar_pdf_samples,
    #     'bkg_samples':  None,
    #     'combination':"Modelling"  
    #     }
    # ],    

    # [{
    # 'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
    # 'name':"Fastsim_closure",  
    # 'direction':"asymm",  
    # 'tree':"nominal",  
    # 'sig_samples':  ttbar_fastsim_samples,
    # 'bkg_samples':  None,
    # 'combination':"Modelling",
    # 'is_afii': True
    # }],    

    [{	
    'vary_background': 't#bar{t}V',
    'n_background_variations': 2,
    'name': 'ttV',
    'weight': "weight_mc*weight_leptonSF*weight_bTagSF_MV2c10_85*weight_pileup*weight_jvt",
    'tree': 'nominal',
    'direction':'asymm',
    'sig_samples':  None,
    'bkg_samples':  None,
    'combination': 'Backgrounds'
    }],


    [{	
    'vary_background': 'Diboson',
    'n_background_variations': 2,
    'name': 'Diboson',
    'weight': "weight_mc*weight_leptonSF*weight_bTagSF_MV2c10_85*weight_pileup*weight_jvt",
    'tree': 'nominal',
    'direction':'asymm',
    'sig_samples':  None,
    'bkg_samples':  None,
    'combination': 'Backgrounds'
    }],
        [{	
    'vary_background': 'W+jets',
    'n_background_variations': 2,
    'name': 'W+jets',
    'weight': "weight_mc*weight_leptonSF*weight_bTagSF_MV2c10_85*weight_pileup*weight_jvt",
    'tree': 'nominal',
    'direction':'asymm',
    'sig_samples':  None,
    'bkg_samples':  None,
    'combination': 'Backgrounds'
    }],
    [{	
    'vary_background': 'Z+jets',
    'n_background_variations': 2,
    'name': 'Z+jets',
    'weight': "weight_mc*weight_leptonSF*weight_bTagSF_MV2c10_85*weight_pileup*weight_jvt",
    'tree': 'nominal',
    'direction':'asymm',
    'sig_samples':  None,
    'bkg_samples':  None,
    'combination': 'Backgrounds'
    }],
    [{	
    'vary_background': 'Single top',
    'n_background_variations': 2,
    'name': 'Single top',
    'weight': "weight_mc*weight_leptonSF*weight_bTagSF_MV2c10_85*weight_pileup*weight_jvt",
    'tree': 'nominal',
    'direction':'asymm',
    'sig_samples':  None,
    'bkg_samples':  None,
    'combination': 'Backgrounds'
    }],
    # Add sample list as last element  of this, default paramaeter of none 
    [
        {
        'weight': "weight_mc*weight_leptonSF*weight_bTagSF_MV2c10_85*weight_pileup*weight_jvt_UP",
        'name': 'JVT Up',
        'direction': 'up',
        'tree': 'nominal',
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination': 'Jet'
        },
        {
        'weight': "weight_mc*weight_leptonSF*weight_bTagSF_MV2c10_85*weight_pileup*weight_jvt_DOWN",
        'name': "JVT Down",
        'direction': "down",  
        'tree': "nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination': "Jet"  
        }
    ],    
    [
        {
        'weight':"weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_EL_SF_Trigger_UP",  
        'name':"e Trigger Up",  
        'direction':"up",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Lepton"  
        },
        {
        'weight':"weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_EL_SF_Trigger_DOWN",  
        'name':"e Trigger Down",  
        'direction':"down",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Lepton"  
        }
    ],    
    [
        {
        'weight':"weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_EL_SF_Reco_UP",  
        'name':"e Reco Up",  
        'direction':"up",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Lepton"  
        },
        {
        'weight':"weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_EL_SF_Reco_DOWN",  
        'name':"e Reco Down",  
        'direction':"down",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Lepton"  
        }
    ],    
    [
        {
        'weight':"weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_EL_SF_ID_UP",  
        'name':"e ID Up",  
        'direction':"up",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Lepton"  
        },
        {
        'weight':"weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_EL_SF_ID_DOWN",  
        'name':"e ID Down",  
        'direction':"down",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Lepton"  
        }
    ],
    [
        {
        'weight':"weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_EL_SF_Isol_UP",  
        'name':"e Isolation Up",  
        'direction':"up",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Lepton"  
        },
        {
        'weight':"weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_EL_SF_Isol_DOWN",  
        'name':"e Isolation Down",  
        'direction':"down",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Lepton"  
        }
    ],    
    [
        {
        'weight':"weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_MU_SF_Trigger_STAT_UP",  
        'name':"#mu Trigger Up",  
        'direction':"up",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Lepton"  
        },
        {
        'weight':"weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_MU_SF_Trigger_STAT_DOWN",  
        'name':"#mu Trigger Down",  
        'direction':"down",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Lepton"  
        }
    ],    
    [
        {
        'weight':"weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_MU_SF_ID_STAT_UP",  
        'name':"#mu ID Up",  
        'direction':"up",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Lepton"  
        },
        {
        'weight':"weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_MU_SF_ID_STAT_DOWN",  
        'name':"#mu ID Down",  
        'direction':"down",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Lepton"  
        }
    ],    
    [
        {
        'weight':"weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_MU_SF_Isol_STAT_UP",  
        'name':"#mu Isolation Up",  
        'direction':"up",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Lepton"  
        },
        {
        'weight':"weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_MU_SF_Isol_STAT_DOWN",  
        'name':"#mu Isolation Down",  
        'direction':"down",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Lepton"  
        }
    ],    
    [
        {
        'weight':"weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_MU_SF_Trigger_SYST_UP",  
        'name':"#mu Trigger Up",  
        'direction':"up",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Lepton"  
        },
        {
        'weight':"weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_MU_SF_Trigger_SYST_DOWN",  
        'name':"#mu Trigger Down",  
        'direction':"down",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Lepton"  
        }
    ],    
    [
        {
        'weight':"weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_MU_SF_ID_SYST_UP",  
        'name':"#mu ID Up",  
        'direction':"up",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Lepton"  
        },
        {
        'weight':"weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_MU_SF_ID_SYST_DOWN",  
        'name':"#mu ID Down",  
        'direction':"down",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Lepton"  
        }
    ],    
    [
        {
        'weight':"weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_MU_SF_Isol_SYST_UP",  
        'name':"#mu Isolation Up",  
        'direction':"up",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Lepton"  
        },
        {
        'weight':"weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_MU_SF_Isol_SYST_DOWN",  
        'name':"#mu Isolation Down",  
        'direction':"down",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Lepton"  
        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_bTagSF_MV2c10_85*weight_jvt*weight_pileup_UP",  
        'name':"Pile-up Up",  
        'direction':"up",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Pile-up"  
        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_bTagSF_MV2c10_85*weight_jvt*weight_pileup_DOWN",
        'name':"Pile-up Down",  
        'direction':"down",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Pile-up"  
        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85_eigenvars_B_up",  
        'name':"B Up",  
        'direction':"up",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Flavour"  
        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85_eigenvars_B_down",  
        'name':"B Down",  
        'direction':"down",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Flavour"  
        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85_eigenvars_C_up",  
        'name':"C Up",  
        'direction':"up",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Flavour"  
        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85_eigenvars_C_down",  
        'name':"C Down",  
        'direction':"down",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Flavour"  
        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85_eigenvars_Light_up",  
        'name':"Light Up",  
        'direction':"up",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,    
        'combination':"Flavour"  
        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85_eigenvars_Light_down",
        'name':"Light Down",  
        'direction':"down",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Flavour"  
        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85_extrapolation_up",  
        'name':"Extrap. Up",  
        'direction':"up",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Flavour"  
        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85_extrapolation_down",  
        'name':"Extrap. Down",  
        'direction':"down",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Flavour"  
        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85_extrapolation_from_charm_up",  
        'name':"Charm Extrap. Up",  
        'direction':"up",  
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Flavour"  
        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85_extrapolation_from_charm_down",
        'name':"Charm Extrap. Down",
        'direction':"down",
        'tree':"nominal",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Flavour"  
        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Reso. Para",  
        'direction':"up_and_down",  
        'tree':"MET_SoftTrk_ResoPara",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"MET"  
        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Reso. Perp",  
        'direction':"up_and_down",  
        'tree':"MET_SoftTrk_ResoPerp",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"MET"  
        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Scale Up",  
        'direction':"up",  
        'tree':"MET_SoftTrk_ScaleUp",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"MET"  
        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Scale Down",  
        'direction':"down",  
        'tree':"MET_SoftTrk_ScaleDown",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"MET"  
        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"JER",  
        'direction':"asymm",  
        'tree':"JET_JER_SINGLE_NP__1up",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  
        }
    ],
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Flavour Comp. down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_Flavor_Composition__1down"            ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  
        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Flavour Comp. up",  
        'direction':"up",  
        'tree':"JET_CategoryReduction_JET_Flavor_Composition__1up"              ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  
        }
    ],
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Flavour Resp. down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_Flavor_Response__1down"               ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  
        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Flavour Resp. up",  
        'direction':"up",  
        'tree':"JET_CategoryReduction_JET_Flavor_Response__1up"                 ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  
        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"b. JES down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_BJES_Response__1down"      ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  
        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"b. JES up",  
        'direction':"up",  
        'tree':"JET_CategoryReduction_JET_BJES_Response__1up"          ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  
        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Det. down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Detector1__1down"         ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  
        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Det. up",  
        'direction':"up",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Detector1__1up"           ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  
        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Modelling 1 down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Modelling1__1down"        ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Modelling 1 up",  
        'direction':"up",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Modelling1__1up"          ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Modelling 2 down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Modelling2__1down"        ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Modelling 2 up",  
        'direction':"up",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Modelling2__1up"          ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        }
    ],    
    [

        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Modelling 3 down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Modelling3__1down"        ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Modelling 3 up",  
        'direction':"up",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Modelling3__1up"          ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Modelling 4 down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Modelling4__1down"        ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Modelling 4 up",  
        'direction':"up",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Modelling4__1up"          ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"#eta intercal mod down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_EtaIntercalibration_Modelling__1down" ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"#eta intercal mod up",  
        'direction':"up",  
        'tree':"JET_CategoryReduction_JET_EtaIntercalibration_Modelling__1up"   ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"#eta intercal non-closure E down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1down",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"#eta intercal non-closure E up",  
        'direction':"up",  
        'tree':"JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1up"  ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"#eta intercal non-closure - down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1down",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"#eta intercal non-closure - up",  
        'direction':"up",  
        'tree':"JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1up"  ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        }
    ],
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"#eta intercal non-closure + down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1down",  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"#eta intercal non-closure + up",  
        'direction':"up",  
        'tree':"JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1up"  ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        }
    ],
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"#eta intercal stat. down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_EtaIntercalibration_TotalStat__1down" ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"#eta intercal stat. up",  
        'direction':"up",  
        'tree':"JET_CategoryReduction_JET_EtaIntercalibration_TotalStat__1up"   ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        }
    ],
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Pile-up #mu down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_Pileup_OffsetMu__1down"               ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Pile-up #mu up",  
        'direction':"up",  
        'tree':"JET_CategoryReduction_JET_Pileup_OffsetMu__1up"                 ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        }
    ],
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Pile-up NPV down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_Pileup_OffsetNPV__1down"              ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        },
        {
        'weight': "weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name': "Pile-up NPV up",  
        'direction': "up",  
        'tree': "JET_CategoryReduction_JET_Pileup_OffsetNPV__1up"                ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination': "Jet",
        }
    ],
    [        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Pile-up pT down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_Pileup_PtTerm__1down"                 ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Pile-up pT up",  
        'direction':"up",  
        'tree':"JET_CategoryReduction_JET_Pileup_PtTerm__1up"                   ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        }
    ],
    [        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Pile-up #rho top. down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_Pileup_RhoTopology__1down"            ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Pile-up #rho top. up",  
        'direction':"up",  
        'tree':"JET_CategoryReduction_JET_Pileup_RhoTopology__1up"              ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Punchthrough down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_PunchThrough_MC16__1down"             ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Punchthrough up",  
        'direction':"up",  
        'tree':"JET_CategoryReduction_JET_PunchThrough_MC16__1up"               ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Single part. high pT down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_SingleParticle_HighPt__1down"         ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Single part. high pT up",  
        'direction':"up",  
        'tree':"JET_CategoryReduction_JET_SingleParticle_HighPt__1up"           ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Mixed 1. down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Mixed1__1down"            ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Mixed 1. up",  
        'direction':"up",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Mixed1__1up"              ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Mixed 2. down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Mixed2__1down"            ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Mixed 2. up",  
        'direction':"up",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Mixed3__1up"              ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Mixed 3. down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Mixed3__1down"            ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Mixed 3. up",  
        'direction':"up",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Mixed3__1up"       ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Stat. 1 down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Statistical1__1down"         ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Stat. 1 up",  
        'direction':"up",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Statistical1__1up"         ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Stat. 2 down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Statistical2__1down"         ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Stat. 2 up",  
        'direction':"up",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Statistical2__1up"         ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Stat. 3 down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Statistical3__1down"         ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Stat. 3 up",  
        'direction':"up",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Statistical3__1up"         ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Stat. 4 down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Statistical4__1down"         ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Stat. 4 up",  
        'direction':"up",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Statistical4__1up"         ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Stat. 5 down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Statistical5__1down"         ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Stat. 5 up",  
        'direction':"up",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Statistical5__1up"         ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        }
    ],    
    [
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Stat. 6 down",  
        'direction':"down",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Statistical6__1down"         ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        },
        {
        'weight':"weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85",  
        'name':"Stat. 6 up",  
        'direction':"up",  
        'tree':"JET_CategoryReduction_JET_EffectiveNP_Statistical6__1up"         ,  
        'sig_samples':  None,
        'bkg_samples':  None,
        'combination':"Jet"  

        }
    ],    
    ]   
