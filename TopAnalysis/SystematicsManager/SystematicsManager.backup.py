# '''
#     Author: Jacob Rawling <jrawling@cern.ch>

#     A simple way to load all of the desired systematic varations based on weights and ntuples for analysisTop output files

#     Usage:
#         for a given unfolder (see TopAnalysis/Unfolder ) simply assign the systmatics as
#         unfolder.systematics = load_systematics(unfolder, ["JVT","Lepton","Pile Up"])


# '''

# from collections import OrderedDict
# from TopAnalysis.Unfolding.Systematic import Systematic

# def select_systematics( selected_systematics = ["JVT", "Lepton", "Pile Up", "Flavour", "MET", "JES"]  ):
#     all_systs =  [
#                 "JVT",
#                 "Lepton",
#                 "Pile Up",
#                 "B-Tag Flavour",
#                 "JES Flavour",
#                 # # The following Requires AnalysisTop to run with systematics turned on
#                 "MET",
#                 "JER",
#                 "b-JES",
#                 "JES Modelling",
#                 "JES #eta-intercal",
#                 "JES Pile-up",
#                 "JES Punchthrough",
#                 "JES Single Part.",
#                 "JES Mixed",
#                 "JES Stat."
#                 ]
    
#     if any("all" in sys for sys in selected_systematics):
#         return all_systs

#     for syst in selected_systematics:
#         if not(syst in sys for sys in all_systs):
#             throw("UNKNOWN Systematic: " + str(syst))

#     return selected_systematics

# def load_systematics(unfolder, selected_systematics = ["JVT", "Lepton", "Pile Up", "Flavour", "MET", "JES"]):
#     """
#         Retru
#     """

#     systematics = OrderedDict()
#     systematics["JVT"] = Systematic([
#                                  ["weight_mc*weight_leptonSF*weight_bTagSF_MV2c10_85*weight_pileup*weight_jvt_UP",  "JVT Up",  "up",   "nominal"],
#                                  ["weight_mc*weight_leptonSF*weight_bTagSF_MV2c10_85*weight_pileup*weight_jvt_DOWN","JVT Down","down", "nominal"],
#                               ],
#                              unfolder.input_mc_tuple, 
#                              unfolder.x_axis_binning, 
#                              unfolder.detector_level_variable,
#                              unfolder.detector_cuts,
#                              output_folder = unfolder.output_folder,
#                              n_iterations = unfolder.n_iterations,
#                              name="jvt",
#                              bootstrap=None,
#                              dropable=False
#                              )

#     systematics["Lepton"] = Systematic([
#                                  ["weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_EL_SF_Trigger_UP",          "e Trigger Up",       "up",     "nominal"],
#                                  ["weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_EL_SF_Trigger_DOWN",        "e Trigger Down",     "down",   "nominal"],
#                                  ["weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_EL_SF_Reco_UP",             "e Reco Up",          "up",     "nominal"],
#                                  ["weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_EL_SF_Reco_DOWN",           "e Reco Down",        "down",   "nominal"],
#                                  ["weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_EL_SF_ID_UP",               "e ID Up",            "up",     "nominal"],
#                                  ["weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_EL_SF_ID_DOWN",             "e ID Down",          "down",   "nominal"],
#                                  ["weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_EL_SF_Isol_UP",             "e Isolation Up",     "up",     "nominal"],
#                                  ["weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_EL_SF_Isol_DOWN",           "e Isolation Down",   "down",   "nominal"],
#                                  ["weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_MU_SF_Trigger_STAT_UP",     "#mu Trigger Up",     "up",     "nominal"],
#                                  ["weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_MU_SF_Trigger_STAT_DOWN",   "#mu Trigger Down",   "down",   "nominal"],
#                                  ["weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_MU_SF_ID_STAT_UP",          "#mu ID Up",          "up",     "nominal"],
#                                  ["weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_MU_SF_ID_STAT_DOWN",        "#mu ID Down",        "down",   "nominal"],
#                                  ["weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_MU_SF_Isol_STAT_UP",        "#mu Isolation Up",   "up",     "nominal"],
#                                  ["weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_MU_SF_Isol_STAT_DOWN",      "#mu Isolation Down", "down",   "nominal"],
#                                  ["weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_MU_SF_Trigger_SYST_UP",     "#mu Trigger Up",     "up",     "nominal"],
#                                  ["weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_MU_SF_Trigger_SYST_DOWN",   "#mu Trigger Down",   "down",   "nominal"],
#                                  ["weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_MU_SF_ID_SYST_UP",          "#mu ID Up",          "up",     "nominal"],
#                                  ["weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_MU_SF_ID_SYST_DOWN",        "#mu ID Down",        "down",   "nominal"],
#                                  ["weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_MU_SF_Isol_SYST_UP",        "#mu Isolation Up",   "up",     "nominal"],
#                                  ["weight_mc*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup*weight_leptonSF_MU_SF_Isol_SYST_DOWN",      "#mu Isolation Down", "down",   "nominal"],
#                                 ],
#                              unfolder.input_mc_tuple, 
#                              unfolder.x_axis_binning, 
#                              unfolder.detector_level_variable,
#                              unfolder.detector_cuts,
#                              output_folder = unfolder.output_folder,
#                              n_iterations = unfolder.n_iterations,
#                              name="lepton",
#                              bootstrap=None,
#                              dropable=False
#                              )


#     systematics["Pile Up"] = Systematic([
#                                  ["weight_mc*weight_leptonSF*weight_bTagSF_MV2c10_85*weight_jvt*weight_pileup_UP",  "Pile-up Up",   "up",   "nominal"],
#                                  ["weight_mc*weight_leptonSF*weight_bTagSF_MV2c10_85*weight_jvt*weight_pileup_DOWN","Pile-up Down", "down", "nominal"],
#                               ],
#                              unfolder.input_mc_tuple, 
#                              unfolder.x_axis_binning, 
#                              unfolder.detector_level_variable,
#                              unfolder.detector_cuts,
#                              output_folder = unfolder.output_folder,
#                              n_iterations = unfolder.n_iterations,
#                              name="pile-up",
#                              bootstrap=None,
#                              dropable=False
#                              )
    
#     systematics["B-Tag Flavour"] = Systematic([
#                                  ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85_eigenvars_B_up",      "B Up",         "up",       "nominal"],
#                                  ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85_eigenvars_B_down",    "B Down",       "down",     "nominal"],
#                                  ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85_eigenvars_C_up",      "C Up",         "up",       "nominal"],
#                                  ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85_eigenvars_C_down",    "C Down",       "down",     "nominal"],
#                                  ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85_eigenvars_Light_up",  "Light Up",     "up",       "nominal"],
#                                  ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85_eigenvars_Light_down","Light Down",   "down",     "nominal"],
#                                  ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85_extrapolation_up",    "Extrap. Up",   "up",       "nominal"],
#                                  ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85_extrapolation_down",  "Extrap. Down", "down",     "nominal"],
#                                  ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85_extrapolation_from_charm_up",  "Charm Extrap. Up",  "up",  "nominal"],
#                                  ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85_extrapolation_from_charm_down","Charm Extrap. Down","down","nominal"],

#                               ],
#                              unfolder.input_mc_tuple, 
#                              unfolder.x_axis_binning, 
#                              unfolder.detector_level_variable,
#                              unfolder.detector_cuts,
#                              output_folder = unfolder.output_folder,
#                              n_iterations = unfolder.n_iterations,
#                              name="flavour_tagging",
#                              bootstrap=1000,
#                              dropable=False
#                              )

    
#     systematics["JES Flavour"] = Systematic([
#                                  ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Flavour Comp. down", "down", "JET_CategoryReduction_JET_Flavor_Composition__1down"            ],
#                                  ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Flavour Comp. up", "up",   "JET_CategoryReduction_JET_Flavor_Composition__1up"              ],
#                                  ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Flavour Resp. down", "down", "JET_CategoryReduction_JET_Flavor_Response__1down"               ],
#                                  ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Flavour Resp. up", "up",   "JET_CategoryReduction_JET_Flavor_Response__1up"                 ],

#                               ],
#                              unfolder.input_mc_tuple, 
#                              unfolder.x_axis_binning, 
#                              unfolder.detector_level_variable,
#                              unfolder.detector_cuts,
#                              output_folder = unfolder.output_folder,
#                              n_iterations = unfolder.n_iterations,
#                              name="jes_flavour_tagging",
#                              bootstrap=1000,
                             
#                              )
#     systematics["MET"] = Systematic([
#                                  ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Reso. Para", "up_and_down", "MET_SoftTrk_ResoPara"],
#                                  ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Reso. Perp", "up_and_down", "MET_SoftTrk_ResoPerp"],
#                                  ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Scale Up",   "up",    "MET_SoftTrk_ScaleUp"],
#                                  ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Scale Down", "down",  "MET_SoftTrk_ScaleDown"],
#                               ],
#                              unfolder.input_mc_tuple, 
#                              unfolder.x_axis_binning, 
#                              unfolder.detector_level_variable,
#                              unfolder.detector_cuts,
#                              output_folder = unfolder.output_folder,
#                              n_iterations = unfolder.n_iterations,
#                              name="met",
#                              bootstrap=1000,
                             
#                              )    

#     systematics["JER"] = Systematic([
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "JER", "asymm",   "JET_JER_SINGLE_NP__1up"                                   ],
#                      ],
#                      unfolder.input_mc_tuple, 
#                      unfolder.x_axis_binning, 
#                      unfolder.detector_level_variable,
#                      unfolder.detector_cuts,
#                      output_folder = unfolder.output_folder,
#                      n_iterations = unfolder.n_iterations,
#                      name="JER",
#                      bootstrap=1000
#                      )  
#     systematics["b-JES"] = Systematic([
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "b. JES down", "down", "JET_CategoryReduction_JET_BJES_Response__1down"      ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "b. JES up", "up",   "JET_CategoryReduction_JET_BJES_Response__1up"          ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Det. down", "down", "JET_CategoryReduction_JET_EffectiveNP_Detector1__1down"         ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Det. up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Detector1__1up"           ],
#                      ],
#                      unfolder.input_mc_tuple, 
#                      unfolder.x_axis_binning, 
#                      unfolder.detector_level_variable,
#                      unfolder.detector_cuts,
#                      output_folder = unfolder.output_folder,
#                      n_iterations = unfolder.n_iterations,
#                      name="b-JES",
#                      bootstrap=1000
#                      )  
#     # systematics["JES Stat."] = Systematic([
#     #                     ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 1 down", "down", "JET_CategoryReduction_JET_EffectiveNP_Statistical1__1down"         ],
#     #                     ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 1 up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Statistical1__1up"         ],
#     #                     ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 2 down", "down", "JET_CategoryReduction_JET_EffectiveNP_Statistical2__1down"         ],
#     #                     ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 2 up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Statistical2__1up"         ],
#     #                     ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 3 down", "down", "JET_CategoryReduction_JET_EffectiveNP_Statistical3__1down"         ],
#     #                     ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 3 up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Statistical3__1up"         ],
#     #                     ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 4 down", "down", "JET_CategoryReduction_JET_EffectiveNP_Statistical4__1down"         ],
#     #                     ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 4 up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Statistical4__1up"         ],
#     #                     ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 5 down", "down", "JET_CategoryReduction_JET_EffectiveNP_Statistical5__1down"         ],
#     #                     ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 5 up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Statistical5__1up"         ],
#     #                     ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 6 down", "down", "JET_CategoryReduction_JET_EffectiveNP_Statistical6__1down"         ],
#     #                     ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 6 up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Statistical6__1up"         ],
#     #                  ],
#     #                  unfolder.input_mc_tuple, 
#     #                  unfolder.x_axis_binning, 
#     #                  unfolder.detector_level_variable,
#     #                  unfolder.detector_cuts,
#     #                  output_folder = unfolder.output_folder,
#     #                  n_iterations = unfolder.n_iterations,
#     #                  name="JES-Stat")  
#     # systematics["JES Mixed"] = Systematic([
#     #                     ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Mixed 1. down", "down", "JET_CategoryReduction_JET_EffectiveNP_Mixed1__1down"            ],
#     #                     ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Mixed 1. up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Mixed1__1up"              ],
#     #                     ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Mixed 2. down", "down", "JET_CategoryReduction_JET_EffectiveNP_Mixed2__1down"            ],
#     #                     ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Mixed 2. up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Mixed3__1up"              ],
#     #                     ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Mixed 3. down", "down", "JET_CategoryReduction_JET_EffectiveNP_Mixed3__1down"            ],
#     #                     ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Mixed 3. up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Mixed3__1up"              ],
#     #                    ],
#     #                  unfolder.input_mc_tuple, 
#     #                  unfolder.x_axis_binning, 
#     #                  unfolder.detector_level_variable,
#     #                  unfolder.detector_cuts,
#     #                  output_folder = unfolder.output_folder,
#     #                  n_iterations = unfolder.n_iterations,
#     #                  name="JES-Mixed")  
#     systematics["JES Modelling"] = Systematic([
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Modelling 1 down", "down", "JET_CategoryReduction_JET_EffectiveNP_Modelling1__1down"        ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Modelling 1 up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Modelling1__1up"          ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Modelling 2 down", "down", "JET_CategoryReduction_JET_EffectiveNP_Modelling2__1down"        ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Modelling 2 up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Modelling2__1up"          ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Modelling 3 down", "down", "JET_CategoryReduction_JET_EffectiveNP_Modelling3__1down"        ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Modelling 3 up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Modelling3__1up"          ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Modelling 4 down", "down", "JET_CategoryReduction_JET_EffectiveNP_Modelling4__1down"        ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Modelling 4 up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Modelling4__1up"          ],
#                      ],
#                      unfolder.input_mc_tuple, 
#                      unfolder.x_axis_binning, 
#                      unfolder.detector_level_variable,
#                      unfolder.detector_cuts,
#                      output_folder = unfolder.output_folder,
#                      n_iterations = unfolder.n_iterations,
#                      name="JES-modelling",
#                      bootstrap=1000)  
#     systematics["JES #eta-intercal"] = Systematic([
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "#eta intercal mod down", "down", "JET_CategoryReduction_JET_EtaIntercalibration_Modelling__1down" ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "#eta intercal mod up", "up",   "JET_CategoryReduction_JET_EtaIntercalibration_Modelling__1up"   ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "#eta intercal non-closure E down", "down", "JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1down"],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "#eta intercal non-closure E up", "up",   "JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1up"  ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "#eta intercal non-closure - down", "down", "JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1down"],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "#eta intercal non-closure - up", "up",   "JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1up"  ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "#eta intercal non-closure + down", "down", "JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1down"],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "#eta intercal non-closure + up", "up",   "JET_CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1up"  ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "#eta intercal stat. down", "down", "JET_CategoryReduction_JET_EtaIntercalibration_TotalStat__1down" ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "#eta intercal stat. up", "up",   "JET_CategoryReduction_JET_EtaIntercalibration_TotalStat__1up"   ],
#                         ],
#                      unfolder.input_mc_tuple, 
#                      unfolder.x_axis_binning, 
#                      unfolder.detector_level_variable,
#                      unfolder.detector_cuts,
#                      output_folder = unfolder.output_folder,
#                      n_iterations = unfolder.n_iterations,
#                      name="JES_etaintercal",
#                      bootstrap=1000)  
#     # systematics["JES Flavour"] = Systematic([
#     #                     ],
#     #                  unfolder.input_mc_tuple, 
#     #                  unfolder.x_axis_binning, 
#     #                  unfolder.detector_level_variable,
#     #                  unfolder.detector_cuts,
#     #                  output_folder = unfolder.output_folder,
#     #                  n_iterations = unfolder.n_iterations,
#     #                  name="JES-flavour-comp")
#     systematics["JES Pile-up"] = Systematic([
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Pile-up #mu down", "down", "JET_CategoryReduction_JET_Pileup_OffsetMu__1down"               ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Pile-up #mu up", "up",   "JET_CategoryReduction_JET_Pileup_OffsetMu__1up"                 ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Pile-up NPV down", "down", "JET_CategoryReduction_JET_Pileup_OffsetNPV__1down"              ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Pile-up NPV up", "up",   "JET_CategoryReduction_JET_Pileup_OffsetNPV__1up"                ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Pile-up pT down", "down", "JET_CategoryReduction_JET_Pileup_PtTerm__1down"                 ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Pile-up pT up", "up",   "JET_CategoryReduction_JET_Pileup_PtTerm__1up"                   ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Pile-up #rho top. down", "down", "JET_CategoryReduction_JET_Pileup_RhoTopology__1down"            ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Pile-up #rho top. up", "up",   "JET_CategoryReduction_JET_Pileup_RhoTopology__1up"              ],
#                        ],
#                      unfolder.input_mc_tuple, 
#                      unfolder.x_axis_binning, 
#                      unfolder.detector_level_variable,
#                      unfolder.detector_cuts,
#                      output_folder = unfolder.output_folder,
#                      n_iterations = unfolder.n_iterations,
#                      name="JES-pile-up",
#                      bootstrap=1000)  

#     systematics["JES Punchthrough"] = Systematic([ ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Punchthrough down", "down", "JET_CategoryReduction_JET_PunchThrough_MC16__1down"             ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Punchthrough up", "up",   "JET_CategoryReduction_JET_PunchThrough_MC16__1up"               ],
#                      ],
#                      unfolder.input_mc_tuple, 
#                      unfolder.x_axis_binning, 
#                      unfolder.detector_level_variable,
#                      unfolder.detector_cuts,
#                      output_folder = unfolder.output_folder,
#                      n_iterations = unfolder.n_iterations,
#                      name="JES-Punchthrough",
#                      bootstrap=1000)    


#     systematics["JES Single Part."] = Systematic([ 
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Single part. high pT down", "down", "JET_CategoryReduction_JET_SingleParticle_HighPt__1down"         ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Single part. high pT up", "up",   "JET_CategoryReduction_JET_SingleParticle_HighPt__1up"           ],
#                      ],
#                      unfolder.input_mc_tuple, 
#                      unfolder.x_axis_binning, 
#                      unfolder.detector_level_variable,
#                      unfolder.detector_cuts,
#                      output_folder = unfolder.output_folder,
#                      n_iterations = unfolder.n_iterations,
#                      name="JES-single-part",
#                      bootstrap=1000)    
    

#     systematics["JES Mixed"] = Systematic([ 
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Mixed 1. down", "down", "JET_CategoryReduction_JET_EffectiveNP_Mixed1__1down"            ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Mixed 1. up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Mixed1__1up"              ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Mixed 2. down", "down", "JET_CategoryReduction_JET_EffectiveNP_Mixed2__1down"            ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Mixed 2. up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Mixed3__1up"              ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Mixed 3. down", "down", "JET_CategoryReduction_JET_EffectiveNP_Mixed3__1down"            ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Mixed 3. up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Mixed3__1up"              ],                        
#                         # ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 1 down", "down", "JET_CategoryReduction_JET_EffectiveNP_Statistical1__1down"         ],
#                         # ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 1 up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Statistical1__1up"         ],
#                         # ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 2 down", "down", "JET_CategoryReduction_JET_EffectiveNP_Statistical2__1down"         ],
#                         # ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 2 up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Statistical2__1up"         ],
#                         # ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 3 down", "down", "JET_CategoryReduction_JET_EffectiveNP_Statistical3__1down"         ],
#                         # ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 3 up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Statistical3__1up"         ],
#                         # ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 4 down", "down", "JET_CategoryReduction_JET_EffectiveNP_Statistical4__1down"         ],
#                         # ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 4 up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Statistical4__1up"         ],
#                         # ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 5 down", "down", "JET_CategoryReduction_JET_EffectiveNP_Statistical5__1down"         ],
#                         # ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 5 up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Statistical5__1up"         ],
#                         # ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 6 down", "down", "JET_CategoryReduction_JET_EffectiveNP_Statistical6__1down"         ],
#                         # ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 6 up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Statistical6__1up"         ],

#                      ],
#                      unfolder.input_mc_tuple, 
#                      unfolder.x_axis_binning, 
#                      unfolder.detector_level_variable,
#                      unfolder.detector_cuts,
#                      output_folder = unfolder.output_folder,
#                      n_iterations = unfolder.n_iterations,
#                      name="JES-Mixed",
#                      bootstrap=1000)    

#     systematics["JES Stat."] = Systematic([ 
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 1 down", "down", "JET_CategoryReduction_JET_EffectiveNP_Statistical1__1down"         ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 1 up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Statistical1__1up"         ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 2 down", "down", "JET_CategoryReduction_JET_EffectiveNP_Statistical2__1down"         ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 2 up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Statistical2__1up"         ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 3 down", "down", "JET_CategoryReduction_JET_EffectiveNP_Statistical3__1down"         ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 3 up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Statistical3__1up"         ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 4 down", "down", "JET_CategoryReduction_JET_EffectiveNP_Statistical4__1down"         ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 4 up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Statistical4__1up"         ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 5 down", "down", "JET_CategoryReduction_JET_EffectiveNP_Statistical5__1down"         ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 5 up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Statistical5__1up"         ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 6 down", "down", "JET_CategoryReduction_JET_EffectiveNP_Statistical6__1down"         ],
#                         ["weight_mc*weight_leptonSF*weight_jvt*weight_pileup*weight_bTagSF_MV2c10_85", "Stat. 6 up", "up",   "JET_CategoryReduction_JET_EffectiveNP_Statistical6__1up"         ],

#                      ],
#                      unfolder.input_mc_tuple, 
#                      unfolder.x_axis_binning, 
#                      unfolder.detector_level_variable,
#                      unfolder.detector_cuts,
#                      output_folder = unfolder.output_folder,
#                      n_iterations = unfolder.n_iterations,
#                      name="JES-Stat",
#                      bootstrap=1000)    
#     # EG_RESOLUTION_ALL__1down
#     # EG_RESOLUTION_ALL__1up
#     # EG_SCALE_AF2__1down
#     # EG_SCALE_AF2__1up
#     # EG_SCALE_ALL__1down
#     # EG_SCALE_ALL__1up

#     # MUON_ID__1up
#     # MUON_MS__1down
#     # MUON_MS__1up
#     # MUON_SAGITTA_RESBIAS__1down
#     # MUON_SAGITTA_RESBIAS__1up
#     # MUON_SAGITTA_RHO__1down
#     # MUON_SAGITTA_RHO__1up
#     # MUON_SCALE__1down
#     # MUON_SCALE__1up
#     final_systematics = OrderedDict()

#     for syst in selected_systematics:
#         final_systematics[syst] = systematics[syst]

#     return final_systematics