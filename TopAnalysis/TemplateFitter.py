"""
Author: Jacob Rawling
Date: June/2018

Perform a binned maximum loglikelihood of data a mixed fraction of two templates
"""
import numpy as np

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from scipy.optimize import minimize
from scipy.special import factorial 
import RootHelperFunctions.RootHelperFunctions as rhf
import TopAnalysis.Likelihoods as likelihoods 
import math 
from tqdm import tqdm
from TopAnalysis.SystematicsManager.all_systs_grouped import combinations

def draw_ATLAS_labels(ax, messages, x=0.05,y=0.95,align='left'):
    """
    Draw the ATLAS label for a Matplotlib plot .
    """
    plt.rc('font', family='sans-serif',size=25)
    plt.text(x, y, '\\textbf{ATLAS} Internal', fontsize=45, transform=ax.transAxes,horizontalalignment=align)
    y -= 0.038
    plt.text(x, y, '$\\sqrt{s}=13$ TeV, 79.8fb$^{-1}$', fontsize=45, transform=ax.transAxes,horizontalalignment=align)
    y -= 0.038
    for msg in messages:
        plt.text(x, y, msg, fontsize=45, transform=ax.transAxes,horizontalalignment=align)
        y -= 0.038
    plt.rc('font', family='serif',size=35)

class TemplateFitter:
    """
    class to handle the fitting of a histogram 
    """
    def __init__(self,      
                 template_1,
                 template_2,
                 data, 
                 likelihood=likelihoods.statistical_likelihood,  
                 normalize=False,
                 n_iterations=30,#1e3,
                 n_pseudoexperiments=int(3e2), 
                 syst_shifts=None, 
                 combinations=None, 
                 ):
        """
        @Parameters
        -----------
        template_1: A list of the bin contents of the  unnormalized first 
                    template 
        template_2: A list of the bin contents of the  unnormalized second 
                    template
        data: A list of the bin contents of the unnormalized  measured data
        normalize (optiona): A boolean indicating whether this is a normalize 
                             measurement or not  
                             NOTE: This changes the likelihood! 
        """
        self.template_1 = template_1
        self.template_2 = template_2
        self.data  = data
        self.likelihood = likelihood
        self.n_iterations=int(n_iterations)
        self.n_pseudoexperiments = n_pseudoexperiments
        self.mu_stats = None
        self.syst_shifts = syst_shifts
        self.combinations = combinations

    def fit(self, stat_uncert=None, syst_uncert=None, return_mean=False):
        '''

        Parameters
        ----------
        stat_uncert: Arrays 
        syst_uncert:  Arrays 
        '''
        mu_stat, mu_syst = [-1],[-1]
        mu_stat_error = -1.0
        if stat_uncert is not None:
            print("Running statistical uncertainty!" )
            # Evaluate the statistical uncertainty 
            data_vars, fitters, mu_stat = [], [], []

            # 
            for i in tqdm(xrange(self.n_pseudoexperiments)):
                data_vars.append( [d*np.random.normal(1,stat_uncert[j]) for j,d 
                                    in enumerate(self.data)])
                fitters.append( TemplateFitter(self.template_1,
                                               self.template_2, 
                                               data_vars[-1])
                              )
                mu_toy = fitters[-1].perform_fit()
                mu_stat.append(mu_toy)

            # Save the mu stat distribution
            self.mu_stats = mu_stat
            mu_stat_error =  np.std(mu_stat)
        
        # Evaluate the systematic uncertainty on 
        syst_error = self.evaluate_systematic_uncertainty()



        if return_mean:
            return np.std(mu_stat), mu_stat_error, syst_error
            
        return self.perform_fit(), mu_stat_error, syst_error

    def evaluate_systematic_uncertainty(self):
        """
        """
        if  self.syst_shifts is None:
                return -1.0

        self.mu_nom = self.perform_fit(data=self.syst_shifts[
                                        "nominal"])

        # First evaluate the nominal value of mu
        self.systematic_uncertainties = {}
        mu_total = 0.0
        for syst_name in self.syst_shifts:
            self.systematic_uncertainties[syst_name] = self.perform_fit(
                                    data=self.syst_shifts[syst_name])-self.mu_nom
            mu_total = ((mu_total)**2+
                        (self.systematic_uncertainties[syst_name])**2)**0.5

        self.mu_total_syst = mu_total
        print( self.systematic_uncertainties)
        return mu_total

    def plot_systematics(self,output_folder,name, messages):
            """
            """
            plt.rc('text', usetex=True)
            plt.rc('font', family='serif',size=25)
            plt.figure(figsize=(30,20))
            ax=plt.gca()

            if 'nominal' in self.systematic_uncertainties:
                del self.systematic_uncertainties['nominal']

            # Sort the systematics by size 
            systematics = {}
            combinations.reverse()
            for comb in combinations:
                systematics[comb] = {}

            for comb in combinations:
                for i,syst_name in enumerate(self.systematic_uncertainties):
                    if syst_name.split(': ')[0] in comb:
                        systematics[comb][syst_name.split(': ')[1]] = self.systematic_uncertainties[syst_name]
            # Sort each one by category 
            sorted_syst_uncert_keys = {}
            for systs in systematics:
                sorted_syst_uncert_keys[systs] = sorted(systematics[systs], 
                        key=lambda dict_key: abs(systematics[systs][dict_key]),#systematic_uncertainties.get,
                        reverse=False)
                print "Sorted ", systs, " combination: "
                print sorted_syst_uncert_keys[systs] 
            
            y_tick_labels = ['']
            ax.xaxis.grid()
            for j, systs in enumerate(["Lepton","Pile-up", "MET","Backgrounds", "Flavour","Jet","Modelling",]):
                y_tick_labels += sorted_syst_uncert_keys[systs]
            y_tick_labels += ['Total','']
            # format the greek letters and spaces
            y_tick_labels = [l.replace("_", " ").replace('eta', '$\\eta$').replace('mu','$\\mu$').\
                               replace("rho", "$\\rho$") for l in y_tick_labels]
            N = len(y_tick_labels)-1

            # ax.fill_between([-self.mu_total_syst*2.0,self.mu_total_syst*2.0], 1,
            #                 N-1, 
            #                 where=True, facecolor='yellow', interpolate=True)
            # ax.fill_between([-self.mu_total_syst,self.mu_total_syst], 1, N-1, 
            #                 where=True, facecolor='green', interpolate=True)
            # plt.grid()

            plt.fill_between([0, self.mu_total_syst], 
                             [N-2] ,[N-1],
                             color='black',
                             label='Total Uncertainty'
                            )
            colors = ['#1E91D6', '#8FC93A', '#00AA30', '#E4CC37', '#E18335', '#E22626', '#CC3BA0']
            colors.reverse()
            n = 0
            # for j, systs in enumerate(sorted_syst_uncert_keys):
            for j,systs in enumerate(["Lepton","Pile-up", "MET","Backgrounds","Flavour","Jet","Modelling"]):
                for i,syst_name in enumerate(sorted_syst_uncert_keys[systs]):
                    print "Plotting:",systs+': '+syst_name
                    if i == 0:
                        plt.fill_between([0,
                                      abs(self.systematic_uncertainties[systs+': '+syst_name])], 
                                     [n] ,[n+1],
                                     color=colors[j],
                                     label=systs
                                    )
                    else:
                        plt.fill_between([0,
                                      abs(self.systematic_uncertainties[systs+': '+syst_name])], 
                                     [n] ,[n+1],
                                     color=colors[j],
                                    )
    
                    n+=1

                                 # fmt='ko',capsize=20,elinewidth=5,capthick=3,markersize=12)
            leg = plt.legend(loc='center right', fontsize=45, frameon=False)
            # ax.yaxis.set_tick_params(horizontalalignment='left')
            ind = np.arange(N)
            plt.xlabel('Uncertainty on signal strength, $\Delta f_{DC}$', fontsize=45)
            plt.xticks(fontsize=45)
            plt.yticks(ind, y_tick_labels, fontsize=22)
            plt.ylim(0.0,N+1 )
            plt.xlim(xmin=0.0)
            plt.tick_params(direction='in', length=8, width=2,right=True,top=True)

            plt.draw()

            p = leg.get_window_extent()
            p= p.transformed(plt.gca().transAxes.inverted())
            draw_ATLAS_labels(plt.gca(), messages,align='left', y=p.ymax+0.038*(len(messages)+1),x=p.xmin+0.01)
            plt.yticks(va='top')

            canvas = ax
            all_ticks = [
            [canvas.xaxis, canvas.xaxis.get_major_ticks()],
            [canvas.xaxis, canvas.xaxis.get_minor_ticks()],
            [canvas.yaxis, canvas.yaxis.get_major_ticks()],
            [canvas.yaxis, canvas.yaxis.get_minor_ticks()]]
            for ticks in all_ticks:
                for tick_index in range(len(ticks[1])):
                    tick_loc = ticks[1][tick_index].get_loc()
                    if (tick_loc <= ticks[0].get_view_interval()[0] or
                            tick_loc >= ticks[0].get_view_interval()[1]):
                        ticks[1][tick_index].tick1On = False
                        ticks[1][tick_index].tick2On = False
            rhf.create_folder(output_folder)
            plt.savefig(output_folder+name)
            plt.show()


    def perform_fit(self, data=None):
        """
        Perform the fit over the given range with the statistical and systematic
         uncertainty evaluated
        """

        # First run a nice algorithm to examine the target space and get the 
        # central value 

        # Clear a holder for the probed parameters, this will be filled by the
        # optimizer 
        if data is None:
            data= self.data

        self.mus_evaluated = []
        args=(data, self.template_1, self.template_2)
        lik_model = minimize(self.likelihood, 
                    (0.5),
                    args=args,
                    method='L-BFGS-B',
                    bounds=[(0.,15.)],
                    callback=self.record_mu,
                    tol=1e-50
                    ) 

        # Store the value of mu for later and generate the values of the 
        # likelihood 
        self.mu = lik_model['x'][0]
        self.mus_evaluated += [ mu for mu in np.linspace(max(0,self.mu-0.2),
                                                     min(1.0,self.mu+0.2),1000)]
        self.Ls = [self.likelihood(
                        [mu], 
                        data,
                        self.template_1, 
                        self.template_2 
                    ) for mu in self.mus_evaluated ]

        # Now perform a fine grain scan around the found value of mu in order 
        # to evaluate the width of the likelihood, thereby finding the uncerta
        # -inty. 

        # First sort the likelihoods and corresponding mu together 
        mus,Ls = zip(*sorted(zip(self.mus_evaluated,self.Ls)))

        # The index of the mu distribution to scan over based on the loglikihood
        # start by trying to get the rane between LL + 2.5 
        l_low = int(max(self.arg_nearest( Ls,np.min(Ls)+2.5), 0))
        l_high = int(min(self.arg_nearest( Ls,np.min(Ls)+2.5, desc=True)+1, 
                        len(self.Ls)-1
                       ))

        # Then symmetrize this 
        mu_start,mu_end = self.mus_evaluated[l_low], self.mus_evaluated[l_high]
        mu_delta = max(np.abs(self.mu-mu_start), np.abs(self.mu- mu_end))
        mu_start, mu_end = self.mu-mu_delta, self.mu+mu_delta

        # And perform the scan 
        for mu in np.linspace(mu_start,mu_end,self.n_iterations):
            self.mus_evaluated.append(mu)
            self.Ls.append(self.likelihood(
                                [mu], 
                                data,
                                self.template_1, 
                                self.template_2 
                                ) 
                            )

        # Again evaluate the minimum loglikelihood and the point that is 0.5
        # above it - corresponding to the 68% CI

        # Start by sorting 
        self.mus_evaluated,self.Ls =  zip(*sorted(zip(self.mus_evaluated,
                                                      self.Ls)))
        mus, Ls = self.mus_evaluated, self.Ls

        # Getting the index of the mus in the inteveral
        l_min = np.argmin(Ls)
        l_low = max(self.arg_nearest( Ls[:l_min],np.min(Ls)+0.5),0)

        l_high = min(self.arg_nearest( Ls,np.min(Ls)+0.5, desc=True)+1, len(mus)-1)

        # Evaluate the 68% CI and the corresponding error
        self.mu, self.mu_high,self.mu_low = mus[l_min], mus[l_high], mus[l_low]
        self.mu_high_err = self.mu_high-self.mu 
        self.mu_low_err = self.mu-self.mu_low

        # print("...mu",self.mu)
        # print("...mu_low",self.mu_low)
        # print("...mu_high",self.mu_high)
        # print("...mu_low_err",self.mu_low_err)
        # print("...mu_high_err",self.mu_high_err)
        # print("mu = ", self.mu, "+",self.mu_high_err,"-",self.mu_low_err)
        return self.mu#,self.mu_high_err,self.mu_low_err

    def record_mu(self, mu):
        self.mus_evaluated.append(mu[0])

    def arg_nearest(self,A, n, desc=False,verbose=False):
        """
        Evaluate the inex of the elemnt in A that is closest
        to n

        Parmaters:
        A: List to examine
        n: number that we want to be clsoe too
        desc: Whether to start looking at A from the right hand
              side not the left. 
        verbose (optional): Whether to output the current iteration of the 
                            finding.
        """
        if desc:
            return len(A)-self.arg_nearest(np.flip(A,0),n,verbose=verbose)

        if len(A) == 0:
            # print("Arg nearest called for array of size zero..")
            return 0
        else:
            d = np.abs(A[0]-n)+1.
        I = 0
        for i, a in enumerate(A):           
            if a > n:
                I = i
            else: 
                return I
            
        return I
    
    def plot_uncertainty_distributions(self, output_folder, name, messages):
        """
        """
        if self.mu_stats is not None:
            self.plot_stat_distribution(output_folder, name,messages)
        else:
            self.plot_likelihood_profile(output_folder, name,messages)

        if self.combinations is not None:
            self.plot_systematics(output_folder,
                                 name.replace("_loglikelihood_profile","_systs"),
                                  messages
                                  )

    def plot_stat_distribution(self,output_folder, name, messages):

        plt.rc('text', usetex=True)
        plt.rc('font', family='serif',size=25)
        plt.figure(figsize=(20,20))
        plt.hist(self.mu_stats)
        plt.xlabel('Deadcone effect signal strength, $f_{DC}$')
        
        # Save this 
        draw_ATLAS_labels(plt.gca(), messages)
        rhf.create_folder(output_folder)
        plt.savefig(output_folder+name)
        plt.show()

    def plot_likelihood_profile(self, output_folder, name, messages):
        """
        Plot -log(L) as a function of mu with some lines showing the 68% CI 
        on this plot 

        @Parameters
        -----------
        output_folder: The folder that the plot will be saved in.
        name: The filename of the plot to be saved.
        """

        plt.rc('text', usetex=True)
        plt.rc('font', family='serif',size=25)
        plt.figure(figsize=(20,20))
        plt.plot(self.mus_evaluated, self.Ls)
        plt.plot([self.mus_evaluated[0], self.mus_evaluated[-1] ], 
                 [np.min(self.Ls)+0.5,np.min(self.Ls)+0.5 ], 
                 'k--')
        plt.ylim(np.min(self.Ls)-0.15,np.min(self.Ls)+2.5 )
        plt.xlim(-1,15)#self.mu-self.mu_high_err*2,self.mu+self.mu_high_err*2)
        plt.xlabel('Deadcone effect signal strength, $f_{DC}$')
        plt.ylabel('-lnL($f_{DC}$)')
        
        draw_ATLAS_labels(plt.gca(), messages)
        # Save this 
        rhf.create_folder(output_folder)
        plt.savefig(output_folder+name)
        plt.show()