import RootHelperFunctions.RootHelperFunctions as rhf
import RootHelperFunctions.StringHelperFunctions as shf
import RootHelperFunctions.DrawingHelperFunctions as dhf
import ATLASStyle.AtlasStyle as AS
import ROOT as r
from collections import OrderedDict
import sys
from copy import copy
import numpy as np
import array 
import config as c 

class Systematic():

    def __init__(self, 
                 information, 
                 input_mc_tuple, 
                 x_axis_binning, 
                 variable, 
                 signal_sample,
                 cuts=[],
                 save_output=True,
                 name="nameless", 
                 output_folder="", 
                 n_iterations=6, 
                 bootstrap = None, 
                 dropable=False,
                 luminosity=1.0,
                 truth_cut_weight="",
                 truth_variable="",
                 truth_tuple="",
                 truth_weight="",

                 ):
        '''
        A Class to handle a generic set of systematic variations and unfold them. 

        @Parameters
        -----------
        infomration: A tuple of dictionarys that contain s
            'weight': String for TTree:draw command weight
            'name': name of the systematic
            'type': up, down or asymm
            'tree': Tree name to draw from
            'sig_samples': signal sample - can be None in which case the default is used 
            'bkg_samples': background samples - can be none in which case the defaults are used (i.e drawing straight from signal sample)
            'combination': What combination this individual systematic is a part of 
        '''
        self.name = "Systematic"
        self.description = "Unfold a generic variation based in order to assess the size of a systematic. "
        self.save_output = save_output
        self.unique_name = name
        self.information = information
        self.cuts = []
        self.variable = variable
        self.detector_cut_weight = ""
        for cut in cuts:
            self.detector_cut_weight += "*(" + cut + ")"

        self.x_axis_binning = x_axis_binning
        self.input_mc_tuple = input_mc_tuple
        self.output_folder = output_folder + "/systematics/"
        self.n_iterations = n_iterations
        self.bootstrapped = bootstrap
        self.verbosity = 1
        self.loading = False 
        self.dropable = False# not "nominal" in weights[0][-2]
        self.unfolded_delta = OrderedDict()
        self.combination = self.information[0]['combination']
        if 'is_afii' in self.information[0]:
            self.is_afii = self.information[0]['is_afii']
        else:
            self.is_afii = False
        self.systematic_delta = {}

        self.vary_background = False
        if 'vary_background' in self.information[0]:
            self.vary_background = self.information[0]['vary_background']
            information = []
            for i in xrange(self.information[0]['n_background_variations']):
                information.append(copy(self.information[0]))
                information[-1]['index'] = i
                if "fake" in self.vary_background.lower():
                    information[-1]['name'] = information[-1]['name'] +' var '+str(i)

                else:
                    information[-1]['name'] = information[-1]['name'] +' #sigma var '+str(i)

            self.information = information 

        
        # For every systematic in the collectino we will 
        # Dictionarires to store the various properties 
        # of the variation
        self.systematic_size_hists = OrderedDict()
        self.systematic_delta_hists = OrderedDict()
        self.toy_distributions = OrderedDict()
        self.unfolded_variation_hists = OrderedDict()
        self.systmatic_breakdowns = OrderedDict()
        self.bs_heatmap = OrderedDict() 
        self.nominal_syst_varation = OrderedDict()
        self.variation_by_spreads = OrderedDict()
        self.unfolded_variation_spreads = OrderedDict()
        self.unfolded_variation_stat_sig = OrderedDict()
        self.unfolded_variation_stat_size = OrderedDict()
        self.signal_sample = signal_sample
        self.luminosity=luminosity
        self.truth_cut_weight=truth_cut_weight
        self.truth_variable=truth_variable
        self.truth_tuple=truth_tuple
        self.truth_weight=truth_weight

    def welcome_message(self):
        self.log(" RUNNING SYSTEMATIC:  ", self.unique_name)
        self.log(" Bootstrap:  ", self.bootstrapped)

    def log(self, *args):
        if self.verbosity == 0:
            return

        total_message = "[ TA-SYST ("+self.unique_name+") ] - "
        for msg in args:
            total_message += " " + str(msg)
        print(total_message)

 
    def draw_correlations(self, output_folder, x_axis_title):
        self.log("Creating the correlation_matrix...")
        cov_matrix = r.TH2D("correlation_matrix_"+self.unique_name, ";"+x_axis_title+";"+x_axis_title, 
                                 len(self.x_axis_binning)-1, array.array('d',self.x_axis_binning),
                                 len(self.x_axis_binning)-1, array.array('d',self.x_axis_binning)
                                 )
        for j in range(1,cov_matrix.GetYaxis().GetNbins()+1):
            for i in range(1,cov_matrix.GetXaxis().GetNbins()+1):
                cov_matrix.SetBinContent(i,j, self.total_cov[i-1,j-1])
        cov_matrix = rhf.normalize_correlation_matrix(cov_matrix)

        self.log("Drawing the correlation matrix ..:"  )
        canvas = r.TCanvas(self.unique_name + "correlation_matrix_canvas",
                           self.unique_name + "correlation_matrix_canvas",
                           800,
                           600)
        r.SetOwnership(canvas, 0)
        dhf.draw_correlation_matrix(cov_matrix, canvas)
        canvas.Print(output_folder+self.unique_name+"_correlation_matrix_test.eps")
        canvas.Print(output_folder+self.unique_name+"_correlation_matrix_test.png")
        self.log("Done."   )

    def draw_breakdown(self, x_axis_title):
        self.log("Drawing draw_breakdown for systematic " + self.unique_name)

        # apparently 600,600 is ATLAS recommednations but it looks like trash.
        AS.SetAtlasStyle()
        canvas = r.TCanvas(self.name + "systematic_breakdown",
                           self.name + "systematic_breakdown", 600, 600)
        r.SetOwnership(canvas, 0)

        dhf.plot_histogram(canvas,
                           self.systmatic_breakdowns,
                           x_axis_title=x_axis_title,
                           y_axis_title="Fractional Uncertainty [%]",
                           # messages =
                           )
        canvas.Print(self.output_folder + self.unique_name + "_breakdown.eps")
        canvas.Print(self.output_folder + self.unique_name + "_breakdown.png")
        self.log( " - Done.")

        if self.bootstrapped is not None:
            self.log( " - Creating BS plots.")
            for  info in self.information:
                name = info['name']
                self.draw_toy_distributions_plots(x_axis_title, self.output_folder + "toys/", name)
                self.draw_bs_importance_plots(x_axis_title, self.output_folder+"importances/", name)
                # for hist in self.toy_distributions[name]:
                #     hist.Write()

    def get_latex_string(self):
        """
        Returns rows in a table in a LaTeX format for each bin by bin systematic
        """
        tex_string = ""
        for info in self.information:
            name = info['name']
            variation_direction = info['direction']

            # Find the bloody thingy 
            variation_for_combination = self.systematic_size_hists[
                name].Clone()


            # Now go across the histogram bin by bin and combine in quadrature
            # as required
            tex_string += name  

            for i in xrange(1, variation_for_combination.GetSize()-1):
                new_uncert = variation_for_combination.GetBinContent(i)

                if variation_direction == "asymm":
                    tex_string += " & $\\pm %.3f $"%(new_uncert)
                else:
                    tex_string += " & $%.3f $"%(new_uncert)

            tex_string += "\\\\"
        return tex_string

    def draw_bs_importance_plots(self, x_axis_title, output_folder, name):
        """
        Draw a plot comparing the statistical unceratinty of an uncertainty
        with the size of the uncertainty itself 
        """

        rhf.create_folder(output_folder+shf.clean_string(name)+"_spread/")

        AS.SetAtlasStyle()
        canvas = r.TCanvas(self.name + "_pull_distribution" + self.unique_name,self.name + "_pull_distribution" + self.unique_name,800,600) 
        r.SetOwnership(canvas, 0)

        AS.SetAtlasStyle()
        down_hist = self. variation_stat_uncert[name].Clone()
        down_hist.Scale(-1) 
        down_nom_hist = self.nominal_detector_systematic_size[name].Clone()
        down_nom_hist.Scale(-1)

        plots = OrderedDict()
        plots["Bootstrap Stat. Uncert"] = [  self. variation_stat_uncert[name],
                                     dhf.get_stat_syst_stlye_opt() ] 

        plots["Bootstrap Stat. Down"] = [  down_hist,
                                     dhf.get_stat_syst_stlye_opt(show_legend=False) ]

        plots[name ] =  [  self.nominal_detector_systematic_size[name],
                           dhf.get_truth_large_legend_stlye_opt(0, False) ] 

        plots[name+"_down"] =  [  down_nom_hist,
                                  dhf.get_truth_large_legend_stlye_opt(0, False,False) ]

        dhf.plot_histogram(canvas,
                   plots, 
                   x_axis_title=x_axis_title,
                   y_axis_title="Fractional Uncertainty [%]",
                   do_atlas_labels = True,
                   labels = ["Detector Level"]
                   )

        canvas.Write()
        self.variation_stat_uncert[name].SetName(name+"_det_bs_stat_importances")
        self. variation_stat_uncert[name].Write()

        canvas.Print(output_folder+shf.clean_string(name)+"_bootstrap_importances.eps")
        canvas.Print(output_folder+shf.clean_string(name)+"_bootstrap_importances.png")
        self.draw_unfolded_bs_importance_plots(x_axis_title, output_folder, name)

    def draw_unfolded_bs_importance_plots(self, x_axis_title, output_foldaer, name):
        """
        Draw a plot comparing the statistical unceratinty of an uncertainty
        with the size of the uncertainty itself 
        """

        rhf.create_folder(output_folder+shf.clean_string(name)+"_spread/")
        AS.SetAtlasStyle()
        canvas = r.TCanvas(self.name + "_unfolded_bootstrap_importances" + self.unique_name,
                           self.name + "_unfolded_bootstrap_importances" + self.unique_name,
                           800,600) 
        r.SetOwnership(canvas, 0)
        AS.SetAtlasStyle()
        down_hist = self. variation_stat_uncert[name].Clone()
        down_hist.Scale(-1)

        self.unfolded_variation_stat_sig[name + "_down"] = self.unfolded_variation_stat_sig[name].Clone()
        self.unfolded_variation_stat_sig[name + "_down"].Scale(-1.0)

        # 
        self.systematic_size_hists[name+"_down"] =  self.systematic_size_hists[name].Clone()
        self.systematic_size_hists[name+"_down"].Scale(-1.0)

        plots = OrderedDict()

        plots["Bootstrap Stat. Uncert"] = [  self. unfolded_variation_stat_sig[name],
                                         dhf.get_stat_syst_stlye_opt() ]
        plots["Bootstrap Stat. Down"] = [  self. unfolded_variation_stat_sig[name + "_down"],
                                         dhf.get_stat_syst_stlye_opt(show_legend=False) ]

        plots[name] =  [ self.systematic_size_hists[name], 
                        dhf.get_truth_large_legend_stlye_opt(0,   False) ]
        plots[name+"_down"] =  [ self.systematic_size_hists[name+"_down"], 
                        dhf.get_truth_large_legend_stlye_opt(0,   False, False) ]


        dhf.plot_histogram(canvas,
                   plots, 
                   x_axis_title=x_axis_title,
                   y_axis_title="Fractional Uncertainty [%]",
                   do_atlas_labels = True,
                   labels = ["Fiducial Phase Space "]
                   )
        self.unfolded_variation_stat_sig[name].SetName(name+"_unfolded_bootstrap_importances")
        self.unfolded_variation_stat_sig[name].Write()

        canvas.Write()
        canvas.Print(output_folder+shf.clean_string(name)+"_unfolded_bootstrap_importances.eps")
        canvas.Print(output_folder+shf.clean_string(name)+"_unfolded_bootstrap_importances.png")


    def draw_toy_distributions_plots(self, x_axis_title, output_folder, name):
        """
        """
        rhf.create_folder(output_folder+shf.clean_string(name)+"_spread/")
        # Set up the sti
        AS.SetAtlasStyle()
        canvas = r.TCanvas(self.name + "_pull_distribution" + self.unique_name,self.name + "_pull_distribution" + self.unique_name,800,600) 
        r.SetOwnership(canvas, 0)
        canvas.SetTopMargin(    0.1)
        canvas.SetRightMargin(  0.15)
        canvas.SetBottomMargin( 0.15)
        canvas.SetLeftMargin(   0.15)
        
        # Set up the styling options for the heatmap
        heatmap =  dhf.set_style_options(self.bs_heatmap[name +"_unfolded_size"], dhf.get_line_options(r.kBlack, 1))
        heatmap.GetXaxis().SetTitle(x_axis_title)
        heatmap.GetYaxis().SetTitle("Unfolded Uncertainty [%]")
        r.gStyle.SetPalette(r.kBird)
        heatmap.Draw("COLZ")
        dhf.draw_atlas_details(["Test MC, Fiducial Phase Space"], x_pos = 0.18,y_pos = 0.85, dy = 0.032, text_size = 0.03)
        heatmap.Write()
        canvas.Write()
        canvas.Print(output_folder+shf.clean_string(name)+"_bootstrap_toy_distributions.eps")
        canvas.Print(output_folder+shf.clean_string(name)+"_bootstrap_toy_distributions.png")

        self.draw_systematic_statistical_signficance(output_folder, name, x_axis_title)
        self.draw_unfolded_systematic_statistical_signficance(output_folder, name, x_axis_title)

    def draw_systematic_statistical_signficance(self, output_folder, name, x_axis_title):
        """

        """
        rhf.create_folder(output_folder+shf.clean_string(name)+"_spread/")
        for info in self.information:
            display_name = weight['name']


            AS.SetAtlasStyle()
            canvas = r.TCanvas(display_name + "acceptance",display_name + "acceptance",600,600)
            r.SetOwnership(canvas, 0)
            canvas.Print(output_folder+shf.clean_string(name)+"_spread/"+shf.clean_string(name)+"_syst_spread.pdf[")
            
            for i, bin_hist in enumerate(self.variation_by_spreads[display_name]):
                unboostrapped_size = self.nominal_detector_systematic_size[display_name].GetBinContent(i+1)
                #
                style_opts = copy(dhf.data_style_options)
                style_opts.x_title_size = 0.035
                style_opts.x_title_offset = 1.15
                style_opts.x_label_size = None
                style_opts.x_axis_label_offset = None

                dhf.plot_histogram(canvas,
                            {
                                "Spread": [bin_hist, style_opts],
                            },
                            x_axis_title = "Detector Level Uncertainty Size [%]",
                            y_axis_title ="Number of toys",
                            do_atlas_labels = True,
                            do_legend = False,
                            labels=[
                                "Detector level",
                                "Bootstrapped uncert: %.3f #pm %.3f"%(
                                                bin_hist.GetMean(), 
                                                self.variation_stat_uncert[display_name].GetBinContent(i+1) 
                                                ),
                                "Nominal uncert: %.3f"%(
                                                unboostrapped_size, 
                                                ),
                                x_axis_title + " #in [%.3f,  %.3f)"%( self.x_axis_binning[i],self. x_axis_binning[i+1])

                            ]
                            )
                arrow = r.TArrow()
                arrow.SetLineWidth(3)
                arrow.SetLineColor(1)
                arrow.SetLineStyle(7)
                max_y = bin_hist.GetMaximum()*0.45
                arrow.DrawArrow(unboostrapped_size,0.0,unboostrapped_size,max_y,0.65,"same")
                bin_hist.SetName(display_name+"_det_uncert_spread") 
                bin_hist.Write()
                canvas.Print(output_folder+shf.clean_string(name)+"_spread/"+shf.clean_string(name)+"_bin_"+str(i)+"_syst_spread.eps")
                canvas.Print(output_folder+shf.clean_string(name)+"_spread/"+shf.clean_string(name)+"_bin_"+str(i)+"_syst_spread.png")
                canvas.Print(output_folder+shf.clean_string(name)+"_spread/"+shf.clean_string(name)+"_syst_spread.pdf")
            canvas.Print(output_folder+shf.clean_string(name)+"_spread/"+shf.clean_string(name)+"_syst_spread.pdf]")

    def load(self, output_folder): 
        """

        """
        self.total_cov = None
        self.systematic_size_hists = OrderedDict()
        self.sigma = OrderedDict()
        self.systmatic_breakdowns = OrderedDict()
        self.loading = True 
        self.unfolded_systematic_size = OrderedDict()

        self.total_up_variation = None
        self.total_down_variation = None

        self.total_up_delta = None
        self.total_down_delta  = None
        self.unfolded_variation_stat_sig = OrderedDict() 
        self.drop_syst = OrderedDict()
        self.systematic_delta = {}

        saved_syst_file = output_folder+self.unique_name+".root"
        outfile = rhf.open_file(saved_syst_file)
        for info in self.information:
            self.log('Loading systeamtic: ', self.unique_name)
            self.log('      Bootstrapped: ', 'True.' if self.bootstrapped else 'False.')
            # rename variables from the tuple so it's easier to read
            weight_string = info['weight']
            display_name = info['name']
            variation_direction = info['direction']
            tuple_name = info['tree']
            self.drop_syst[display_name] = False

            self.unfolded_systematic_size[display_name] = []
            self.systematic_size_hists[display_name] = outfile.Get("systematic_variation_" + display_name)
            
            if  not isinstance(self.systematic_size_hists[display_name],r.TH1D):
                print "Failed to find systmeatic file: "
                print " In file.  : ", saved_syst_file
                print " Hist.     : ", self.systematic_size_hists[display_name]
                print " syst.     : ", display_name
                continue 
            
            self.systematic_size_hists[display_name].SetDirectory(0)

            # if self.is_afii:
            #     # Stored the incorrect uncertainty breakdown, so instead of spending ages re running 
            #     # Correct this: 
            #     uncerts = []
            #     for j in xrange(1,self.systematic_size_hists[display_name].GetNbinsX()+1 ):
            #         uncerts.append(self.systematic_size_hists[display_name].GetBinContent(j)**2)

            #     # Get the difference hist 
            #     self.systematic_delta[display_name] = outfile.Get("systematic_variation_delta_" + display_name)

            #     # Scale the uncert down to the correct size (i.e decimals  not percentages )
            #     temp_Uncert = self.systematic_size_hists[display_name].Clone()
            #     temp_Uncert.Scale(1.0/100.0)

            #     temp_N =  self.systematic_delta[display_name].Clone()
            #     temp_N.Divide(temp_Uncert )
            #     temp_delta =self.systematic_delta[display_name].Clone()
            #     self.systematic_size_hists[display_name] = temp_delta
            #     self.systematic_size_hists[display_name].Divide(temp_N)
            #     self.systematic_size_hists[display_name].SetDirectory(0)

            #     uncerts = []
            #     for j in xrange(1,self.systematic_size_hists[display_name].GetNbinsX()+1 ):
            #         uncerts.append(self.systematic_size_hists[display_name].GetBinContent(j)**2)

            #     # VALIDATION: This method of correcting the AFII uncertainties 
            #     # had a closure test performed on it - with thise xact treatment we 
            #     # pass the closure test
                
            if self.bootstrapped:
                self.unfolded_variation_stat_sig[display_name] = outfile.Get(display_name+"_unfolded_bootstrap_importances")
                
                if  not isinstance(self.unfolded_variation_stat_sig[display_name],r.TH1D):
                    self.log("Failed to get bootstrap importance histogram for: ", display_name)
                else:
                    self.unfolded_variation_stat_sig[display_name].SetDirectory(0)
                    # self.unfolded_variation_stat_sig[display_name].Scale(1.0/100.0)
                
                    if self.dropable:
                        self.drop_syst[display_name] = True
                        for i in xrange(1,self.unfolded_variation_stat_sig[display_name].GetNbinsX()+1 ):
                            if self.unfolded_variation_stat_sig[display_name].GetBinContent(i) < self.systematic_size_hists[display_name].GetBinContent(i):
                                self.drop_syst[display_name] = False

                        if self.drop_syst[display_name]:
                            self.log('Dropping uncertainty ', display_name, ' not statistically signficant.')

                    # Create the covariance matrix for this systematic 
                    if not self.drop_syst[display_name]:
                        uncer_totals = []
                        self.systematic_delta[display_name] = outfile.Get("systematic_variation_delta_" + display_name)
                        for i in xrange(self.bootstrapped):
                            # self.unfolded_systematic_size[display_name].append(outfile.Get("Systematic_"+display_name+"/"+display_name+"_uncert_size_"+str(i)))
                            self.unfolded_systematic_size[display_name].append(outfile.Get(display_name+"_delta_"+str(i)))

                            if not isinstance(self.unfolded_systematic_size[display_name][-1], r.TH1D) and not isinstance(self.unfolded_systematic_size[display_name][-1], r.TH1F) :
                                print "Failed to read syst hist: Systematic_"+display_name+"/DetectorToyHists/"+display_name+"_uncert_size_"+str(i)
                                print self.unfolded_systematic_size[display_name][-1] 
                            uncerts = []
                            for j in xrange(1,self.unfolded_systematic_size[display_name][i].GetNbinsX()+1 ):
                                uncerts.append(self.unfolded_systematic_size[display_name][i].GetBinContent(j))

                            # 
                            uncerts = np.array(uncerts)
                            uncer_totals.append(uncerts)
                        self.sigma[display_name] = np.cov(np.stack(tuple(uncer_totals)).T)
                        # self.log("Cov matrix ", display_name, ': ')
                        # print self.sigma[display_name]
            else:
                # If we're not bootstrapped just set the covariance to be unity
                self.log('Loading systematic that has NOT been bootstrapped. Setting correlation to unity.')
                self.systematic_delta[display_name] = outfile.Get("systematic_variation_delta_" + display_name)

                # Bugged Frag. and Had. ucnertainties caused the delta to be multiplied by 100 mistakenly 
                # if self.is_afii:
                #     print "CORRECTING ", display_name, " UNCERT "
                #     self.systematic_delta[display_name].Scale(1.0/100.0)

                uncerts = []

                for j in xrange(1,self.systematic_delta[display_name].GetNbinsX()+1 ):
                    uncerts.append(self.systematic_delta[display_name].GetBinContent(j)**2)

                self.sigma[display_name] = np.diag(uncerts) #rhf.calculate_covariance_matrix(np.std(uncer_totals,axis=0))
                self.log('SIGMA: ', self.sigma[display_name])
            #   
            if self.total_up_variation is None and self.total_down_variation is None:
                self.total_up_variation = rhf.clear_histogram(
                    self.systematic_size_hists[display_name].Clone("systmeatic_total_up" + self.unique_name))
                self.total_down_variation = rhf.clear_histogram(
                    self.systematic_size_hists[display_name].Clone("systmeatic_total_udown" + self.unique_name))
                self.total_up_variation.SetDirectory(0)
                self.total_down_variation.SetDirectory(0)

            #   
            if self.total_up_variation is None and self.total_down_variation is None:
                self.total_up_variation = rhf.clear_histogram(
                    self.systematic_size_delta[display_name].Clone("systmeatic_total_up" + self.unique_name))
                self.total_down_variation = rhf.clear_histogram(
                    self.systematic_size_delta[display_name].Clone("systmeatic_total_udown" + self.unique_name))
                self.total_up_variation.SetDirectory(0)
                self.total_down_variation.SetDirectory(0)

        outfile.Close()

        self.retrieve_truth_level_hist(output_folder)
        if self.total_up_variation is not None:
            self.combine_variations()
        else:
            return False

        # 
        self.total_cov = None
        for name in self.sigma:
            self.log("Combining covariance of ", name)
            if self.total_cov is None:
                self.total_cov = self.sigma[name]
            else:
                self.total_cov += self.sigma[name]
        self.log('Total covariance: ', self.total_cov)
        return True

    def draw_unfolded_systematic_statistical_signficance(self, output_folder, name, x_axis_title):
        """

        """
        rhf.create_folder(output_folder+shf.clean_string(name)+"_spread/")

        for info in self.information:
            display_name = weight['name']


            AS.SetAtlasStyle()
            canvas = r.TCanvas(display_name + "acceptance",display_name + "acceptance",600,600)
            r.SetOwnership(canvas, 0)
            canvas.Print(output_folder+shf.clean_string(name)+"_spread/"+shf.clean_string(name)+"_unfolded_syst_spread.pdf[")
            
            for i, bin_hist in enumerate(self.unfolded_variation_spreads[display_name]):
                unboostrapped_size = self.systematic_size_hists[display_name].GetBinContent(i+1)
                
                #
                style_opts = copy(dhf.data_style_options)
                style_opts.x_title_size = 0.035
                style_opts.x_title_offset = 1.15
                style_opts.x_label_size = None
                style_opts.x_axis_label_offset = None

                dhf.plot_histogram(canvas,
                            {
                                "Spread": [bin_hist, style_opts],
                            },
                            x_axis_title = "Fiducial phase space uncertainty size [%]",
                            y_axis_title ="Number of toys",
                            do_atlas_labels = True,
                            do_legend = False,
                            labels=[
                                "Fiducial phase space",
                                "Bootstrapped uncert: %.3f #pm %.3f"%(
                                                bin_hist.GetMean(), 
                                                self.unfolded_variation_stat_sig[display_name].GetBinContent(i+1) 
                                                ),
                                "Nominal uncert: %.3f"%(
                                                unboostrapped_size, 
                                                ),
                                x_axis_title + " #in [%.3f,  %.3f)"%( self.x_axis_binning[i],self. x_axis_binning[i+1])

                            ]
                            )
                arrow = r.TArrow()
                arrow.SetLineWidth(3)
                arrow.SetLineColor(1)
                arrow.SetLineStyle(7)
                max_y = bin_hist.GetMaximum()*0.45
                arrow.DrawArrow(unboostrapped_size,0.0,unboostrapped_size,max_y,0.65,"same")
                bin_hist.SetName(display_name+"_unfolded_spread_"+str(i))
                bin_hist.Write()
                canvas.Print(output_folder+shf.clean_string(name)+"_spread/"+shf.clean_string(name)+"_bin_"+str(i)+"_unfolded_syst_spread.eps")
                canvas.Print(output_folder+shf.clean_string(name)+"_spread/"+shf.clean_string(name)+"_bin_"+str(i)+"_unfolded_syst_spread.png")
                canvas.Print(output_folder+shf.clean_string(name)+"_spread/"+shf.clean_string(name)+"_unfolded_syst_spread.pdf")
            canvas.Print(output_folder+shf.clean_string(name)+"_spread/"+shf.clean_string(name)+"_unfolded_syst_spread.pdf]")


    def combine_variations(self):
        """
        Adds up all the systematic variation managed by this Systematic object such that 
        we produce a TH1 with all positive varitions and and TH1 with all negative variations

        """
        self.log( "Evaluating total combined systematic uncertainty for: " + self.unique_name)

        self.total_down_delta = rhf.clear_histogram(
            self.total_up_variation.Clone("systematic_total_down_delta" + self.unique_name))
        self.total_up_delta = rhf.clear_histogram(
            self.total_up_variation.Clone("systematic_total_up_delta" + self.unique_name))

        self.total_up_variation = rhf.clear_histogram(
            self.total_up_variation.Clone("systmeatic_total_up" + self.unique_name))
        self.total_down_variation = rhf.clear_histogram(
            self.total_up_variation.Clone("systmeatic_total_down" + self.unique_name))


        for info in self.information:
            # rename variables from the tuple so it's easier to read
            weight_string = info['weight']
            display_name = info['name']
            variation_direction = info['direction']
            tuple_name = info['tree']

            if  not isinstance(self.systematic_size_hists[display_name],r.TH1D):
                print " GONNA SKIP : ", display_name
                continue
            if self.loading:
                if self.drop_syst[display_name]:
                    continue

            variation_for_combination = self.systematic_size_hists[
                display_name].Clone()

            # Scale down by 100.0, reversing the previous upscaling that was performed
            # for display purposes.
            variation_for_combination.Scale(1.0 / 100.0)

            # Now go across the histogram bin by bin and combine in quadrature
            # as required
            for i in xrange(1, variation_for_combination.GetSize()):
                up_uncert = self.total_up_variation.GetBinContent(i)
                down_uncert = self.total_down_variation.GetBinContent(i)
                new_uncert = variation_for_combination.GetBinContent(i)

                # 
                up_delta = self.total_up_delta .GetBinContent(i)
                down_delta = self.total_down_delta.GetBinContent(i) 
                new_delta = None
                if display_name in self.systematic_delta:
                    new_delta = self.systematic_delta[
                        display_name].GetBinContent(i) 

                if variation_direction == "asymm":
                    self.total_up_variation.SetBinContent(
                        i, ((up_uncert)**2 + (new_uncert)**2)**0.5)
                    self.total_down_variation.SetBinContent(
                        i, -((down_uncert)**2 + (new_uncert)**2)**0.5)

                    if new_delta is not None:
                        self.total_up_delta.SetBinContent(
                            i, ((up_delta)**2 + (new_delta)**2)**0.5)
                        self.total_down_delta.SetBinContent(
                            i, -((down_delta)**2 + (new_delta)**2)**0.5)

                else:
                    if new_uncert > 0:
                        self.total_up_variation.SetBinContent(
                            i, ((up_uncert)**2 + (new_uncert)**2)**0.5)

                        if new_delta is not None:
                            self.total_up_delta.SetBinContent(
                                i, ((up_delta)**2 + (new_delta)**2)**0.5)

                    else:
                        self.total_down_variation.SetBinContent(
                            i, -((down_uncert)**2 + (new_uncert)**2)**0.5)

                        if new_delta is not None:
                            self.total_down_delta.SetBinContent(
                                i, -((down_delta)**2 + (new_delta)**2)**0.5)

        self.total_up_variation.Scale(100.0)
        self.total_down_variation.Scale(100.0)
        self.systmatic_breakdowns["Total"] = [
            self.total_up_variation,
            dhf.get_stat_syst_stlye_opt(),
            dhf.get_stat_syst_stlye_opt(is_ratio=True),
        ]
        self.systmatic_breakdowns["Total Down"] = [
            self.total_down_variation,
            dhf.get_stat_syst_stlye_opt(show_legend=False),
            dhf.get_stat_syst_stlye_opt(is_ratio=True, show_legend=False),
        ]
        self.add_hist_for_drawing()

    def add_hist_for_drawing(self):
        syst_count = 0
        for info in self.information:
            # rename variables from the tuple so it's easier to read
            weight_string = info['weight']
            display_name = info['name']
            variation_direction = info['direction']
            tuple_name = info['tree']

            if self.bootstrapped is not None and not self.loading:
                self.avg_detector_systematic_size[display_name] = dhf.set_style_options( self.avg_detector_systematic_size[display_name],
                    dhf.get_truth_large_legend_stlye_opt(syst_count, False)
                    )

                self.avg_detector_systematic_size[display_name].SetLineStyle(9)
                self.avg_detector_systematic_size[display_name].SetLineWidth(6)

                self.nominal_detector_systematic_size[display_name] = dhf.set_style_options( self.nominal_detector_systematic_size[display_name],
                    dhf.get_truth_large_legend_stlye_opt(syst_count, False)
                    )

                self.nominal_detector_systematic_size[display_name].SetLineStyle(9)
                self.nominal_detector_systematic_size[display_name].SetLineWidth(6)

            # now store this uncertainty into a dictionary that's the correct
            # type for our plotting scripts
            if variation_direction == "asymm":
                self.systmatic_breakdowns[display_name] = [
                    self.systematic_size_hists[display_name],
                    dhf.get_truth_large_legend_stlye_opt(syst_count, False),
                    dhf.get_truth_large_legend_stlye_opt(syst_count, True),
                ]

                # Our procedure for one-driectional errors is to symmetrize
                # This will be handeled else where for the total ,(i.e in combine_variations)
                # For drwaing though, we need to duplice the variation and
                # reflect it about the x-axis
                downward_var = self.systematic_size_hists[display_name].Clone(
                    self.unique_name + "_downward_" + display_name)
                downward_var.SetDirectory(0)
                downward_var.Scale(-1.0)

                self.systmatic_breakdowns[display_name + "downward"] = [
                    downward_var,
                    dhf.get_truth_large_legend_stlye_opt(
                        syst_count, False, show_legend=False),
                    dhf.get_truth_large_legend_stlye_opt(
                        syst_count, True, show_legend=False),
                ]

            else:
                self.systmatic_breakdowns[display_name] = [
                    self.systematic_size_hists[display_name],
                    dhf.get_truth_large_legend_stlye_opt(syst_count, False),
                    dhf.get_truth_large_legend_stlye_opt(syst_count, True),
                ]
            syst_count += 1




    def run_bootstrap(self,
        response,
        nominal_unfolded, 
        nominal_detector, 
        efficiency, 
        acceptance, 
        method, 
        nominal_bs_hists=[],
        outfile = None
        ):
        """
        Evaluate the systematic variation for each detector level bin of the variable 

        The question we're trying to answer is "does the statical uncertainty of MC cover the
        variation in question" if it doesn't then we are not assessing the uncertainty, instead
        we are assessing. 
        """


        # Ensurethat we are writing to the correct ROOT object, not guaranteed
        # given other parts of this analysis base. 
        if outfile is not None:
            outfile.cd()
        # r.gDirectory.cd("Systematic_" + self.unique_name)

        self.log( "Evaluating systematic variation: " + 
                self.unique_name + " n_toys: " + str(len(nominal_bs_hists)) + 
                " >= " + str(self.bootstrapped))
            
        # Set up obejcts to hold the detector level distirubtions and the 
        # systematic  size  for each given individual systeamtic in an ordered
        # way.
        bootstrap_hists, self.systematic_size_hists, self.unfolded_systematic_size = OrderedDict(), OrderedDict(), OrderedDict()
        self.systematic_delta_hists = OrderedDict()
        self.detector_systematic_size = OrderedDict()
        self.nominal_detector_systematic_size = OrderedDict()
        self.avg_nominal_detector_systematic_size = OrderedDict()
        self.variation_stat_uncert = OrderedDict()
        self.avg_detector_systematic_size = OrderedDict()

        # Normalize the nominal histogram, which is used to find he relative 
        # size of the unceratinty
        # nominal_unfolded = rhf.normalize_histogram(
        #     nominal_unfolded).Clone("systmeatic_nom" + self.unique_name)
        nominal_unfolded.SetDirectory(0)

        # Create objects to hold the total histograms, not including those 
        # dropped by the bootstrapping procedure. 
        self.total_up_variation = rhf.clear_histogram(
            nominal_unfolded.Clone("systmeatic_total_up" + self.unique_name))
        self.total_down_variation = rhf.clear_histogram(
            nominal_unfolded.Clone("systmeatic_total_udown" + self.unique_name))
        self.total_up_variation.SetDirectory(0)
        self.total_down_variation.SetDirectory(0)

        # iterate over each variation in this set of systematics 
        for info in self.information:
            # rename variables from the tuple so it's easier to read
            weight_string = info['weight']
            display_name = info['name']
            variation_direct = info['direction']
            tuple_name = info['tree']
            
            if info['sig_samples'] is None:
                input_file = self.input_mc_tuple
            else:
                input_file = info['sig_samples'].input_file_names

            # The reco level detector tuple name is likely in the weight and variable name string
            # so replace that with the tuple we're trying to draw with.
            detector_cut_weight = self.detector_cut_weight.replace(
                "nominal.", tuple_name + ".")
            variable = self.variable.replace("nominal.", tuple_name + ".")
            self.log("\tRunning bootstrap on variation: " + display_name)

            # Get each toy distribution at detector level
            syst_detector_hist, bootstrap_hists[display_name] = rhf.get_bootstrapped_distribution(
                                                        input_file,
                                                        tuple_name,
                                                        variable,
                                                        self.x_axis_binning,
                                                        weight_string + detector_cut_weight,
                                                        bootstrap_weight="weight_poisson",
                                                        n_toys=self.bootstrapped
                                                    )

            # 
            self.systematic_size_hists[display_name] = []
            self.systematic_delta_hists[display_name] = []
            self.nominal_detector_systematic_size[display_name] = []
            self.detector_systematic_size[display_name] = []
            self.unfolded_systematic_size[display_name] = []
            self.avg_detector_systematic_size[display_name] = [] 
            self.unfolded_delta[display_name] = []
            if outfile is not None:
                outfile.cd()

            # r.gDirectory.cd("Systematic_" + self.unique_name)
            # r.gDirectory.mkdir("DetectorToyHists")
            # r.gDirectory.mkdir("UnfoldedHists")
            # r.gDirectory.cd("DetectorToyHists")

            # Evaluate the variation at detector level before unfolding 
            for i in xrange(self.bootstrapped):
                bootstrap_hists[display_name][i].SetName(
                    display_name + "_detector_hist_" + str(i))

                bootstrap_hists[display_name][i].Write()
                nominal_bs_hists[i].SetName("nominal_toy_" + str(i))
                nominal_bs_hists[i].Write() 

                # Create the variation histogram
                self.detector_systematic_size[display_name].append(bootstrap_hists[display_name][i].Clone(display_name+"_uncert_size_"+str(i) ) )
                self.detector_systematic_size[display_name][-1].Divide(nominal_bs_hists[i])

                self.detector_systematic_size[display_name][-1] = rhf.shift_hist_yaxis(
                    self.detector_systematic_size[display_name][-1], -1.0)
                self.detector_systematic_size[display_name][-1].Scale(100.0)
                self.detector_systematic_size[display_name][-1].Write()

                # Also perform an unfolding
                self.unfolded_systematic_size[display_name].append(bootstrap_hists[display_name][i].Clone(display_name+"_uncert_size_"+str(i) ) )
                self.unfolded_systematic_size[display_name][-1].Multiply(efficiency)
                unfolded_mc_toy = r.RooUnfoldBayes(response, self.unfolded_systematic_size[display_name][-1], self.n_iterations)
                self.unfolded_systematic_size[display_name][-1] = unfolded_mc_toy.Hreco()
                self.unfolded_systematic_size[display_name][-1].Divide(acceptance)

                # Now calculate the actuall size of the uncertainties 
                rhf.normalize_histogram(self.unfolded_systematic_size[display_name][-1])
                self.unfolded_delta[display_name].append(self.unfolded_systematic_size[display_name][-1].Clone(display_name+"_delta_"+str(i)))
                self.unfolded_delta[display_name][-1].Add(rhf.normalize_histogram(nominal_unfolded.Clone()), -1)

                # Relative uncertainty as well 
                self.unfolded_systematic_size[display_name][-1].Divide(rhf.normalize_histogram(nominal_unfolded.Clone()) )

                # Move this to zero and show as percentage 
                self.unfolded_systematic_size[display_name][-1] = rhf.shift_hist_yaxis(
                    self.unfolded_systematic_size[display_name][-1], -1.0)
                self.unfolded_systematic_size[display_name][-1].Scale(100.0)

            # 
            # r.gDirectory.cd("../")
            self.bs_heatmap[display_name] = rhf.convert_list_of_hists_to_heatmap( self.detector_systematic_size[display_name], self.x_axis_binning)
            self.bs_heatmap[display_name].Write()

            self.bs_heatmap[display_name+"_unfolded_size"] = rhf.convert_list_of_hists_to_heatmap(    self.unfolded_systematic_size[display_name], self.x_axis_binning)
            self.bs_heatmap[display_name+"_unfolded_size"].Write()

            self.bs_heatmap[display_name+"_unfolded_size"] = rhf.convert_list_of_hists_to_heatmap(    self.unfolded_systematic_size[display_name], self.x_axis_binning)

            # Evalaute each bins spread
            self.avg_detector_systematic_size[display_name], self.variation_stat_uncert[display_name], self.variation_by_spreads[display_name] = rhf.get_bin_by_bin_average(
                    self.detector_systematic_size[display_name], bin_hist_name="syst_var_"+display_name
                )

            # Evaliuate the size of the nominal variation 
            syst_detector_hist.SetName(display_name + "_detector_hist")
            syst_detector_hist.Write()
            nominal_detector.SetName("nominal_detector_hist")
            nominal_detector.Write()
            self.nominal_detector_systematic_size[display_name] = syst_detector_hist.Clone(display_name + "_nominal_uncet_det_size")
            self.nominal_detector_systematic_size[display_name].Divide(nominal_detector)
            self.nominal_detector_systematic_size[display_name] = rhf.shift_hist_yaxis(
                self.nominal_detector_systematic_size[display_name], -1.0)
            self.nominal_detector_systematic_size[display_name].Scale(100.0)
            self.nominal_detector_systematic_size[display_name].Write()


            # Now evaluate the unfolded systematic size
            self.unfolded_variation_stat_size[display_name], self.unfolded_variation_stat_sig[display_name], self.unfolded_variation_spreads[display_name] = rhf.get_bin_by_bin_average(
                    self.unfolded_systematic_size[display_name], bin_hist_name="unfolded_syst_var_"+display_name
                )
            
            # Now we need to save every single unfolded and detecto variation 
            for i in xrange(len(self.unfolded_systematic_size[display_name])):
                self.unfolded_systematic_size[display_name][i].SetName(display_name+"_unfolded_syt_size_"+str(i))
                self.unfolded_systematic_size[display_name][i].Write()
                self.unfolded_delta[display_name][i].Write()
 

    def retrieve_truth_level_hist(self, output_folder):
        for info in self.information:
            self.truth_histogram = None
            display_name = info['name']
            self.evaluate_truth = False 
            if info['sig_samples'] is None:
                input_file = self.input_mc_tuple
            else:
                input_file = info['sig_samples'].input_file_names
                signal_sample = info['sig_samples']
                self.evaluate_truth = True 
            
            if not self.evaluate_truth:
                return None

            saved_syst_file = output_folder+self.unique_name+".root"
            outfile = rhf.open_file(saved_syst_file)
            self.truth_histogram =  outfile.Get(display_name+ "_truth")
            self.truth_histogram.SetDirectory(0)
            outfile.Close()

            return self.truth_histogram

    def run(self, 
            response, 
            nominal_unfolded, 
            nominal_detector, 
            efficiency,
            acceptance, 
            method, 
            nominal_bs_hists=[], 
            outfile = None , 
            unfolded_x_axis_title = "X axis",
            migration_matrix=None,
            lumi_scaling=1.0,
            prompt_weight='1.0',
            nominal_truth=None,
            # migration_matrix=None,
            ):
        self.log("Running the systematic variation: ", self.unique_name)
   
        self.total_up_variation = rhf.clear_histogram(
            nominal_unfolded.Clone("systmeatic_total_up" + self.unique_name))
        self.total_down_variation = rhf.clear_histogram(
            nominal_unfolded.Clone("systmeatic_total_udown" + self.unique_name))
        self.total_up_variation.SetDirectory(0)
        self.total_down_variation.SetDirectory(0)

        if outfile is not None:
            if isinstance(outfile, str):
                self.log ("Creating root file.")
                outfile = rhf.open_file(outfile+self.unique_name+".root", "UPDATE")
            outfile.cd()

        # RooUnfold supports SVD,Iterative Bayesian and BinByBin methods
        response = r.RooUnfoldResponse( migration_matrix.ProjectionX(),migration_matrix.ProjectionY(), migration_matrix.Clone())
        response.UseOverflow(True)

        for info in self.information:
            display_name = info['name']
            weight_string = info['weight']
            display_name = info['name']
            variation_direct = info['direction']
            tuple_name = info['tree']
            signal_sample = self.signal_sample
            self.evaluate_truth = False 

            if info['sig_samples'] is None:
                input_file = self.input_mc_tuple
            else:
                input_file = info['sig_samples'].input_file_names
                signal_sample = info['sig_samples']
                self.truth_weight = info['truth_weight']
                self.evaluate_truth = True 
                self.log('Probing truth!! ')

            # ensure we have evaluated the luminosity
            try:
                self.log('Lumi: ', signal_sample.mc_lumi)
            except AttributeError:
                self.log('Signal sample effective luminosity not calculated.'
                         'Examining....' )
                signal_sample.evaluate_luminosity()
                self.log('Lumi: ', signal_sample.mc_lumi)


            # The reco level detector tuple name is likely in the weight and variable name string
            # so replace that with the tuple we're trying to draw with.
            detector_cut_weight = self.detector_cut_weight.replace(
                "nominal.", tuple_name + ".")
            variable = self.variable.replace("nominal.", tuple_name + ".")
            
            self.detector_hist = rhf.get_histogram_from_sample(
                            signal_sample,
                            tuple_name,
                            variable,
                            self.x_axis_binning,
                            weight_string + detector_cut_weight + "*("+prompt_weight+")",
                            scale_to_lumi=True,
                            lumi=self.luminosity
                            )
            


            # sum_weights_chain = r.TChain("sumWeights")
            # for f in input_file:
            #     for g in f:
            #         sum_weights_chain.Add(g)
            # nEvents= 0.0
            # for e in sum_weights_chain:
            #     nEvents += e.totalEventsWeighted_mc_generator_weights[6]*e.totalEventsWeighted_mc_generator_weights[194]
            # k_factors = 1.139
            # x_secs =  397.11000000000001
            # self.mc_luminosity_nom = nEvents/(x_secs*k_factors)
            # self.detector_hist.Scale(c.luminosity/self.mc_luminosity_nom )

            nominal_detector = nominal_detector.Clone("detectorHist" + self.unique_name)
            nominal_detector.SetDirectory(0)
            nominal_detector.Multiply(efficiency)

            self.detector_hist.Multiply(efficiency)
            self.detector_hist.SetDirectory(0)
            

            if method == "SVD":
                self.unfolded_mc   = r.RooUnfoldSvd(response, self.detector_hist)
                self.unfolded_nom   = r.RooUnfoldSvd(response, nominal_detector)

            elif method == "BinByBin":
                self.unfolded_mc   = r.RooTUnfold(response, self.detector_hist, 20 )
                self.unfolded_nom   = r.RooTUnfold(response, nominal_detector, 20 )
            else:
                #default is bayes
                self.unfolded_mc   = r.RooUnfoldBayes(response, self.detector_hist, self.n_iterations )
                self.unfolded_nom   = r.RooUnfoldBayes(response, nominal_detector, self.n_iterations )

            self.unfolded_mc_hist = self.unfolded_mc.Hreco()
            self.corr_unfolded_mc = self.unfolded_mc_hist.Clone()
            self.corr_unfolded_mc.Divide(acceptance)
            self.corr_unfolded_mc.Scale(1.0/self.corr_unfolded_mc.Integral())

            # Normalzie the 0unfolded nominal result
            nominal_unfolded = self.unfolded_nom.Hreco().Clone()
            nominal_unfolded.Divide(acceptance)
            nominal_unfolded.Scale(1.0/nominal_unfolded.Integral())

            # Retrieve the unfolded result 
            if self.evaluate_truth:
                # The reco level detector tuple name is likely in the weight and variable name string
                # so replace that with the tuple we're trying to draw with.
                detector_cut_weight = self.detector_cut_weight.replace(
                    "nominal.", tuple_name + ".")
                variable = self.variable.replace("nominal.", tuple_name + ".")

                self.truth_hist = rhf.get_histogram_from_sample(
                                signal_sample,
                                self.truth_tuple,
                                self.truth_variable,
                                self.x_axis_binning,
                                self.truth_weight + self.truth_cut_weight,
                                scale_to_lumi=True,
                                lumi=self.luminosity
                                )
                self.truth_hist.Scale(1.0/self.truth_hist.Integral())
                self.truth_hist.SetName(display_name+"_truth")
                self.truth_hist.Write()

                nominal_truth = rhf.normalize_histogram(nominal_truth)


                self.log('Evaluated truth distribtuion for this sys')                
                for i in range(1,self.truth_hist.GetXaxis().GetNbins()+1):
                    delta = self.truth_hist.GetBinContent(i) 
                    delta -= nominal_truth.GetBinContent(i)
                    self.log('Delta(trth, old truth) bin, ', i, ' = ', delta   )    

                for i in range(1,self.truth_hist.GetXaxis().GetNbins()+1):
                    delta = self.truth_hist.GetBinContent(i) 
                    delta -= nominal_unfolded.GetBinContent(i)
                    self.log('Delta(truth,unfolded det) bin, ', i, ' = ', delta )

            else: 
                self.truth_hist = nominal_unfolded.Clone(nominal_unfolded.GetName() +self.unique_name)

            # Here we perform a closure check to unsure that our unfolding is not biased 

            self.systematic_size_hists[display_name] = self.corr_unfolded_mc.Clone()
            self.systematic_size_hists[display_name].Divide(self.truth_hist)
            self.systematic_size_hists[display_name] = rhf.shift_hist_yaxis(
                    self.systematic_size_hists[display_name], -1.0)
            self.systematic_size_hists[display_name].Scale(100.0)

            outfile.cd()
            self.systematic_size_hists[display_name].SetName(
                "systematic_variation_" + display_name)
            self.systematic_size_hists[display_name].Write()
            self.systematic_size_hists[display_name].SetDirectory(0)

            self.systematic_delta_hists[display_name] =  self.corr_unfolded_mc.Clone()
            self.systematic_delta_hists[display_name].Add(nominal_unfolded,-1)
            self.systematic_delta_hists[display_name].SetName(
                "systematic_variation_delta_" + display_name)
            self.systematic_delta_hists[display_name].Write()

        self.combine_variations()
        self.draw_breakdown(unfolded_x_axis_title)


    def run_background_vars(self, 
            response, 
            nominal_unfolded, 
            nominal_detector, 
            efficiency,
            acceptance, 
            method, 
            nominal_bs_hists=[], 
            outfile = None , 
            unfolded_x_axis_title = "X axis",
            migration_matrix=None,
            lumi_scaling=1.0,
            background_vars=[]
            ):

        self.log("Running the systematic background variation.")
        self.total_up_variation = rhf.clear_histogram(
            nominal_unfolded.Clone("systmeatic_total_up" + self.unique_name))
        self.total_down_variation = rhf.clear_histogram(
            nominal_unfolded.Clone("systmeatic_total_udown" + self.unique_name))
        self.total_up_variation.SetDirectory(0)
        self.total_down_variation.SetDirectory(0)

        if outfile is not None:
            if isinstance(outfile, str):
                self.log ("Creating root file.")
                outfile = rhf.open_file(outfile+self.unique_name+".root", "UPDATE")
            outfile.cd()

        # RooUnfold supports SVD,Iterative Bayesian and BinByBin methods
        # response = r.RooUnfoldResponse( migration_matrix.ProjectionX(),migration_matrix.ProjectionY(), migration_matrix.Clone())
        # response.UseOverflow(True)
        nominal_unfolded.Scale(1.0/nominal_unfolded.Integral())

        # duplicate the information such that we have as many info slots as 
        # bckground variations 
        information = []

        for info in self.information:
            if info['sig_samples'] is None:
                input_file = self.input_mc_tuple
            else:
                input_file = info['sig_samples'].input_file_names
            display_name = info['name']

            self.log('Examining: ',display_name )   
            self.log('      N Varaitons: ', len(background_vars )   )
            self.log('      Expected   :  ', info['index'] )

            self.systematic_size_hists[display_name] = background_vars[info['index']].Clone()
            self.systematic_size_hists[display_name].Scale(1.0/self.systematic_size_hists[display_name].Integral())
            self.systematic_size_hists[display_name].Divide(nominal_unfolded)

            self.systematic_size_hists[display_name] = rhf.shift_hist_yaxis(
                    self.systematic_size_hists[display_name], -1.0)
            self.systematic_size_hists[display_name].Scale(100.0)

            outfile.cd()

            self.systematic_size_hists[display_name].SetName(
                "systematic_variation_" + display_name)
            self.systematic_size_hists[display_name].Write()
            self.systematic_size_hists[display_name].SetDirectory(0)


            self.systematic_delta_hists[display_name] =  background_vars[info['index']].Clone()
            self.systematic_delta_hists[display_name].Scale(1.0/self.systematic_size_hists[display_name].Integral())
            self.systematic_delta_hists[display_name].Add(nominal_unfolded,-1)
            self.systematic_delta_hists[display_name].SetName(
                "systematic_variation_delta_" + display_name)
            self.systematic_delta_hists[display_name].Write()


        self.combine_variations()
        self.draw_breakdown(unfolded_x_axis_title)

