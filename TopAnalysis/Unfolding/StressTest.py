import ROOT as r
import RootHelperFunctions.RootHelperFunctions as rhf 
import RootHelperFunctions.StringHelperFunctions as shf 
import RootHelperFunctions.DrawingHelperFunctions as dhf 
from RootHelperFunctions.ReweightingTool import ReweightingTool

import ATLASStyle.AtlasStyle as AS
from copy import deepcopy
from collections import OrderedDict
import array 
import glob

class StressTest():
    """
        Recalculates the truth and detector histograms after reweighting the variable by a slope given by the input stress. 
        Unfolds the reweighted detector histogram usnig the migration matrix provided by run(...), then plots the results
        nicely in draw(..) 

        TODO: Clean up and remove hard coded dependence on reweighted variable, that should be set in constructor 
             
    """
    def __init__(self,
                stresses = [0.2,-0.2],
                save_output = True, 
                name = None,
                input_mc_tuple = None,
                truth_tuple_name = None,
                truth_level_variable = None,
                detector_tuple_name = None,
                detector_level_variable = None,
                x_axis_binning = None,
                detector_cuts = [],
                truth_cuts = [],
                truth_weight   = "weight_mc",
                detector_weight= "weight_mc",
                reweighting_variable = "truth.MC_ttbar_afterFSR_pt/1e3",
                reweighting_var_binning = None,
                n_iterations = 4,
                draw_unstressed = True
                ):
        if name == None:
            name = "Stress Test"    
        self.name        = name#"Stress Test"
        self.description = "Convolutes the measured distribution by a factor 1+stress in first bin and 1-stress in last bin." + "\n\t Stresses chosen are: " + str(stresses) + ""
        self.stresses      = stresses
        self.save_output = save_output

        self.unfolded_mcs = {}
        self.unfolded_mc_hists = {} 
        self.draw_unstressed = draw_unstressed
        self.names       = {}
        self.style_options = {}
        self.unique_name = name
        self.stressed_reweighting_spectrum = {}
        self.input_mc_tuple = input_mc_tuple
        self.truth_tuple_name  = truth_tuple_name
        self.truth_level_variable  = truth_level_variable
        self.detector_tuple_name  = detector_tuple_name
        self.detector_level_variable  = detector_level_variable
        self.x_axis_binning  = x_axis_binning
        self.n_iterations    = n_iterations
        self.truth_weight    = truth_weight
        self.detector_weight = detector_weight
        self.detector_cut_weight = ""

        for cut in detector_cuts:
            self.detector_cut_weight += "*(" + cut + ")"
        #
        self.truth_cut_weight = ""
        for cut in truth_cuts:
            self.truth_cut_weight += "*(" + cut + ")"


        self.reweighting_variable = reweighting_variable
        if reweighting_var_binning == None:
            self.reweighting_var_binning = self.x_axis_binning

        self.create_stressed_distributions()
        self.create_style_options()

    def create_stressed_distributions(self):
        """
            Create the required set of stressed truth and detector level histograms 

            We avoid TTRee::Draw commands as an optimization to avoid lengthy Draw commands
            that occur with two large trees that are friends. 
        """
        print ("[ TA-UNFOLD ] - Creating stressed histograms.  ")

        #create the blank histograms to fill
        self.construct_stressed_histograms()

        #create the reweighting spectrum and string
        self.evaluate_reweighting_spectrum()

        for i, file_list in enumerate(self.input_mc_tuple):

            # if len(file_list) == 1 and file_list[-6:]=='*.root':
            print ('[ TA-UNFOLD ] -Creating the list of input files in folder: ',file_list[0][:-6])
            file_list = glob.glob(file_list[0])

            for j, file in enumerate(file_list):
                f = rhf.open_file(file)
                if not isinstance(f, r.TFile):
                    print("Error with file", file)
                    continue 
                    
                reco_tree     = f.Get(self.detector_tuple_name)
                particle_tree = f.Get(self.truth_tuple_name)

                if not isinstance(reco_tree, r.TTree) or \
                    not isinstance(particle_tree, r.TTree):
                    print("Error with file ", file, ' trees not found: ')
                    print(self.detector_tuple_name)
                    print(self.truth_tuple_name)
                    continue 
                # we are going to sync events by eventNumbers
                reco_tree.BuildIndex('eventNumber')
                particle_tree.BuildIndex('eventNumber') 
                # Set up a way to check that we both pass the cuts and can retrieve the unfolding variable 
                # as defined by a formulae.  NB StackExchange suggests claling GetNdata() but that messes 
                # things up! 
                truth_formula        = r.TTreeFormula("truth_variable_formula", self.truth_level_variable,     particle_tree)
                truth_cut_formula    = r.TTreeFormula("truth_cut_formula",      "1.0"+ self.truth_cut_weight,  particle_tree)
                truth_weight_formula = r.TTreeFormula("truth_weight_formula",    self.truth_weight,            particle_tree)

                reco_formula        = r.TTreeFormula("reco_variable_formula", self.detector_level_variable,    reco_tree)
                reco_cut_formula    = r.TTreeFormula("reco_cut_formula",      "1.0"+ self.detector_cut_weight, reco_tree)
                reco_weight_formula = r.TTreeFormula("reco_weight_formula",   self.detector_weight,            reco_tree)

                # Now iteratte over the reco tree
                for event in reco_tree:
                    # But also ensure that the particle level event passes
                    p_index = particle_tree.GetEntryNumberWithIndex(event.eventNumber)
                    particle_tree.GetEntry(p_index)
                    if p_index == -1:
                        continue

                    for x in xrange( truth_formula.GetNdata() ):
                        truth_variable  = truth_formula.EvalInstance()
                        truth_cut       = truth_cut_formula.EvalInstance() 
                        truth_weight    = truth_weight_formula.EvalInstance()

                    for x in xrange( reco_formula.GetNdata() ):
                        reco_variable   = reco_formula.EvalInstance()
                        reco_cut        = reco_cut_formula.EvalInstance() 
                        weight          = reco_weight_formula.EvalInstance()

                    # Cut the cuts and ensure we pass both levels            
                    if reco_cut != 1.0 or truth_cut != 1.0 and reco_variable != 0. and truth_variable != 0.:
                        continue

                    # Finally iterate over the tress 
                    self.unstressed_detector_hist.Fill( reco_variable, weight)
                    self.unstressed_truth_hist.Fill   ( truth_variable, weight)
                    
                    self.migration_matrix.Fill(reco_variable,truth_variable,weight)

                    for stress in self.stresses:
                        self.stressed_detector_hists[stress].Fill( reco_variable,  weight*self.reweighter[stress].weight(truth_variable))
                        self.stressed_truth_hists[stress].Fill   ( truth_variable, weight*self.reweighter[stress].weight(truth_variable))


        self.create_plotting_dictionary()


    def construct_stressed_histograms(self):
        """
            Build blank histograms to store the truth and detector level
            stressed distributions. 
        """

        # Dictionaries that have the stress as a index will be used
        self.stressed_detector_hists  = {}    
        self.stressed_truth_hists     = {}
        self.unstressed_detector_hist  = r.TH1F(self.unique_name + "_detector_unstressed","", len(self.x_axis_binning)-1, array.array('d', self.x_axis_binning)  )
        self.unstressed_truth_hist     = r.TH1F(self.unique_name + "_truth_unstressed","", len(self.x_axis_binning)-1, array.array('d', self.x_axis_binning)  )
        self.migration_matrix = r.TH2F("migration_matrix_" +self.unique_name,"",len(self.x_axis_binning)-1,array.array('d',self.x_axis_binning),
                                                                                            len(self.x_axis_binning)-1,array.array('d',self.x_axis_binning))

        self.unstressed_detector_hist.SetDirectory(0)
        self.unstressed_truth_hist   .SetDirectory(0)

        # 
        for stress in self.stresses:
            self.stressed_detector_hists[stress] = r.TH1F(self.unique_name + "_detector_"+str(int(100*stress)),"", len(self.x_axis_binning)-1, array.array('d', self.x_axis_binning)  )
            self.stressed_truth_hists[stress]     = r.TH1F(self.unique_name + "_truth_"+str(int(100*stress)),"", len(self.x_axis_binning)-1, array.array('d', self.x_axis_binning)  )

            self.stressed_detector_hists[stress].SetDirectory(0) 
            self.stressed_truth_hists[stress].SetDirectory(0) 

    def create_plotting_dictionary(self):
        """
            Put all the stressed and unstressed histograms into a dictionary 
            that can interface nicely with DrawingHelperFunctions
        """
        self.create_style_options()

        self.unfolded_stressed_plots = OrderedDict()
        self.truth_stressed_plots    = OrderedDict()
        self.detector_stressed_plots = OrderedDict()

        # self.unfolded_stressed_plots["Nominal"]  = [
        #         rhf.normalize_histogram(self.unstressed_truth_hist.Clone()),
        #         dhf.mc_style_options, 
        #         dhf.mc_ratio_style_options,
        #         "Nominal"
        #     ]

    def evaluate_reweighting_spectrum(self):
        """
            Creates a histogram of the variable that will be used as part of the stressing
            tool. 
        """
        print ("[ TA-UNFOLD ] - Constructing the spectrum for reweighting purposes.")
        self.reweighting_spectrum = rhf.get_histogram(self.input_mc_tuple,
                                              self.detector_tuple_name,
                                              self.reweighting_variable,
                                              self.reweighting_var_binning,
                                              self.truth_weight + self.truth_cut_weight,
                                              hist_name = "truth_stressVariable_" + shf.clean_string(self.reweighting_variable)+"_"+self.unique_name,
                                              friend_name = self.truth_tuple_name,
                                              index_variable = "eventNumber"
                                            )
        self.reweighting_spectrum.SetDirectory(0)

        # A reweighting tool (part of RootHelperFunctions) will calculate
        # the stress function and return a multiplictative weight for
        # events that have been stressed. 
        self.reweighter = {}
        for stress in self.stresses:
            self.reweighter[stress] = ReweightingTool(self.reweighting_spectrum,stress)


    def draw(self,output_folder,x_axis_title):
        """
            Draw the unfolded distributions before and after stress formatted such that 
            unfolded detector level is a point, and lines are truth level distributions. 

        """
        print ("[ TA-UNFOLD ] - Saving and drawing stressted unfolded distributions.")
        canvas = r.TCanvas(self.name + "_" + self.unique_name,self.name + "_" + self.unique_name,600,600) # apparently 600,600 is ATLAS recommednations but it looks like trash.
         
        #
        ratio_histograms = dhf.ratio_plot(   canvas,
                        self.unfolded_stressed_plots,
                        ratio_y_axis_title    = "#frac{Unfolded}{Truth}",
                        x_axis_title = x_axis_title,
                        messages = ["Points: Unfolded detector", "Lines: Particle level"],
                        leg_font_size = 0.045,
                        # ratio_max_y = 1.36,
                        # ratio_min_y = 0.69
                       )

        # Create the file
        self.out_file = rhf.open_file(output_folder+self.unique_name+".root", "RECREATE")


        if self.save_output:
            canvas.Write()
            for name in ratio_histograms:                
                ratio_histograms[name][0].SetName("ratio_" + name+"_"+self.unique_name)
                ratio_histograms[name][0].Write() 
            self.reweighting_spectrum.SetName("Rewighting_spectrum_"+ self.unique_name)
            self.reweighting_spectrum.Write()
        
        canvas.Print(output_folder+"stress_tests.eps")
        canvas.Print(output_folder+"stress_tests.png")

        self.draw_stressed_detector_distributonss(output_folder,x_axis_title)
        self.draw_stressed_truth_distributonss(output_folder,x_axis_title)
        self.draw_migration_matrix(output_folder, x_axis_title)

        # exit this working directory so future saved objects
        # are loose in the ROOT file 
        self.out_file.Close()
 
    def draw_migration_matrix(self,output_folder, x_axis_title):
        """ 
            Draw the migration matrix neatly, heavily based on the DrawingHelperFuntion method.
        """
        print ("[ TA-UNFOLD ] - Drawing the migration matrix .."   )

        canvas = r.TCanvas(self.name + "migration_matrix",self.name + "migration_matrix",800,600)
        
        self.migration_matrix.SetName("migration_matrix_"+self.unique_name)
        self.migration_matrix.Write()
        
        migration_matrix_drawing = rhf.normalize_migration_matrix(self.migration_matrix.Clone("migration_matrix_for_Drawing"))
        migration_matrix_drawing.GetXaxis().SetTitle("Measured " + x_axis_title)
        migration_matrix_drawing.GetXaxis().SetTitle("Particle Level " +x_axis_title)
        migration_matrix_drawing.Write()

        dhf.draw_migration_matrix(migration_matrix_drawing,canvas)
        canvas.Print(output_folder+"response_matrix_test.eps")
    #
    def draw_stressed_detector_distributonss(self,output_folder,x_axis_title):
        """ 
            Draw the distributions of the variable before the unfolding procedure.

            Parameters:
                output_folder: location of output eps files 
                x_axis_title:  self evident.
        """

        print ("[ TA-UNFOLD ] - Saving stressted detector distributions")
        canvas = r.TCanvas(self.name + "_stressed_detector_distributuions_" + self.unique_name,self.name + "_stressed_detector_distributuions_" + self.unique_name,600,600) # apparently 600,600 is ATLAS recommednations but it looks like trash.
        
        #show the detector level stressed plots 
        ratio_histograms = dhf.ratio_plot(   canvas,
                        self.detector_stressed_plots,
                        ratio_y_axis_title    = "#frac{Stressed}{Unstressed}",
                        x_axis_title = "Detector level " + x_axis_title,
                        messages = ["Solid: Downward variation", "Dashed: Upward variation"],
                        leg_font_size = 0.045,

                       )

        # Also save them to the root file currently open
        if self.save_output:
            canvas.Write()
            for name in  self.detector_stressed_plots:
                self.detector_stressed_plots[name][0].SetName("detector_stresesed_" + name+"_"+self.unique_name)
                self.detector_stressed_plots[name][0].Write()

        canvas.Print(output_folder+"detector_stressed_distributions.eps")
        canvas.Print(output_folder+"detector_stressed_distributions.png")

    def draw_stressed_truth_distributonss(self,output_folder,x_axis_title):
        """ 
            Draw the distributions of the variable before the unfolding procedure.

            Parameters:
                output_folder: location of output eps files 
                x_axis_title:  self evident.
        """
        print ("[ TA-UNFOLD ] - Saving stressted truth distributions")

        canvas = r.TCanvas(self.name + "_stressed_truth_distributuions_" + self.unique_name,self.name + "_stressed_truth_distributuions_" + self.unique_name,600,600) # apparently 600,600 is ATLAS recommednations but it looks like trash.
        
        #show the detector level stressed plots 
        ratio_histograms = dhf.ratio_plot(   canvas,
                        self.truth_stressed_plots,
                        ratio_y_axis_title    = "#frac{Stressed}{Unstressed}",
                        x_axis_title ="Particle level " +  x_axis_title,
                        messages = [ "Solid: Downward variation", "Dashed: Upward variation"],
                        leg_font_size = 0.045,
                        
                       )

        # Also save them to the root file currently open
        if False:
            canvas.Write()
            for hist_name in self.truth_stressed_plots:
                self.truth_stressed_plots[hist_name][0].SetName("truth_stressed_distribution_" + hist_name + "_" + self.unique_name)
                self.truth_stressed_plots[hist_name][0].Write()

        canvas.Print(output_folder+"truth_stressed_distributions.eps")
        canvas.Print(output_folder+"truth_stressed_distributions.png")

    def unfold_unstressed(self,response,method, efficiency,  acceptance):
        """
            Unfold the nominal unstressed samples.
        """
        # RooUnfold supports SVD,Iterative Bayesian and BinByBin methods
        # self.unstressed_detector_hist.Multiply(  efficiency )

        if method == "SVD":
            self.unfolded_mcs["Unstressed"]   = r.RooUnfoldSvd(response, self.unstressed_detector_hist)
        elif method == "BinByBin":
            self.unfolded_mcs["Unstressed"]   = r.RooTUnfold(response, self.unstressed_detector_hist, 20 )
        else:
            #default is bayes
            self.unfolded_mcs["Unstressed"]   = r.RooUnfoldBayes(response, self.unstressed_detector_hist, self.n_iterations )

        # save the unfolded MC distribution and correct 
        # for fidicuial acceptance. 
        self.unfolded_mcs["Unstressed"].SetVerbose(0)
        unfolded_mc_hist = self.unfolded_mcs["Unstressed"].Hreco()
        self.unfolded_mc_hists["Unstressed"] = unfolded_mc_hist.Clone()
        self.unfolded_mc_hists["Unstressed"].SetDirectory(0)
        # self.unfolded_mc_hists["Unstressed"].Divide(acceptance)

        # Shove it to the plotting dictionary if that's what we'd like to see 
        if self.draw_unstressed:
            self.store_histograms_for_plotting(0.0,
                                    self.unfolded_mc_hists["Unstressed"], 
                                    self.unstressed_truth_hist, 
                                    self.unstressed_detector_hist,
                                    0)
    def run(self, response, truth, detector_hist, efficiency, acceptance,method,migration_matrix):
        """
            Perform the stress tests and evaluate how well the stressed detector level plots unfold
            back to the stressed truth level plots

            Parameters:
                response: 
                truth
                detector_hist
                efficiency
                acceptance,method
        """
        print ("[ TA-UNFOLD ] - Performing the Stress tests")
        
        # We want to plot the originals as well as the stressed so store
        # copy
        self.orig_detector_hist = detector_hist.Clone("")
        self.orig_detector_hist.SetDirectory(0)
        
        # We want to unfold the reweightied histograms with potentially different MC stats
        # so we instead reconstruct the migration matrix with the correct statistcs 
        # If we are 100% certain that we've got the same stats then migration_matri and self.migration_matrix are degenerate
        self.response = r.RooUnfoldResponse(self.migration_matrix.ProjectionX(),self.migration_matrix.ProjectionY(), self.migration_matrix)
        self.response.UseOverflow(True)

        # Unfold the unstressed
        self.unfold_unstressed(self.response,method, efficiency,  acceptance)

        count = 1 
        for stress in self.stresses:
            print ("[ TA-UNFOLD ] -      Using a stress of " + str(stress))

            #reconstruct the response
            self.stressed_detector_hists[stress].SetName("stressed_unfolded_detector_hist_"+str(int(100*stress))+self.unique_name)
            self.stressed_truth_hists[stress].SetName("stressed_unfolded_detector_hist_"+str(int(100*stress))+self.unique_name)
            self.stressed_detector_hists[stress].SetDirectory(0)
            self.stressed_truth_hists[stress].SetDirectory(0)

            # Perform detector acceptance correction 
            # self.stressed_detector_hists[stress].Multiply(  efficiency )

            # RooUnfold supports SVD,Iterative Bayesian and BinByBin methods
            if method == "SVD":
                self.unfolded_mcs[stress]   = r.RooUnfoldSvd(self.response, self.stressed_detector_hists[stress])
            elif method == "BinByBin":
                self.unfolded_mcs[stress]   = r.RooTUnfold(self.response, self.stressed_detector_hists[stress], 20 )
            else:
                #default is bayes
                self.unfolded_mcs[stress]   = r.RooUnfoldBayes(self.response, self.stressed_detector_hists[stress], self.n_iterations )


            # save the unfolded MC distribution and correct 
            # for fidicuial acceptance. 
            self.unfolded_mcs[stress] .SetVerbose(0)
            unfolded_mc_hist = self.unfolded_mcs[stress].Hreco()
            self.unfolded_mc_hists[stress] = unfolded_mc_hist.Clone()
            self.unfolded_mc_hists[stress].SetDirectory(0)

            # Perform fiducial ps efficiency correction
            # self.unfolded_mc_hists[stress].Divide(acceptance)

            #######################################################
            # Append these histograms to dictionarys that will be #
            # drawn later.                                        #
            #######################################################
            self.store_histograms_for_plotting(stress,
                                        self.unfolded_mc_hists[stress], 
                                        self.stressed_truth_hists[stress], 
                                        self.stressed_detector_hists[stress], 
                                        count)
            count += 1 


    def store_histograms_for_plotting(self, stress, unfolded_stressed_mc,  stressted_truth_histogram, stressed_detector_hist, count):
        """
            Add the unfolded histogrma of the variable of interstet (with all correction fators applied) to the list of plots
            to be drawn. Also add the stressed truth level plot and stressed detector level plot .

            Stress is the level of stress as used as key for various dictionaries
            Histograms to passed to this function:
                unfoldeds_stressed_mc       
                stressted_truth_histogram
                stressed_detector_hist

            count: an integer that decides upon the style options to be used. 
        """
        if stress != 0.0:    
            self.names[stress] = "%.1f"%(100.0*stress) + " %"
        else:
            self.names[stress] = "Unstressed"

        if stress != 0.0:
            self.unfolded_stressed_plots[self.names[stress] + " Truth"]  = [
                    rhf.normalize_histogram(stressted_truth_histogram.Clone()), 
                    self.truth_style_options[count], 
                    self.truth_ratio_style_options[count],
                    None
                ]

        self.unfolded_stressed_plots[self.names[stress]]  = [
                rhf.normalize_histogram(unfolded_stressed_mc.Clone()), 
                self.unfolded_mc_style_otions[count], 
                self.unfolded_mc_ratio_style_otions[count],
                self.names[stress] + " Truth" if stress != 0.0 else "Unstressed"
            ]

        self.truth_stressed_plots[self.names[stress]] = [
                rhf.normalize_histogram(stressted_truth_histogram.Clone()),
                self.truth_leg_style_options[count], 
                self.truth_leg_ratio_style_options[count],
                "Unstressed"
        ]

        self.detector_stressed_plots[self.names[stress]] = [
                rhf.normalize_histogram(stressed_detector_hist.Clone()),
                self.truth_leg_style_options[count], 
                self.truth_leg_ratio_style_options[count],
                "Unstressed"
            ]

    def create_style_options(self):
        colors = [r.kBlack,r.kRed-4, r.kRed, r.kGreen + 3, r.kGreen - 3, r.kAzure -2, r.kAzure +2, r.kMagenta -2, r.kMagenta+2, r.kGray, r.kGray+2]
        markers = [34,20,24,21,25,22,26,23,32, 24,20]

        self.unfolded_mc_style_otions = OrderedDict()
        self.unfolded_mc_ratio_style_otions = OrderedDict()
        self.truth_style_options = OrderedDict()
        self.truth_ratio_style_options = OrderedDict()

        self.truth_leg_style_options = OrderedDict()
        self.truth_leg_ratio_style_options = OrderedDict()

        for count in range(0,len(colors)):  
            line_style = 1 if count%2 == 0 else 9
            self.unfolded_mc_style_otions[count] = dhf.StyleOptions(
                                                        draw_options = "HIST P",
                                                        x_label_size = 0.0,
                                                        legend_options = "lp")
            self.unfolded_mc_style_otions[count].marker_style = markers[count]
            self.unfolded_mc_style_otions[count].marker_color = colors[count]
            self.unfolded_mc_style_otions[count].line_color   = colors[count]

            #ratio is basically the same except the label sizes and things are changed a bit 
            self.unfolded_mc_ratio_style_otions[count] = dhf.StyleOptions(x_label_size = 0.0,
                                                        line_style = line_style,
                                                        legend_options = "lp",
                                                        marker_style = markers[count],
                                                        marker_color = colors[count],
                                                        line_color = colors[count],
                                                        line_width   = 4,)
            self.unfolded_mc_ratio_style_otions[count].set_default_ratio_options()
            self.unfolded_mc_ratio_style_otions[count].x_axis_label_offset = None

            #set the drawing style options options to be nice 
            self.truth_style_options[count] = dhf.StyleOptions(
                                 draw_options = "HIST",
                                 line_style   = line_style,
                                 line_width   = 4,
                                 fill_color   = r.kWhite,
                                 fill_style   = 0,
                                 marker_color = r.kBlack,
                                 marker_style = 0,
                                 marker_size  = 0,
                                 x_label_size = 0.0,
                                 legend_options = None,
                                 line_color = colors[count] 
                                 )
            #ratio is basically the same except the label sizes and things are changed a bit 
            self.truth_ratio_style_options[count] =deepcopy(self.truth_style_options[count])
            self.truth_ratio_style_options[count].set_default_ratio_options()
            self.truth_ratio_style_options[count].x_axis_label_offset = None

            #set the drawing style options options to be nice 
            self.truth_leg_style_options[count] = dhf.StyleOptions(
                                 draw_options = "HIST",
                                 line_style   = line_style,
                                 line_width   = 4,
                                 fill_color   = r.kWhite,
                                 fill_style   = 0,
                                 marker_color = r.kBlack,
                                 marker_style = 0,
                                 marker_size  = 0,
                                 x_label_size = 0.0,
                                 line_color = colors[count] 
                                 )
            #ratio is basically the same except the label sizes and things are changed a bit 
            self.truth_leg_ratio_style_options[count] =deepcopy(self.truth_leg_style_options[count])
            self.truth_leg_ratio_style_options[count].set_default_ratio_options()
            self.truth_leg_ratio_style_options[count].x_axis_label_offset = None


