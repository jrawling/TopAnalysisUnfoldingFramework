"""
    Perform's a closure test on the unfolding. This invovles:   
        - Split MC in half, testing and training multiple times, in many difference ways
        - Unfold the test sample with response matrices from training   
        - Then plot pulls = (unfolded - truth)/(error(unfolded))    
        - Show the variation in folded results with the  MC stat uncertainty visible 
    We consider the test to be pased if the variation is within stat uncertainty. 

    First we're going turn off the efficiency and acceptance corrections, such that
    we are only assessing the matrix inversion and the errors are sensible 

    TODO: 
        Split the sample into expect number of data vs Rest of the MC sample instead of 50/50
        Correct current way of doing this it's not working 


"""
from __future__ import print_function 

import RootHelperFunctions.RootHelperFunctions as rhf 
import RootHelperFunctions.StringHelperFunctions as shf 
import RootHelperFunctions.DrawingHelperFunctions as dhf 
import ATLASStyle.AtlasStyle as AS
import ROOT as r
from collections import OrderedDict
import numpy as np 
import time 
import array
import glob

class ClosureTest():
    """ 

    """ 
    def __init__(self,
            input_mc_tuple,          #MC that we base the unfolding on
            truth_level_variable,    #Truth level variable name that will be unfolded to
            detector_level_variable, #Detector level variable name that will be unfolded from
            x_axis_binning,
            x_axis_title = "",
            y_axis_title = "",
            method = "bayes",        # we're going to use iterative bayesian 
            truth_tuple_name = "particleLevel",
            detector_tuple_name = "nominal",
            save_output = True, 
            truth_cuts      = [""],
            detector_cuts   = [""],
            detector_weight = "",
            truth_weight    = "",
            name = "closure",
            draw_pulls=False,
            draw_test_detector_spread=True,
            n_toys = 300,
            luminosity    = 80000,
            mc_luminosity = None,
            n_iterations  = 4,
        ):

        self.name        = "Closure Test"
        self.description = "Split the MC into a training and test sample. Unfold the test with training and examine the variance compared to the nominal result." 
        self.save_output = save_output
        self.unique_name = name
        self.n_iterations = n_iterations
        self.method       = "Bayes"
        self.n_toys       = n_toys

        #options for drawing the pulls and the test detector spread
        self.draw_pulls                = draw_pulls 
        self.draw_test_detector_spread = draw_test_detector_spread

        # variables tha tstore information for each toy.
        self.train_migration_matrix  = []
        self.train_truth_hists       = []
        self.train_detector_hists    = []
        self.test_truth_hists        = []
        self.train_truth_total_hists = []
        self.efficiency_plot         = []
        self.acceptance_plot         = []
        self.test_detector_hists     = []
        self.unfolded_detector_hist  = []
        self.delta_unfolding_truth   = []
        self.toy_pull_hist           = []
        self.train_responses         = []

        # basic plotting variables
        self.signal_sample           = input_mc_tuple 
        self.input_mc_tuple           = input_mc_tuple.input_file_names
        self.truth_level_variable     = truth_level_variable
        self.detector_level_variable  = detector_level_variable
        self.truth_tuple_name         = truth_tuple_name 
        self.detector_tuple_name      = detector_tuple_name 
        self.x_axis_binning           = x_axis_binning
        self.x_axis_title             = x_axis_title
        self.detector_weight = detector_weight
        self.truth_weight    = truth_weight   
        self.y_axis_title = y_axis_title
        self.luminosity    = luminosity   
        if mc_luminosity is None:
            self.evaluate_nom_mc_luminiosity()
            print("[ TA-UNFOLD - CLOSURE ] - Calculated MC luminosity: ", self.mc_luminosity) 

        else:
            self.mc_luminosity = mc_luminosity

        # cuts can are handed as an array and processed into
        # a string for a TTree::Draw command
        self.detector_cut_weight = ""
        for cut in detector_cuts:
            self.detector_cut_weight += "*(" + cut + ")"
        # different cuts at truth and detector leve
        self.truth_cut_weight = ""
        for cut in truth_cuts:
            self.truth_cut_weight += "*(" + cut + ")"
        
        self.output_folder = None

        # We cereate the toy distributions here to avoid
        # ROOT funkyness that occurs when opening it later
        self.create_test_train_histograms()

    def setup_migration_matrix_saving(self,output_folder):
        """
            creates a pdf that migration matrices will be collated in. Also creates the folder to store this pdf in.
        """
        rhf.create_folder(output_folder+"train_migration_matrices/")        
        self.migration_matrix_canvas.Print(output_folder+"train_migration_matrices/migration_matrices.pdf[")

    def unfold_toy(self, toy_response, toy_detector_hist):
        """
            Performs an unfolding based on the method and parameters given to this class at 
            initialization for an arbitrary response and detector histograms 

            Parameters:
                toy_respose: a RooUnfold object that that has been constructed with the migration
                             matrix, truth and detector-level distributions
                toy_detector_hist: the histogram to be unfolded

            Returns: 
                The unfolded distribution as found by the method set in the constructor
                of this class
                The errors on the bins as defined by option too of TRooUnfold::ErecoV 
                option (2)
                    "Errors from the covariance matrix given by the unfolding"
        """

        # We support three methods
        if self.method == "SVD":
            unfolded_mc   = r.RooUnfoldSvd(toy_response, toy_detector_hist)
        elif self.method == "BinByBin":
            unfolded_mc   = r.RooTUnfold(toy_response, toy_detector_hist, 20 )
        else:
            r.RooUnfold().SetVerbose(0)
            unfolded_mc   = r.RooUnfoldBayes(toy_response, toy_detector_hist, self.n_iterations )

        # return both the unfolded distribution and the errors on the bins
        return unfolded_mc.Hreco(1), unfolded_mc.ErecoV(1)

    def draw_migration_matrix(self,toy_num,output_folder):
        """ 
            Draw the migration matrix neatly, heavily based on the DrawingHelperFuntion method.
        """
        
        migration_matrix_drawing = rhf.normalize_migration_matrix(self.train_migration_matrix[toy_num].Clone())
        migration_matrix_drawing.GetXaxis().SetTitle(self.x_axis_title)
        migration_matrix_drawing.GetYaxis().SetTitle(self.y_axis_title)

        r.gDirectory.cd("toy_"+str(toy_num))
        migration_matrix_drawing.SetName("response_matrix_"+str(toy_num)) 
        migration_matrix_drawing.Write()
        r.gDirectory.cd("../")

        dhf.draw_migration_matrix(migration_matrix_drawing,self.migration_matrix_canvas )
        self.migration_matrix_canvas.Print(output_folder+"train_migration_matrices/migration_matrices.pdf")

    def draw_total_migration_matrix(self,output_folder):
        """ 
            Draw the migration matrix neatly, heavily based on the DrawingHelperFuntion method.
        """
        
        migration_matrix_drawing = rhf.normalize_migration_matrix(self.total_migration_matrix.Clone())
        migration_matrix_drawing.GetXaxis().SetTitle(self.x_axis_title)
        migration_matrix_drawing.GetYaxis().SetTitle(self.y_axis_title)

        migration_matrix_drawing.SetName("total_response_matrix")
        migration_matrix_drawing.Write()

        dhf.draw_migration_matrix(migration_matrix_drawing, self.migration_matrix_canvas )
        self.migration_matrix_canvas.Print(output_folder+"total_migration_matrix.png")

    #
    def draw(self,output_folder,x_axis_title):
        """
            The main draw command for this tool. Creates the pull width and pull mean plot 
            which is the plot of interest for this tool, as well as calling the funttions
            to draw bin by bin pull and test detector bin content spreads. 

            output_folder: where the output will be stored, sub folders will becreated 
                           in this location. 
            x_axis_title: the truth level x_axis_title thatwill be used for all plots
        """

        # For this batch of histograms we need to ensure that we are displaying the 
        # correct luminosity in the ATLAS label.
        old_lumi_string = AS.lumi_string 
        AS.lumi_string  = "#sqrt{s} = 13 TeV, %.1f fb^{-1}"%(self.luminosity/1000.0)
 
        # Set up an ATLAS style canvas
        canvas = r.TCanvas(self.name + "_stressed_truth_distributuions_" + self.unique_name,self.name + "_stressed_truth_distributuions_" + self.unique_name,600,600) 
        dhf.plot_histogram(canvas,
                    self.pull_figures_of_merit,
                    x_axis_title = self.x_axis_title, 
                    y_axis_title ="Measure of Pull", 
                    do_atlas_labels = True,
                    )

        # Save the canvas to the 
        r.gDirectory.cd(self.unique_name)
        canvas.Write()
        # canvas.Print(output_folder+"pull_foms.eps")
        canvas.Print(output_folder+"pull_foms.png")

        if self.draw_pulls:
            self.draw_pull_distributions(output_folder)
            self.draw_pull_components(output_folder)
        self.draw_pull_heatmap(output_folder)

        if self.draw_test_detector_spread:
            self.draw_test_detector_heatmap(output_folder)
            self.create_test_detector_spread()
            self.draw_test_detector_distributions(output_folder)

        # save all the training migraiton matrices to a pdf 
        self.migration_matrix_canvas = r.TCanvas(self.name + "migration_matrix",self.name + "migration_matrix",800,600)
        self.draw_total_migration_matrix(output_folder)
        
        # self.draw_training_migration_matrices(output_folder)

        #we've fininshed with the test writing now so return to the folder 
        r.gDirectory.cd("../")


        # undo the overwriting of the lumistrong to whatever itwwas before
        AS.lumi_string  = old_lumi_string 

    def draw_training_migration_matrices(self,output_folder):
        """ 
            Draw all of the migration matrices for every single toy! 
        """

        self.setup_migration_matrix_saving(output_folder)
        for toy_num in xrange(self.n_toys):
            self.draw_migration_matrix(toy_num,output_folder)

        self.migration_matrix_canvas.Print(output_folder+"train_migration_matrices/migration_matrices.pdf]")

    def construct_test_train_histograms(self):
        """
            Creates blank histograms for the test detector and truth distributions as well as 
            the train migration matrix.
        """
        self.total_migration_matrix = r.TH2F("migration_matrix_total"+self.unique_name,
                                             "",
                                             len(self.x_axis_binning)-1,array.array('d',self.x_axis_binning),
                                             len(self.x_axis_binning)-1,array.array('d',self.x_axis_binning))
        self.total_migration_matrix.SetDirectory(0) 


        self.n_train, self.n_test= [],[]
        for toy_num in xrange(self.n_toys):
            self.n_train.append(0)
            self.n_test.append(0)
            self.train_migration_matrix.append(r.TH2F("migration_matrix_" + str(toy_num)+self.unique_name,"",len(self.x_axis_binning)-1,array.array('d',self.x_axis_binning),
                                                                                            len(self.x_axis_binning)-1,array.array('d',self.x_axis_binning)))
            self.test_detector_hists   .append(r.TH1F("test_detector_hists_"  + str(toy_num)+self.unique_name,"",len(self.x_axis_binning)-1,array.array('d',self.x_axis_binning)))
            self.test_truth_hists   .append(r.TH1F("test_truth_hists_"  + str(toy_num)+self.unique_name,"",len(self.x_axis_binning)-1,array.array('d',self.x_axis_binning)))

            self.train_migration_matrix[-1].SetDirectory(0)
            self.test_detector_hists   [-1].SetDirectory(0)

    def construct_train_responses(self):
        """
            Creates the training truth and detector distributions 
        """
        print("[ TA-UNFOLD - CLOSURE ] - Generating ", self.n_toys, " toys...")
        for toy_num in xrange(self.n_toys):
                
            # Makes literally no difference to RooUnfold..
            self.rescale_mc_hist_to_data_lumi(self.train_migration_matrix[toy_num],is_2d= True)

            self.train_truth_hists     .append(self.train_migration_matrix[toy_num].Clone().ProjectionY().Clone("train_truth_hist_"+str(toy_num)))
            self.train_detector_hists  .append(self.train_migration_matrix[toy_num].Clone().ProjectionX().Clone("train_detector_hist_"+str(toy_num)))
            self.train_truth_hists   [-1].SetDirectory(0)
            self.train_detector_hists[-1].SetDirectory(0)

            self.train_responses.append(r.RooUnfoldResponse(self.train_detector_hists[toy_num], self.train_truth_hists[toy_num], self.train_migration_matrix[toy_num].Clone()))
            self.train_responses[-1].UseOverflow(True)

            # print ("[ TA-UNFOLD ] - Toy "+ str(toy_num)+  " split MC into: ")
            # print ( "                         " + str(self.n_train[toy_num]) + " training events ")
            # print ( "                         " + str(self.n_test[toy_num]) + " test events.")
            # print ( "                         " + str(self.n_events) + " total events.")


    def is_train(self,event_number,toy_num): 
        """ 

        """
        r.gRandom.SetSeed( int(toy_num*(10**7) + event_number))
        return (r.gRandom.Uniform() > 0.5 )

    def create_test_train_histograms(self):
        """
            Create the required test and train data
        """
        # First open the tree and ge the tuples
        self.construct_test_train_histograms()

        # Now iteratte over the reco tree
        self.n_events = 0
        print ("[ TA-UNFOLD - CLOSURE ] - Generating " + str(self.n_toys)+  " histograms.")
        
        for i, file_list in enumerate(self.input_mc_tuple):

            # if len(file_list) == 1 and file_list[-6:]=='*.root':
            #     print ('[ TA-UNFOLD | CLOSURE ] -Creating the list of input files in folder: ',file_list[0][:-6])
            file_list = glob.glob(file_list[0])

            for j, file in enumerate(file_list):
                in_file = rhf.open_file(file)
                if not isinstance(in_file, r.TFile):
                    print("[ TA-UNFOLD - CLOSURE ] - Error with file", file)
                    continue 

                reco_tree     = in_file.Get(self.detector_tuple_name)
                particle_tree = in_file.Get(self.truth_tuple_name)

                if not isinstance(reco_tree, r.TTree) or \
                    not isinstance(particle_tree, r.TTree):
                    print("[ TA-UNFOLD - CLOSURE ] - Error with file ", file, ' trees not found: ')
                    print(self.detector_tuple_name)
                    print(self.truth_tuple_name)
                    continue 

                # we are going to sync events by eventNumbers
                reco_tree.BuildIndex('eventNumber')
                particle_tree.BuildIndex('eventNumber') 

                # Set up a way to check that we both pass the cuts and can retrieve the unfolding variable 
                # as defined by a formulae.  NB StackExchange suggests claling GetNdata() but that messes 
                # things up! ls
                truth_formula        = r.TTreeFormula("truth_variable_formula", self.truth_level_variable,     particle_tree)
                truth_cut_formula    = r.TTreeFormula("truth_cut_formula",      "1.0"+ self.truth_cut_weight,  particle_tree)
                truth_weight_formula = r.TTreeFormula("truth_weight_formula",    self.truth_weight,            particle_tree)

                reco_formula        = r.TTreeFormula("reco_variable_formula", self.detector_level_variable,    reco_tree)
                reco_cut_formula    = r.TTreeFormula("reco_cut_formula",      "1.0"+ self.detector_cut_weight, reco_tree)
                reco_weight_formula = r.TTreeFormula("reco_weight_formula",   self.detector_weight,            reco_tree)
            

                for event in reco_tree:
                    # But also ensure that the particle level event passes
                    p_index = particle_tree.GetEntryNumberWithIndex(event.eventNumber)
                    particle_tree.GetEntry(p_index)
                    if p_index == -1:
                        continue

                    # for x in xrange(1, truth_formula.GetNdata() ):
                    truth_variable  = truth_formula.EvalInstance()
                    truth_cut  = truth_cut_formula.EvalInstance() 
                    truth_weight    = truth_weight_formula.EvalInstance()

                    # for x in xrange(1, reco_formula.GetNdata() ):
                    reco_variable  = reco_formula.EvalInstance()
                    reco_cut = reco_cut_formula.EvalInstance() 
                    weight = reco_weight_formula.EvalInstance()

                    # if reco_formula.GetNdata() == 0 or truth_formula.GetNdata():
                    #     # print('ERROR No N data in tttree...')
                    #     continue
                    # Cut the cuts and ensure we pass both levels            
                    if reco_cut != 1.0 or truth_cut != 1.0 and reco_variable != 0. and truth_variable != 0.:
                        continue

                    # Now get the variable and weight that we care about

                    self.n_events += 1
                    self.total_migration_matrix.Fill( reco_variable, truth_variable, weight)
                    # Finally iterate over the number of toys and 
                    for toy_num in xrange(self.n_toys):
                        # fill the train stuff first
                        if self.is_train(event.eventNumber,toy_num):
                            self.n_train[toy_num] += 1 
                            self.train_migration_matrix[toy_num].Fill( reco_variable, truth_variable, weight)
                        else:
                            self.n_test[toy_num] += 1 
                            self.test_detector_hists[toy_num].Fill( reco_variable, weight)
                            self.test_truth_hists[toy_num].Fill   ( truth_variable, weight)

                in_file.Close()

        # Finally create the RooUnfoldResposne for all toys
        self.construct_train_responses()        



    def evaluate_nom_mc_luminiosity(self):
        """
            - Evaluate the MC effective luminosities of the data sets used
        """ 
        self.signal_sample.mc_lumi = []
        self.mc_luminosity =  0.0
        for i, file_list in enumerate(self.signal_sample.input_file_names):
                assert len(file_list) != 0, 'ERROR: signal_sample file list is empty!'

                # Evaluate n
                nEvents = 0
                sum_weights_chain = r.TChain("sumWeights")
                for f in file_list:
                    sum_weights_chain.Add(f)
                for e in sum_weights_chain:
                    nEvents += e.totalEventsWeighted

                # Then evaluating the MC luminosity
                mc_luminosity_nom = nEvents/(self.signal_sample.x_secs[i]*self.signal_sample.k_factors[i])
                self.signal_sample.mc_lumi.append(mc_luminosity_nom)
                self.mc_luminosity += mc_luminosity_nom

    def run(self, response,truth,detector_hist, efficiency, acceptance,method, migration_matrix, output_folder=None):
        """
            Performs the test in its entirety, creating a fixed number of 
            test distribtusions as described in the header part of this Class

        """

        if self.output_folder is not None:
            self.output_file = rhf.open_file(self.output_folder + self.unique_name + ".root", "RECREATE")

        r.gDirectory.mkdir(self.unique_name)
        r.gDirectory.cd(self.unique_name)

        # Note this ais coded in an excpetionally slow manner! 2
        print ("[ TA-UNFOLD - CLOSURE ] - Performing the Pull Test")
        self.error_on_unfolding = []   

        for toy_num in xrange(self.n_toys):  
            # print ("[ TA-UNFOLD ] - Unfolding toy: " + str(toy_num))
            r.gDirectory.mkdir("toy_"+str(toy_num))
            r.gDirectory.cd("toy_"+str(toy_num))

            #rescale the test dataset to have the same number of events as we expect in data
            self.rescale_mc_hist_to_data_lumi( self.test_detector_hists[toy_num] )
            self.rescale_mc_hist_to_data_lumi( self.test_truth_hists[toy_num] )

            # Now perform the unfolding on the test distribtuion using the statistically independet 
            # test unfolder.
            toy_unfolded_hist, toy_errors = self.unfold_toy(self.train_responses[toy_num], self.test_detector_hists[toy_num])
            self.unfolded_detector_hist.append( toy_unfolded_hist )

            # now evaluate the bin by by bin difference between truth and unfolded distribution 
            # Wasn't sure whether I should be evaluating the difference between the unfolded test and the train truth 
            # or the unfolded test with the test truth 
            self.delta_unfolding_truth.append(  self.unfolded_detector_hist[toy_num].Clone("delta_unfolding_truth_"+str(toy_num) ))
            # self.delta_unfolding_truth[-1].Add( self.test_truth_hists[toy_num] , -1)
            self.delta_unfolding_truth[-1].Add( self.train_truth_hists[toy_num] , -1)

            self.error_on_unfolding.append( self.construct_toy_error_hist(self.unfolded_detector_hist[-1], self.train_truth_hists[toy_num], normalize = False))

            # Save all of these plots to the relevant toy_N subfolder 
            self.delta_unfolding_truth[-1].Write()
            self.test_detector_hists[toy_num].Write()
            self.unfolded_detector_hist[toy_num].Write()
            self.train_truth_hists[toy_num].Write()
            self.error_on_unfolding[-1].SetName("error_on_unfolding_"+str(toy_num))
            self.error_on_unfolding[-1].Write()
            
            # Finally construct the pull by dividing throught delta(unfolding,truth) with
            # the error 
            self.toy_pull_hist.append( self.delta_unfolding_truth[-1].Clone("pull_"+str(toy_num)))
            self.toy_pull_hist[-1].Divide(self.error_on_unfolding[-1])
            self.toy_pull_hist[-1].Write()
            r.gDirectory.cd("../")

        # collate the pull plots 
        self.pull_mid_points, self.pull_rms, self.pull_mean_erros, self.pull_width_errors = self.evaluate_pulls()
        self.create_pull_plot( self.pull_mid_points, self.pull_rms, self.pull_mean_erros, self.pull_width_errors)

        r.gDirectory.cd("../")

    def draw_test_detector_heatmap(self,output_folder):
        """
            Draws 2D histograms showing the spread of the test detector level variable 
            as a function for all test toys. 

            We expect roughly Gaussian distributions in each bins, so this is a quick way to
            examine any deviances from this. 

            A more thorough check can be performed by examining the output of the following methods:
                draw_test_detector_distributions
        """

        # Set up the style options
        AS.SetAtlasStyle()
        canvas = r.TCanvas(self.name + "_test_detector_distribution" + self.unique_name,self.name + "_test_detector_distribution" + self.unique_name,800,600) 
        canvas.SetTopMargin(    0.1)
        canvas.SetRightMargin(  0.15)
        canvas.SetBottomMargin( 0.15)
        canvas.SetLeftMargin(   0.15)
        
        # Convert the set of histograms to a TH2F with the same binning 
        heatmap = rhf.convert_list_of_hists_to_heatmap(self.test_detector_hists, self.x_axis_binning)
        # Set up the styling options for the heatmap
        heatmap =  dhf.set_style_options(heatmap, dhf.get_line_options(r.kBlack, 1))
        heatmap.GetXaxis().SetTitle(self.x_axis_title)
        heatmap.GetYaxis().SetTitle("Normalized Entries ")
        r.gStyle.SetPalette(r.kBird)

        # Now it's nice and pretty draw it.
        heatmap.Draw("COLZ")

        # We don't draw the default atlas label as the width of the aspect ratio
        # to the expected 1:1 
        AS.ATLASLabel(  0.15, 0.96,  r.kBlack, 0.04*2.0, 0.04, "  Simulation Internal")        
        AS.myText    (  0.15, 0.92,r.kBlack,0.04, AS.lumi_string)  

        canvas.Print(output_folder+"test_detector_spread.eps")  
        canvas.Print(output_folder+"test_detector_spread.png")  

    def draw_pull_heatmap(self,output_folder):
        """
            Draws 2D histograms showing the spread of pulls for each toy in each bin of 
            the unfolding variable.

            We expect roughly Gaussian distributions in each bins, so this is a quick way to
            examine any deviances from this. 

            A more thorough check can be performed by examining the output of the following methods:
                draw_pull_distributions
        """

        # Set up the canvas approrioately 
        AS.SetAtlasStyle()
        canvas = r.TCanvas(self.name + "_pull_distribution" + self.unique_name,self.name + "_pull_distribution" + self.unique_name,800,600) 
        canvas.SetTopMargin(    0.1)
        canvas.SetRightMargin(  0.15)
        canvas.SetBottomMargin( 0.15)
        canvas.SetLeftMargin(   0.15)
        
        heatmap = rhf.convert_list_of_hists_to_heatmap(self.toy_pull_hist, self.x_axis_binning, n_bins = int(np.sqrt(self.n_toys)+3) )
        
        # Set up the styling options for the heatmap
        heatmap =  dhf.set_style_options(heatmap, dhf.get_line_options(r.kBlack, 1))
        heatmap.GetXaxis().SetTitle(self.x_axis_title)
        heatmap.GetYaxis().SetTitle("Pull")
        r.gStyle.SetPalette(r.kBird )

        # Now it's nice and pretty draw it.
        heatmap.Draw("COLZ")

        # We don't draw the default atlas label as the width of the aspect ratio
        # to the expected 1:1, we aslo put this at the top of the plot so the 
        # colors don't make it tricky to read.
        AS.ATLASLabel(  0.15, 0.96, r.kBlack, 0.04*2.0, 0.04, "  Simulation Internal")        
        AS.myText    (  0.15, 0.92, r.kBlack,0.04, AS.lumi_string)  
        
        # canvas.Print(output_folder+"pull.eps")
        canvas.Print(output_folder+"pull.png")
   
    def draw_pull_components(self,output_folder):
        """
            Draws 2D histograms showing the spread of the error on each bin for each toy in each bin of 
            the unfolding variable, as well as the difference between the truth and the unfolded 

            We expect roughly Gaussian distributions in each bins, so this is a quick way to
            examine any deviances from this. 

            A more thorough check can be performed by examining the output of the following methods:
                draw_pull_distributions
        """

        # Set up the canvas approrioately 
        AS.SetAtlasStyle()
        canvas = r.TCanvas(self.name + "_pull_distribution" + self.unique_name,self.name + "_pull_distribution" + self.unique_name,800,600) 
        canvas.SetTopMargin(    0.1)
        canvas.SetRightMargin(  0.15)
        canvas.SetBottomMargin( 0.15)
        canvas.SetLeftMargin(   0.15)
        
        heatmap = rhf.convert_list_of_hists_to_heatmap(self.delta_unfolding_truth, self.x_axis_binning, n_bins = int(np.sqrt(self.n_toys)+3) )
        
        # Set up the styling options for the heatmap
        heatmap =  dhf.set_style_options(heatmap, dhf.get_line_options(r.kBlack, 1))
        heatmap.GetXaxis().SetTitle(self.x_axis_title)
        heatmap.GetYaxis().SetTitle("Unfolded - Truth")
        r.gStyle.SetPalette(r.kBird )

        # Now it's nice and pretty draw it.
        heatmap.Draw("COLZ")

        # We don't draw the default atlas label as the width of the aspect ratio
        # to the expected 1:1, we aslo put this at the top of the plot so the 
        # colors don't make it tricky to read.
        AS.ATLASLabel(  0.15, 0.96, r.kBlack, 0.04*2.0, 0.04, "  Simulation Internal")        
        AS.myText    (  0.15, 0.92, r.kBlack,0.04, AS.lumi_string)  
        
        canvas.Print(output_folder+"delta_unfolding_truth_spread.eps")
        canvas.Print(output_folder+"delta_unfolding_truth_spread.png")

        heatmap = rhf.convert_list_of_hists_to_heatmap(self.error_on_unfolding, self.x_axis_binning, n_bins = int(np.sqrt(self.n_toys)+3) )
        
        # Set up the styling options for the heatmap
        heatmap =  dhf.set_style_options(heatmap, dhf.get_line_options(r.kBlack, 1))
        heatmap.GetXaxis().SetTitle(self.x_axis_title)
        heatmap.GetYaxis().SetTitle("Error on unfolded")
        r.gStyle.SetPalette(r.kBird )

        # Now it's nice and pretty draw it.
        heatmap.Draw("COLZ")

        # We don't draw the default atlas label as the width of the aspect ratio
        # to the expected 1:1, we aslo put this at the top of the plot so the 
        # colors don't make it tricky to read.
        AS.ATLASLabel(  0.15, 0.96, r.kBlack, 0.04*2.0, 0.04, "  Simulation Internal")        
        AS.myText    (  0.15, 0.92, r.kBlack,0.04, AS.lumi_string)  
        canvas.Print(output_folder+"error_on_unfolding.eps")
        canvas.Print(output_folder+"error_on_unfolding.png")


    def evaluate_pulls(self):
        """
            Collate the pulls of each toy and find the mean and width of the pull
            distributions as a funtion of unfolding variable.
        """

        pull_mid_points, pull_mean_errors = [], []
        pull_rms, pull_width_errors    = [], []
        pull_points,self.pull_fit = {},{}
        self.pull_hists={}
        self.pull_avg, self.pull_std, self.pull_RMS = {}, {}, {}

        # 
        print ("[ TA-UNFOLD - CLOSURE ] - Evaluating pulls...")
        for bin in xrange(1,self.toy_pull_hist[-1].GetNbinsX()+1):
            # print("[ TA-UNFOLD ] Evaluating pull for bin: ", self.toy_pull_hist[-1].GetBinLowEdge(bin) , " - ", self.toy_pull_hist[-1].GetBinLowEdge(bin+1))

            # Create array of the bin points so we can apply np      
            pull_points[bin] = []
            for toy_num in xrange(self.n_toys):
                pull_points[bin].append(self.toy_pull_hist[toy_num].GetBinContent(bin))

            # We now have a numpy array of all pulls for this bin. Let's construct a
            # histogram that has an appropriate binning
            x_min = -5#max(-5, -abs(np.percentile(pull_points[bin],15)) )  
            x_max = 5#min(5,  abs(np.percentile(pull_points[bin],15)) ) 

            self.pull_hists[bin] = r.TH1F("pull_spread_bin_" + str(bin),"",
                                    # int(np.sqrt(float(self.n_toys)))-1,
                                    20,
                                    x_min,
                                    x_max)
            for pull in pull_points[bin]:
                self.pull_hists[bin].Fill(pull)

            # Fit a gaussian to this, fit over the full range! 
            self.pull_fit[bin] = r.TF1("pull_gaussian_fit_"+str(bin),"gaus", x_min,x_max)
            self.pull_hists[bin].Fit(self.pull_fit[bin], "SQ")
            self.pull_avg[bin] = np.mean(pull_points[bin])
            self.pull_std[bin] = np.std(pull_points[bin])
            self.pull_RMS[bin] = np.sqrt(np.mean(np.square(pull_points[bin])))

            # Store the fitted gaussian parameters and their erros
            pull_mid_points.append(self.pull_fit[bin].GetParameter(1) )
            pull_rms.append(self.pull_fit[bin].GetParameter(2) )
            pull_mean_errors.append(self.pull_fit[bin].GetParError(1) )
            pull_width_errors.append(self.pull_fit[bin].GetParError(2) )

            # Save the indidivudal pull histogram 
            self.pull_hists[bin].Write()

        return pull_mid_points, pull_rms,pull_mean_errors, pull_width_errors

    def draw_pull_distributions(self,output_folder):
        """
            Draw the distirbutions of the pulls for each bin and the fitted Gaussian.

            Creates indiivudal eps plots as swell as a summary pdf.
        """
        canvas = r.TCanvas(self.name + "_pull_distribution" + self.unique_name,self.name + "_pull_distribution" + self.unique_name,600,600) 
        # it would really clutter the closure test folder to have n_bin Gaussians
        # instead lets shove them all into one individual folders for each toy
        pull_output_folder = output_folder + "/pull_distributions/"
        rhf.create_folder(pull_output_folder)

        # we will also create a summary file for all of these plots for easy sharing
        canvas.Print(pull_output_folder+"pull_distribution_collated.pdf[")


        for bin in xrange(1,self.toy_pull_hist[-1].GetNbinsX()+1):
            dhf.plot_histogram(canvas,
                {
                    "Pull": [self.pull_hists[bin], dhf.get_point_options(r.kBlack, 21) ],
                    "Pull Line": [self.pull_hists[bin], dhf.get_line_options(r.kBlack, 1) ],
                },
                x_axis_title = "Pull = (unfolded - truth)/#sigma_{unfolded}", 
                y_axis_title ="Entries", 
                do_atlas_labels = True,
                do_legend = False,
                labels = [
                    "%.3f <  %s #leq %.3f"%(self.x_axis_binning[bin-1],self.x_axis_binning[bin], self.x_axis_binning[bin]),
                    "#color[2]{Fit: #mu = %.3f #pm %.3f, #sigma = %.3f #pm %.3f}" %( self.pull_fit[bin].GetParameter(1),self.pull_fit[bin].GetParError(1),self.pull_fit[bin].GetParameter(2),self.pull_fit[bin].GetParError(2)),
                    "#color[4]{Mean: #mu = %.3f , #sigma = %.3f }" %( 
                        self.pull_avg[bin], self.pull_std[bin]),
                    "#color[6]{RMS = %.3f }" %( 
                        self.pull_RMS[bin]),
                  ]
                )   

            # The fit's are not draw automatically by default, so we've saved
            # them and call them now 
            self.pull_fit[bin].SetLineWidth(4)
            self.pull_fit[bin].SetLineStyle(1)
            self.pull_fit[bin].SetLineColor(r.kRed)
            self.pull_fit[bin].Draw("SAME")

            # canvas.Print(pull_output_folder+"pull_distribution_bin_"+str(bin)+".eps")
            # canvas.Print(pull_output_folder+"pull_distribution_bin_"+str(bin)+".png")
            canvas.Print(pull_output_folder+"pull_distribution_collated.pdf")

        canvas.Print(pull_output_folder+"pull_distribution_collated.pdf]")
   
    def rescale_mc_hist_to_data_lumi(self, hist, is_2d = False):
        """
            As eart of the closure test we rescale the test statitc such that it has staitistcal errors
            and NEvents the size expected in a data sample of that size. 
        """
        scale_factor = self.luminosity/self.mc_luminosity
        hist.Scale(scale_factor)

        for bin in xrange(1,hist.GetNbinsX()+1):   
            bin_error = 0.0

            if is_2d:
                for bin_y in xrange(1,hist.GetNbinsY()+1):   
                    bin_content = hist.GetBinContent(bin,bin_y)
                    bin_error = hist.GetBinError(bin,bin_y)

            else:
                bin_content = hist.GetBinContent(bin)
                bin_error = hist.GetBinError(bin)
            
            hist.SetBinError(bin, bin_error*scale_factor )

            
    def create_pull_plot(self, pull_mid_points, pull_rms, pull_mean_errors, pull_width_errors):
        """
            Creates a histogram showing the mean and width of the pulls
            evaluated for all toys
        """
        #store the plots 
        self.pull_figures_of_merit = {}

        #
        self.pull_mean_plot  = self.toy_pull_hist[-1].Clone("Pull_mean")
        self.pull_width_plot = self.toy_pull_hist[-1].Clone("Pull_width")


        for bin in xrange(1,self.toy_pull_hist[-1].GetNbinsX()+1):
            self.pull_mean_plot.SetBinContent (bin, pull_mid_points[bin-1])
            self.pull_width_plot.SetBinContent(bin, pull_rms[bin-1])
            self.pull_mean_plot.SetBinError (bin,  pull_mean_errors[bin-1])
            self.pull_width_plot.SetBinError(bin, pull_width_errors[bin-1] )

        self.pull_figures_of_merit["Pull Mean"] = [
            self.pull_mean_plot,
            dhf.get_point_options(r.kRed, 21)
        ]
        self.pull_figures_of_merit["Pull Width"] = [
            self.pull_width_plot,
            dhf.get_point_options(r.kBlue, 22)
        ]
        self.pull_mean_plot .Write()
        self.pull_width_plot.Write()


    def create_test_detector_spread(self):
        """
            Creates histograms showing the spread of all toy detector unfolding bin contenst
            for every bin. This is a validation set of histogram, if we are drawing 
            the test and train histogram grams correctly they should indeed by a Gaussian
        """
        # store an array of all bin contents of the toys for each bin 
        bin_content = {}
        self.test_detector_spread_hist = {}

        # create numpy arrays of the bin content for each toy per bin
        for bin in xrange(1, self.test_detector_hists[-1].GetNbinsX()+1):
            bin_content[bin] = []

            for toy_num in xrange(self.n_toys):
                bin_content[bin].append(self.test_detector_hists[toy_num].GetBinContent(bin))

        # Keep the open root file clean and tidy! 
        r.gDirectory.mkdir("test_detector_spreads")
        r.gDirectory.cd("test_detector_spreads")

        # Now we can create the TH1F, where we hand the binning based on the 
        # 5-95 percentile range of the bin_contents
        for bin in xrange(1, self.test_detector_hists[-1].GetNbinsX()+1):
            self.test_detector_spread_hist[bin] = r.TH1F("test_detector_spread_bin_" + str(bin),"",
                                    max(int(np.sqrt(float(self.n_toys))),11),
                                    # use the 1sigma percentiles of the pulls
                                    # as the range, that way we definitely capture the majority of the distribution
                                    # but do not have to be concerned with extrmes that will through the fit
                                    np.percentile(bin_content[bin],16),
                                    np.percentile(bin_content[bin],84))

            for b in bin_content[bin]:
                self.test_detector_spread_hist[bin].Fill(b)
            self.test_detector_spread_hist[bin].Write()

        r.gDirectory.cd("../")

    def draw_test_detector_distributions(self,output_folder):
        """
            Saves the test_detetector spread distributions, where we plot the bin content of 
            each toy's test detector distribution 

            Creates a new folder "test_detector_distributions"
        """
        canvas = r.TCanvas(self.name + "_test_detector_distributions" + self.unique_name,self.name + "_test_detector_distributions" + self.unique_name,600,600) 

        # it would really clutter the closure test folder to have n_bin Gaussians
        # instead lets shove them all into one individual folders for each toy
        test_spread_output_folder = output_folder + "/test_detector_distributions/"
        rhf.create_folder(test_spread_output_folder)

        # we will also create a summary file for all of these plots for easy sharing
        canvas.Print(test_spread_output_folder+"test_detector_distributions.pdf[")


        for bin in xrange(1,self.toy_pull_hist[-1].GetNbinsX()+1):
            dhf.plot_histogram(canvas,
                {
                    "Test Detector Line":  [ self.test_detector_spread_hist[bin], dhf.get_line_options(r.kBlack, 1) ],
                    "Test Detector Point": [ self.test_detector_spread_hist[bin], dhf.get_point_options(r.kBlack, 21) ],
                },
                x_axis_title = "Bin Conent", 
                y_axis_title ="Number of toys", 
                do_atlas_labels = True,
                do_legend = False,
                labels = [
                    str(self.x_axis_binning[bin-1]) + " <  " + self.x_axis_title  +  " #leq "   + str(self.x_axis_binning[bin])
                    ]
                )   
            # canvas.Print(test_spread_output_folder+"test_detector_distributions_bin"+str(bin)+".eps")
            # canvas.Print(test_spread_output_folder+"test_detector_distributions_bin"+str(bin)+".png")
            canvas.Print(test_spread_output_folder+"test_detector_distributions.pdf")

        canvas.Print(test_spread_output_folder+"test_detector_distributions.pdf]")


    def construct_toy_error_hist(self, unfolded_hist, before_unfolding_hist, normalize = True):
        """
            Converst a TVectorD of errors into a histogram of dimensions unfolded_hist 

            Parmaters:
                unfolded_hist: a TH1F with correct binning, the contents is unimportant 
                               we simply are after it's dimensions
                errors:        a TVectorD of errors for the unfolding that will be set as the 
                               bin contents of the unfolded_hist
        """

        error_hist = unfolded_hist.Clone()
        error_hist.SetDirectory(0)

        scale_factor = 1.0
        if normalize and unfolded_hist.Integral() != 0.0:
            scale_factor /= unfolded_hist.Integral() 

        # Rescale the error on the unfolding to account for the fact that we're
        # unfolding a reduced stat sample that's been rescaled 
        scale_factor *= self.mc_luminosity/self.luminosity

        for bin in xrange(1,unfolded_hist.GetNbinsX()+1):
            error_hist.SetBinContent(bin, ((unfolded_hist.GetBinError(bin)*scale_factor)**2 + before_unfolding_hist.GetBinError(bin)**2)**0.5 )

        # Now go bin by bin across the
        return error_hist


