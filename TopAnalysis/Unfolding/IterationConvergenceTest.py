"""
    A test designed to assess the optimum number of iterations for an iterative bayesian unfolding procedure

    The test works by:
        1. Split MC in half to create a test and training set 
        2. Unfold the test sample with response matrices from training with a varying number of iterations
        3. Evaluate the chi2/NDof for each of the iterations, i.e:
                chi2_numbers[iterations].append(unfold.Chi2(testing_truth)/testing_truth.GetNbinsX())

        3. Draw a heatmap that shows chi2/NDoF on the y, and the x_Axis is the unfolded binnning distribution 

"""
import RootHelperFunctions.RootHelperFunctions as rhf 
import RootHelperFunctions.StringHelperFunctions as shf 
import RootHelperFunctions.DrawingHelperFunctions as dhf 
import ATLASStyle.AtlasStyle as AS

import ROOT as r
from collections import OrderedDict
import numpy as np 
import time 
import array
import glob

class IterationConvergenceTest():
    """ 

    """ 
    def __init__(self,
            input_mc_tuple,          #MC that we base the unfolding on
            truth_level_variable,    #Truth level variable name that will be unfolded to
            detector_level_variable, #Detector level variable name that will be unfolded from
            x_axis_binning,
            x_axis_title = "",
            y_axis_title = "",
            truth_tuple_name = "particleLevel",
            detector_tuple_name = "nominal",
            save_output = True, 
            truth_cuts      = [""],
            detector_cuts   = [""],
            detector_weight = "",
            truth_weight    = "",
            name = "closure",
            draw_test_detector_spread = True,
            n_toys = 250,


        ):

        self.name         = "Iteration Convergence test"
        self.description  = "Split the MC into a training and test sample. Unfold the test with training and examine the goodness of unfolding as a function of number of iterations. Asssumes you are using iterative bayesian unfolding." 
        self.save_output  = save_output
        self.unique_name  = name
        self.n_iterations = [1,2,3,4,5,6,7,8,9,10]
        self.method       = "Bayes"
        self.n_toys       = n_toys

        #options for drawing the pulls and the test detector spread
        self.train_migration_matrix  = []
        self.train_truth_hists       = []
        self.train_detector_hists    = []
        self.test_truth_hists        = []
        self.train_truth_total_hists = []
        self.efficiency_plot         = []
        self.acceptance_plot         = []
        self.test_detector_hists     = []
        self.unfolded_detector_hist  = {}
        self.delta_unfolding_truth   = []
        self.toy_pull_hist           = []
        self.train_responses         = []

        self.input_mc_tuple           = input_mc_tuple 
        self.truth_level_variable     = truth_level_variable
        self.detector_level_variable  = detector_level_variable
        self.truth_tuple_name         = truth_tuple_name 
        self.detector_tuple_name      = detector_tuple_name 
        self.x_axis_binning           = x_axis_binning
        self.x_axis_title             = x_axis_title
        self.detector_weight = detector_weight
        self.truth_weight    = truth_weight   
        self.y_axis_title  = y_axis_title
        self.draw_test_detector_spread = draw_test_detector_spread 
        self.detector_cut_weight = ""
        for cut in detector_cuts:
            self.detector_cut_weight += "*(" + cut + ")"
        #
        self.truth_cut_weight = ""
        for cut in truth_cuts:
            self.truth_cut_weight += "*(" + cut + ")"

        self.create_test_train_histograms()

    def setup_migration_matrix_saving(self,output_folder):
        """
            creates a pdf that migration matrices will be collated in. Also creates the folder to store this pdf in.
        """
        self.migration_matrix_canvas = r.TCanvas(self.name + "migration_matrix",self.name + "migration_matrix",800,600)
        rhf.create_folder(output_folder+"train_migration_matrices/")        
        self.migration_matrix_canvas.Print(output_folder+"train_migration_matrices/migration_matrices.pdf[")

    def unfold_toy(self, toy_response, toy_detector_hist, toy_truth_hist, n_iter):
        """
            Performs an unfolding based on the method and parameters given to this class at 
            initialization for an arbitrary response and detector histograms 

            Parameters:
                toy_respose: a RooUnfold object that that has been constructed with the migration
                             matrix, truth and detector-level distributions
                toy_detector_hist: the histogram to be unfolded

            Returns: 
                The unfolded distribution as found by the method set in the constructor
                of this class
                The errors on the bins as defined by option too of TRooUnfold::ErecoV 
                option (2)
                    "Errors from the covariance matrix given by the unfolding"
        """

        # We support three methods
        if self.method == "SVD":
            unfolded_mc   = r.RooUnfoldSvd(toy_response, toy_detector_hist)
        elif self.method == "BinByBin":
            unfolded_mc   = r.RooTUnfold(toy_response, toy_detector_hist, 20 )
        else:
            r.RooUnfold().SetVerbose(0)
            unfolded_mc   = r.RooUnfoldBayes(toy_response, toy_detector_hist, n_iter )

        # return both the unfolded distribution and the errors on the bins
        return unfolded_mc.Hreco(2), unfolded_mc.Chi2(toy_truth_hist)/toy_truth_hist.GetNbinsX()

    def draw_migration_matrix(self,toy_num,output_folder):
        """ 
            Draw the migration matrix neatly, heavily based on the DrawingHelperFuntion method.
        """
        
        migration_matrix_drawing = rhf.normalize_migration_matrix(self.train_migration_matrix[toy_num].Clone())
        migration_matrix_drawing.GetXaxis().SetTitle(self.x_axis_title)
        migration_matrix_drawing.GetYaxis().SetTitle(self.y_axis_title)

        r.gDirectory.cd("toy_"+str(toy_num))
        migration_matrix_drawing.SetName("response_matrix_"+str(toy_num)) 
        migration_matrix_drawing.Write()
        r.gDirectory.cd("../")

        dhf.draw_migration_matrix(migration_matrix_drawing,self.migration_matrix_canvas )
        self.migration_matrix_canvas.Print(output_folder+"train_migration_matrices/migration_matrices.pdf")

    #
    def draw(self,output_folder,x_axis_title):
        """
            The main draw command for this tool. Creates the pull width and pull mean plot 
            which is the plot of interest for this tool, as well as calling the funttions
            to draw bin by bin pull and test detector bin content spreads. 

            output_folder: where the output will be stored, sub folders will becreated 
                           in this location. 
            x_axis_title: the truth level x_axis_title thatwill be used for all plots
        """
        r.gDirectory.cd(self.unique_name)

        #draw the actual distribution we care about 
        self.draw_toy_collated_plots(output_folder)
        self.draw_chi2_spreads(output_folder)

        if self.draw_test_detector_spread:
            self.create_test_detector_spread()
            self.draw_test_detector_distributions(output_folder)

        # self.draw_training_migration_matrices(output_folder)

        r.gDirectory.cd("../")


    def draw_chi2_spreads(self,output_folder):
        """
            Draw the chi2/Ndof distirbution for each n_iteration examined.
        """
        chi2_plots = OrderedDict()
        chi2_plots_all = OrderedDict()
        #firest create the dictionary for plotting purposes
        modulo_n = 3
        for n_iteration in self.n_iterations:

            chi2_plots_all[str(n_iteration) + " Iterations"] = [
                self.chi2_distribution[n_iteration],
                #the first parameter just choses the color style 
                dhf.get_truth_large_legend_stlye_opt( n_iteration-1, is_ratio = False )
            ]

            # By default we test itearations 1 to 10
            # that's quite hard to see, so instread let's plot only
            # a third of the plots.
            if n_iteration%3 != 0:
                continue

            chi2_plots[str(n_iteration) + " Iterations"] = [
                self.chi2_distribution[n_iteration],
                #the first parameter just choses the color style 
                dhf.get_truth_large_legend_stlye_opt( n_iteration-1, is_ratio = False )
            ]

 
        canvas = r.TCanvas(self.name + "_chi2_distribution" + self.unique_name,self.name + "_chi2_distribution" + self.unique_name,600,600) 

        dhf.plot_histogram(canvas,
            chi2_plots_all,
            y_axis_title = "Number of Toys", 
            x_axis_title ="#chi^{2}/N_{DoF}", 
            do_atlas_labels = True,
            do_legend = True,
            )   

        canvas.Print(output_folder+"chi2_distributions_all.eps")
        canvas.Print(output_folder+"chi2_distributions_all.png")
        dhf.plot_histogram(canvas,
            chi2_plots,
            y_axis_title = "Number of Toys", 
            x_axis_title ="#chi^{2}/N_{DoF}", 
            do_atlas_labels = True,
            do_legend = True,
            )   

        canvas.Print(output_folder+"chi2_distributions.eps")
        canvas.Print(output_folder+"chi2_distributions.png")

    def draw_training_migration_matrices(self,output_folder):
        """ 
            Draw all of the migration matrices for every single toy! 
        """

        self.setup_migration_matrix_saving(output_folder)
        for toy_num in xrange(self.n_toys):
            self.draw_migration_matrix(toy_num,output_folder)

        self.migration_matrix_canvas.Print(output_folder+"train_migration_matrices/migration_matrices.pdf]")

    def construct_test_train_histograms(self):
        """
        """
        self.n_train, self.n_test= [],[]
        for toy_num in xrange(self.n_toys):
            self.n_train.append(0)
            self.n_test.append(0)
            self.train_migration_matrix.append(r.TH2F("migration_matrix_" + str(toy_num),"",len(self.x_axis_binning)-1,array.array('d',self.x_axis_binning),
                                                                                            len(self.x_axis_binning)-1,array.array('d',self.x_axis_binning)))
            self.test_detector_hists   .append(r.TH1F("test_detector_hists_"  + str(toy_num),"",len(self.x_axis_binning)-1,array.array('d',self.x_axis_binning)))
            self.test_truth_hists   .append(r.TH1F("test_truth_hists_"  + str(toy_num),"",len(self.x_axis_binning)-1,array.array('d',self.x_axis_binning)))

            self.train_migration_matrix[-1].SetDirectory(0)
            self.test_detector_hists   [-1].SetDirectory(0)

    def construct_train_responses(self):
        """
        """
        for toy_num in xrange(self.n_toys):
            self.train_truth_hists     .append(self.train_migration_matrix[toy_num].Clone().ProjectionY().Clone("train_truth_hist_"+str(toy_num)))
            self.train_detector_hists  .append(self.train_migration_matrix[toy_num].Clone().ProjectionX().Clone("train_detector_hist_"+str(toy_num)))
            self.train_truth_hists   [-1].SetDirectory(0)
            self.train_detector_hists[-1].SetDirectory(0)

            self.train_responses.append(r.RooUnfoldResponse(self.train_detector_hists[toy_num], self.train_truth_hists[toy_num], self.train_migration_matrix[toy_num].Clone()))
            self.train_responses[-1].UseOverflow(True)

            print ("[ TA-UNFOLD ] - Toy "+ str(toy_num)+  " split MC into: ")
            print ( "                         " + str(self.n_train[toy_num]) + " training events ")
            print ( "                         " + str(self.n_test[toy_num]) + " test events.")
            print ( "                         " + str(self.n_events) + " total events.")


    def is_train(self,event_number,toy_num): 
        """ 

        """
        r.gRandom.SetSeed( int(toy_num*(10**7) + event_number))
        return (r.gRandom.Uniform() > 0.5 )


    def create_test_train_histograms(self):
        """
            Create the required test and train data
        """
        # First open the tree and ge the tuples
        self.construct_test_train_histograms()

        for i, file_list in enumerate(self.input_mc_tuple):

            # if len(file_list) == 1 and file_list[-6:]=='*.root':
            print ('[ TA-UNFOLD ] -Creating the list of input files in folder: ',file_list[0][:-6])
            file_list = glob.glob(file_list[0])
            # print ('[ TA-UNFOLD ] - Read file list: ', file_list)

            for j, file in enumerate(file_list):
                f = rhf.open_file(file)

                if not isinstance(f, r.TFile):
                    print("Error with file", file)
                    continue 

                reco_tree     = f.Get(self.detector_tuple_name)
                particle_tree = f.Get(self.truth_tuple_name)


                if not isinstance(reco_tree, r.TTree) or \
                    not isinstance(particle_tree, r.TTree):
                    print("Error with file ", file, ' trees not found: ')
                    print(self.detector_tuple_name)
                    print(self.truth_tuple_name)
                    continue 

                # we are going to sync events by eventNumbers
                reco_tree.BuildIndex('eventNumber')
                particle_tree.BuildIndex('eventNumber') 

                # Set up a way to check that we both pass the cuts and can retrieve the unfolding variable 
                # as defined by a formulae.  NB StackExchange suggests claling GetNdata() but that messes 
                # things up! 
                truth_formula        = r.TTreeFormula("truth_variable_formula", self.truth_level_variable,     particle_tree)
                truth_cut_formula    = r.TTreeFormula("truth_cut_formula",      "1.0"+ self.truth_cut_weight,  particle_tree)
                truth_weight_formula = r.TTreeFormula("truth_weight_formula",    self.truth_weight,            particle_tree)

                reco_formula        = r.TTreeFormula("reco_variable_formula", self.detector_level_variable,    reco_tree)
                reco_cut_formula    = r.TTreeFormula("reco_cut_formula",      "1.0"+ self.detector_cut_weight, reco_tree)
                reco_weight_formula = r.TTreeFormula("reco_weight_formula",   self.detector_weight,            reco_tree)

                # Now iteratte over the reco tree
                self.n_events = 0
            
                print ("[ TA-UNFOLD ] - Generating " + str(self.n_toys)+  " histograms.")

                for event in reco_tree:
                    # But also ensure that the particle level event passes
                    p_index = particle_tree.GetEntryNumberWithIndex(event.eventNumber)
                    particle_tree.GetEntry(p_index)
                    if p_index == -1:
                        continue

                    for x in xrange( truth_formula.GetNdata() ):
                        truth_variable  = truth_formula.EvalInstance()
                        truth_cut  = truth_cut_formula.EvalInstance() 
                        truth_weight    = truth_weight_formula.EvalInstance()

                    for x in xrange( reco_formula.GetNdata() ):
                        reco_variable   = reco_formula.EvalInstance()
                        reco_cut   = reco_cut_formula.EvalInstance() 
                        weight          = reco_weight_formula.EvalInstance()

                    # Cut the cuts and ensure we pass both levels            
                    if reco_cut != 1.0 or truth_cut != 1.0 and reco_variable != 0. and truth_variable != 0.:
                        continue

                    # Now get the variable and weight that we care about

                    self.n_events += 1
                    # Finally iterate over the number of toys and 
                    for toy_num in xrange(self.n_toys):
                        # fill the train stuff first
                        if self.is_train(event.eventNumber,toy_num):
                            self.n_train[toy_num] += 1 
                            self.train_migration_matrix[toy_num].Fill( reco_variable, truth_variable, weight)
                        else:
                            self.n_test[toy_num] += 1 
                            self.test_detector_hists[toy_num].Fill( reco_variable, weight)
                            self.test_truth_hists[toy_num].Fill   ( truth_variable, weight)

        for n_iteration in self.n_iterations:
            self.unfolded_detector_hist[n_iteration] = []

        # Finally create the RooUnfoldResposne for all toys
        self.construct_train_responses()

    def run(self, response,truth,detector_hist, efficiency, acceptance,method,migration_matrix):
        """
            Performs the test in its entirety, creating a fixed number of 
            test distribtusions as described in the header part of this Class

        """
        r.gDirectory.mkdir(self.unique_name)
        r.gDirectory.cd(self.unique_name)
        print ("[ TA-UNFOLD ] - Performing the Closure Test")

        #
        self.n_iteration_vs_chi2 = r.TH2F(self.unique_name + "_nIterations_vs_chi2", "",
                                    len(self.n_iterations)-1,
                                    array.array('d',self.n_iterations), 
                                    # 30 bins a long the y-axis, might need tweaking
                                    int(np.sqrt(self.n_toys))+2,0,6)
        self.chi2_distribution = {}
        for n_iteration in self.n_iterations:
            self.chi2_distribution[n_iteration]  = r.TH1F(self.unique_name + "_chi2_" + str(n_iteration),"",
                                        int(np.sqrt(self.n_toys)),0,6)


        for toy_num in xrange(self.n_toys):  
            print ("[ TA-UNFOLD ] - Unfolding toy: " + str(toy_num))
            r.gDirectory.mkdir("toy_"+str(toy_num))
            r.gDirectory.cd("toy_"+str(toy_num))

               # Now perform the unfolding on the test distribtuion using the statistically independet 
            # test unfolder.
            for n_iteration in self.n_iterations:
                toy_unfolded_hist, chi2_by_n_dof = self.unfold_toy(self.train_responses[toy_num], self.test_detector_hists[toy_num], self.test_truth_hists[toy_num],n_iteration)
                
                print "N_ITER: ",n_iteration, "     chi2 = ", chi2_by_n_dof

                self.unfolded_detector_hist[n_iteration].append( toy_unfolded_hist )
                self.chi2_distribution[n_iteration].Fill(chi2_by_n_dof)
                self.n_iteration_vs_chi2.Fill(n_iteration, chi2_by_n_dof)

            r.gDirectory.cd("../")
            print ""
            print ""
        #save this 
        self.n_iteration_vs_chi2.Write()

        # collate the pull plots 
        r.gDirectory.cd("../")

    def draw_toy_collated_plots(self,output_folder):
        """
        """
        # Set up the sti
        AS.SetAtlasStyle()
        canvas = r.TCanvas(self.name + "_pull_distribution" + self.unique_name,self.name + "_pull_distribution" + self.unique_name,800,600) 
        canvas.SetTopMargin(    0.1)
        canvas.SetRightMargin(  0.15)
        canvas.SetBottomMargin( 0.15)
        canvas.SetLeftMargin(   0.15)
        
        # Convert the set of histograms to a TH2F with the same binning 
        heatmap = rhf.convert_list_of_hists_to_heatmap(self.test_detector_hists, self.x_axis_binning)
        
        # Set up the styling options for the heatmap
        heatmap =  dhf.set_style_options(heatmap, dhf.get_line_options(r.kBlack, 1))
        heatmap.GetXaxis().SetTitle(self.x_axis_title)
        heatmap.GetYaxis().SetTitle("Normalized Entries ")
        r.gStyle.SetPalette(r.kBird)
        heatmap.Draw("COLZ")
        dhf.draw_atlas_details(["Test MC, Detector Level"], x_pos = 0.18,y_pos = 0.85, dy = 0.032, text_size = 0.03)
        canvas.Print(output_folder+"test_detector_spread.pdf.eps")       
        canvas.Print(output_folder+"test_detector_spread.pdf.png")       

        # Set up the styling options for the heatmaps
        self.n_iteration_vs_chi2 =  dhf.set_style_options(self.n_iteration_vs_chi2, dhf.get_line_options(r.kBlack, 1))
        self.n_iteration_vs_chi2.GetXaxis().SetTitle("Number of Iterations")
        self.n_iteration_vs_chi2.GetYaxis().SetTitle("#chi^{2}/N_{DoF}")
        r.gStyle.SetPalette(r.kBird )
        self.n_iteration_vs_chi2.Draw("COLZ")
        dhf.draw_atlas_details(["Test MC"], x_pos = 0.18,y_pos = 0.85, dy = 0.032, text_size = 0.03)
        canvas.Print(output_folder+"chi2_by_n_dof.eps")
        canvas.Print(output_folder+"chi2_by_n_dof.png")


   
    def create_test_detector_spread(self):
        """
            Creates histograms showing the spread of all toy detector unfolding bin contenst
            for every bin. This is a validation set of histogram, if we are drawing 
            the test and train histogram grams correctly they should indeed by a Gaussian
        """
        # store an array of all bin contents of the toys for each bin 
        bin_content = {}
        self.test_detector_spread_hist = {}

        # create numpy arrays of the bin content for each toy per bin
        for bin in xrange(1, self.test_detector_hists[-1].GetNbinsX()+1):
            bin_content[bin] = []

            for toy_num in xrange(self.n_toys):
                bin_content[bin].append(self.test_detector_hists[toy_num].GetBinContent(bin))

        # Keep the open root file clean and tidy! 
        r.gDirectory.mkdir("test_detector_spreads")
        r.gDirectory.cd("test_detector_spreads")

        # Now we can create the TH1F, where we hand the binning based on the 
        # 5-95 percentile range of the bin_contents
        for bin in xrange(1, self.test_detector_hists[-1].GetNbinsX()+1):
            self.test_detector_spread_hist[bin] = r.TH1F("test_detector_spread_bin_" + str(bin),"",
                                    max(int(np.sqrt(float(self.n_toys))),11),
                                    np.min(bin_content[bin]),
                                    np.max(bin_content[bin]))
            # self.test_detector_spread_hist[bin].SetDirectory(0)

            for b in bin_content[bin]:
                self.test_detector_spread_hist[bin].Fill(b)
            self.test_detector_spread_hist[bin].Write()

        r.gDirectory.cd("../")

    def draw_test_detector_distributions(self,output_folder):
        """
            Saves the test_detetector spread distributions, where we plot the bin content of 
            each toy's test detector distribution 

            Creates a new folder "test_detector_distributions"
        """
        canvas = r.TCanvas(self.name + "_test_detector_distributions" + self.unique_name,self.name + "_test_detector_distributions" + self.unique_name,600,600) 

        # it would really clutter the closure test folder to have n_bin Gaussians
        # instead lets shove them all into one individual folders for each toy
        test_spread_output_folder = output_folder + "/test_detector_distributions/"
        rhf.create_folder(test_spread_output_folder)

        # we will also create a summary file for all of these plots for easy sharing
        canvas.Print(test_spread_output_folder+"test_detector_distributions.pdf[")


        for bin in self.test_detector_spread_hist:
            dhf.plot_histogram(canvas,
                {
                    "Test Detector Line": [self.test_detector_spread_hist[bin], dhf.get_line_options(r.kBlack, 1) ],
                    "Test Detector Point":      [self.test_detector_spread_hist[bin], dhf.get_point_options(r.kBlack, 21) ],
                },
                x_axis_title = "Bin Conent", 
                y_axis_title ="Number of toys", 
                do_atlas_labels = True,
                do_legend = False,
                labels = [
                    str(self.x_axis_binning[bin-1]) + " <  " + self.x_axis_title  +  " #leq "   + str(self.x_axis_binning[bin])
                    ]
                )   
            canvas.Print(test_spread_output_folder+"test_detector_distributions_bin"+str(bin)+".eps")
            canvas.Print(test_spread_output_folder+"test_detector_distributions_bin"+str(bin)+".png")
            canvas.Print(test_spread_output_folder+"test_detector_distributions.pdf")

        canvas.Print(test_spread_output_folder+"test_detector_distributions.pdf]")


