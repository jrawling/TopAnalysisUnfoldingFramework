import RootHelperFunctions.RootHelperFunctions as rhf 
import RootHelperFunctions.StringHelperFunctions as shf 
import RootHelperFunctions.DrawingHelperFunctions as dhf 
import ROOT as r
from collections import OrderedDict

class IdentityTest():
    def __init__(self,save_output = True, name = "nameless", n_iterations = 4):
        self.name        = "Identity Test"
        self.description = "Unfold the detector level MC to truth and examine the ratio. We expect a perfect unfolding in this case, any deviance from this implies an error. "
        self.save_output = save_output
        self.unique_name = name
        self.n_iterations = n_iterations
        
    def draw(self,output_folder,x_axis_title):
        canvas = r.TCanvas(self.name + "_" + self.unique_name,self.name + "_" + self.unique_name,600,600) # apparently 600,600 is ATLAS recommednations but it looks like trash.
        
        # Format and draw the histograms neatly, showing the ratio w.r.t MC 
        mc_ratio_style_options = dhf.mc_ratio_style_options
        mc_ratio_style_options.x_title_offset  = None
        mc_ratio_style_options.x_axis_label_offset = None
        # Format and draw the histograms neatly, showing the ratio w.r.t MC 
        data_ratio_style_options = dhf.data_ratio_style_options
        data_ratio_style_options.x_axis_label_offset = None
        #
        ratio_histograms = dhf.ratio_plot(   canvas,
                        {
                           "Unfolded" : [ rhf.normalize_histogram(self.corr_unfolded_mc), dhf.data_style_options, data_ratio_style_options, "Truth"],
                           "Truth" : [   rhf.normalize_histogram(self.truth_hist), dhf.mc_style_options, mc_ratio_style_options,None ], 
                        },
                        ratio_y_axis_title    = "#frac{Unfolded}{Truth}",
                        x_axis_title = x_axis_title,
                        messages= ["Fiducial phase space"]
                       )

        #
        r.gDirectory.mkdir(shf.clean_string(self.name))
        r.gDirectory.cd(shf.clean_string(self.name))
        if self.save_output:
            canvas.Write()
            for name in ratio_histograms:
                ratio_histograms[name][0].Write() 
        r.gDirectory.cd("../")

        canvas.Print(output_folder+"unfolded_identity_test.eps")

    def run(self, response,truth,detector_hist, efficiency, acceptance,method,migration_matrix):
        print ("[ TA-UNFOLD ] - Performing the indentityTest")
        self.detector_hist = detector_hist.Clone("identityTest_detectorHist" + self.unique_name)
        self.detector_hist.Multiply(efficiency)
        self.detector_hist.SetDirectory(0)

        # RooUnfold supports SVD,Iterative Bayesian and BinByBin methods
        response = r.RooUnfoldResponse( migration_matrix.ProjectionX(),migration_matrix.ProjectionY(), migration_matrix.Clone())
        response.UseOverflow(True)

        if method == "SVD":
            self.unfolded_mc   = r.RooUnfoldSvd(response, self.detector_hist)
        elif method == "BinByBin":
            self.unfolded_mc   = r.RooTUnfold(response, self.detector_hist, 20 )
        else:
            #default is bayes
            self.unfolded_mc   = r.RooUnfoldBayes(response, self.detector_hist, self.n_iterations )

        self.truth_hist = truth.Clone(truth.GetName() +self.unique_name)
        self.unfolded_mc_hist = self.unfolded_mc.Hreco()
        self.corr_unfolded_mc = self.unfolded_mc_hist.Clone()
        self.corr_unfolded_mc.Divide(acceptance)
