import ROOT as r
import RootHelperFunctions.RootHelperFunctions as rhf
import RootHelperFunctions.StringHelperFunctions as shf
import RootHelperFunctions.DrawingHelperFunctions as dhf
import ATLASStyle.AtlasStyle as AS
import copy
from collections import OrderedDict
import numpy as np
import array
from time import time
# import threading
import os
import psutil

# from scipy.stats import distributions
class Unfolder:
    def __init__(
            self,
            signal_sample,          #MC that we base the unfolding on
            input_data_tuple,        #data tuple that we want to unfold
            output_folder,           #Where all ROOT  and  eps will be put
            truth_level_variable,    #Truth level variable name that will be unfolded to
            detector_level_variable, #Detector level variable name that will be unfolded from
            x_axis_binning,
            method = "bayes",        # we're going to use iterative bayesian
            truth_tuple_name = "particleLevel",
            detector_tuple_name = "nominal",
            save_output = True,       # we want everything to be shoved to a ROOT file
            verbosity = 2,
            x_axis_label = "",
            y_axis_label = "",
            unfolded_x_axis_title = "",
            unfolded_y_axis_title = "",
            detector_cuts = [],
            truth_cuts = [],
            messages = [],
            systematics = {},
            luminosity = 361000,
            name = None,
            n_iterations = 4,
            set_log_y = False,
            background_samples = [],
            np_background_samples = [],
            signal_mc_name="t#bar{t}",
            do_bootstrap = True,
            load_unfolder=None,
            additional_samples=None,
            data_tuple_is_mc =True,
            truth_weight="weight_mc",
            detector_weight="weight_mc*weight_leptonSF*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup",
            prompt_weight="Alt$(mu_true_isPrompt[0], Alt$(el_true_isPrompt[0], -1))==1",
            nonprompt_weight="Alt$(mu_true_isPrompt[0], Alt$(el_true_isPrompt[0], -1))==0",
            signal_fastsim_sample=None,
            fake_tuple_name="nominal_Loose",
            data_fake_weight="(weight_mm_p[9]*(Length$(mu_pt)==0)+(Length$(mu_pt)==1)*weight_mm_p[16])",
            data_fake_var_weight=[
                "(weight_mm_p[4]*(Length$(mu_pt)==0)+(Length$(mu_pt)==1)*weight_mm_p[4])",
                "(weight_mm_p[6]*(Length$(mu_pt)==0)+(Length$(mu_pt)==1)*weight_mm_p[6])",
                ]
        ):


        self.signal_sample = signal_sample
        self.signal_fastsim_sample = signal_fastsim_sample
        self.input_mc_tuple,          = signal_sample.input_file_names,
        self.input_data_tuple,        = input_data_tuple,
        self.output_folder,           = output_folder,
        self.truth_level_variable     = truth_level_variable
        self.detector_level_variable  = detector_level_variable
        self.method                   = method
        self.truth_tuple_name         = truth_tuple_name
        self.detector_tuple_name      = detector_tuple_name
        self.data_tuple_is_mc         = ( self.input_mc_tuple == self.input_data_tuple )
        self.save_output              = save_output
        self.verbosity                = verbosity
        self.x_axis_binning           = x_axis_binning
        self.luminosity               = luminosity
        self.name                     = name
        self.n_iterations             = n_iterations
        self.set_log_y                = set_log_y
        self.background_samples       = background_samples
        self.np_background_samples    = np_background_samples
        self.signal_mc_name           = signal_mc_name
        self.total_cov                = None
        self.run_afii =  signal_fastsim_sample is not None
        self.input_afii_tuple = signal_fastsim_sample.input_file_names if self.run_afii else None
        self.last_time = time()
        self.start_time = time()
        self.chi2 = None
        self.fake_tuple_name = fake_tuple_name
        self.data_fake_weight = data_fake_weight
        self.data_fake_var_weight = data_fake_var_weight
        # Tests that will be ran when run_tests is called, e.g IdentityTest
        # where the MC used to setup the unfolder is unfolded.
        # See TopAnalysis/Unfolding/Tests.py
        self.tests                    = []

        # Whether to generate toys for the bootstrapping
        self.do_bootstrap = do_bootstrap

        #
        self.double_ratio_sample_names = None
        # Weights used in the TTree::Draw commands, note these must be different in order to
        # to account for scale factors at reco level thatdo not make sense to apply at particle level
        if truth_weight is None:
            truth_weight="weight_mc"
        if detector_weight is None:
            detector_weight="weight_mc*weight_leptonSF*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup"

        self.truth_weight             = truth_weight
        self.detector_weight          = detector_weight + "*(" + prompt_weight + ")"
        self.data_weight              = self.detector_weight if self.data_tuple_is_mc else "1.0"
        self.prompt_weight            = prompt_weight 
        self.nonprompt_weight         = nonprompt_weight
        #Normalize the MC using the x_sec. etc
        self.x_axis_label = x_axis_label
        self.y_axis_label = y_axis_label

        # Messages are the additional information display on a plot
        # Underneat the ATLAS labels, see DrawingHelperFunctions for more details
        self.messages = messages
        self.unfolded_x_axis_title = unfolded_x_axis_title
        self.unfolded_y_axis_title = unfolded_y_axis_title

        #objects to handle systematic variations
        self.systematics = systematics
        self.normalize = False

        # 
        self.systematic_histograms = OrderedDict()
        self.systematic_size_histograms = OrderedDict()
        self.reco_plots = OrderedDict()

        self.detector_cut_weight = ""
        for cut in detector_cuts:
            self.detector_cut_weight += "*(" + cut + ")"
        #
        self.truth_cut_weight = ""
        for cut in truth_cuts:
            self.truth_cut_weight += "*(" + cut + ")"

        self.truth_cuts = truth_cuts
        self.detector_cuts = detector_cuts
        self.additional_samples     = OrderedDict()
        self.additional_sample_hist = OrderedDict()
        self.ratio_plot_name =  None

        if additional_samples is not None:
            self.additional_samples=additional_samples 
        self.bootstrapped = 1000

        self.event_yield = {}

        # if we're cluttering up the terminal with a lot of output, display a welcome message
        # and the settings the user has chosen
        if self.verbosity > 0:
            print("==================================================")
            print("                 TopAnalysis: Unfolder            ")
            print("==================================================")

            # Count how many samples 
            n_signal_mc = 0 
            for tuple_list in self.input_mc_tuple:
                n_signal_mc += len(tuple_list)

            # 
            n_afii = 0 
            if self.run_afii:
                for tuple_list in self.input_afii_tuple:
                    n_afii += len(tuple_list)

            #
            n_data = 0 
            for tuple_list in self.input_data_tuple:
                n_data += len(tuple_list)

            if len(self.input_mc_tuple[0]) > 10:
                print("input_mc_tuple ("+str(n_signal_mc)+"): "+ str(self.input_mc_tuple[0][0]) + "... \n")
            else:
                print("input_mc_tuple ("+str(n_signal_mc)+"): " + str(self.input_mc_tuple) + "\n")

            if len(self.input_data_tuple[0]) > 10:
                print("input_data_tuple ("+str(n_data)+"): " + str(self.input_data_tuple[0][0])+ "..."+ "\n")
            else:
                print("input_data_tuple ("+str(n_data)+"): " + str(self.input_data_tuple)+ "\n")

            if self.run_afii:
                print("AFII Sample ("+str(n_afii)+"): "  + str(self.input_afii_tuple[0][0])+ "\n")

            print("output_folder: " + str(self.output_folder) + "\n")
            print("truth_level_variable: " + str(self.truth_level_variable)+ "\n" )
            print("detector_level_variable: " + str(self.detector_level_variable)+ "\n" )
            print("truth weight:" +str(self.truth_weight)+ "\n")
            print("detector weight:" +str(self.detector_weight)+ "\n")
            print("Name:" + self.name)
            print("==================================================")

        self.construct_name()
        self.output_folder += "/" + self.name +"/"
        rhf.create_folder(self.output_folder)

        if load_unfolder is None:
            self.begin_saving(self.name)
            #evaluate the effective luminosity of the MC samples being used
            # TODO: Generalize to accoutn for background samples
            self.evaluate_nom_mc_luminiosity()
        else:
            self.load_saved_unfolder(self.output_folder+self.name+".root")
            self.mc_luminosity_nom = 1.0
        if self.verbosity > 1:
            print("--------------------------------------------------")


        # Dictionary to store systmeatics
        self.systematic_variation_sizes = OrderedDict()


        rhf.hide_root_infomessages()

    def create_combined_grouped_systs(self, combinations):
        """
        
        """
        # Create a dictionary for each combaintion thhat has overflow also
        syst_combo_sorted = {}
        max_per_plot = 8
        for combination in combinations:
            overflow = 0
            name = combination
            syst_combo_sorted[name] = OrderedDict()


            plot_counter = 0
            self.log('Creating combination for plotting: ', combination)

            for syst_name in self.systematics: 
                if not self.systematics[syst_name].combination in combination:
                    continue 

                self.log('      Adding syst', syst_name, ' to ', name )
                syst_combo_sorted[name][syst_name] = self.systematics[syst_name]

                # If we've exceed the amount we want to shove in this combination 
                # then overflow 
                if len(syst_combo_sorted[name]) > max_per_plot:
                    # if there is 
                    if 'up' in syst_name.lower():
                        if syst_name.lower().replace('up','down') in [k.lower() for k in syst_combo_sorted[name].keys()]:
                            self.log('Satisified ', syst_name, ' has  down variation in this plot.')
                            name = combination + "_overflow_"+str(overflow)
                            syst_combo_sorted[name] = OrderedDict()
                            overflow+=1 
                            self.log('Moving to overflow: ', name )

                    elif 'down' in syst_name.lower() :
                        if syst_name.lower().replace('down', 'up') in [k.lower() for k in syst_combo_sorted[name].keys()]:
                            self.log('      Satisified ', syst_name, ' has  up variation in this plot.')
                            name = combination + "_overflow_"+str(overflow)
                            syst_combo_sorted[name] = OrderedDict()
                            overflow+=1 
                            self.log('Moving to overflow: ', name )
                    else:
                        name = combination + "_overflow_"+str(overflow)
                        syst_combo_sorted[name] = OrderedDict()
                        overflow+=1 
                        self.log('Moving to overflow: ', name )
        return syst_combo_sorted

    def create_grouped_systematic_plot_2(self,systematics_folder, combinations):
        """
        Combine systematics of the same type in quadrature to produce a neat plot
        that shows the total systematic uncertainties size. 
        """
        self.reduced_syst_plots = OrderedDict()
        self.combined_uncertainty_up = OrderedDict()
        self.combined_uncertainty_down = OrderedDict()
        
        # Not a particularly elegant way of doing the plotting
        # but start by shoving everything we want on in reverse order
        self.reduced_syst_plots["Syst. #otimes Stat."] = [
             self.total_stat_syst_up,
             dhf.get_stat_syst_stlye_opt(),
             dhf.get_stat_syst_stlye_opt(is_ratio = True),
        ]
        self.reduced_syst_plots["Stat Syst Down"] = [
             self.total_stat_syst_down,
             dhf.get_stat_syst_stlye_opt(show_legend = False),
             dhf.get_stat_syst_stlye_opt(is_ratio =True,show_legend = False),
        ]

        #add the systematic affer
        self.reduced_syst_plots["Stat."] = [
             self.statistical_uncertainty_hist_upscaled,
             dhf.get_uncert_stlye_opt(),
             dhf.get_uncert_stlye_opt(is_ratio = True),
        ]
        self.reduced_syst_plots["Stat. Down"] = [
             self.statistical_uncertainty_hist_down_upscaled,
             dhf.get_uncert_stlye_opt(show_legend = False),
             dhf.get_uncert_stlye_opt(is_ratio =True,show_legend = False),
        ]
        self.combination_plots = {}

        # #
        syst_count = 0 
        grouped_plots = self.create_combined_grouped_systs(combinations)
        for combination in grouped_plots: 

            # For each combination construct the total histogram 
            uncertainties_for_combination_down,  uncertainties_for_combination_up = [], []
            name= combination
            # For each combination we want a breakdown so we can see which component contributes signficantly to 
            # it 
            self.combination_plots[combination] = OrderedDict()
          
            self.combination_plots[combination]["MC Stat."] = [
                 self.mc_statistical_uncertainty_hist_upscaled,
                 dhf.get_uncert_stlye_opt(),
                 dhf.get_uncert_stlye_opt(is_ratio = True),
            ]
            self.combination_plots[combination]["MC Stat. Down"] = [
                 self.mc_statistical_uncertainty_hist_down_upscaled,
                 dhf.get_uncert_stlye_opt(show_legend = False),
                 dhf.get_uncert_stlye_opt(is_ratio =True,show_legend = False),
            ]

            for combination_index, syst_name in enumerate(grouped_plots[combination]):

                # Grab the upward and downard contributions due to this systematic 
                if grouped_plots[name][syst_name].total_down_variation is None:
                    continue 
                # 
                uncertainties_for_combination_down.append(grouped_plots[name][syst_name].total_down_variation.Clone())
                uncertainties_for_combination_down[-1].SetDirectory(0)
                uncertainties_for_combination_down[-1].Scale(1.0/100.0)
                uncertainties_for_combination_up.append(grouped_plots[name][syst_name].total_up_variation.Clone())
                uncertainties_for_combination_up[-1].SetDirectory(0)
                uncertainties_for_combination_up[-1].Scale(1.0/100.0)

                if grouped_plots[name][syst_name].information[0]['direction'] == 'asymm':
                    self.combination_plots[name][syst_name] =  [ grouped_plots[name][syst_name].total_up_variation, 
                                    dhf.get_truth_large_legend_stlye_opt(combination_index,   False) ]
                    self.combination_plots[name][syst_name+"_down"] =  [ grouped_plots[name][syst_name].total_down_variation, 
                                    dhf.get_truth_large_legend_stlye_opt(combination_index,   False, False) ]
                    combination_index+=1
                else:   
                     self.combination_plots[name][syst_name] =  [
                        rhf.get_largest_abs_bin_content_hist(
                                [grouped_plots[name][syst_name].total_up_variation,
                                 grouped_plots[name][syst_name].total_down_variation
                                ]
                            ),
                        dhf.get_truth_large_legend_stlye_opt(combination_index,   False) 
                        ]


            # Combine the select systematics into one total 
            if len(uncertainties_for_combination_down) != 0:
                self.combined_uncertainty_up[combination]   = rhf.combine_bincenters_in_quadrature(uncertainties_for_combination_up)
                self.combined_uncertainty_down[combination] = rhf.combine_bincenters_in_quadrature(uncertainties_for_combination_down)
                self.combined_uncertainty_up[combination].Scale(100.0)  
                self.combined_uncertainty_down[combination].Scale(-100.0)
                # Store them for plotting 
                self.reduced_syst_plots[combination] = [
                     self.combined_uncertainty_up[combination],
                     dhf.get_truth_large_legend_stlye_opt(syst_count,False),
                     dhf.get_truth_large_legend_stlye_opt(syst_count,True)
                    ]
                self.reduced_syst_plots[combination+"down"] = [
                     self.combined_uncertainty_down[combination],
                     dhf.get_truth_large_legend_stlye_opt(syst_count,False,show_legend = False),
                     dhf.get_truth_large_legend_stlye_opt(syst_count,True,show_legend = False)
                    ]
                syst_count += 2

            else:
                self.combined_uncertainty_up[combination]   = rhf.clear_histogram(self.corr_unfolded_mc)
                self.combined_uncertainty_down[combination] = rhf.clear_histogram(self.corr_unfolded_mc)
               
          


    def create_grouped_systematic_plot(self,systematics_folder, combinations):
        """
        Combine systematics of the same type in quadrature to produce a neat plot
        that shows the total systematic uncertainties size. 
        """
        self.reduced_syst_plots = OrderedDict()
        self.combined_uncertainty_up = OrderedDict()
        self.combined_uncertainty_down = OrderedDict()
        
        # Not a particularly elegant way of doing the plotting
        # but start by shoving everything we want on in reverse order
        self.reduced_syst_plots["Syst. #otimes Stat."] = [
             self.total_stat_syst_up,
             dhf.get_stat_syst_stlye_opt(),
             dhf.get_stat_syst_stlye_opt(is_ratio = True),
        ]
        self.reduced_syst_plots["Stat Syst Down"] = [
             self.total_stat_syst_down,
             dhf.get_stat_syst_stlye_opt(show_legend = False),
             dhf.get_stat_syst_stlye_opt(is_ratio =True,show_legend = False),
        ]

        #add the systematic affer
        self.reduced_syst_plots["Stat."] = [
             self.statistical_uncertainty_hist_upscaled,
             dhf.get_uncert_stlye_opt(),
             dhf.get_uncert_stlye_opt(is_ratio = True),
        ]
        self.reduced_syst_plots["Stat. Down"] = [
             self.statistical_uncertainty_hist_down_upscaled,
             dhf.get_uncert_stlye_opt(show_legend = False),
             dhf.get_uncert_stlye_opt(is_ratio =True,show_legend = False),
        ]
        self.combination_plots = {}

        # #
        syst_count = 0 

        for combination in combinations: 
            # For each combination construct the total histogram 
            uncertainties_for_combination_down,  uncertainties_for_combination_up = [], []

            # For each combination we want a breakdown so we can see which component contributes signficantly to 
            # it 
            self.combination_plots[combination] = OrderedDict()
            combination_index = 0 
            overflow_n = 0 
            combination_dict_name = combination 

            for syst_name in self.systematics:
                # Grab the upward and downard contributions due to this systematic 
                if self.systematics[syst_name].combination in combination: 
                    if self.systematics[syst_name].total_down_variation is None:
                        continue 
                    # 
                    uncertainties_for_combination_down.append(self.systematics[syst_name].total_down_variation.Clone())
                    uncertainties_for_combination_down[-1].SetDirectory(0)
                    uncertainties_for_combination_down[-1].Scale(1.0/100.0)
                    uncertainties_for_combination_up.append(self.systematics[syst_name].total_up_variation.Clone())
                    uncertainties_for_combination_up[-1].SetDirectory(0)
                    uncertainties_for_combination_up[-1].Scale(1.0/100.0)

                    if len(self.combination_plots[combination_dict_name]) >= 8*2:
                        combination_dict_name = combination+"_overflow_"+str(overflow_n)
                        self.combination_plots[combination_dict_name] = OrderedDict()
                        combination_index = 0 
                        overflow_n += 1 

                    self.combination_plots[combination_dict_name][syst_name] =  [ self.systematics[syst_name].total_up_variation, 
                                    dhf.get_truth_large_legend_stlye_opt(combination_index,   False) ]
                    self.combination_plots[combination_dict_name][syst_name+"_down"] =  [ self.systematics[syst_name].total_down_variation, 
                                    dhf.get_truth_large_legend_stlye_opt(combination_index,   False, False) ]
                    combination_index += 1
            # Combine the select systematics into one total 
            if len(uncertainties_for_combination_down) != 0:
                self.combined_uncertainty_up[combination]   = rhf.combine_bincenters_in_quadrature(uncertainties_for_combination_up)
                self.combined_uncertainty_down[combination] = rhf.combine_bincenters_in_quadrature(uncertainties_for_combination_down)
                self.combined_uncertainty_up[combination].Scale(100.0)  
                self.combined_uncertainty_down[combination].Scale(-100.0)
            else:
                self.combined_uncertainty_up[combination]   = rhf.clear_histogram(self.corr_unfolded_mc)
                self.combined_uncertainty_down[combination] = rhf.clear_histogram(self.corr_unfolded_mc)
               
            # Store them for plotting 
            self.reduced_syst_plots[combination] = [
                 self.combined_uncertainty_up[combination],
                 dhf.get_truth_large_legend_stlye_opt(syst_count,False),
                 dhf.get_truth_large_legend_stlye_opt(syst_count,True)
                ]
            self.reduced_syst_plots[combination+"down"] = [
                 self.combined_uncertainty_down[combination],
                 dhf.get_truth_large_legend_stlye_opt(syst_count,False,show_legend = False),
                 dhf.get_truth_large_legend_stlye_opt(syst_count,True,show_legend = False)
                ]
            syst_count += 2

    def construct_afii(self):
        """"
        Create all the requieste histograms for unfolding for the additional AFII samples 
        """
        self.log("Constructing AFII truth, detector and migration histograms")

        # create the migrationion matrix, for the same reason as above
        self.migration_matrix_afii = rhf.get_2d_histogram_from_sample(
                                     self.signal_fastsim_sample,
                                     self.detector_tuple_name,
                                     self.truth_level_variable+":"+self.detector_level_variable,
                                     self.x_axis_binning,
                                     self.x_axis_binning,   #We want symmetric matrices
                                     self.detector_weight+self.detector_cut_weight+self.truth_cut_weight, #we're going to manage efficiency and acceptance corrections ourselves
                                     friend_name     = self.truth_tuple_name,
                                     index_variable  = "eventNumber",
                                     hist_name="afii_response_matrix_"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable),
                                     scale_to_lumi=True,
                                     lumi=self.luminosity
                                     )

        self.log( "Created AFII migration matrix with NEntries: ", self.migration_matrix_afii.GetEntries() )
        # We choose to projec the migration matrix, M, in X and Y to get the measured and truth
        # histograms respectively
        # In effect we are preventing RooUnfold from calculating the efficiency,e, and acceptance,a
        # i.e Unfolded = [aMe] Data
        self.truth_hist_afii = self.migration_matrix_afii.ProjectionY().Clone()
        self.truth_hist_afii.SetDirectory(0)
        self.save_object(self.truth_hist_afii, "mc_afii_truth_passTruthAndDetector_" + shf.clean_string(self.truth_level_variable))

        #similarly for measured histogram
        self.detector_mc_hist_afii  = self.migration_matrix_afii.ProjectionX().Clone()
        self.detector_mc_hist_afii.SetDirectory(0)
        self.save_object(self.detector_mc_hist_afii, "mc_afii_detector_passTruthAndDetector_" + shf.clean_string(self.detector_level_variable))

        # The matrix needs to be normalized such that a row adds up to unity
        self.migration_matrix_drawing_afii = rhf.normalize_migration_matrix(self.migration_matrix_afii.Clone(self.migration_matrix_afii.GetName() + "_drawing_clone"))
        self.migration_matrix_drawing_afii.GetXaxis().SetTitle(self.x_axis_label)
        self.migration_matrix_drawing_afii.GetYaxis().SetTitle(self.y_axis_label)
        self.save_object(self.migration_matrix_afii)
        self.save_object(self.migration_matrix_drawing_afii)
        r.SetOwnership( self.migration_matrix_drawing_afii,0 )

        #
        self.construct_corrections_afii()



    def draw_sys_combination_breakdowns(self):
        """
        Systematics can be ran individually but ultimately plotted as a combination, as defined in the systematics manager

        Here wecan collate all components of one 

        """
        rhf.create_folder(self.output_folder+"systematic_combination_breakdowns/")
        AS.SetAtlasStyle()
        canvas = r.TCanvas(self.name + "_combation_breakdowns",
                           self.name + "_combation_breakdowns",
                           800,600) 
        r.SetOwnership(canvas, 0)
        AS.SetAtlasStyle()

        for combination in self.combination_plots:
            if len(self.combination_plots[combination]) <= 0:
                continue 

            dhf.plot_histogram(canvas,
                   self.combination_plots[combination], 
                   x_axis_title=self.x_axis_label,
                   y_axis_title="Fractional Uncertainty [%]",
                   do_atlas_labels = True,
                   labels = ["Fiducial Phase Space "]
                   )

            canvas.Print(self.output_folder+"systematic_combination_breakdowns/"+combination+"_syst_breakdown.eps")
            canvas.Print(self.output_folder+"systematic_combination_breakdowns/"+combination+"_syst_breakdown.png")


    def construct_latex_table(self):

        latex_table_head,latex_table_footer = "", ""
        table_size = "| l || " + ''.join(["c |" for x in self.x_axis_binning[:-1]] )
        headers = "\\multirow{2}{*}{Uncertainty source} & \\multicolumn{ "+str(len(self.x_axis_binning[:-1]))+ "}{c|}{ Uncertainty Size [$\\%$] }"
        headers += "\\\\\n" +''.join(["& %.2f $\\leq A_{\\theta} <$%.2f "%( self.x_axis_binning[i], self.x_axis_binning[i+1]) for i in xrange(len(self.x_axis_binning)-1)]) 
        latex_table_head = "\\begin{sidewaystable}\n\
                                \\centering \n\
                                \\resizebox{\\columnwidth}{!}{% \
                                \\begin{tabular}{"+table_size+"}\n\
                                    \\hline\n\
                                    "+headers+ " \\\\ \\hline \n"
        latex_table_footer = "\\hline \n \
                \\end{tabular}\n \
                }\
                \\caption{systematic table}\n \
                \\label{tab:}\n \
            \\end{sidewaystable}\n \
            "
        return latex_table_head,latex_table_footer


    def evaluate_statistical_cov(self):
        """
        Evaluate the covariance matrix for the statistical component of the uncertaintiy 
        """
        uncer_totals = []
        truth_normed = rhf.normalize_histogram(self.truth_all_hist.Clone())
        if self.do_bootstrap:
            for i in xrange(self.bootstrapped):
                uncerts = []

                for j in xrange(1,self.nominal_unfolded_bs_hists[i].GetNbinsX()+1 ):
                    uncerts.append(self.nominal_unfolded_bs_hists[i].GetBinContent(j)-truth_normed.GetBinContent(j))
                # 
                uncerts = np.array(uncerts)
                uncer_totals.append(uncerts)

            corr_stat  = np.corrcoef(np.stack(tuple(uncer_totals)).T)
            self.stat_cov = rhf.calculate_covariance_matrix(np.std(uncer_totals,axis=0))#, corr_stat)
            self.stat_cov_in  = np.linalg.inv(self.stat_cov)
            self.log('stat cov: \n',self.stat_cov)
            self.log('')
            self.log(uncerts)
            self.log('STat from cov:', np.sqrt( self.stat_cov .diagonal() ))
        else:
            for j in xrange(1,self.statistical_uncertainty_hist.GetNbinsX()+1 ):
                uncer_totals.append(self.statistical_uncertainty_hist.GetBinContent(j)*truth_normed.GetBinContent(j))

            self.stat_cov = rhf.calculate_covariance_matrix(uncer_totals)#, corr_stat)
            self.stat_cov_in  = np.linalg.inv(self.stat_cov)
        return self.stat_cov 

    def load_systematics(self, systematic_folder):
        """
        Read in all the required systematics and reconstruct the total systematic 
        """

        table_content = {}
        self.total_cov = self.evaluate_statistical_cov()

        # closure_hist.Scale(1.0/100.0)
        for i, syst_name in enumerate(self.systematics):
            # Need to read the file and grab the total up systematic 
            self.systematics[syst_name].verbosity = self.verbosity
            if not self.systematics[syst_name].load(systematic_folder):
                self.log("WARNING RERUN SYST:", i)
                continue 

            # Correct for a systematic non-closure in all systeamtics
            # Necessary for certainty unfolders as the intiial ran meant syst
            # -ematics did not have an efficiency correction applied. 
            # Just a quick fix as I don't want ot have rerun everything! 
            if self.name == "hadronic_control_region":
                self.systematics["closure"].load(systematic_folder)
                closure_hist = self.systematics["closure"].total_up_variation.Clone()

                if self.systematics[syst_name].is_afii:
                    continue 

                for hist_name in self.systematics[syst_name].systematic_size_hists:
                    bin_contents =  [
                            self.systematics[syst_name].systematic_size_hists[hist_name].GetBinContent(i)
                            for i in xrange(1,self.systematics[syst_name].systematic_size_hists[hist_name].GetNbinsX()+1)]
                    closure_bin_contents =  [
                            closure_hist.GetBinContent(i)
                            for i in xrange(1,self.systematics["closure"].total_up_variation.GetNbinsX()+1)]
                    self.log("non-closure terms: ", closure_bin_contents)
                    self.log("Before fixing bug in non-closure terms: ", bin_contents)

                    # self.systematics[syst_name].systematic_size_hists[hist_name].Divide(closure_hist)
                    # self.systematics[syst_name].systematic_size_hists[hist_name].Scale(100.0)

                    for i in xrange(1,self.systematics[syst_name].systematic_size_hists[hist_name].GetNbinsX()+1):
                        old = self.systematics[syst_name].systematic_size_hists[hist_name].GetBinContent(i)
                        self.systematics[syst_name].systematic_size_hists[hist_name].SetBinContent(i,
                                np.sign( old)*( abs(closure_bin_contents[i-1]) - abs(old))
                            )
                    bin_contents =  [
                            self.systematics[syst_name].systematic_size_hists[hist_name].GetBinContent(i)
                            for i in xrange(1,self.systematics[syst_name].systematic_size_hists[hist_name].GetNbinsX()+1)]
                    self.log("After Fixing bug in non-closure terms: ", bin_contents)
                self.systematics[syst_name].combine_varaitions()

            try:
                table_content["all"] += self.systematics[syst_name].get_latex_string() + "\n"
            except KeyError:
                table_content["all"] = ""
                table_content["all"] += self.systematics[syst_name].get_latex_string() + "\n"

            if self.total_cov is None:
                self.total_cov += self.systematics[syst_name].total_cov
                # self.systematics[syst_name].draw_correlations(self.output_folder+"systematics/", self.unfolded_x_axis_title )
            else:
                try:
                    self.total_cov += self.systematics[syst_name].total_cov
                    # self.systematics[syst_name].draw_correlations(self.output_folder+"systematics/", self.unfolded_x_axis_title )
                except TypeError:
                    print "WARNING: Issue with covariance: ",syst_name
                    print "\n\n\n\n"
                except np.linalg.linalg.LinAlgError:
                    print "ERROR: Singular matrix.", syst_name
                    print self.systematics[syst_name].total_cov

        self.total_cov = self.evaluate_statistical_cov()

        # 
        self.evaluate_total_systematic()
        for comb in table_content:
            self.write_table(table_content[comb], comb)
        self.draw_correlations()

    def draw_correlations(self):
        self.log("Creating the correlation_matrix...")
        stat_cov_matrix = r.TH2D("statistical correlation_matrix", ";"+self.unfolded_x_axis_title+";"+self.unfolded_x_axis_title, 
                                 len(self.x_axis_binning)-1, array.array('d',self.x_axis_binning),
                                 len(self.x_axis_binning)-1, array.array('d',self.x_axis_binning)
                                 )

        cov_matrix = r.TH2D("correlation_matrix", ";"+self.unfolded_x_axis_title+";"+self.unfolded_x_axis_title, 
                                 len(self.x_axis_binning)-1, array.array('d',self.x_axis_binning),
                                 len(self.x_axis_binning)-1, array.array('d',self.x_axis_binning)
                                 )
        for j in range(1,cov_matrix.GetYaxis().GetNbins()+1):
            for i in range(1,cov_matrix.GetXaxis().GetNbins()+1):
                cov_matrix.SetBinContent(i,j, self.total_cov[i-1,j-1])
                stat_cov_matrix.SetBinContent(i,j, self.stat_cov[i-1,j-1])

        cov_matrix = rhf.normalize_correlation_matrix(cov_matrix)
        stat_cov_matrix = rhf.normalize_correlation_matrix(stat_cov_matrix)

        self.log("Drawing the correlation matrix ..:"  )
        canvas = r.TCanvas(self.name + "correlation_matrix_canvas",
                           self.name + "correlation_matrix_canvas",
                           800,
                           600)
        r.SetOwnership(canvas, 0)
        dhf.draw_correlation_matrix(cov_matrix, canvas)
        canvas.Print(self.output_folder+"correlation_matrix_test.eps")
        canvas.Print(self.output_folder+"correlation_matrix_test.png")
        dhf.draw_correlation_matrix(stat_cov_matrix, canvas)
        canvas.Print(self.output_folder+"stat_correlation_matrix_test.eps")
        canvas.Print(self.output_folder+"stat_correlation_matrix_test.png")
        self.log("Done."   )

    def write_table(self, table_content, name):
        latex_table_head,latex_table_footer = self.construct_latex_table()

        tex_file = open(self.output_folder + 'systematics_'+name.replace("#","").replace(" ", "_")+'.tex', 'w')
        tex_file.write(latex_table_head)
        tex_file.write(table_content)

        tex_file.write(latex_table_footer)
        tex_file.close()
        self.log('Saving tex table ', name, '.')
        self.log('\n', table_content)

    def evaluate_chi2(self, ):
        """
        Construct the chi2 metric and associated p-values based on the 
        the correlations evaluated using a bootstrap procedure OR
        (if that hasn't been run) assuming the correlations between bins
        is diag(1). 

        A quick word word types of systematic: 
        """
        systematic_folder = self.output_folder + "systematics/"
        normed_truth_nom = rhf.normalize_histogram(self.truth_all_hist)
        # Load the covariance matrix if it has not been done so already
        self.total_cov = None
        if self.total_cov is None:
            self.log('evaluating the stat cov.')
            syst_uncert =  [(normed_truth_nom.GetBinContent(i+1)*self.total_stat_syst_up.GetBinContent(i+1)/100.0 )**2 for i in range(self.statistical_uncertainty_hist.GetNbinsX())]
            relative_syst_uncert =  [ self.total_stat_syst_up.GetBinContent(i+1)/100.0  for i in range(self.statistical_uncertainty_hist.GetNbinsX())]
            self.log('Relative stat+syst uncert: ', relative_syst_uncert )
            self.total_cov = np.diag(syst_uncert) 

            if self.do_bootstrap:
                stat_uncert =  [self.statistical_uncertainty_hist.GetBinContent(i+1)**2 for i in range(self.statistical_uncertainty_hist.GetNbinsX())]
                self.total_cov = np.diag(stat_uncert) 
                for syst_name in self.systematics:
                    if self.systematics[syst_name].load(systematic_folder):
                        syst_uncert =  [ (self.systematics[syst_name].total_up_variation.GetBinContent(i+1)/100.0)**2 for i in range(self.systematics[syst_name].total_up_variation.GetNbinsX())]
                        self.log(syst_name,' uncert: ', syst_uncert)
                        self.total_cov += np.diag(syst_uncert)

        M_nom,D = [], []
        M_samples = {}
        for sample in self.additional_samples:
            M_samples[sample] = []

        for i in xrange(1,self.corr_unfolded_data.GetNbinsX()+1):
            for sample_name in self.additional_samples:
                M_samples[sample_name].append(self.additional_sample_hist[sample_name].GetBinContent(i))
            D.append(rhf.normalize_histogram(self.corr_unfolded_data).GetBinContent(i))
            M_nom.append(rhf.normalize_histogram(self.truth_all_hist).GetBinContent(i))

        #  Create the numpy objects
        n_blinded = 1 
        M_nom, D = np.array(M_nom),np.array(D)
        D_r, M_nom_r = np.delete(D, n_blinded),  np.delete(M_nom, n_blinded)
        # D_r, M_nom_r = D, M_nom  

        M_r = {}
        for sample in self.additional_samples:
            M_r[sample] = np.delete(np.array( M_samples[sample]), n_blinded)
            # M_r[sample] = M_samples[sample]

        cov_reduced = (self.total_cov)
        cov_reduced = np.linalg.inv(self.total_cov)
        # Blind one of the Columns 
        cov_reduced = np.delete(cov_reduced, n_blinded, 1)
        cov_reduced = np.delete(cov_reduced, n_blinded, 0)

        # Evlauate the chi2, reduced chi2 ad p-vlaues
        chi2, chi2_red, p_values, z_scores = {}, {}, {}, {}
        chi2['closure'] = np.dot( (D_r-D_r).T, np.dot(cov_reduced, (D_r-D_r)))
        chi2['nominal'] = np.dot( (D_r-M_nom_r).T, np.dot(cov_reduced, (D_r-M_nom_r)))
        for sample in self.additional_samples:
            chi2[sample] = np.dot( (D_r-M_r[sample]).T, np.dot(cov_reduced, (D_r-M_r[sample])))

        # Now re-evaluate the p-vlues to give P( DC off | DC on) 
        D,M_nom = [],[]
        for i in xrange(1,self.corr_unfolded_data.GetNbinsX()+1):
            D.append(rhf.normalize_histogram(self.additional_sample_hist["Pythia8 DC off"]).GetBinContent(i))#/normed_truth_nom.GetBinContent(i))
            M_nom.append(rhf.normalize_histogram(self.additional_sample_hist["Pythia8 DC on"]).GetBinContent(i))#/normed_truth_nom.GetBinContent(i))
        D_r, M_nom_r = np.array(D), np.array(M_nom )
        D_r, M_nom_r = np.delete(D, n_blinded),  np.delete(M_nom, n_blinded)

        chi2['DCon'] = np.dot( (D_r-M_nom_r).T, np.dot(cov_reduced, (D_r-M_nom_r)))

        # Evaluate the p-value of having absovered this hypthoses
        # p-value = Prob(Chi2 > Chi2Data | H0)

        n_dof = len(D_r) #self.corr_unfolded_data.GetNbinsX()-1
        import scipy.stats as stats 
        stats.chisqprob = lambda chisq, df: stats.chi2.sf(chisq, df)
        for sample in chi2:
            chi2_red[sample] = chi2[sample]/n_dof
            p_values[sample] = stats.chisqprob(chi2[sample],n_dof)
            z_scores[sample] = -stats.norm.ppf(p_values[sample]/2.0)

        self.chi2 = chi2
        self.chi2_red = chi2_red
        self.p_val = p_values
        self.z_scores = z_scores
        self.messages += ["p_{0}^{DC off} = %.6g"%p_values["DCon"]]
        self.log( 'chi2: \n                  ', chi2) 
        self.log( 'chi2 Reduced:\n                  ', chi2_red)
        self.log( 'p_vales: \n                   ', p_values)
        self.log( 'z_scores: \n                   ', z_scores)
        chi2_file = open(self.output_folder + 'chi2_vals.tex', 'w')
        for name in chi2:
            chi2_file.write(name + " & %.5g & %.5g & %.5g \\\\\n"%(chi2[name],chi2_red[name], p_values[name]) )
        chi2_file.close()

    def load_saved_unfolder(self, unfolder_root_file):
        """
        Load a previously ran unfolder. 
        """
        # Get the core migration matrix
        self.migration_matrix = rhf.retrive_hist(unfolder_root_file,  "response_matrix;1")
        # proejctions along X and Y
        self.truth_hist = rhf.retrive_hist(unfolder_root_file,  "mc_truth_passTruthAndDetector_" + shf.clean_string(self.truth_level_variable))
        self.detector_mc_hist = rhf.retrive_hist(unfolder_root_file,  "mc_detector_passTruthAndDetector_" + shf.clean_string(self.detector_level_variable))

        # Formatted for drawing
        self.migration_matrix_drawing = rhf.retrive_hist(unfolder_root_file,  "response_matrix_"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable)+"_drawing_clone")

        # The data distribution 
        self.data_hist = rhf.retrive_hist(unfolder_root_file,  "data_detectorLevel_" +
                         shf.clean_string(self.detector_level_variable))

        # Load the corrections 
        self.reco_all_hist = rhf.retrive_hist(unfolder_root_file,  self.truth_tuple_name + "_reco_all" + shf.clean_string(self.detector_level_variable))
        self.truth_all_hist = rhf.retrive_hist(unfolder_root_file,  self.truth_tuple_name + "_truth_all" + shf.clean_string(self.detector_level_variable))
        self.reco_and_truth_reco_binning_hist = rhf.retrive_hist(unfolder_root_file,  self.truth_tuple_name + "reco_and_truth_reco_binning_hist" + shf.clean_string(self.detector_level_variable))
        self.reco_and_truth_truth_binning_hist = rhf.retrive_hist(unfolder_root_file,  self.truth_tuple_name + "reco_and_truth_truth_binning_hist" + shf.clean_string(self.detector_level_variable))
        self.acceptance_plot = rhf.retrive_hist(unfolder_root_file,   self.truth_tuple_name + "_acceptance" + shf.clean_string(self.detector_level_variable))
        self.efficiency_plot = rhf.retrive_hist(unfolder_root_file,   self.truth_tuple_name + "_efficiency" + shf.clean_string(self.detector_level_variable))

        # Load the background and reco plots        
        self.mc_stack = rhf.retrive_hist(unfolder_root_file,  "mc_stack")
        self.total_backgrounds = rhf.retrive_hist(unfolder_root_file,  "total_bkg")
        self.total_mc = rhf.retrive_hist(unfolder_root_file,  "total_mc")
        self.bkg_histograms = {}

        histKeys = self.mc_stack.GetHists()
        next_hist = r.TIter(histKeys)
        hist = next_hist()
        while hist:
            self.bkg_histograms[hist.GetName()] = hist
            hist = next_hist()

        # Load the additional plots 
        for sample_name in self.additional_samples:
            self.additional_sample_hist[sample_name] = rhf.retrive_hist(unfolder_root_file,  
                                    self.name+"_truth_var_"+sample_name)

        # Construct the plotting dictionaries 
        data_label = "Pseudo-data" if self.data_tuple_is_mc else "Data"
        data_hist  = self.mc_stack.GetStack().Last().Clone() if self.data_tuple_is_mc else self.data_hist

        data_hist.SetMinimum(self.mc_stack.GetStack().First().GetMinimum())
        self.mc_stack.GetStack().Last().SetMinimum(self.mc_stack.GetStack().First().GetMinimum())

        # Dumb stlye opts

        # Format and draw the histograms neatly, showing the ratio w.r.t MC
        mc_ratio_style_options = dhf.mc_ratio_style_options
        mc_ratio_style_options.x_title_offset  = 1.1
        mc_ratio_style_options.x_axis_label_offset = None
        mc_ratio_style_options.draw_options = "HIST E1"
        mc_ratio_style_options.legend_options = None

        mc_style_options = copy.deepcopy(dhf.mc_style_options)
        mc_style_options.legend_options = None

        self.reco_plots["Total MC"] = [
                self.total_mc,
                dhf.get_bkg_mc_style_opts(r.kWhite, r.kBlack),
                mc_ratio_style_options,
                data_label
            ]
        self.reco_plots[data_label] = [
                data_hist,
                dhf.data_style_options,
                dhf.data_ratio_style_options,
                None
                ] 

        # Load the unfolded results
        self.unfolded_data_hist = rhf.retrive_hist(unfolder_root_file,  "unfolded_data_hist"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable) )
        self.unfolded_data = rhf.retrive_hist(unfolder_root_file,  "unfolded_data"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable) )
        self.unfolded_mc  = rhf.retrive_hist(unfolder_root_file,  "unfolded_mc"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable) )
        self.corr_unfolded_data = rhf.retrive_hist(unfolder_root_file,  "corr_unfolded_data_"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable) )
        self.corr_unfolded_mc = rhf.retrive_hist(unfolder_root_file,  "corr_unfolded_mc_"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable) )

        # Load the saved response 
        self.response = rhf.retrive_hist(unfolder_root_file,  "roounfold_response_matrix_"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable))

        # load the bootstrapped results
        if self.do_bootstrap:
            self.nominal_unfolded_bs_hists = []
            self.nominal_detector_bs_hists = []
            for i in xrange(self.bootstrapped):
                self.nominal_unfolded_bs_hists.append( rhf.retrive_hist(unfolder_root_file, "nominal_unfolded_hist_toy_" + str(i) ))
                self.nominal_detector_bs_hists.append( rhf.retrive_hist(unfolder_root_file, "nominal_detector_hist_" + str(i) ))
                
        # evaluate 
        # Load total uncertainteis
        self.statistical_uncertainty_hist = rhf.retrive_hist(unfolder_root_file,  "stat_uncert")
        self.mc_statistical_uncertainty_hist = rhf.retrive_hist(unfolder_root_file,  "mc_stat_uncert")
        self.stat_uncert_graph = rhf.retrive_hist(unfolder_root_file,  "mc_stat_uncert_graph")
        self.stat_syst_graph = rhf.retrive_hist(unfolder_root_file,   "stat_syst_graph")
        self.total_stat_syst_up = rhf.retrive_hist(unfolder_root_file,  "total_stat_syst_up")
        self.total_stat_syst_down = rhf.retrive_hist(unfolder_root_file,  "total_stat_syst_down")
        self.perform_background_subtraction()


        # Get the core migration matrix
        if self.run_afii:
            self.migration_matrix_afii = rhf.retrive_hist(unfolder_root_file,  "afii_response_matrix_"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable))

            # proejctions along X and Y
            self.truth_hist_afii = rhf.retrive_hist(unfolder_root_file,  "mc_afii_truth_passTruthAndDetector_" + shf.clean_string(self.truth_level_variable))
            self.detector_mc_hist_afii = rhf.retrive_hist(unfolder_root_file,  "mc_afii_detector_passTruthAndDetector_" + shf.clean_string(self.detector_level_variable))

            # Formatted for drawing
            self.migration_matrix_drawing_afii = rhf.retrive_hist(unfolder_root_file,  self.migration_matrix_afii.GetName() + "_drawing_clone")

            # Load the corrections 
            self.reco_all_hist_afii = rhf.retrive_hist(unfolder_root_file,  self.truth_tuple_name + "_reco_all_afii" + shf.clean_string(self.detector_level_variable))
            self.truth_all_hist_afii= rhf.retrive_hist(unfolder_root_file,  self.truth_tuple_name + "_truth_all_afii" + shf.clean_string(self.detector_level_variable))
            self.reco_and_truth_reco_binning_hist_afii = rhf.retrive_hist(unfolder_root_file,  self.truth_tuple_name + "reco_and_truth_reco_binning_hist_afii" + shf.clean_string(self.detector_level_variable))
            self.reco_and_truth_truth_binning_hist_afii = rhf.retrive_hist(unfolder_root_file,  self.truth_tuple_name + "reco_and_truth_truth_binning_hist_afii" + shf.clean_string(self.detector_level_variable))
            self.acceptance_plot_afii = rhf.retrive_hist(unfolder_root_file,   self.truth_tuple_name + "_afii_acceptance" + shf.clean_string(self.detector_level_variable))
            self.efficiency_plot_afii = rhf.retrive_hist(unfolder_root_file,   self.truth_tuple_name + "_afii_efficiency" + shf.clean_string(self.detector_level_variable))
            
              # Load the unfolded results
            self.unfolded_data_hist_afii = rhf.retrive_hist(unfolder_root_file,  "afii_unfolded_data_hist"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable) )
            self.unfolded_data_afii = rhf.retrive_hist(unfolder_root_file,  "afii_unfolded_data"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable) )
            self.unfolded_mc_afii  = rhf.retrive_hist(unfolder_root_file,  "afii_unfolded_mc"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable) )
            self.corr_unfolded_data_afii = rhf.retrive_hist(unfolder_root_file,  "afii_corr_unfolded_data_"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable) )
            self.corr_unfolded_mc_afii = rhf.retrive_hist(unfolder_root_file,  "afii_corr_unfolded_mc_"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable) )
            
            self.response_afii =  rhf.retrive_hist( unfolder_root_file,  "roounfold_response_matrix_"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable)) 

        # load the bootstrapped results
        if self.do_bootstrap:
            self.nominal_unfolded_bs_hists_afii = []
            self.nominal_detector_bs_hists_afii = []
            for i in xrange(self.bootstrapped):
                self.nominal_unfolded_bs_hists_afii.append( rhf.retrive_hist(unfolder_root_file, "afii_nominal_unfolded_hist_toy_" + str(i) ))
                self.nominal_detector_bs_hists_afii.append( rhf.retrive_hist(unfolder_root_file, "afii_nominal_detector_hist_" + str(i) ))

        self.fake_hists  =rhf.retrive_hist(unfolder_root_file, "Fakes")
        self.begin_saving(self.name+"_2")

        # # Re-evaluate the MC stat uncert. 
        # self.mc_statistical_uncertainty_hist = self.unfold_with_bootstrap(self.reco_all_hist, 
        #                     rhf.normalize_histogram(self.corr_unfolded_mc.Clone()),
        #                     name = "mc" )
        # 

    def construct_name(self):
        """
            Builds a name for the current instances of this class that will used as part of the labeling of all out put
        """
        if self.name == None:
            self.name = shf.clean_string(self.truth_level_variable) + "_to_" + shf.clean_string(self.detector_level_variable)+"_" +self.method

            if self.verbosity > 1:
                print("name: " +  self.name)

    def log(self, *args):
        if self.verbosity == 0:
            return

        # py = psutil.Process(pid)
        memoryUse = -1 # py.memory_info()[0]/2.**30  # memory use in GB...I think
        curr_time = time()
        total_message = "[ TA-UNFOLD ( %.2f|%.2f|%.4fGB) ] - "%(curr_time - self.start_time,curr_time - self.last_time,memoryUse) 
        self.last_time = time()
        for msg in args:
            total_message += " " + str(msg)
        print(total_message)


    def evaluate_nom_mc_luminiosity(self):
        """
            - Evaluate the MC effective luminosities of the data sets used
        """ 
        self.signal_sample.mc_lumi = []
        self.mc_luminosity_nom = 0.0
        for i, file_list in enumerate(self.signal_sample.input_file_names):
                assert len(file_list) != 0, 'ERROR: signal_sample file list is empty!'

                # Evaluate n
                nEvents = 0
                sum_weights_chain = r.TChain("sumWeights")
                for f in file_list:
                    sum_weights_chain.Add(f)
                for e in sum_weights_chain:
                    nEvents += e.totalEventsWeighted

                # Then evaluating the MC luminosity
                mc_luminosity_nom = nEvents/(self.signal_sample.x_secs[i]*self.signal_sample.k_factors[i])
                self.signal_sample.mc_lumi.append(mc_luminosity_nom)
                self.mc_luminosity_nom += mc_luminosity_nom

        if  self.signal_fastsim_sample is not None:
            self.signal_fastsim_sample.mc_lumi = [] 
            for i, file_list in enumerate(self.signal_fastsim_sample.input_file_names):
                    assert len(file_list) != 0, 'ERROR: signal_sample file list is empty!'

                    # Evaluate n
                    nEvents = 0
                    sum_weights_chain = r.TChain("sumWeights")
                    for f in file_list:
                        sum_weights_chain.Add(f)
                    for e in sum_weights_chain:
                        nEvents += e.totalEventsWeighted

                    # Then evaluating the MC luminosity
                    mc_luminosity_nom = nEvents/(self.signal_fastsim_sample.x_secs[i]*self.signal_fastsim_sample.k_factors[i])
                    self.signal_fastsim_sample.mc_lumi.append(mc_luminosity_nom)

        # Should be 
        self.log("Data Luminosity: " +  str(self.luminosity))
        self.log(' MC Lumi breakdown:' + str(self.signal_sample.mc_lumi))
        self.log("Effective MC Luminosity: " +  str(self.mc_luminosity_nom))
        self.log("nEvents (Before cuts ) " +  str(nEvents))
 
    def bootstrap_nominal_afii(self):
        """ 
        """ 
        self.nominal_result_afii, self.bootstrap_hists_afii = None, []
        if not self.do_bootstrap:
            return 
        self.log('Evaluating bootstrap for nominal AFII Samples.')

        response = r.RooUnfoldResponse(self.detector_mc_hist_afii, self.truth_hist_afii, self.migration_matrix_afii)
        response.UseOverflow(True)

        # Evaluate teh 1000 toys for bootstrapping
        self.nominal_result_afii, self.nominal_detector_bs_hists_afii = rhf.get_bootstrapped_distribution(
                                                            self.input_afii_tuple,
                                                            self.detector_tuple_name,
                                                            self.detector_level_variable,
                                                            self.x_axis_binning,
                                                            self.detector_weight + self.detector_cut_weight,
                                                            bootstrap_weight="weight_poisson",
                                                            n_toys=self.bootstrapped
                                                        )
        # Correct and unfold each one in tern
        self.nominal_unfolded_bs_hists_afii = []
        for i in xrange(self.bootstrapped):
            corr_toy_det = self.nominal_detector_bs_hists_afii[i].Clone()
            self.nominal_detector_bs_hists_afii[i].SetName(
            "nominal_detector_hist_" + str(i))
            self.save_object(self.nominal_detector_bs_hists_afii[-1], "afii_nominal_detector_hist_" + str(i))

            # corr_toy_det.Multiply(self.efficiency_plot)
            corr_toy_det.SetDirectory(0)


            # Unfold the toy 
            unfolded_mc_toy = r.RooUnfoldBayes(response, corr_toy_det, self.n_iterations)

            # Normalize the unfolded histogram and save it
            unfolded_mc_toy_hist = unfolded_mc_toy.Hreco()
            self.nominal_unfolded_bs_hists_afii.append(unfolded_mc_toy_hist.Clone(
                "nominal_unfolded_hist_toy_" + str(i))) 
            self.nominal_unfolded_bs_hists_afii[-1].Divide(self.acceptance_plot)
            rhf.normalize_histogram(self.nominal_unfolded_bs_hists_afii[-1])
            self.save_object(self.nominal_unfolded_bs_hists_afii[-1], "afii_nominal_unfolded_hist_toy_" + str(i))

            del unfolded_mc_toy
    
        self.nominal_result_afii.Multiply(self.efficiency_plot_afii)
        self.nominal_result_afii.SetDirectory(0)

        # Unfold the toy 
        unfolded_mc = r.RooUnfoldBayes(response, self.nominal_result, self.n_iterations)

        # Normalize the unfolded histogram and save it
        unfolded_mc_hist = unfolded_mc.Hreco()
        self.nominal_result_afii = unfolded_mc_hist.Clone(
            "nominal_unfolded_result")
        self.nominal_result.Divide(self.acceptance_plot_afii)
        rhf.normalize_histogram(self.nominal_result_afii)
        self.log('Fininshed with the bootstrap')
        del unfolded_mc


    def bootstrap_nominal(self):
        """ 
        """ 
        self.nominal_result, self.bootstrap_hists = None, []
        if not self.do_bootstrap:
            return 

        response = r.RooUnfoldResponse(self.detector_mc_hist, self.truth_hist, self.migration_matrix)
        response.UseOverflow(True)

        self.log('Evaluating bootstrap for nominal ')
        # Evaluate teh 1000 toys for bootstrapping
        self.nominal_result, self.nominal_detector_bs_hists = rhf.get_bootstrapped_distribution(
                                                            self.input_mc_tuple,
                                                            self.detector_tuple_name,
                                                            self.detector_level_variable,
                                                            self.x_axis_binning,
                                                            self.detector_weight + self.detector_cut_weight,
                                                            bootstrap_weight="weight_poisson",
                                                            n_toys=self.bootstrapped
                                                        )
        # Correct and unfold each one in tern
        self.nominal_unfolded_bs_hists = []
        for i in xrange(self.bootstrapped):
            corr_toy_det = self.nominal_detector_bs_hists[i].Clone()
            self.nominal_detector_bs_hists[i].SetName(
            "nominal_detector_hist_" + str(i))
            self.save_object(self.nominal_detector_bs_hists[-1], "nominal_detector_hist_" + str(i))

            # corr_toy_det.Multiply(self.efficiency_plot)
            corr_toy_det.SetDirectory(0)


            # Unfold the toy 
            unfolded_mc_toy = r.RooUnfoldBayes(response, corr_toy_det, self.n_iterations)

            # Normalize the unfolded histogram and save it
            unfolded_mc_toy_hist = unfolded_mc_toy.Hreco()
            self.nominal_unfolded_bs_hists.append(unfolded_mc_toy_hist.Clone(
                "nominal_unfolded_hist_toy_" + str(i))) 
            self.nominal_unfolded_bs_hists[-1].Divide(self.acceptance_plot)
            rhf.normalize_histogram(self.nominal_unfolded_bs_hists[-1])
            self.save_object(self.nominal_unfolded_bs_hists[-1], "nominal_unfolded_hist_toy_" + str(i))

            del unfolded_mc_toy
    
        self.nominal_result.Multiply(self.efficiency_plot)
        self.nominal_result.SetDirectory(0)

        # Unfold the toy 
        unfolded_mc = r.RooUnfoldBayes(response, self.nominal_result, self.n_iterations)

        # Normalize the unfolded histogram and save it
        unfolded_mc_hist = unfolded_mc.Hreco()
        self.nominal_result = unfolded_mc_hist.Clone(
            "nominal_unfolded_result")
        self.nominal_result.Divide(self.acceptance_plot)
        rhf.normalize_histogram(self.nominal_result)
        self.log('Fininshed with the bootstrap')
        del unfolded_mc


    # Construct a bin by bin level accepatance  = events that pass RECO but FAIL truth
    # See paper: https://arxiv.org/pdf/1801.02052.pdf
    def construct_corrections(self):
        '''
            Evaluate the correction factors needed to unfold.


            The follwoing four histograms to construct everything:
                - All Reco in Reco ins
                - All Truth in Truth bins
                - Reco&truth in truth bins
                - Reco&truth in reco   bins

            The corrections then are simply:
                efficiency = reco&truth / all reco (in reco bins)   ( applied to the data before unfolding)
                acceptance = reco&truth / all truth (in truth bins) ( applied to the result after unfolding )

            An acceptance correction facc is applied that accounts for events that are generated outside the fiducial or
            parton phase space but pass the detector-level selection.

            See paper: https://arxiv.org/pdf/1801.02052.pdf

        '''
        self.log("Constructing acceptance and efficiency correction factors"   )

        #All reco
        
        self.reco_all_hist = rhf.get_histogram_from_sample(
                            self.signal_sample,
                            self.detector_tuple_name,
                            self.detector_level_variable,
                            self.x_axis_binning,
                            self.detector_weight+self.detector_cut_weight,
                            scale_to_lumi=True,
                            lumi=self.luminosity
                            )
        self.log('Constructed reco histogram. Yield: ',  self.reco_all_hist.Integral())
        self.log( '                         Entries: ',  self.reco_all_hist.GetEntries())


        self.reco_and_truth_reco_binning_hist = rhf.get_histogram_from_sample(
                            self.signal_sample,
                            self.detector_tuple_name,
                            self.detector_level_variable,
                            self.x_axis_binning,
                            self.detector_weight+self.detector_cut_weight+self.truth_cut_weight, #we're going to manage efficiency and acceptance corrections ourselves
                            friend_name     = self.truth_tuple_name,
                            index_variable  = "eventNumber",
                            scale_to_lumi=True,
                            lumi=self.luminosity
                            )
        self.log('Constructed reco&truth histogram. Yield: ',  self.reco_and_truth_reco_binning_hist.Integral())
        self.log( '                               Entries: ',  self.reco_and_truth_reco_binning_hist.GetEntries())
        self.save_object(self.reco_and_truth_reco_binning_hist, "DEBUG_reco_and_truth_reco_binning_hist" + shf.clean_string(self.detector_level_variable))

        #All Truth
        self.truth_all_hist = rhf.get_histogram_from_sample(
                            self.signal_sample,
                            self.truth_tuple_name,
                            self.truth_level_variable,
                            self.x_axis_binning,
                            self.truth_weight+self.truth_cut_weight,
                            scale_to_lumi=True,
                            lumi=self.luminosity
                            )
        self.log('Constructed truth histogram - Yield: ',  self.truth_all_hist.Integral())
        self.log( '                           Entries: ',  self.truth_all_hist.GetEntries())

        self.reco_and_truth_reco_binning_hist = self.detector_mc_hist.Clone()
        self.reco_and_truth_truth_binning_hist = self.truth_hist.Clone()


        self.log('reco & truth histogram | R - Yield: ',  self.reco_and_truth_reco_binning_hist.Integral())
        self.log( '                          Entries: ',  self.reco_and_truth_reco_binning_hist.GetEntries())

        self.log('reco & truth histogram | T- Yield: ',  self.reco_and_truth_truth_binning_hist.Integral())
        self.log( '                         Entries: ',  self.reco_and_truth_truth_binning_hist.GetEntries())

        #save the 4 key histograms for our unfolding method
        self.save_object(self.reco_all_hist, self.truth_tuple_name + "_reco_all" + shf.clean_string(self.detector_level_variable))
        self.save_object(self.truth_all_hist, self.truth_tuple_name + "_truth_all" + shf.clean_string(self.detector_level_variable))
        self.save_object(self.reco_and_truth_reco_binning_hist, self.truth_tuple_name + "reco_and_truth_reco_binning_hist" + shf.clean_string(self.detector_level_variable))
        self.save_object(self.reco_and_truth_truth_binning_hist, self.truth_tuple_name + "reco_and_truth_truth_binning_hist" + shf.clean_string(self.detector_level_variable))

        #calculate the correction factors and save these also
        self.acceptance_plot = self.reco_and_truth_truth_binning_hist.Clone( shf.clean_string(self.detector_level_variable) + "_acceptance")
        self.acceptance_plot.Divide(self.truth_all_hist)

        self.efficiency_plot = self.reco_and_truth_reco_binning_hist.Clone( shf.clean_string(self.detector_level_variable) + "_efficiency")
        self.efficiency_plot.Divide(self.reco_all_hist)

        self.save_object(self.acceptance_plot, self.truth_tuple_name + "_acceptance" + shf.clean_string(self.detector_level_variable))
        self.save_object(self.efficiency_plot, self.truth_tuple_name + "_efficiency" + shf.clean_string(self.detector_level_variable))
        self.log('Constructed corrections.')

    # Construct a bin by bin level accepatance  = events that pass RECO but FAIL truth
    # See paper: https://arxiv.org/pdf/1801.02052.pdf
    def construct_corrections_afii(self):
        '''
            Evaluate the correction factors needed to unfold.


            The follwoing four histograms to construct everything:
                - All Reco in Reco ins
                - All Truth in Truth bins
                - Reco&truth in truth bins

            The corrections then are simply:
                efficiency = reco&truth / all reco (in reco bins)   ( applied to the data before unfolding)
                acceptance = reco&truth / all truth (in truth bins) ( applied to the result after unfolding )

            An acceptance correction facc is applied that accounts for events that are generated outside the fiducial or
            parton phase space but pass the detector-level selection.

            See paper: https://arxiv.org/pdf/1801.02052.pdf

        '''
        self.log("Constructing AFII acceptance and efficiency correction factors")

        #All reco
        self.reco_all_hist_afii = rhf.get_histogram_from_sample(
                            self.signal_fastsim_sample,
                            self.detector_tuple_name,
                            self.detector_level_variable,
                            self.x_axis_binning,
                            self.detector_weight+self.detector_cut_weight,
                            scale_to_lumi=True,
                            lumi=self.luminosity)

        #All Truth
        self.truth_all_hist_afii = rhf.get_histogram_from_sample(
                            self.signal_fastsim_sample,
                            self.truth_tuple_name,
                            self.truth_level_variable,
                            self.x_axis_binning,
                            self.truth_weight+self.truth_cut_weight,
                            scale_to_lumi=True,
                            lumi=self.luminosity)

        self.reco_and_truth_reco_binning_hist_afii = self.detector_mc_hist_afii.Clone()
        self.reco_and_truth_truth_binning_hist_afii = self.truth_hist_afii.Clone()

        #save the 4 key histograms for our unfolding method
        self.save_object(self.reco_all_hist_afii, self.truth_tuple_name + "_reco_all_afii" + shf.clean_string(self.detector_level_variable))
        self.save_object(self.truth_all_hist_afii, self.truth_tuple_name + "_truth_all_afii" + shf.clean_string(self.detector_level_variable))
        self.save_object(self.reco_and_truth_reco_binning_hist_afii, self.truth_tuple_name + "reco_and_truth_reco_binning_hist_afii" + shf.clean_string(self.detector_level_variable))
        self.save_object(self.reco_and_truth_truth_binning_hist_afii, self.truth_tuple_name + "reco_and_truth_truth_binning_hist_afii" + shf.clean_string(self.detector_level_variable))

        #calculate the correction factors and save these also
        self.acceptance_plot_afii = self.reco_and_truth_truth_binning_hist_afii.Clone( shf.clean_string(self.detector_level_variable) + "_acceptance_afii")
        self.acceptance_plot_afii.Divide(self.truth_all_hist_afii)

        self.efficiency_plot_afii = self.reco_and_truth_reco_binning_hist_afii.Clone( shf.clean_string(self.detector_level_variable) + "_efficiency_afii")
        self.efficiency_plot_afii.Divide(self.reco_all_hist_afii)

        self.save_object(self.acceptance_plot_afii, self.truth_tuple_name + "_afii_acceptance" + shf.clean_string(self.detector_level_variable))
        self.save_object(self.efficiency_plot_afii, self.truth_tuple_name + "_afii_efficiency" + shf.clean_string(self.detector_level_variable))
        self.log('Constructed AFII corrections.')

    def draw_corrections(self, draw_afii=False):
        """
        Draws the correction factors evaluated so far.
        """
        self.log("Drawing the acceptance and effieicny profiles..."   )

        AS.SetAtlasStyle()
        canvas = r.TCanvas(self.name + "acceptance",self.name + "acceptance",600,600)
        r.SetOwnership(canvas, 0)

        # ToDo: Move this styling stuff to the
        eff_style_opts = copy.deepcopy(dhf.data_style_options)
        eff_style_opts.line_color =  r.kRed
        eff_style_opts.marker_style = 20
        eff_style_opts.marker_color = r.kRed
        eff_style_opts.x_title_offset = 1.1
        eff_style_opts.x_label_size   = 0.045
        eff_style_opts.x_title_size   = 0.045
        eff_style_opts.y_label_size   = 0.045
        eff_style_opts.y_title_size   = 0.045
        eff_style_opts.y_title_offset   = 1.45
        eff_style_opts.legend_options = "p"
        eff_style_opts.draw_options = "HIST"
        #
        acc_style_opts              = copy.deepcopy(dhf.data_style_options)
        acc_style_opts.line_color   = 4
        # acc_style_opts.fill_color   = r.kBlue
        acc_style_opts.marker_style = 22
        acc_style_opts.marker_color = r.kBlue + 1
        acc_style_opts.x_title_offset = 1.1
        acc_style_opts.x_label_size   = 0.045
        acc_style_opts.x_title_size   = 0.045
        acc_style_opts.y_label_size   = 0.045
        acc_style_opts.y_title_offset   = 1.45
        acc_style_opts.y_title_size   = 0.045
        acc_style_opts.legend_options = "p"
        acc_style_opts.draw_options = "HIST"

        if draw_afii:
            self.acceptance_plot_afii.SetMinimum(0)
            self.efficiency_plot_afii.SetMinimum(0)
            corrections =  {
                "1/Acceptance": [self.acceptance_plot_afii, acc_style_opts],
                "Efficiency": [self.efficiency_plot_afii, eff_style_opts]
                }   
            plot_name = "afii_corrections"
        else:
            self.acceptance_plot.SetMinimum(0)
            self.efficiency_plot.SetMinimum(0)
            corrections =  {
                "1/Acceptance": [self.acceptance_plot, acc_style_opts],
                "Efficiency": [self.efficiency_plot, eff_style_opts]
                }   
            plot_name = "corrections"

        x_axis_label = self.x_axis_label.replace("Measured ", "")
        dhf.plot_histogram(canvas,
                    corrections,
                    x_axis_title = x_axis_label,
                    y_axis_title ="Correction Factor",
                    do_atlas_labels = True 
                    )

        #deeply iretating feature of ROOT means to draw the plot style I'd like we have to use 2 draw commands
        acceptance_plot = self.acceptance_plot.Clone() if not draw_afii else self.acceptance_plot_afii.Clone()
        acceptance_plot.SetFillColor(r.kBlue)
        acceptance_plot.SetFillStyle(3005)
        acceptance_plot.Draw("e2same")

        #deeply iretating feature of ROOT means to draw the plot style I'd like we have to use 2 draw commands
        efficiency_plot = self.efficiency_plot.Clone() if not draw_afii else self.efficiency_plot_afii.Clone()
        efficiency_plot.SetFillColor(r.kRed)
        efficiency_plot.SetFillStyle(3004)
        efficiency_plot.Draw("e2same")

        #save as pboth eps and png in order to preserve the alpha value which the eps format does not support
        canvas.Print(self.output_folder+plot_name+".png")
        canvas.Print(self.output_folder+plot_name+".eps")

    def construct_fakes_background(self):
        """
        Use the matrix method event-by-event wight estimation to get an estimated
        number of fakeevents. 
        """
        #Finally the measured data
        if self.data_tuple_is_mc:
            return [] 

        self.fake_hists = rhf.get_histogram(
                                           self.input_data_tuple,
                                           self.fake_tuple_name,
                                           self.detector_level_variable.replace(self.detector_tuple_name, self.fake_tuple_name),
                                           self.x_axis_binning,
                                           self.data_fake_weight+self.detector_cut_weight.replace(self.detector_tuple_name, self.fake_tuple_name),
                                           hist_name="Fakes",
                                           )
        self.fake_hists =  dhf.set_style_options(self.fake_hists,
                               dhf.get_bkg_mc_style_opts( r.TColor.GetColor('#5D1362'),
                                                          r.TColor.GetColor('#5D1362')  
                                                          )
                               )
        self.event_yield["Fakes"] = self.fake_hists.Integral()

        if self.save_output:
            self.fake_hists.Write()

        return [self.fake_hists]


    def construct_np_backgrounds(self):
        """
        Evaluate total size of the backgrounds with the current set of cuts,
        etc.
        Store these indivdiually and as a stack, where they are normalized to
        the expected luminosity of each sample
        """

        self.log("Constructing non prompt backounds")
        self.nonprompt_histograms = {}
        # Format and draw the histograms neatly, showing the ratio w.r.t MC
        mc_ratio_style_options = dhf.mc_ratio_style_options
        mc_ratio_style_options.x_title_offset  = 1.1
        mc_ratio_style_options.x_axis_label_offset = None
        mc_ratio_style_options.draw_options = "HIST E1"
        mc_ratio_style_options.legend_options = None

        mc_style_options = copy.deepcopy(dhf.mc_style_options)
        mc_style_options.legend_options = None


        have_backgrounds = False
        for bkg in self.np_background_samples + self.background_samples + [self.signal_sample]:
            have_backgrounds = True
            self.log("   Constructing NP background: " + bkg.name)
            # get the reco distirubtion
            total_hist = None
            for i,file_list in enumerate(bkg.input_file_names):
                # Skip files that we are missing
                if len(file_list) == 0:
                    continue

                hist = rhf.get_histogram(
                            [file_list],
                            self.detector_tuple_name,
                            self.detector_level_variable,
                            self.x_axis_binning,
                            self.detector_weight.replace(self.prompt_weight, self.nonprompt_weight)+self.detector_cut_weight)


                # Normalize the histogram by first reading the total amount of MC there
                nEvents = 0
                sum_weights_chain = r.TChain("sumWeights")
                for f in file_list:
                    sum_weights_chain.Add(f)
                for e in sum_weights_chain:
                    nEvents += e.totalEventsWeighted

                # Then evaluating the MC luminosity
                mc_luminosity_nom = nEvents/(bkg.x_secs[i]*bkg.k_factors[i])
                if mc_luminosity_nom <= 0:
                    continue 
                # Rescale to the expected luminsity based on the data here
                hist.Scale(self.luminosity/mc_luminosity_nom)
                if total_hist is None:
                    total_hist = hist.Clone()
                else:
                    total_hist.Add(hist)

            #
            self.nonprompt_histograms[bkg.name] = dhf.set_style_options(
                                                total_hist.Clone(),
                                                bkg.style_options
                                                )
            self.nonprompt_histograms[bkg.name].SetName(bkg.name)
            self.log(' Fake contribution from ', bkg.name, ' = ', self.nonprompt_histograms[bkg.name].Integral())

        for i, name  in enumerate(self.nonprompt_histograms):
            if i == 0:
                self.total_np =  self.nonprompt_histograms[name].Clone('Non-prompt')
            else:
                self.total_np.Add(self.nonprompt_histograms[name])
            if self.save_output:
                self.nonprompt_histograms[name].Write()

        self.total_np =  dhf.set_style_options(self.total_np,
                               dhf.get_bkg_mc_style_opts( r.TColor.GetColor('#eaa9ef'),
                                                          r.TColor.GetColor('#eaa9ef')  
                                                          )
                               )
        self.event_yield["Non-prompt"] = self.total_np.Integral()

        if self.save_output:
            self.total_np.Write()

        return [ self.total_np ]


    def construct_backgrounds(self):
        """
        Evaluate total size of the backgrounds with the current set of cuts,
        etc.
        Store these indivdiually and as a stack, where they are normalized to
        the expected luminosity of each sample
        """

        self.log("Constructing backgrounds with luminosity: L = ", self.luminosity)
        self.bkg_histograms,bkg_hists = {},[]


        # Add the signal ttbar last so it's at the top
        signal_hist = self.reco_all_hist.Clone()
        signal_hist.SetName(self.signal_mc_name)
        bkg_hist_names = []

        self.have_backgrounds = len(self.background_samples) > 0
        for bkg in self.background_samples:
            self.log("Constructing background: " + bkg.name)
            # get the reco distirubtion
            total_hist = None
            for i,file_list in enumerate(bkg.input_file_names):
                # Skip files that we are missing
                if len(file_list) == 0:
                    continue

                hist = rhf.get_histogram(
                            [file_list],
                            self.detector_tuple_name,
                            self.detector_level_variable,
                            self.x_axis_binning,
                            self.detector_weight+self.detector_cut_weight)


                # Normalize the histogram by first reading the total amount of MC there
                nEvents = 0
                sum_weights_chain = r.TChain("sumWeights")
                for f in file_list:
                    sum_weights_chain.Add(f)
                for e in sum_weights_chain:
                    nEvents += e.totalEventsWeighted

                # Then evaluating the MC luminosity
                mc_luminosity_nom = nEvents/(bkg.x_secs[i]*bkg.k_factors[i])
                if mc_luminosity_nom <= 0:
                    continue 
                # Rescale to the expected luminsity based on the data here
                hist.Scale(self.luminosity/mc_luminosity_nom)
                if total_hist is None:
                    total_hist = hist.Clone()
                else:
                    total_hist.Add(hist)

            if total_hist is not None:
                self.event_yield[bkg.name] = total_hist.Integral()
            else:
                self.event_yield[bkg.name] = 0

            #
            self.bkg_histograms[bkg.name] = dhf.set_style_options(
                                                total_hist.Clone(),
                                                bkg.style_options
                                                )
            self.bkg_histograms[bkg.name].SetName(bkg.name)
            bkg_hists.append(self.bkg_histograms[bkg.name])

            if self.save_output:
                self.bkg_histograms[bkg.name].Write()
        return bkg_hists

    def construct_mc_stack(self,  bkg_hists ):

        # Format and draw the histograms neatly, showing the ratio w.r.t MC
        mc_ratio_style_options = dhf.mc_ratio_style_options
        mc_ratio_style_options.x_title_offset  = 1.1
        mc_ratio_style_options.x_axis_label_offset = None
        mc_ratio_style_options.draw_options = "HIST E1"
        mc_ratio_style_options.legend_options = None

        mc_style_options = copy.deepcopy(dhf.mc_style_options)
        mc_style_options.legend_options = None
        # Add the signal ttbar last so it's at the top
        signal_hist = self.reco_all_hist.Clone()
        signal_hist.SetName(self.signal_mc_name)

        # We want THStack of all histograms, so we can draw them broekn down
        self.mc_stack  = rhf.get_sorted_stack("All MC", bkg_hists)
        if self.have_backgrounds:
            self.total_backgrounds = self.mc_stack.GetStack().Last().Clone("total_bkg")
        else:
            self.total_backgrounds = rhf.clear_histogram(self.data_hist.Clone("total_bkg"))

        self.mc_stack.Add(signal_hist)
        self.event_yield["$t\\bar{t}$"] = signal_hist.Integral()

        self.total_mc = self.mc_stack.GetStack().Last().Clone("total_mc")
        self.save_object(self.mc_stack, "mc_stack")
        self.save_object(self.total_backgrounds, "total_bkg")
        self.save_object(self.total_mc, "total_mc")
        self.reco_plots = OrderedDict()


        data_label = "Pseudo-data" if self.data_tuple_is_mc else "Data"
        data_hist  = self.mc_stack.GetStack().Last().Clone() if self.data_tuple_is_mc else self.data_hist

        data_hist.SetMinimum(self.mc_stack.GetStack().First().GetMinimum())
        self.mc_stack.GetStack().Last().SetMinimum(self.mc_stack.GetStack().First().GetMinimum())
        self.reco_plots["Total MC"] = [
                self.total_mc,
                dhf.get_bkg_mc_style_opts(r.kWhite, r.kBlack),
                mc_ratio_style_options,
                data_label
            ]
        self.reco_plots[data_label] = [
                data_hist,
                dhf.data_style_options,
                dhf.data_ratio_style_options,
                None
            ]

    # Build the migration matrix and any tests that have been requested
    def construct(self):
        """
            Build the migration matrix, truth and detctor spectra for the variable of interst

            Migration matrix will be normalzied correctly, RooUnfold is not allowed to calculate
            correction factors. THat is performed in construct_corrections.
        """
        self.log("Constructing truth, detector and migration histograms")

        # create the migrationion matrix, for the same reason as above
        self.migration_matrix = rhf.get_2d_histogram_from_sample(
                                                     self.signal_sample,
                                                     self.detector_tuple_name,
                                                     self.truth_level_variable+":"+self.detector_level_variable,
                                                     self.x_axis_binning,
                                                     self.x_axis_binning,   #We want symmetric matrices
                                                     self.detector_weight+self.detector_cut_weight+self.truth_cut_weight, #we're going to manage efficiency and acceptance corrections ourselves
                                                     friend_name     = self.truth_tuple_name,
                                                     index_variable  = "eventNumber",
                                                     hist_name       = "response_matrix_"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable),
                                                     scale_to_lumi=True,
                                                     lumi=self.luminosity,
                                                     )

        self.log( "Created migration matrix with NEntries:  ", self.migration_matrix.GetEntries() )
        self.log( "                              Integral:  ", self.migration_matrix.Integral() )

        # We choose to project the migration matrix, M, in X and Y to get the measured and truth
        # histograms respectively
        # In effect we are preventing RooUnfold from calculating the efficiency,e, and acceptance,a
        # i.e Unfolded = [aMe] Data
        self.truth_hist = self.migration_matrix.ProjectionY().Clone()
        self.truth_hist.SetDirectory(0)
        self.save_object(self.truth_hist, "mc_truth_passTruthAndDetector_" + shf.clean_string(self.truth_level_variable))
        self.log('Created truth projection.')

        #similarly for measured histogram
        self.detector_mc_hist  = self.migration_matrix.ProjectionX().Clone()
        self.detector_mc_hist.SetDirectory(0)
        self.save_object(self.detector_mc_hist, "mc_detector_passTruthAndDetector_" + shf.clean_string(self.detector_level_variable))
        self.log('Created detector projection.')

        # The matrix needs to be normalized such that a row adds up to unity
        self.migration_matrix_drawing = rhf.normalize_migration_matrix(self.migration_matrix.Clone(self.migration_matrix.GetName() + "_drawing_clone"))
        self.migration_matrix_drawing.GetXaxis().SetTitle(self.x_axis_label)
        self.migration_matrix_drawing.GetYaxis().SetTitle(self.y_axis_label)
        self.save_object(self.migration_matrix, "response_matrix")
        self.save_object(self.migration_matrix_drawing)
        r.SetOwnership( self.migration_matrix_drawing,0 )
        self.log('Normed matrix.')

        #Finally the measured data
        if not self.data_tuple_is_mc:
            self.data_hist = rhf.get_histogram(self.input_data_tuple,
                                           self.detector_tuple_name,
                                           self.detector_level_variable,
                                           self.x_axis_binning,
                                           self.data_weight+self.detector_cut_weight,
                                           hist_name=self.detector_tuple_name + "_data_" + shf.clean_string(self.detector_level_variable),
                                           )
            self.event_yield["Data"] = self.data_hist.Integral()
            self.log('Constructed data histogram.')


        # After creating the base objects for unfolding also evaluate other factors
        self.construct_corrections()
        if self.data_tuple_is_mc:
            self.data_hist = self.reco_all_hist.Clone()
            self.data_hist.SetName(
                self.detector_tuple_name + "_data_" + shf.clean_string(self.detector_level_variable),
                )
            self.event_yield["Data"] = self.data_hist.Integral()
            self.log('Constructed data histogram from MC.')

        
        self.save_object(self.data_hist, "data_detectorLevel_" +
                         shf.clean_string(self.detector_level_variable))
        self.mc_bkg_hists     = self.construct_backgrounds()
        # self.non_prompt_hists = self.construct_np_backgrounds()
        self.fake_hists       = self.construct_fakes_background()
        self.construct_mc_stack(
                self.mc_bkg_hists + 
                # self.non_prompt_hists 
                self.fake_hists      
            )
        self.construct_additional_sample_distribution()
        self.bootstrap_nominal()

        if self.run_afii:
            self.construct_afii()
            self.bootstrap_nominal_afii()

    def reconstruct_background_and_data(self):
        self.log('Reconstructing the data histogram.')
        #Finally the measured data
        self.data_hist = rhf.get_histogram(self.input_data_tuple,
                                           self.detector_tuple_name,
                                           self.detector_level_variable,
                                           self.x_axis_binning,
                                           self.data_weight+self.detector_cut_weight,
                                           hist_name=self.detector_tuple_name + "_data_" + shf.clean_string(self.detector_level_variable),
                                           scale=self.luminosity/self.mc_luminosity_nom if self.data_tuple_is_mc else 1.0
                                           )
        self.log('Constructed data histogram.')
        self.event_yield["Data"] = self.data_hist.Integral()
        self.save_object(self.data_hist, "data_detectorLevel_" +
                         shf.clean_string(self.detector_level_variable))

        self.non_prompt_hists = self.construct_np_backgrounds()

        self.mc_bkg_hists     = self.construct_backgrounds()
        self.construct_mc_stack( self.mc_bkg_hists + self.non_prompt_hists )


    def divide_histograms_by_width(self):
        self.truth_hist.Scale(1.0,"width")
        self.unfolded_mc_hist.Scale(1.0,"width")
        self.unfolded_data_hist.Scale(1.0,"width")

    def draw_reco_plots(self):
        """
        """
        self.log("Drawing the Reco Distributions")

        # apparently 600,600 is ATLAS recommednations but it looks like trash.
        canvas = r.TCanvas(self.name + "reco_distribution",
                           self.name + "reco_distribution", 600, 600)
        r.SetOwnership(canvas, 0)

        messages = []
        for m in self.messages:
            if m == "Fiducial phase space":
                m = "Detector Level"
            messages.append(m)

        ratio_histograms = dhf.ratio_plot(canvas,
                                          self.reco_plots,
                                          x_axis_title=self.unfolded_x_axis_title,
                                          y_axis_title="Number of events",
                                          messages=messages,
                                          leg_font_size=0.032,
                                          ratio_y_axis_title="#frac{Data}{Prediction}",
                                          set_log_y=self.set_log_y,
                                          additional_stack=self.mc_stack,
                                          # stack_names=self.stack_names,
                                          )


        canvas.Print(self.output_folder+"reco_distribution.eps")
        canvas.Print(self.output_folder+"reco_distribution.png")

        self.log("The unfolded distribution has been drawn.")

    def perform_background_subtraction(self):
        """
        Remove the background distribution from the data 
        """
        self.data_hist_unsubtracted = self.data_hist.Clone("total_data")

        # Not necessary for a pure signal_mc 
        if self.data_tuple_is_mc: 
            return

        self.data_hist.Add(self.total_backgrounds, -1)


    def perform_detector_efficiency_correction(self):
        """
            Correct for the fact that some events that fail particle level selection
            will pass detector cuts due to differences in acceptance.
        """

        self.corr_data_hist = self.data_hist.Clone()
        self.corr_data_hist.Multiply(self.efficiency_plot)

        self.corr_detector_mc_hist = self.reco_all_hist.Clone()
        self.corr_detector_mc_hist.Multiply(self.efficiency_plot)

        # Free from ROOTs memorry managenet so we don't segfault when we leave the scope of this function
        self.corr_data_hist.SetDirectory(0)
        self.corr_detector_mc_hist.SetDirectory(0)

    def perform_fiducial_ps_acceptance_correction(self):
        '''
            Performt the acceptance correction for events that are in fiducial phase space but do not
            Meet the acceptance requirements of the detector.

            I appreciate many people call these corrections different things. This correction is the one
            that's done AFTER THE UNFOLDING HAS BEEN RAN!!
        '''
        self.corr_unfolded_data = self.unfolded_data_hist.Clone()
        self.corr_unfolded_data.Divide(self.acceptance_plot)

        self.corr_unfolded_mc = self.unfolded_mc_hist.Clone()
        self.corr_unfolded_mc.Divide(self.acceptance_plot)

        # Free from ROOTs memorry managenet so we don't segfault when we leave the scope of this function
        self.corr_unfolded_data.SetDirectory(0)
        self.corr_unfolded_mc  .SetDirectory(0)
   
    def perform_detector_efficiency_correction_afii(self):
        """
            Correct for the fact that some events that fail particle level selection
            will pass detector cuts due to differences in acceptance.
        """

        self.corr_data_hist_afii = self.data_hist.Clone()
        self.corr_data_hist_afii.Multiply(self.efficiency_plot_afii)

        self.corr_detector_mc_hist_afii = self.detector_mc_hist_afii.Clone()
        self.corr_detector_mc_hist_afii.Multiply(self.efficiency_plot_afii)

        # Free from ROOTs memorry managenet so we don't segfault when we leave the scope of this function
        self.corr_data_hist_afii.SetDirectory(0)
        self.corr_detector_mc_hist_afii.SetDirectory(0)

    def perform_fiducial_ps_acceptance_correction_afii(self):
        '''
            Performt the acceptance correction for events that are in fiducial phase space but do not
            Meet the acceptance requirements of the detector.

            I appreciate many people call these corrections different things. This correction is the one
            that's done AFTER THE UNFOLDING HAS BEEN RAN!!
        '''
        self.corr_unfolded_data_afii = self.unfolded_data_hist_afii.Clone()
        self.corr_unfolded_data_afii.Divide(self.acceptance_plot_afii)

        self.corr_unfolded_mc_afii = self.unfolded_mc_hist_afii.Clone()
        self.corr_unfolded_mc_afii.Divide(self.acceptance_plot_afii)

        # Free from ROOTs memorry managenet so we don't segfault when we leave the scope of this function
        self.corr_unfolded_data_afii.SetDirectory(0)
        self.corr_unfolded_mc_afii.SetDirectory(0)

    # Unfold the truth
    def evaluate_afii(self):
        """
            Perform the unfolding and save the histograms to a root file

            Correction factors are applied here before and after the unfolding.
        """
        self.log("Performing the unfolding...." )

        #account fo events that pass detector slection but are outside of fidiucal phase-sapce
        self.perform_detector_efficiency_correction_afii()

        #construct the RooUnfoldResponse object
        self.response_afii = r.RooUnfoldResponse(self.detector_mc_hist_afii, self.truth_hist_afii, self.migration_matrix_afii)
        self.response_afii.UseOverflow(True)
        self.save_object(self.response_afii, "roounfold_response_matrix_"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable))

        # RooUnfold supports SVD,Iterative Bayesian and BinByBin methods
        if self.method == "SVD":
            self.unfolded_data_afii = r.RooUnfoldSvd(self.response_afii, self.corr_data_hist_afii)
            self.unfolded_mc_afii   = r.RooUnfoldSvd(self.response_afii, self.corr_detector_mc_hist_afii)
        elif self.method == "BinByBin":
            self.unfolded_data_afii = r.RooTUnfold(self.response_afii, self.corr_data_hist_afii, 20 )
            self.unfolded_mc_afii   = r.RooTUnfold(self.response_afii, self.corr_detector_mc_hist_afii, 20 )
        else:
            #default is bayes
            self.unfolded_data_afii = r.RooUnfoldBayes(self.response_afii, self.corr_data_hist_afii, self.n_iterations, False )
            self.unfolded_mc_afii   = r.RooUnfoldBayes(self.response_afii, self.corr_detector_mc_hist_afii, self.n_iterations, False )

        #perform the unfolding
        self.unfolded_data_hist_afii  = self.unfolded_data_afii.Hreco()
        self.unfolded_mc_hist_afii    = self.unfolded_mc_afii.Hreco()

        self.perform_fiducial_ps_acceptance_correction_afii()

        #
        self.save_object(self.unfolded_data_hist_afii, "afii_unfolded_data_hist"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable) )
        self.save_object(self.unfolded_data_afii, "afii_unfolded_data"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable) )
        self.save_object(self.unfolded_mc_afii, "afii_unfolded_mc"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable) )

        #
        self.save_object(self.corr_unfolded_data, "afii_corr_unfolded_data_"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable) )
        self.save_object(self.corr_unfolded_mc, "afii_corr_unfolded_mc_"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable) )


    # Unfold the truth
    def evaluate(self):
        """
            Perform the unfolding and save the histograms to a root file

            Correction factors are applied here before and after the unfolding.
        """
        self.log("Performing the unfolding...." )

        #
        self.perform_background_subtraction()
        #account fo events that pass detector slection but are outside of fidiucal phase-sapce
        self.perform_detector_efficiency_correction()

        #construct the RooUnfoldResponse object
        self.response = r.RooUnfoldResponse(self.detector_mc_hist, self.truth_hist, self.migration_matrix)
        self.response.UseOverflow(True)
        self.save_object(self.response, "roounfold_response_matrix_"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable))

        # RooUnfold supports SVD,Iterative Bayesian and BinByBin methods
        if self.method == "SVD":
            self.unfolded_data = r.RooUnfoldSvd(self.response, self.corr_data_hist)
            self.unfolded_mc   = r.RooUnfoldSvd(self.response, self.corr_detector_mc_hist)
        elif self.method == "BinByBin":
            self.unfolded_data = r.RooTUnfold(self.response, self.corr_data_hist, 20 )
            self.unfolded_mc   = r.RooTUnfold(self.response, self.corr_detector_mc_hist, 20 )
        else:
            #default is bayes
            self.unfolded_data = r.RooUnfoldBayes(self.response, self.corr_data_hist, self.n_iterations, False )
            self.unfolded_mc   = r.RooUnfoldBayes(self.response, self.corr_detector_mc_hist, self.n_iterations, False )

        #perform the unfolding
        self.unfolded_data_hist  = self.unfolded_data.Hreco()
        self.unfolded_mc_hist    = self.unfolded_mc.Hreco()

        self.perform_fiducial_ps_acceptance_correction()

        #
        self.save_object(self.unfolded_data_hist, "unfolded_data_hist"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable) )
        self.save_object(self.unfolded_data, "unfolded_data"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable) )
        self.save_object(self.unfolded_mc, "unfolded_mc"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable) )

        #
        self.save_object(self.corr_unfolded_data, "corr_unfolded_data_"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable) )
        self.save_object(self.corr_unfolded_mc, "corr_unfolded_mc_"+shf.clean_string(self.detector_level_variable)+"_"+shf.clean_string(self.truth_level_variable) )

        if self.run_afii:
            self.evaluate_afii()

    def draw_tests(self):
        """
           Draw the tests that have been performed, the test classes themeselves dictate what is important
           enough to be drawn.
        """
        self.log('Drawing the results of the tests.')
        for test in self.tests:
            self.log('Drawing test:', test)
            rhf.create_folder(self.output_folder+"/"+shf.clean_string(test.unique_name) +"/")
            test.draw(self.output_folder+"/"+shf.clean_string(test.unique_name)  +"/",
                      x_axis_title = self.unfolded_x_axis_title,
                     )

    def run_tests(self):
        """
            Perform all tests that have been pased to the unfolder. The aim of these tests is to validate
            the unfolding procedure.

            tests can be found in Test.py and StressTest.py
        """
        self.log("Complete."   )
        self.log("Running tests..."   )

        for test in self.tests:
            test.output_folder = self.output_folder+"/"+shf.clean_string(test.unique_name)  +"/"
            test.run(
                     self.response,
                     self.truth_all_hist,
                     self.reco_all_hist,
                     self.efficiency_plot,
                     self.acceptance_plot,
                     self.method,
                     self.migration_matrix.Clone(),
                     )


    def draw_migration_matrix(self, draw_afii=False):
        """
            Draw the migration matrix neatly, heavily based on the DrawingHelperFuntion method.
        """
        self.log("Drawing the "+ 'AFII' if draw_afii else ''+ " migration matrix"  )
        canvas = r.TCanvas(self.name + "migration_matrix_canvas",
                           self.name + "migration_matrix_canvas",
                           800,
                           600)
        r.SetOwnership(canvas, 0)
        if draw_afii:
            dhf.draw_migration_matrix(self.migration_matrix_drawing_afii, canvas)
            plot_name = "afii_response_matrix_test"
        else:
            dhf.draw_migration_matrix(self.migration_matrix_drawing, canvas)
            plot_name = "response_matrix_test"

        canvas.Print(self.output_folder+plot_name+".eps")
        canvas.Print(self.output_folder+plot_name+".png")
        self.log("Done."   )

    def construct_additional_sample_distribution(self):
        """
            Construct the truth level distributions for each additional MC sample.
        """
        for sample_name in self.additional_samples:
            self.log("Creating additional sample distribution: " + sample_name )

            self.additional_sample_hist[sample_name] = rhf.get_histogram(
                            self.additional_samples[sample_name],
                            self.truth_tuple_name,
                            self.truth_level_variable,
                            self.x_axis_binning,
                            "weight_mc"+self.truth_cut_weight,
                            hist_name = self.name+"_truth_var_"+sample_name)
            r.SetOwnership(self.additional_sample_hist[sample_name],0)  

            # Normalize this bad boy 
            nEvents = 0
            for dsid_list in self.additional_samples[sample_name]:
                for file in dsid_list:
                    cut_flow_hist = rhf.retrive_hist(file, "ejets_particle/cutflow_particle_level_mc")
                    nEvents += cut_flow_hist.GetBinContent(1)
            self.log("Total events in additional sample: ", nEvents)
            k_factors = 1.139
            x_secs =  397.11000000000001
            self.additional_samples_lumi = nEvents/k_factors/x_secs #(self.additional_samples_xsec[sample_name]*self.additional_samples_k_factors[sample_name])
            self.additional_sample_hist[sample_name].Scale(self.luminosity/self.additional_samples_lumi)


    def construct_unfolded_distribution_plot(self):
        """
            Build the dictionary that is used in plotting functions. Brings
            together the unfolded result, the nominal MC and any
            additional samples we care about
        """
        # dhf.data_style_options.y_label_size = 0.045
        # dhf.data_style_options.y_title_size = 0.045
        # dhf.data_style_options.y_title_offset = 1.45

        # Format and draw the histograms neatly, showing the ratio w.r.t MC
        mc_ratio_style_options = dhf.mc_ratio_style_options
        mc_ratio_style_options.x_title_offset  = 1.05
        mc_ratio_style_options.x_axis_label_offset = None
        mc_ratio_style_options.draw_options = "HIST"
        mc_ratio_style_options.legend_options = "None"

        data_ratio_style_options = dhf.data_ratio_style_options
        data_ratio_style_options.legend_options = "None"
        data_ratio_style_options.x_title_offset  = 1.05

        # It's useful to show on the plot that we are not displaying data
        data_label = "Pseudo-data" if self.data_tuple_is_mc else "Data"

        # To do set this to the last element
        if self.ratio_plot_name == None:
            self.ratio_plot_name = data_label
        #
        self.unfolded_distribution_plots =  OrderedDict()
        self.unfolded_distribution_absolute_plots =  OrderedDict()
        self.unfolded_distribution_plots_afii =  OrderedDict()

        # Construct the absolute measurement 
        truth_hist = self.truth_all_hist.Clone("nominal_mc_unnormed")
        # truth_hist.Scale(self.luminosity/self.mc_luminosity_nom)
        self.unfolded_distribution_absolute_plots[data_label]        =  [ self.corr_unfolded_data.Clone("data_unnormed"), dhf.data_style_options, data_ratio_style_options, self.ratio_plot_name]
        self.unfolded_distribution_absolute_plots["Powheg+Pythia8"]  =  [ truth_hist, dhf.mc_style_options, mc_ratio_style_options, self.ratio_plot_name]

        # And the Relaitve 
        self.unfolded_distribution_plots[data_label]        =  [ rhf.normalize_histogram(self.corr_unfolded_data), dhf.data_style_options, data_ratio_style_options, self.ratio_plot_name]
        self.unfolded_distribution_plots["Powheg+Pythia8"]  =  [ rhf.normalize_histogram(self.truth_all_hist), dhf.mc_style_options, mc_ratio_style_options, self.ratio_plot_name]

        if self.run_afii:
            self.unfolded_distribution_plots_afii[data_label] =  [ rhf.normalize_histogram(self.corr_unfolded_data_afii), dhf.data_style_options, data_ratio_style_options, self.ratio_plot_name]
            self.unfolded_distribution_plots_afii["Powheg+Pythia8"] =  [ rhf.normalize_histogram(self.truth_all_hist_afii), dhf.mc_style_options, mc_ratio_style_options, self.ratio_plot_name]

        # Add additional sample histograms
        self.color_index = 0
        for sample_name in self.additional_samples:
            self.unfolded_distribution_absolute_plots[sample_name] =  [
                self.additional_sample_hist[sample_name].Clone(sample_name+"_unnormed"),
                dhf.get_large_legend_stlye_opt(self.color_index, is_ratio = False),
                dhf.get_large_legend_stlye_opt(self.color_index, is_ratio = True),
                self.ratio_plot_name
              ]
            self.unfolded_distribution_plots[sample_name] =  [
                rhf.normalize_histogram(self.additional_sample_hist[sample_name] ) ,
                dhf.get_large_legend_stlye_opt(self.color_index, is_ratio = False),
                dhf.get_large_legend_stlye_opt(self.color_index, is_ratio = True),
                self.ratio_plot_name
              ]
        
            if self.run_afii:
                self.unfolded_distribution_plots_afii[sample_name] =  [
                    rhf.normalize_histogram(self.additional_sample_hist[sample_name].Clone() ) ,
                    dhf.get_large_legend_stlye_opt(self.color_index, is_ratio = False),
                    dhf.get_large_legend_stlye_opt(self.color_index, is_ratio = True),
                    self.ratio_plot_name
                  ]
            self.color_index +=1

    def create_triple_ratio_plotting_dictionary(self, plotting_dictionary):
        triple_rato_plots = copy.copy(plotting_dictionary)

        for name in triple_rato_plots:
            # Append an additional name for the second ratio pannel   
            if any([name in f for f in self.double_ratio_sample_names]):
                triple_rato_plots[name].append(self.double_ratio_sample_name)
            else:
                triple_rato_plots[name].append(None)
                
        return triple_rato_plots


    def append_additional_systs(self, plotting_dictionary, additional_systs_to_draw):
        
        addtional_unfolded_plots = {}
        # loop over the additional systs to draw
        for syst_name in additional_systs_to_draw:
            self.log('Constructiing systematic unfolded plot for diplay: ', syst_name)

            # Reconstruct the total distribution based on the size of the systematic 
            if  self.systematics[syst_name].truth_histogram is None:
                self.log('ERROR: Truth level histogram for systematic ', syst_name, ' is the same as nominal.')
                sys.exit(-1)
                continue

            # The above is the difference between the nom. MC and the variation
            # So add the nomMC back to get back 
            addtional_unfolded_plots[syst_name] = self.systematics[syst_name].truth_histogram.Clone()
            addtional_unfolded_plots[syst_name].SetDirectory(0)
            rhf.normalize_histogram(addtional_unfolded_plots[syst_name])
            plotting_dictionary[syst_name.replace('_',' ')] = [
                addtional_unfolded_plots[syst_name],
                dhf.get_large_legend_stlye_opt(self.color_index, is_ratio = False),
                dhf.get_large_legend_stlye_opt(self.color_index, is_ratio = True),
                self.ratio_plot_name
                ]
            self.color_index +=1

        return plotting_dictionary


    def draw_unfolded_distribution(self, draw_afii=False, plot_name="unfolded_distribution", additional_systs_to_draw=None, divide_by_bin_width=True ):
        """
        Draw the main result of the script and a ratio plot showing
        the compatabilty of unfolded data with MC and any additionaly
        hypotheses added.
        """
        self.log("Drawing the unfolded distribution")

        # apparently 600,600 is ATLAS recommednations but it looks like trash.
        canvas = r.TCanvas(self.name + "unfolded_distro",self.name + "unfolded_distro",600,600)
        r.SetOwnership(canvas, 0)

        # Store the ratio plots in an OrderedDict to ensure that
        # we plot the largest of the two first!
        uncertainty_graphs_for_ratio_plot = OrderedDict()
        uncertainty_graphs_for_ratio_plot["Syst #otimes Stat"] = [
            self.stat_syst_graph,
            dhf.get_stat_syst_graph_stlye_opt(show_border=False),
            dhf.get_stat_syst_graph_stlye_opt(is_ratio=True, show_border=False)
            ]
        uncertainty_graphs_for_ratio_plot["Stat."] = [self.stat_uncert_graph, dhf.get_uncert_graph_stlye_opt(show_border=False), dhf.get_uncert_graph_stlye_opt(is_ratio = True,show_border=False)]

        self.construct_unfolded_distribution_plot()
        unfolded_distribution_plots = self.unfolded_distribution_plots
        if draw_afii:
            plot_name = "afii_" + plot_name
            unfolded_distribution_plots = self.unfolded_distribution_plots_afii

        if additional_systs_to_draw is not None:
            plot_name = "additional_systs_" + plot_name
            unfolded_distribution_plots = self.append_additional_systs(unfolded_distribution_plots,
                                 additional_systs_to_draw)  

        ## Ad a data style option to the end of the plot so it's always on top of whatever esle is shown.
        data_opt =copy.copy(dhf.data_style_options)
        data_ratio_style_options = dhf.data_ratio_style_options
        data_ratio_style_options.legend_options = "None"
        data_ratio_style_options.x_title_offset  = 1.05
        data_opt.legend_options = None
        data_label = "Pseudo-data" if self.data_tuple_is_mc else "Data"
        self.unfolded_distribution_plots[data_label+"_2"] =  [ self.corr_unfolded_data.Clone("data_unnormed"), 
                            data_opt, 
                            data_ratio_style_options, 
                            self.ratio_plot_name]

        if self.double_ratio_sample_names is not None:
            # canvas = r.TCanvas(self.name + "unfolded_distro",self.name + "unfolded_distro",600,600)
            # r.SetOwnership(canvas, 0)
            triple_rato_plots = self.create_triple_ratio_plotting_dictionary(unfolded_distribution_plots)
            ratio_histograms = dhf.double_ratio_plot(canvas,
                          triple_rato_plots,
                          x_axis_title=self.unfolded_x_axis_title,
                          y_axis_title=self.unfolded_y_axis_title,
                          messages=self.messages,
                          ratio_histograms=uncertainty_graphs_for_ratio_plot,
                          # ratio_histograms_lower_pannel=None,
                          leg_font_size=0.04,
                          ratio_y_axis_title="#frac{Prediction}{"+self.ratio_plot_name + "}",
                          ratio_2_axis_title="#frac{Prediction}{"+self.double_ratio_sample_name + "}",
                          set_log_y=self.set_log_y,
                          divide_by_bin_width=divide_by_bin_width,
                          normalize_after_binwidth_divide=True,
                          )
            canvas.Print(self.output_folder+plot_name+"_double_ratio.eps")
            canvas.Print(self.output_folder+plot_name+"_double_ratio.png")
            self.log('    Created double ratio plots.')


        ratio_histograms = dhf.ratio_plot(canvas,
                                          unfolded_distribution_plots,
                                          x_axis_title=self.unfolded_x_axis_title,
                                          y_axis_title=self.unfolded_y_axis_title,
                                          messages=self.messages,
                                          ratio_histograms=uncertainty_graphs_for_ratio_plot,
                                          leg_font_size=0.045,
                                          ratio_y_axis_title="#frac{Prediction}{"+self.ratio_plot_name + "}",
                                          set_log_y=self.set_log_y,
                                          divide_by_bin_width=divide_by_bin_width,
                                          normalize_after_binwidth_divide=True,
                                          )

        # Set the rerror to be whatever is large in whatever direction
        for i in xrange(1, ratio_histograms[self.ratio_plot_name][0].GetNbinsX()+1):
            # The most conservative method of evaluate the chi2 and thus
            # p-value is by symmetrising the upward and downward variation
            # Recal that we times by 100 to show the fractional uncertainty
            # so undo this.
            total_error = 0.01*max(self.total_stat_syst_up.GetBinContent(i),
                              -self.total_stat_syst_down.GetBinContent(i))
            ratio_histograms[self.ratio_plot_name][0].SetBinError(i, total_error)

        ratio_histograms = dhf.ratio_plot(canvas,
                                          unfolded_distribution_plots,
                                          x_axis_title=self.unfolded_x_axis_title,
                                          y_axis_title=self.unfolded_y_axis_title,
                                          messages=self.messages,
                                          ratio_histograms=uncertainty_graphs_for_ratio_plot,
                                          leg_font_size=0.032,
                                          ratio_y_axis_title="#frac{Prediction}{"+self.ratio_plot_name + "}",
                                          set_log_y=self.set_log_y,
                                          divide_by_bin_width=divide_by_bin_width,
                                          normalize_after_binwidth_divide=True,
                                          )

        for sample_name in self.additional_sample_hist:
            # self.additional_sample_hist[sample_name].Draw("E1 same")
            self.additional_sample_hist[sample_name].Write()
            # self.additional_sample_hist[sample_name].Draw("E2 same")
        if self.save_output:
            for name in ratio_histograms:
                ratio_histograms[name][0].Write()
            canvas.Write()

        canvas.Print(self.output_folder+plot_name+".eps")
        canvas.Print(self.output_folder+plot_name+".png")



        ratio_histograms = dhf.ratio_plot(canvas,
                                      self.unfolded_distribution_absolute_plots,
                                      x_axis_title=self.unfolded_x_axis_title,
                                      y_axis_title=self.unfolded_y_axis_title.replace("#frac{1}{#sigma_{0}} ",""),
                                      messages=self.messages,
                                      ratio_histograms=uncertainty_graphs_for_ratio_plot,
                                      leg_font_size=0.032,
                                      ratio_y_axis_title="#frac{Prediction}{"+self.ratio_plot_name + "}",
                                      set_log_y=self.set_log_y,
                                      divide_by_bin_width=divide_by_bin_width,
                                      normalize_after_binwidth_divide=True,
                                      )

         # Set the rerror to be whatever is large in whatever direction
        for i in xrange(1, ratio_histograms[self.ratio_plot_name][0].GetNbinsX()+1):
            # The most conservative method of evaluate the chi2 and thus
            # p-value is by symmetrising the upward and downward variation
            # Recal that we times by 100 to show the fractional uncertainty
            # so undo this.
            total_error = 0.01*max(self.total_stat_syst_up.GetBinContent(i),
                              -self.total_stat_syst_down.GetBinContent(i))
            ratio_histograms[self.ratio_plot_name][0].SetBinError(i, total_error)

        ratio_histograms = dhf.ratio_plot(canvas,
                                          self.unfolded_distribution_absolute_plots,
                                          x_axis_title=self.unfolded_x_axis_title,
                                          y_axis_title=self.unfolded_y_axis_title.replace("#frac{1}{#sigma_{0}} ",""),
                                          messages=self.messages,
                                          ratio_histograms=uncertainty_graphs_for_ratio_plot,
                                          leg_font_size=0.032,
                                          ratio_y_axis_title="#frac{Prediction}{"+self.ratio_plot_name + "}",
                                          set_log_y=self.set_log_y,
                                          divide_by_bin_width=divide_by_bin_width,
                                          normalize_after_binwidth_divide=True,
                                          )
        
        canvas.Print(self.output_folder+"unfolded_distribution_absolute.eps")
        canvas.Print(self.output_folder+"unfolded_distribution_absolute.png")
        self.log("The unfolded distribution has been drawn.")

    def draw_afii(self):
        if not self.run_afii:
            return

        self.log("Drawing AFII distributiosn.")
        self.draw_migration_matrix(draw_afii=True)
        self.draw_unfolded_distribution(draw_afii=True)
        self.draw_corrections(draw_afii=True)


    def draw(self, combinations):
        """
            Draw all parts of the unfolding, the migration matrix, the unfolded
            distribution, tests correction factors and systematics.
        """
        self.draw_nominal_unfolding_plots()
        self.draw_tests()
        self.draw_systematics(combinations)


    def draw_nominal_unfolding_plots(self):
        self.output_file.cd()
        self.draw_migration_matrix()
        self.draw_unfolded_distribution()
        self.draw_corrections()

    ##
    def add_test(self, test):
        """
            Tell the unfolder that another test is to be perfomed.
        """
        self.log("Added test: " + test.name    )
        self.log("\t" +  test.description    )

        self.tests.append(test)

    def begin_saving(self, name):
        """
            Tell the unfolder that we wish to save all output to a root file. The name of the root file is set
            by construct_name, OR if passed in the constructor whatever a user desides.
        """
        if self.save_output:
            self.output_file = rhf.open_file(self.output_folder + "/" + name + ".root", "RECREATE")
            if self.verbosity > 1:
                print("Saving root output to : " + self.output_folder + name  + ".root")

    def unfold_with_bootstrap(self, hist_to_unfold, unfolded_hist, name=None,
                              n_toys=500):
        """

        Unfolds a histogram n_toys times after poisson fluctuating it according to the number of events
        in the bin.
        Don't be afraid to go to extreme numbers of toys (e.g multiple thousand), there isn't any slow
        reading of info from a file at this point, it's just matrix operations.

        hist_to_unfold: a histogram that requires unfolded. This is what we are trying to assess the size of the statitsical uncertianties of
        unfolded_hist: the nominal unfolded histogram of hist_to_unfold. Is used to compute the RMS spread of the toys
        n_toys: number of toys/pseudo-experiments in this procedure

        """
        if name == None:
            name = hist_to_unfold.GetName()
        # 
        self.output_file.cd()

        self.log('Unfolding ', n_toys, ' times to evaluate the data stat uncert.')
        random_gen = r.TRandom3(int(time()))
        rms_hist = r.TH2D(self.name + "_stat_uncert_rms", "", len(self.x_axis_binning)-1,array.array('d',self.x_axis_binning), 30, 0.7, 1.3)

        # Run the toy generation n_toys time and fill a 2d hist of RMS spread vs x_Xais
        var_values,rms_values = {},{}
        for bin in range(1, unfolded_hist.GetNbinsX()+1):
            var_values[bin] = []

        for i in xrange(n_toys):
            toy_hist = rhf.poisson_fluctuate(hist_to_unfold.Clone(),random_gen)
            toy_hist.Add(self.total_backgrounds,-1)
            toy_hist.Multiply(self.efficiency_plot)

            # Unfold this
            if self.method == "SVD":
                unfolded_result = r.RooUnfoldSvd(self.response, toy_hist)
            elif self.method == "BinByBin":
                unfolded_result   = r.RooTUnfold(self.response, toy_hist, 20 )
            else:
                unfolded_result = r.RooUnfoldBayes(self.response, toy_hist, self.n_iterations, False )

            #Normalzie and corect for fiducial inefficency
            temp_result = unfolded_result.Hreco().Clone(self.name + "_unfolded_toy_"+str(i))#
            temp_result.Divide(self.acceptance_plot)
            rhf.normalize_histogram(temp_result)

            for b in range(1, temp_result.GetNbinsX()+1):
                norm = unfolded_hist.GetBinContent(b)
                if norm == 0:
                    norm = 1
                var_values[b].append( (temp_result.GetBinContent(b) -  norm)/norm )

        # Now fit each bin with a Gaussian and extract the width as the stat uncertainty
        # in bins of the unfolded variable
        stat_error = r.TH1F(self.name + "_stat_uncert_bins", "", len(self.x_axis_binning)-1,array.array('d',self.x_axis_binning))

        #put the toy results into a subfolder so they don't cluter the main directory
        r.gDirectory.mkdir("stat_uncert_bins_"+name)
        r.gDirectory.cd("stat_uncert_bins_"+name)

        for bin in range(1, unfolded_hist.GetNbinsX()+1):
            var_values[bin] =  np.array(var_values[bin])
            rms_values[bin] =  np.sqrt(np.mean(np.square(var_values[bin])))
            temp_rms_hist = r.TH1F(self.name + "_stat_uncert_var_hist_bin"+str(bin),"",int(np.sqrt(float(n_toys))),
                                    np.min(var_values[bin]),
                                    np.max(var_values[bin]))
                #np.mean(var_values[bin])-3.0*rms_values[bin], np.mean(var_values[bin])+3.0*rms_values[bin])
            for x in var_values[bin]:
                temp_rms_hist.Fill(x)
            fit_result = temp_rms_hist.Fit("gaus", "SQ")
            temp_rms_hist.Write()
            # stat_error.SetBinContent(bin,rms_values[bin])#.Parameter(2))
            stat_error.SetBinContent(bin,fit_result.Parameter(2))

        stat_error.SetDirectory(0)
        self.save_object(stat_error,self.name + "_stat_uncert")
        r.gDirectory.cd("../")

        return stat_error,rms_hist


    def evaluate_statistical_uncertainty(self):
        """
            Evaluates the statistical uncertainty via the bootstrapping method, we generate 1000 toys
            Such that

        """
        self.log("Evaluating statistical uncertainty with poisson fluctuation of unfolded distributions.")
        self.statistical_uncertainty_hist,_ = self.unfold_with_bootstrap(self.data_hist_unsubtracted,
                                                rhf.normalize_histogram(self.corr_unfolded_data.Clone()),
                                                name = "data" )

        self.corr_unfolded_mc_normed = rhf.normalize_histogram(self.corr_unfolded_mc.Clone(), correct_uncert = True)
        mc_stat = self.reco_all_hist.Clone('mc_unnormed')
        mc_stat.Scale(self.mc_luminosity_nom/self.luminosity)
        self.mc_statistical_uncertainty_hist,_ = self.unfold_with_bootstrap(mc_stat, 
                                            rhf.normalize_histogram(self.corr_unfolded_mc.Clone()),
                                            name = "mc" )

        #rhf.extract_errors_to_hist(self.corr_unfolded_mc_normed)
    
        # self.statistical_uncertainty_hist = self.mc_statistical_uncertainty_hist
        # Make a copy for display
        self.statistical_uncertainty_hist_upscaled = self.statistical_uncertainty_hist.Clone()
        self.statistical_uncertainty_hist_upscaled.Scale(100.0)
        self.statistical_uncertainty_hist_upscaled.SetDirectory(0)

        self.statistical_uncertainty_hist_down_upscaled = self.statistical_uncertainty_hist.Clone()
        self.statistical_uncertainty_hist_down_upscaled.Scale(-100.0)
        self.statistical_uncertainty_hist_down_upscaled.SetDirectory(0)

        # Make a copy for display
        self.mc_statistical_uncertainty_hist_upscaled = self.mc_statistical_uncertainty_hist.Clone()
        self.mc_statistical_uncertainty_hist_upscaled.Scale(100.0)
        self.mc_statistical_uncertainty_hist_upscaled.SetDirectory(0)

        self.mc_statistical_uncertainty_hist_down_upscaled = self.mc_statistical_uncertainty_hist.Clone()
        self.mc_statistical_uncertainty_hist_down_upscaled.Scale(-100.0)
        self.mc_statistical_uncertainty_hist_down_upscaled.SetDirectory(0)

        self.save_object(self.statistical_uncertainty_hist, "stat_uncert")
        self.save_object(self.mc_statistical_uncertainty_hist, "mc_stat_uncert")

    def print_stat_uncertaintiy(self):
        """
        Output the statistical uncertainty of the data
        """
        stat_uncert, syst_down_uncert, syst_up_uncert = [], [], []
        for i in  xrange(1,self.statistical_uncertainty_hist.GetNbinsX() + 1):
            stat_uncert.append(self.statistical_uncertainty_hist.GetBinContent(i))
            syst_down_uncert.append(self.total_stat_syst_down.GetBinContent(i))
            syst_up_uncert.append(self.total_stat_syst_up.GetBinContent(i))




    def evaluate_total_systematic(self):
        """
            Combines all evaluated systematics in quadrature taking into account the different directions these
            can apply in.

            Stores the total result in histogram self.total_up, self.total_down and self.total_uncert_asymm_graph
            where  the last one is TGraphAsymmErrors
        """
        #start off by assessing the easy stat uncert.
        self.evaluate_statistical_uncertainty()

        # Get list of upward and downard systematic totals
        total_down_hists, total_up_hists = [], []
        for syst_name in self.systematics:
            if self.systematics[syst_name].total_up_variation is None:
                continue
                
            total_up_hists.append(self.systematics[syst_name].total_up_variation.Clone())
            total_down_hists.append(self.systematics[syst_name].total_down_variation.Clone())
            total_up_hists[-1].SetDirectory(0)
            total_down_hists[-1].SetDirectory(0)
            total_up_hists[-1].Scale(1.0/100.0)
            total_down_hists[-1].Scale(1.0/100.0)

        total_up_hists.append(self.statistical_uncertainty_hist)
        total_down_hists.append(self.statistical_uncertainty_hist)

        self.total_up = rhf.combine_bincenters_in_quadrature(total_up_hists)
        self.total_down = rhf.combine_bincenters_in_quadrature(total_down_hists)
        self.stat_uncert_graph = rhf.convert_up_down_uncert_to_asymmerrors(self.statistical_uncertainty_hist, self.statistical_uncertainty_hist)
        self.save_object( self.stat_uncert_graph, "mc_stat_uncert_graph")

        #rescale them up to be a percentage
        self.total_up.Scale(100.0)
        self.total_down.Scale(-100.0)

        #let's not forget about the statistical uncertainty
        # total_up_hists.append(self.statistical_uncertainty_hist)
        # total_down_hists.append(self.statistical_uncertainty_hist)

        self.total_stat_syst_up   = rhf.combine_bincenters_in_quadrature(total_up_hists)
        self.total_stat_syst_down = rhf.combine_bincenters_in_quadrature(total_down_hists)

        if True:
            self.stat_syst_graph = rhf.convert_up_down_uncert_to_asymmerrors(self.total_stat_syst_up  , self.total_stat_syst_down)
        else:
            self.log("WARNING: Not showing syst. uncert."  )
            self.stat_syst_graph = rhf.convert_up_down_uncert_to_asymmerrors(self.statistical_uncertainty_hist  , self.statistical_uncertainty_hist)
        self.save_object( self.stat_syst_graph, "stat_syst_graph")


        self.save_object(self.total_stat_syst_up, "total_stat_syst_up")
        self.save_object(self.total_stat_syst_down, "total_stat_syst_down")

        self.total_stat_syst_up.Scale(100.0)
        self.total_stat_syst_down.Scale(-100.0)

        # Not a particularly elegant way of doing the plotting
        # but start by shoving everything we want on in reverse order
        self.systematic_variation_sizes["Syst. #otimes Stat."] = [
             self.total_stat_syst_up,
             dhf.get_stat_syst_stlye_opt(),
             dhf.get_stat_syst_stlye_opt(is_ratio = True),
        ]
        self.systematic_variation_sizes["Stat Syst Down"] = [
             self.total_stat_syst_down,
             dhf.get_stat_syst_stlye_opt(show_legend = False),
             dhf.get_stat_syst_stlye_opt(is_ratio =True,show_legend = False),
        ]

        #add the systematic affer
        self.systematic_variation_sizes["Stat."] = [
             self.statistical_uncertainty_hist_upscaled,
             dhf.get_uncert_stlye_opt(),
             dhf.get_uncert_stlye_opt(is_ratio = True),
        ]
        self.systematic_variation_sizes["Stat. Down"] = [
             self.statistical_uncertainty_hist_down_upscaled,
             dhf.get_uncert_stlye_opt(show_legend = False),
             dhf.get_uncert_stlye_opt(is_ratio =True,show_legend = False),
        ]

        syst_count = 0
        for syst_name in self.systematics:
            self.systematic_variation_sizes[syst_name] = [
                 self.systematics[syst_name].total_up_variation,
                 dhf.get_truth_large_legend_stlye_opt(syst_count,False),
                 dhf.get_truth_large_legend_stlye_opt(syst_count,True)
                ]
            self.systematic_variation_sizes[syst_name+"down"] = [
                 self.systematics[syst_name].total_down_variation,
                 dhf.get_truth_large_legend_stlye_opt(syst_count,False,show_legend = False),
                 dhf.get_truth_large_legend_stlye_opt(syst_count,True,show_legend = False)
                ]
            syst_count+=2

        # self.systematic_variation_sizes["MC stat."] = [
        #      self.mc_statistical_uncertainty_hist_upscaled,
        #      dhf.get_truth_large_legend_stlye_opt(syst_count,False),
        #      dhf.get_truth_large_legend_stlye_opt(syst_count,True)
        #     ]
        # self.systematic_variation_sizes["MC stat. down"] = [
        #      self.mc_statistical_uncertainty_hist_down_upscaled,
        #      dhf.get_truth_large_legend_stlye_opt(syst_count,False,show_legend = False),
        #      dhf.get_truth_large_legend_stlye_opt(syst_count,True,show_legend = False)
        #     ]
    def construct_background_varied_nominal_unfolding(self, background_sample):
        """
        Returns the nominal unfolded distribution with thebackground subtraction 
        performed with a systematically varied background. 
        """

        # Find the background sample 
        self.log('Constructing systematic variation of sample: ', background_sample)
        sf_variations = None
        for bkg in self.background_samples:
            if bkg.name == background_sample:
                sf_variations = bkg.normalisation_uncertainties 


        assert sf_variations is not None, str("ERORR: unknown background sample attempt to being varied " + background_sample)
        self.log('Variations of size', sf_variations)
        sf_variations = [0.] + sf_variations

        # Construct the difference between the nominal histogram and that of
        # the systematic variation. This is all that needs to be changed 
        # for the nominal sample. 
        variation_hists = []
        for sf in sf_variations:
            variation_hist = self.bkg_histograms[background_sample].Clone()
            variation_hist.Scale(sf)

            varied_reco_mc_hist = self.reco_all_hist.Clone()
            varied_reco_mc_hist.Add(variation_hist)
            varied_reco_mc_hist.Multiply(self.efficiency_plot)
            variation_hists.append(varied_reco_mc_hist)

        varied_unfolded_mc = []
        for hist in variation_hists:
             # RooUnfold supports SVD,Iterative Bayesian and BinByBin methods
            if self.method == "SVD":
                unfolded_mc   = r.RooUnfoldSvd(self.response, hist)
            elif self.method == "BinByBin":
                unfolded_mc   = r.RooTUnfold(self.response, hist, 20 )
            else:
                #default is bayes
                unfolded_mc   = r.RooUnfoldBayes(self.response, hist, self.n_iterations, False )
    
            unfolded_mc_hist    = unfolded_mc.Hreco()
            # unfolded_mc_hist.Divide(self.acceptance_plot)
            unfolded_mc_hist.Divide(self.acceptance_plot)
            varied_unfolded_mc.append(unfolded_mc_hist)
            varied_unfolded_mc[-1].SetDirectory(0)
        return varied_unfolded_mc


    def construct_fake_variation(self):
        """
        Returns the nominal unfolded distribution with thebackground subtraction 
        performed with a systematically varied background. 
        """

        # Find the background sample 
        self.log('Constructing systematic variation of MM fake')
        self.fake_hists_vars = []
        for fake_weight in self.data_fake_var_weight:
            self.fake_hists_vars.append( rhf.get_histogram(
                                           self.input_data_tuple,
                                           self.fake_tuple_name,
                                           self.detector_level_variable.replace(self.detector_tuple_name, self.fake_tuple_name),
                                           self.x_axis_binning,
                                           fake_weight +self.detector_cut_weight.replace(self.detector_tuple_name, self.fake_tuple_name),
                                           hist_name="Fakes_var",
                                           )
                                        )
            # Color it the same as the main fake histogram 
            style_opts =  dhf.get_bkg_mc_style_opts( r.TColor.GetColor('#5D1362'),
                                                          r.TColor.GetColor('#5D1362')  
                                                          )

            self.fake_hists_vars[-1] =  dhf.set_style_options(self.fake_hists_vars[-1],
                                                style_opts)


        # Construct the difference between the nominal histogram and that of
        # the systematic variation. This is all that needs to be changed 
        # for the nominal sample. 
        variation_hists = [self.reco_all_hist.Clone()]
        variation_hists[0].Multiply(self.efficiency_plot)

        # Add an upward and downward variation 
        for fake_hist in self.fake_hists_vars:
            variation_hist = self.fake_hists.Clone()
            variation_hist.Add(fake_hist, -1 )

            variation_hist_down = fake_hist.Clone()
            variation_hist_down.Add(self.fake_hists, -1 )

            varied_reco_mc_hist = self.reco_all_hist.Clone()
            varied_reco_mc_hist.Add(variation_hist)
            varied_reco_mc_hist.Multiply(self.efficiency_plot)
            variation_hists.append(varied_reco_mc_hist)

            varied_reco_mc_hist = self.reco_all_hist.Clone()
            varied_reco_mc_hist.Add(variation_hist_down)
            varied_reco_mc_hist.Multiply(self.efficiency_plot)
            variation_hists.append(varied_reco_mc_hist)

        varied_unfolded_mc = [ ]
        for hist in variation_hists:
             # RooUnfold supports SVD,Iterative Bayesian and BinByBin methods
            if self.method == "SVD":
                unfolded_mc   = r.RooUnfoldSvd(self.response, hist)
            elif self.method == "BinByBin":
                unfolded_mc   = r.RooTUnfold(self.response, hist, 20 )
            else:
                #default is bayes
                unfolded_mc   = r.RooUnfoldBayes(self.response, hist, self.n_iterations, False )
    
            unfolded_mc_hist    = unfolded_mc.Hreco()
            # unfolded_mc_hist.Divide(self.acceptance_plot)
            unfolded_mc_hist.Divide(self.acceptance_plot)
            varied_unfolded_mc.append(unfolded_mc_hist)
            varied_unfolded_mc[-1].SetDirectory(0)

        return varied_unfolded_mc

    def evaluate_systematics(self):
        """
        Run the evaluation of the systematics as defined by their implementation. requires construct() and
        evaluate() to have been called.

        Systematics are stored as class (defined in Systmeatic.py) and in general are a shifted distribution
        by an amount govenered by the systematic in question. Each systematic is a combination of various
        shifts this function draws a breakdown of the individual vairations.
        """

        # Store the systematics as an ordred dit so that plotting preserves the order
        # they were stored
        self.log('Evaluating the systematics:\n',self.systematics)
        rhf.create_folder(self.output_folder+"/systematics/")
        self.output_file.cd()
        #evalute the set of possible measurements that would be found under these variations
        for syst_name in self.systematics:
            if self.systematics[syst_name].is_afii:
                self.systematics[syst_name].run(
                        response=self.response_afii,
                        nominal_unfolded=self.corr_unfolded_mc_afii.Clone(),
                        nominal_detector=self.reco_all_hist_afii,
                        efficiency=self.efficiency_plot_afii,
                        acceptance=self.acceptance_plot_afii,
                        method=self.method,
                        nominal_bs_hists=self.nominal_detector_bs_hists_afii if self.do_bootstrap else [] ,#self.nominal_unfolded_bs_hists,
                        outfile=self.output_folder+"/systematics/",
                        unfolded_x_axis_title=self.unfolded_x_axis_title,
                        lumi_scaling=self.luminosity/self.mc_luminosity_nom,
                        migration_matrix=self.migration_matrix_afii.Clone(),
                        prompt_weight=self.prompt_weight,
                        nominal_truth=self.truth_all_hist.Clone(),
                        )


            elif self.systematics[syst_name].vary_background:
                if "fake" in syst_name.lower():
                    background_vars=self.construct_fake_variation()
                    
                else:
                    background_vars=self.construct_background_varied_nominal_unfolding(
                                self.systematics[syst_name].vary_background
                            )

                self.systematics[syst_name].run_background_vars(
                        response= self.response.Clone(),
                        nominal_unfolded=background_vars[0].Clone(),
                        nominal_detector=self.reco_all_hist.Clone(),
                        efficiency=self.efficiency_plot.Clone(),
                        acceptance=self.acceptance_plot.Clone(),
                        method=self.method,
                        nominal_bs_hists=self.nominal_detector_bs_hists if self.do_bootstrap else [] ,#self.nominal_unfolded_bs_hists,
                        outfile=self.output_folder+"/systematics/",
                        unfolded_x_axis_title=self.unfolded_x_axis_title,
                        lumi_scaling=self.luminosity/self.mc_luminosity_nom,
                        migration_matrix=self.migration_matrix.Clone(),
                        background_vars=background_vars[1:]
                        )
            else:
                self.systematics[syst_name].run(
                        response= self.response.Clone(),
                        nominal_unfolded=self.corr_unfolded_mc.Clone(),
                        nominal_detector=self.reco_all_hist.Clone(),
                        efficiency=self.efficiency_plot.Clone(),
                        acceptance=self.acceptance_plot.Clone(),
                        method=self.method,
                        nominal_bs_hists=self.nominal_detector_bs_hists if self.do_bootstrap else [] ,#self.nominal_unfolded_bs_hists,
                        outfile=self.output_folder+"/systematics/",
                        unfolded_x_axis_title=self.unfolded_x_axis_title,
                        lumi_scaling=self.luminosity/self.mc_luminosity_nom,
                        migration_matrix=self.migration_matrix.Clone(),
                        prompt_weight=self.prompt_weight,
                        nominal_truth=self.truth_all_hist.Clone(),
                        )



    def draw_systematics(self, combinations):
        """
            Draw a total breakdown of the systematics evalauted. Each total 'group' of
            systematics is shown.
        """
        self.log("Drawing the systematics."  )
        self.create_grouped_systematic_plot_2(self.output_folder,combinations)
        self.draw_sys_combination_breakdowns()
        self.create_grouped_systematic_plot(self.output_folder,combinations)

        canvas = r.TCanvas(self.name + "unfolded_distro",self.name + "unfolded_distro",600,600)
        r.SetOwnership(canvas, 0)

        dhf.plot_histogram(  canvas,
                        self.reduced_syst_plots,
                        x_axis_title = self.unfolded_x_axis_title,
                        y_axis_title = "Fractional Uncertainty [%]",
                        do_atlas_labels = True,
                        labels = self.messages)

        canvas.Print(self.output_folder+"systematic_sizes.eps")
        canvas.Print(self.output_folder+"systematic_sizes.png")

    def save_object(self,obj,rename=None):
        """
            Save an object to the output file and rename it if that parameter is not
            None.

        """
        if rename != None:
            obj.SetName(rename)

        if self.save_output:
            self.output_file.cd()
            obj.Write()


    def close(self):
        """
            Close the root output file and display the output message.
        """

        self.log("Yields: ")
        for name in  self.event_yield:
            self.log(name, "   ", self.event_yield[name])

        if self.chi2 is not None:
            self.log('Chi2s\n------')
            self.log('Chi2', self.chi2)
            self.log('Reduced Chi2:', self.chi2_red)
            self.log('P-values: ' , self.p_val)
        if self.save_output:
            self.output_file.Close()

        if self.verbosity > 1:
            print("==================================================")
            self.log("Finished unfolding. ")
            print("==================================================")
            # print("--- %s seconds ---" % (time() - start_time))
