'''
    Author: Jacob Rawling
    Date:

    Generates a set of plots for th
'''
import numpy as np
import config as c
import ROOT as r
from collections import OrderedDict
import RootHelperFunctions.RootHelperFunctions as rhf
import RootHelperFunctions.StringHelperFunctions as shf
import RootHelperFunctions.DrawingHelperFunctions as dhf
from copy import deepcopy
import psutil
from time import time
import os
pid = os.getpid()
start_time = time()
last_time = time()


class GeneralPlot:
    """
        A class to store all infromation associated with a plot
        created using TTree::Draw, so the string that needs to be drawn,
        the x and y axis titles, et.c

        Used in to GeneralPlotter
    """

    def __init__(
            self,
            variable,
            x_axis_binning,
            cut_weight,
            name,
            x_axis_title,
            y_axis_title,
            log_y=False,
            force_min=None,
            normalize=False,
            divide_by_bin_width=False,
            mc_variable=None,
    ):
        self.variable = variable
        self.mc_variable = mc_variable
        if mc_variable is None:
            self.mc_variable = variable
        self.x_axis_binning = x_axis_binning
        self.cut_weight = cut_weight
        self.name = name
        self.x_axis_title = x_axis_title
        self.y_axis_title = y_axis_title
        self.sample_histograms = {}
        self.sample_unweighted_histograms = {}
        self.mc_stack = None
        self.total_mc = None
        self.total_data = None
        self.total_fakes = None
        self.event_yield = {}
        self.log_y = log_y
        self.force_min = None  # force_min
        self.total_unweighted_mc = None
        self.normalize = normalize
        self.divide_by_bin_width = divide_by_bin_width

    def plot_dictionary(self, data_name="Data"):
        """
        Converts the stored mc_stack, total_mc and total_data
        histograms into a dictionary of resultss
        """

        plot_dict = OrderedDict()
        # Format and draw the histograms neatly, showing the ratio w.r.t MC
        if self.total_data is not None:
            data_ratio_style_options = deepcopy(dhf.data_ratio_style_options)

            data_ratio_style_options.draw_options = "HIST E1"
            data_ratio_style_options.x_axis_label_offset = 0.05
            data_ratio_style_options.x_label_size = 0.135
            data_ratio_style_options.y_label_size = 0.135

            data_ratio_style_options.y_title_size = None
            data_ratio_style_options.y_title_size = 0.135
            data_ratio_style_options.y_title_offset = 0.45

            data_ratio_style_options.x_title_size = 0.135
            data_ratio_style_options.x_title_offset = 1.1

            plot_dict[data_name] = [
                rhf.normalize_histogram(
                    self.total_data, divide_by_bin_width=self.divide_by_bin_width) if self.normalize else self.total_data,
                dhf.data_style_options,
                data_ratio_style_options,
                "Total MC"
            ]

        if self.total_mc is not None:
            mc_ratio_style_options = dhf.data_ratio_style_options
            mc_ratio_style_options.draw_options = "HIST E1"
            # mc_ratio_style_options.legend_options = None
            mc_ratio_style_options.x_axis_label_offset = 0.05
            mc_ratio_style_options.x_label_size = 0.135
            mc_ratio_style_options.y_label_size = 0.135

            mc_ratio_style_options.y_title_size = 0.135
            mc_ratio_style_options.y_title_offset = 0.45

            mc_ratio_style_options.x_title_size = 0.135
            mc_ratio_style_options.x_title_offset = 1.1

            # mc_ratio_style_options.x_label_size = None
            # mc_ratio_style_options.y_label_size = None
            # mc_ratio_style_options.y_title_offset = None
            # mc_ratio_style_options.x_title_offset = None

            plot_dict["Total MC"] = [
                rhf.normalize_histogram(
                    self.total_mc, divide_by_bin_width=self.divide_by_bin_width) if self.normalize else self.total_mc,
                dhf.get_sig_mc_style_opts(),
                mc_ratio_style_options,
                None
            ]

        # Force the minimum of all plots to allow for visiblilty of negative
        # Fakes
        if self.force_min is not None:
            for name in plot_dict:
                plot_dict[name][0].SetMinimum(self.force_min)
            self.mc_stack.SetMinimum(self.force_min)

        return plot_dict

    def create_latex_table(self, output_folder, name):
        """

        """
        latex_table_head = "\\begin{table}\n \\centering \n \\begin{tabular}{|l|r|}\n \\hline\n"
        table_content = ""
        for sample_name in self.sample_histograms:
            table_content += sample_name + \
                " & %.1f" % self.sample_histograms[
                    sample_name].Integral() + "\\\\\n"
        if self.total_fakes is not None:
            table_content += "Fakes" + " & %.1f" % self.total_fakes.Integral() + "\\\\\n"

        table_content += "\\hline"
        table_content += "Total MC" + " & %.1f" % self.total_mc.Integral() + "\\\\\n"
        table_content += "Data" + " & %.1f" % self.total_data.Integral() + "\\\\\n"
        table_content += "\\hline \\hline Data/MC" + \
            " & %.2f" % (self.total_data.Integral() /
                         self.total_mc.Integral()) + "\\\\\n"
        table_footer = "\\hline \n \\end{tabular}\n \\caption{Yield table}\n \\label{tab:}\n \\end{table}\n"
        tex_file = open(output_folder + name + '.tex', 'w')
        tex_file.write(latex_table_head)
        tex_file.write(table_content)
        tex_file.write(table_footer)
        tex_file.close()

    def plot_unweighted_dictionary(self, data_name="Data"):
        """
        Converts the stored mc_stack, total_mc and total_data
        histograms into a dictionary of resultss
        """
        plot_dict = OrderedDict()

        # Format and draw the histograms neatly, showing the ratio w.r.t MC
        if self.total_data is not None:
            data_ratio_style_options = deepcopy(dhf.data_ratio_style_options)
            # data_ratio_style_options.x_axis_label_offset =0.05
            # data_ratio_style_options.x_label_size =0.135

            plot_dict[data_name] = [
                self.total_data,
                dhf.data_style_options,
                dhf.data_ratio_style_options,
                "Total MC"
            ]

        if self.total_unweighted_mc is not None:
            mc_ratio_style_options = dhf.data_ratio_style_options
            mc_ratio_style_options.draw_options = "HIST E1"
            # mc_ratio_style_options.legend_options = None
            # mc_ratio_style_options.x_axis_label_offset =0.05
            # mc_ratio_style_options.x_label_size =0.135

            plot_dict["Total MC"] = [
                self.total_unweighted_mc,
                dhf.get_sig_mc_style_opts(),
                mc_ratio_style_options,
                None
            ]
        return plot_dict


class GeneralPlotter:
    """
    A general class for making reconstruction level control plots that
    supports all backgrounds

    The Aim is to quickly and cleanly examine the data/MC ratio formmany
    different variables
    """

    def __init__(self, output_folder, MC_samples, data_samples=None,
                 luminosity=120000, tuple_name="nominal",
                 # Sherpa has a few extremely high weighted events, we're going to cut those out
                 # this is formally incorrect, howeverin the low stats limit it is better than the alteranitve
                 # of including these events and having a random region of phase space artificially enhanced due to a high
                 # stat uncertainty
                 mc_event_weight="weight_mc*weight_leptonSF*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup",
                 prompt_weight="*(Alt$(mu_true_isPrompt[0], Alt$(el_true_isPrompt[0], -1))==1)",
                 nonprompt_weight="*(Alt$(mu_true_isPrompt[0], Alt$(el_true_isPrompt[0], 0))==0)",
                 data_event_weight="1.0", name="control_plots",
                 truth_tuple=None,
                 messages=[],
                 cut_weight="",
                 debug_mode=False,
                 fake_tuple_name="nominal_Loose",
                 data_fake_weight="weight_mm",
                 fake_estimation_method='mc',
                 MC_fake_samples=[]
                 ):
        """
        Configure the tool upon initialization providing all the details
        of samples, etc. required for future pllotting.

        @Parameters
        output_folder: string to a folder where output will be stored
        MC_samples:    all samples to consider, stored in the format of the Sample
                       class detailed in TopAnalysis/Backgrounds.py
        data_Samples:  Data tuples to consider, if None then Pseudo-data is used
                       Which is MC
        luminosity: Luminostiy of data in pb
        """
        self.output_folder = output_folder + "/" + name + "/"
        rhf.create_folder(self.output_folder)

        self.MC_samples = MC_samples
        self.data_samples = data_samples

        # Adictionary of plots that will be constructe
        self.variable_info = []
        self.verbosity = 1
        self.luminosity = luminosity
        self.tuple_name = tuple_name
        self.fake_tuple_name = fake_tuple_name
        self.use_pseudo_data = True if data_samples is None else False
        self.data_name = "Pseudo-data" if data_samples is None else "Data"
        # Store default weights for drawing data nad mc
        self.mc_event_weight = mc_event_weight
        self.data_event_weight = data_event_weight
        self.data_fake_weight = data_fake_weight

        # We apply cuts using weights, e.g multiple the weight by
        # (top_pt/1e3 >200) to cut on events with less top pT <  200 GeV
        self.cut_weight = cut_weight

        # It's useful to assign each instance of the tool a name
        # To help with things like output foldres and root names
        self.name = name

        # Optionally allow access to truth level information
        self.truth_tuple = truth_tuple

        # set ROOT options to suppress annoying output
        r.gROOT.SetBatch()
        rhf.hide_root_infomessages()
        self.messages = messages

        # create a root file to save all output to aswell
        self.examine_mc_stats = False

        # Whether we are running on the first 10 files in a set of files
        self.debug_mode = debug_mode
        self.last_time = time()
        #
        self.prompt_weight = prompt_weight
        self.nonprompt_weight = nonprompt_weight
        self.fake_estimation_method = fake_estimation_method
        self.MC_fake_samples = MC_fake_samples

    def add_variable(self, variable):
        """
        Store a new variable for plotting s
        """
        assert isinstance(
            variable, GeneralPlot), "ERROR: variable must be an instnace of 'GeneralPlot' "

        # Alert the user of the basic settings of the tool
        # if this annoys you just set verbosity to 0
        # self.log("Adding info for new plot: ", variable.name)

        self.variable_info.append(variable)

    def log(self, *messages):
        """
        Outputs message to print stream with the prefix indicating the
        origin of the message, and only if the verbosity of the tool
        is greater than zero.
        """
        if self.verbosity > 0:

            py = psutil.Process(pid)
            # py.memory_info()[0]/2.**30  # memory use in GB...I think
            memoryUse = 0.0
            curr_time = time()
            total_message = "[ TA-PLOT ( %.2f|%.2f|%.4fGB) ] - " % (
                curr_time - start_time, curr_time - self.last_time, memoryUse)
            self.last_time = time()
            for msg in messages:
                total_message += " " + str(msg)
            print(total_message)

    def construct_mc_driven_fakes(self, variable):
        """
        For an individual variable create the required histograms
        for plotting
        """
        mc_hists, mc_unweighted_hists = [], []
        self.reco_plots = OrderedDict()
        self.reco_unweighted_mc_plots = OrderedDict()
        variable.total_fakes = None
        variable.fake_hists = {}

        for sample in self.MC_samples + self.MC_fake_samples:
            self.log(
                ' Constructing fake estimate through MC method of sample ', sample.name)

            # get the reco distirubtion
            total_hist = None
            total_hist_mc_stats = None
            for i, file_list in enumerate(sample.input_file_names):
                # Skip files that we are missing
                if len(file_list) == 0:
                    continue

                if self.debug_mode and i > 2:
                    continue

                # Draw the histogram for this set of samples
                hist = rhf.get_histogram(
                    [file_list],
                    self.tuple_name,
                    variable.mc_variable,
                    variable.x_axis_binning,
                    self.mc_event_weight +
                    self.nonprompt_weight +
                    self.cut_weight +
                    variable.cut_weight,
                    hist_name=self.name + "_" +
                    variable.name + "_mc_" + str(i),
                    friend_name=self.truth_tuple,
                    debug_mode=self.debug_mode
                )

                # Normalize the histogram by first reading the total amount of
                # MC there
                nEvents = 0
                sum_weights_chain = r.TChain("sumWeights")
                for f in file_list:
                    sum_weights_chain.Add(f)
                for e in sum_weights_chain:
                    nEvents += e.totalEventsWeighted

                if nEvents <= 0:
                    self.log("ERROR: sample has no SumWeights. ", file_list)
                    self.log(' Skippping. ')
                    continue

                mc_luminosity_nom = nEvents / \
                    (sample.x_secs[i] * sample.k_factors[i])

                # Rescale to the expected luminsity based on the data here
                hist.Scale(self.luminosity / mc_luminosity_nom)

                if total_hist is None:
                    total_hist = hist.Clone()
                else:
                    total_hist.Add(hist)

            variable.fake_hists[sample.name + '_fakes'] = total_hist.Clone()
            self.log('     ', sample.name, ' fake contribution: ',
                     total_hist.Integral())

            if variable.total_fakes is None:
                variable.total_fakes = total_hist.Clone()
            else:
                variable.total_fakes .Add(total_hist)

        variable.total_fakes = dhf.set_style_options(
            variable.total_fakes,
            dhf.get_bkg_mc_style_opts(r.TColor.GetColor(
                '#5D1362'), r.TColor.GetColor('#5D1362'))
        )
        variable.total_fakes.SetName("Fakes")
        return variable

    def construct_mc_plot(self, variable):
        """
        For an individual variable create the required histograms
        for plotting
        """
        mc_hists, mc_unweighted_hists = [], []
        self.reco_plots = OrderedDict()
        self.reco_unweighted_mc_plots = OrderedDict()
        for sample in self.MC_samples:
            self.log(' Construction prediction for sample: ', sample.name)


            # get the reco distirubtion
            total_hist = None
            total_hist_mc_stats = None
            for i, file_list in enumerate(sample.input_file_names):

                small_weight_saftey = ''
                for file_name in file_list:
                    if 'sherpa' in file_name.lower():
                        small_weight_saftey = '*(abs(weight_mc)<1.3)'
                        self.log('Sherpa sample detected in ', sample.name, '. Restricting weight to < 50')

                # Skip files that we are missing
                if len(file_list) == 0:
                    continue

                if self.debug_mode and i > 2:
                    continue

                # Draw the histogram for this set of samples
                hist = rhf.get_histogram(
                    [file_list],
                    self.tuple_name,
                    variable.mc_variable,
                    variable.x_axis_binning,
                    self.mc_event_weight +
                    self.prompt_weight +
                    self.cut_weight +
                    small_weight_saftey +
                    variable.cut_weight,
                    hist_name=self.name + "_" +
                    variable.name + "_mc_" + str(i),
                    friend_name=self.truth_tuple,
                    debug_mode=self.debug_mode
                )

                # Get the histogram which contains the unweighted MC events
                # we can use this to understand whether our expected number of
                # events exceeds the MC stats substantially or not
                if self.examine_mc_stats:
                    hist_mc_unweighted = rhf.get_histogram(
                        [file_list],
                        self.tuple_name,
                        variable.mc_variable,
                        variable.x_axis_binning,
                        "1.0" +
                        self.cut_weight +
                        variable.cut_weight,
                        hist_name=self.name + "_" + variable.name +
                        "_mcunweighted__" + str(i),
                        friend_name=self.truth_tuple,
                        debug_mode=self.debug_mode

                    )

                # Normalize the histogram by first reading the total amount of
                # MC there
                nEvents = 0
                sum_weights_chain = r.TChain("sumWeights")
                for f in file_list:
                    sum_weights_chain.Add(f)
                for e in sum_weights_chain:
                    nEvents += e.totalEventsWeighted

                if nEvents <= 0:
                    self.log("ERROR: sample has no SumWeights. ", file_list)
                    self.log(' Skippping. ')
                    continue

                mc_luminosity_nom = nEvents / \
                    (sample.x_secs[i] * sample.k_factors[i])

                # Rescale to the expected luminsity based on the data here
                hist.Scale(self.luminosity / mc_luminosity_nom)

                # Store the expected number of events for this sample
                variable.event_yield[sample.name] = hist.Integral()

                if total_hist is None:
                    total_hist = hist.Clone()
                    if self.examine_mc_stats:
                        total_hist_mc_stats = hist_mc_unweighted.Clone()
                else:
                    total_hist.Add(hist)
                    if self.examine_mc_stats:
                        total_hist_mc_stats.Add(hist_mc_unweighted)

            # Formate the total histogram for this sample
            variable.sample_histograms[sample.name] = dhf.set_style_options(
                total_hist.Clone(),
                sample.style_options
            )

            # The name of the histograms will be used as their label in the
            # legend so set it to something clear.
            variable.sample_histograms[sample.name].SetName(sample.name)

            if self.examine_mc_stats:
                variable.sample_unweighted_histograms[sample.name] = dhf.set_style_options(
                    total_hist_mc_stats.Clone(),
                    sample.style_options
                )
                variable.sample_unweighted_histograms[
                    sample.name].SetName(sample.name + " Unweighted")
                mc_unweighted_hists.append(
                    variable.sample_unweighted_histograms[sample.name])

        # We want THStack of all histograms, so we can draw them broekn down
        for sample in self.MC_samples:
            # Store so we can sort it by Integral and put it into a stack
            mc_hists.append(variable.sample_histograms[sample.name])

        if self.fake_estimation_method == 'mm':
            variable = self.construct_datadriven_fakes_plot(variable)
            variable.event_yield["Fakes"] = variable.total_fakes.Integral()
            print 'Fake yield: ', variable.event_yield["Fakes"]
            mc_hists.append(variable.total_fakes)

        elif self.fake_estimation_method == 'mc':
            variable = self.construct_mc_driven_fakes(variable)
            variable.event_yield["Fakes"] = variable.total_fakes.Integral()
            print 'Fake yield: ', variable.event_yield["Fakes"]
            mc_hists.append(variable.total_fakes)

        variable.mc_stack = rhf.get_sorted_stack("AllMCTemp", mc_hists)
        variable.total_mc = variable.mc_stack.GetStack(
        ).Last().Clone("total_mc_" + variable.name)

        norm_fact = variable.total_mc.Integral()
        mc_hists = []
        for sample in self.MC_samples:
            if variable.normalize:
                variable.sample_histograms[sample.name].Scale(1.0 / norm_fact)
            if variable.divide_by_bin_width:
                for i in xrange(1, variable.sample_histograms[sample.name].GetXaxis().GetNbins() + 1):
                    variable.sample_histograms[sample.name].SetBinContent(
                        i,
                        variable.sample_histograms[sample.name].GetBinContent(
                            i) / variable.sample_histograms[sample.name].GetBinWidth(i)
                    )
            # Store so we can sort it by Integral and put it into a stack
            mc_hists.append(variable.sample_histograms[sample.name])

        # Add the fakes again after we've noramlized
        if self.fake_estimation_method == 'mm':
            variable = self.construct_datadriven_fakes_plot(variable)
            mc_hists.append(variable.total_fakes)
        elif self.fake_estimation_method == 'mc':
            variable = self.construct_mc_driven_fakes(variable)
            mc_hists.append(variable.total_fakes)

        self.mc_hists = mc_hists
        variable.mc_stack = rhf.get_sorted_stack("All MC", mc_hists)
        r.SetOwnership(variable.mc_stack, 0)

        if self.examine_mc_stats:
            variable.mc_unweighted_stack = rhf.get_sorted_stack(
                "All MC", mc_unweighted_hists)
            variable.total_unweighted_mc = variable.mc_unweighted_stack.GetStack(
            ).Last().Clone("total_mc_" + variable.name)
            r.SetOwnership(variable.mc_unweighted_stack, 0)

        if variable.force_min is not None:
            variable.total_mc.SetMinimum(variable.force_min)
            if self.examine_mc_stats:
                variable.total_unweighted_mc.SetMinimum(variable.force_min)

        return variable

    def construct_datadriven_fakes_plot(self, variable):
        """
        Takes a generic plotting info and creates the data, or pseudo data,
        distirbution
        """
        if self.use_pseudo_data:
            self.log("Using pseudo-data")
            variable.total_data = rhf.clear(
                variable.total_mc.Clone(variable.name + "_pseudodata"))
            variable.total_data.SetMinimum(0.0)
            return variable
        self.log("Constructing fakes histogram from files: \n", self.data_samples)

        total_hist = None
        # get the reco distirubtion
        for i, file_list in enumerate(self.data_samples):
            # Skip files that we are missing
            if len(file_list) == 0:
                continue

            # Draw the histogram for this set of samples
            hist = rhf.get_histogram(
                [file_list],
                self.fake_tuple_name,
                variable.variable.replace(
                    self.tuple_name, self.fake_tuple_name),
                variable.x_axis_binning,
                self.data_fake_weight +
                self.cut_weight.replace(self.tuple_name, self.fake_tuple_name) +
                variable.cut_weight.replace(
                    self.tuple_name, self.fake_tuple_name),
                hist_name=self.name + "_" + variable.name + "_fake_" + str(i),
            )

            if total_hist is None:
                total_hist = hist.Clone()
            else:
                total_hist.Add(hist)

        variable.total_fakes = total_hist
        variable.total_fakes = dhf.set_style_options(
            variable.total_fakes,
            dhf.get_bkg_mc_style_opts(r.TColor.GetColor(
                '#5D1362'), r.TColor.GetColor('#5D1362'))
        )
        variable.total_fakes.SetName("Fakes")
        return variable

    def construct_data_plot(self, variable):
        """
        Takes a generic plotting info and creates the data, or pseudo data,
        distirbution
        """
        if self.use_pseudo_data:
            self.log("Using pseudo-data")
            variable.total_data = variable.total_mc.Clone(
                variable.name + "_pseudodata")
            variable.total_data.SetMinimum(0.0)
            return variable
        self.log("Constructing data histograms..")

        total_hist = None
        # get the reco distirubtion
        for i, file_list in enumerate(self.data_samples):
            # Skip files that we are missing
            if len(file_list) == 0:
                continue

            # Draw the histogram for this set of samples
            hist = rhf.get_histogram(
                [file_list],
                self.tuple_name,
                variable.variable,
                variable.x_axis_binning,
                self.data_event_weight +
                self.cut_weight +
                variable.cut_weight,
                hist_name=self.name + "_" + variable.name + "_data_" + str(i),

            )
            if total_hist is None:
                total_hist = hist.Clone()
            else:
                total_hist.Add(hist)

        variable.total_data = total_hist
        return variable

    def draw_ratio_plot(self, variable):
        """
        Taking the nformaiton stored in construct_mc_plot and create_data_plot
        construct a ratio plot that shows a full break down of the mc
        as well as the raito of the predicioton and MC and associated
        data stat  error.

        @params:
        variable: a GeneralPlot that has had it's mc_stack, and total_mc
        and total_data histograms filled.
        """
        self.out_file = rhf.open_file(
            self.output_folder + variable.name + "_general_plots.root", "UPDATE")
        canvas = r.TCanvas(self.name + variable.name + "_distribution",
                           self.name + variable.name + "_distribution", 600, 600)

        plotting_dictionary = variable.plot_dictionary(
            data_name=self.data_name)

        ratio_histograms = dhf.ratio_plot(canvas,
                                          plotting_dictionary,
                                          x_axis_title=variable.x_axis_title,
                                          y_axis_title=variable.y_axis_title,
                                          messages=self.messages,
                                          leg_font_size=0.032,
                                          ratio_y_axis_title="#frac{Data}{Prediction}",
                                          set_log_y=variable.log_y,
                                          additional_stack=variable.mc_stack,
                                          # ratio_max_y=1.25,
                                          # ratio_min_y=0.75,
                                          )

        uncertainty_graphs_for_ratio_plot = {}

        uncertainty_graphs_for_ratio_plot["Stat."] = [
            rhf.extract_error_at_unity(
                rhf.extract_stat_error(plotting_dictionary["Data"][0].Clone())
            ),
            dhf.get_uncert_graph_stlye_opt(show_border=False),
            dhf.get_uncert_graph_stlye_opt(is_ratio=True, show_border=False)
        ]

        ratio_histograms = dhf.ratio_plot(canvas,
                                          plotting_dictionary,
                                          x_axis_title=variable.x_axis_title,
                                          y_axis_title=variable.y_axis_title,
                                          ratio_histograms=uncertainty_graphs_for_ratio_plot,
                                          messages=self.messages,
                                          leg_font_size=0.045,
                                          ratio_y_axis_title="#frac{Data}{Prediction}",
                                          set_log_y=variable.log_y,
                                          additional_stack=variable.mc_stack,
                                          ratio_max_y=1.1,
                                          ratio_min_y=0.8,
                                          )

        self.out_file.cd()
        for name in plotting_dictionary:
            plotting_dictionary[name][0].Write()
        # Save these plots
        canvas.Print(self.output_folder + variable.name + ".eps")
        canvas.Print(self.output_folder + variable.name + ".png")
        canvas.Write()

        for hist in self.mc_hists:
            hist.Write()
        if self.fake_estimation_method == 'mc':
            for name in variable.fake_hists:
                variable.fake_hists[name].SetName(name)
                variable.fake_hists[name].Write()

        self.out_file.Close()

    def close(self):
        """

        """
        # self.out_file.Close()

    def create_plots(self, name=None):
        """
        Create and draw plots that have been requested to be drawn
        """

        # For each variable we wish to create the plot, draw it and
        # free the plot from memory in one go.
        for var_info in self.variable_info:
            if name is not None:
                if name != var_info.name:
                    continue

            self.log("Constructing mc plots: ", var_info.name)
            self.log("     Cut: ", var_info.cut_weight)
            self.log("     x-axis title: ", var_info.x_axis_title)
            self.log("     y-axis title: ", var_info.y_axis_title)
            self.log("     Using Pseudo-data: ", self.use_pseudo_data)
            self.log("     Output folder: ", self.output_folder)
            self.log("     Deubg mode:", self.debug_mode)
            var_info = self.construct_mc_plot(var_info)

            self.log("Constructing data plots: ", var_info.name)
            var_info = self.construct_data_plot(var_info)

            self.log(" Drawing variable: ", var_info.name)
            self.draw_ratio_plot(var_info)

            var_info.create_latex_table(self.output_folder, var_info.name)
            self.log(" Saved: ", var_info.name)
