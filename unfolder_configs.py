"""

Definition of signal and control regions that are being unfolded in terms of cuts based on 
ntuple variables, see AnalysisTop cut file for more exact details on the definition of booleans. 



"""
from TopAnalysis.Unfolding.Unfolder import Unfolder
import config as c
from TopAnalysis.Backgrounds import ttbar_samples, ttbar_fastsim_samples,ttbar_debug_samples, ttbar_debug_fastsim_samples, ttbar_pdf_samples, ttbar_allhad_samples
import numpy as np  


def load_mu_pt(load_unfolder,
               additional_samples,
               background_samples,
               np_background_samples=[],
               aux_settings="",
               verbosity=1):
    """
    Basic curve of muon pT 
    """    
    name = "mu_pt"
    truth_weight, detector_weight = None, None
    input_mc = c.input_mc_tuple
    if "pdf" in aux_settings.lower():
        background_samples = []
        input_mc = c.input_pdf_tuple
        truth_weight = "mc_generator_weights[11]"
        detector_weight = "mc_generator_weights[11]*weight_leptonSF*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup"
        name += "_pdf"

    if "fastsim" in aux_settings.lower():
        input_mc = c.input_fastsim_mc_tuple
        name += "_AFII"

    reco_cuts  = ["(nominal.mujets_2015 || nominal.mujets_2016 )"]
    reco_cuts.append("(Length$(nominal.jet_pt)==5)")

    truth_cuts = ["(particleLevel.mujets_particle)"]
    truth_cuts.append("(Length$(particleLevel.jet_pt)==5)")

    return Unfolder(
                ttbar_samples,        #MC that we base the unfolding on
                c.input_data_tuple,      #data tuple that we want to unfold
                c.output_folder,         #Where all ROOT  and  eps will be put
                "particleLevel.mu_pt[0]/1e3",                 #Truth level variable name that will be unfolded to
                "nominal.mu_pt[0]/1e3",                 #Detector level variable name that will be unfolded from
                # A better binning that achieves a reasonable result of the pull test
                [30,50,75,100,140,180,225,300],
                method="bayes",        # we're going to use iterative bayesian
                truth_tuple_name = "particleLevel",
                detector_tuple_name = "nominal",
                data_tuple_is_mc = True, # say that we are in test mode and therefiore the data tuple is infact MC
                save_output = True,       # we want everything to be shoved to a ROOT file
                x_axis_label = "Measured leading muon p_{T} [GeV]",
                y_axis_label = "Truth leading muon p_{T} [GeV]",
                unfolded_x_axis_title = "leading muon p_{T} [GeV]",
                unfolded_y_axis_title = "#frac{1}{#sigma_{0}} #frac{d#sigma}{p_{T,lepton}}",
                messages=["Fiducial phase space"],
                truth_cuts=truth_cuts,
                detector_cuts=reco_cuts,
                name=name,
                n_iterations=7,
                set_log_y=True,
                verbosity=verbosity,
                load_unfolder=load_unfolder,
                additional_samples=additional_samples,
                background_samples=background_samples,
                luminosity=c.luminosity,
                truth_weight=truth_weight,
                detector_weight=detector_weight

               )

def load_el_phi(load_unfolder,
               additional_samples,
               background_samples,
               np_background_samples=[],
               aux_settings="",
               verbosity=1):
    """
    Basic electron phi distirution, primarily here for validaiton purposes. 
    """ 

    name = "el_phi"
    truth_weight, detector_weight = None, None
    input_mc = c.input_mc_tuple
    if "pdf" in aux_settings.lower():
        background_samples = []
        input_mc = c.input_pdf_tuple
        truth_weight = "mc_generator_weights[11]"
        detector_weight = "mc_generator_weights[11]*weight_leptonSF*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup"
        name += "_pdf"

    if "fastsim" in aux_settings.lower():
        input_mc = c.input_fastsim_mc_tuple
        name += "_AFII"

    reco_cuts  = ["(nominal.ejets_2015 || nominal.ejets_2016 )"]
    reco_cuts.append("(Length$(nominal.jet_pt)==5)")

    truth_cuts = ["(particleLevel.ejets_particle)"]
    truth_cuts.append("(Length$(particleLevel.jet_pt)==5)")

    return Unfolder(input_mc,        #MC that we base the unfolding on
                c.input_data_tuple,      #data tuple that we want to unfold
                c.output_folder,         #Where all ROOT  and  eps will be put
                "particleLevel.el_phi/3.14159",   #Truth level variable name that will be unfolded to
                "nominal.el_phi/3.14159",         #Detector level variable name that will be unfolded from
                np.linspace(-1,1,10).tolist(), 
                method = "bayes",        # we're going to use iterative bayesian
                truth_tuple_name = "particleLevel",
                detector_tuple_name = "nominal",
                data_tuple_is_mc = True, #say that we are in test mode and therefiore the data tuple is infact MC
                save_output = True,       # we want everything to be shoved to a ROOT file
                x_axis_label = "Measured leading electron |#phi/#pi|",
                y_axis_label = "Truth leading electron |#phi/#pi|",
                unfolded_x_axis_title = "leading electron |#phi/#pi|",
                unfolded_y_axis_title = "#frac{1}{#sigma_{0}} #frac{d#sigma}{d|#phi/#pi|}",
                messages = ["Fiducial phase space", "Control Region"],
                truth_cuts = truth_cuts,
                detector_cuts = reco_cuts,
                name=name,
                n_iterations = 8,
                set_log_y = False,
                load_unfolder=load_unfolder,
                additional_samples=additional_samples,
                background_samples=background_samples,
                luminosity=c.luminosity,
                truth_weight=truth_weight,
                detector_weight=detector_weight

               )


def load_control_region(load_unfolder,
               additional_samples,
               background_samples,
               np_background_samples=[],
               aux_settings="",
               verbosity=1):
    """
    Returns an unfolder configured to a well defined control region configured for
    leptonic top measurement. 
    """
    name = "Leptonic_Asymmetry_CR"
    truth_weight, detector_weight = None, None
    input_mc = c.input_mc_tuple
    from TopAnalysis.Backgrounds import ttbar_samples, ttbar_pdf_samples, ttbar_fastsim_samples,ttbar_debug_samples, ttbar_debug_fastsim_samples
    input_ttbar = ttbar_samples
    if "pdf" in aux_settings.lower():
        background_samples = []
        input_ttbar = ttbar_pdf_samples
        truth_weight = "mc_generator_weights[11]"
        detector_weight = "mc_generator_weights[11]*weight_leptonSF*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup"
        name += "_pdf"
    # Frustratingly necessary
    if 'debug' in aux_settings:
        ttbar_samples = ttbar_debug_samples
        ttbar_fastsim_samples = ttbar_debug_fastsim_samples

    reco_cuts  = ["(nominal.ejets_2015 || nominal.ejets_2016 || nominal.mujets_2015 || nominal.mujets_2016)"]
    reco_cuts.append("(Length$(nominal.jet_pt)==5)")
    reco_cuts.append("(nominal.g_pt[0]/1e3 >  25)")
    reco_cuts.append("(nominal.g_pt[0]/nominal.top_lep_pt > 0.05)")
    reco_cuts.append("(nominal.top_lep_pt/1000 < 100)")
    reco_cuts.append("(nominal.theta < nominal.theta_had)")
    reco_cuts.append("(nominal.top_lep_m/1e3 > 150  && nominal.top_lep_m/1e3 < 300)")

    truth_cuts = ["(particleLevel.mujets_particle || particleLevel.ejets_particle)"]
    truth_cuts.append("(Length$(particleLevel.jet_pt)==5)")
    truth_cuts.append("(particleLevel.g_pt[0]/1e3 > 25)")
    truth_cuts.append("(particleLevel.g_pt[0]/particleLevel.top_lep_pt > 0.05)")
    truth_cuts.append("(particleLevel.top_lep_pt/1000 < 100)")
    truth_cuts.append("(particleLevel.top_lep_m/1e3 > 150 && particleLevel.top_lep_m/1e3 < 300)")
    truth_cuts.append("(particleLevel.theta < particleLevel.theta_had)")

    return Unfolder(
                input_ttbar,        #MC that we base the unfolding on
                c.input_data_tuple,      #data tuple that we want to unfold
                c.output_folder,         #Where all ROOT  and  eps will be put
                "(particleLevel.theta[0]-particleLevel.theta_d)/(particleLevel.theta[0]+particleLevel.theta_d)",                 #Truth level variable name that will be unfolded to
                "(nominal.theta[0]-nominal.theta_d)/(nominal.theta[0]+nominal.theta_d)",                 #Detector level variable name that will be unfolded from
                [-1.0, 0.0508475, 0.322034, 0.457627, 0.59322, 0.661017, 0.728814, 0.79661, 0.864407, 1.0],
                method="bayes",        # we're going to use iterative bayesian
                truth_tuple_name = "particleLevel",
                detector_tuple_name = "nominal",
                data_tuple_is_mc = True, # say that we are in test mode and therefiore the data tuple is infact MC
                save_output = True,       # we want everything to be shoved to a ROOT file
                x_axis_label="Measured A_{#theta}",
                y_axis_label="Truth A_{#theta}",
                unfolded_x_axis_title = "Leptonic A_{#theta}",
                unfolded_y_axis_title = "#frac{1}{#sigma_{0}} #frac{d#sigma}{dA_{#theta}}",
                messages=["Fiducial phase space", "Validation region"],
                truth_cuts=truth_cuts,
                detector_cuts=reco_cuts,
                name=name,
                n_iterations=8,
                set_log_y=True,
                verbosity=verbosity,
                load_unfolder=load_unfolder,
                additional_samples=additional_samples,
                background_samples=background_samples,
                luminosity=c.luminosity,
                truth_weight=truth_weight,
                detector_weight=detector_weight,
                np_background_samples=np_background_samples,
                do_bootstrap=False,
                signal_fastsim_sample=ttbar_fastsim_samples
               )

def load_hadronic_control_region(load_unfolder,
               additional_samples,
               background_samples,
               np_background_samples=[],
               aux_settings="",
               verbosity=1):
    """
    Returns an unfolder configured to a well defined control region configured for
    leptonic top measurement. 
    """
    name = "hadronic_control_region"
    truth_weight, detector_weight = None, None
    input_mc = c.input_mc_tuple
    from TopAnalysis.Backgrounds import ttbar_samples, ttbar_pdf_samples, ttbar_fastsim_samples,ttbar_debug_samples, ttbar_debug_fastsim_samples
    input_ttbar = ttbar_samples
    if "pdf" in aux_settings.lower():
        background_samples = []
        input_ttbar = ttbar_pdf_samples
        truth_weight = "mc_generator_weights[11]"
        detector_weight = "mc_generator_weights[11]*weight_leptonSF*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup"
        name += "_pdf"

    if 'debug' in aux_settings:
        input_ttbar = ttbar_debug_samples
        ttbar_fastsim_samples = ttbar_debug_fastsim_samples
        c.input_data_tuple =[c.input_data_tuple[0][0:]]
        name+="_debug"
        
    # HadSR_topPt110_minTopM150_mTopW150_thetaD100_gPT25
    reco_cuts  = ["(nominal.ejets_2015 || nominal.ejets_2016 || nominal.mujets_2015 || nominal.mujets_2016)"]
    reco_cuts.append("(Length$(nominal.jet_pt)==5)")
    reco_cuts.append("(nominal.g_pt[0]/1e3 >  25)")
    reco_cuts.append("(nominal.g_pt[0]/nominal.top_had_pt > 0.05)")
    reco_cuts.append("(nominal.top_lep_pt/1000 < 110)")
    reco_cuts.append("(nominal.theta_d < 1.0)")
    reco_cuts.append("(nominal.theta > nominal.theta_had)")
    reco_cuts.append("(nominal.top_had_m/1e3 > 150  && nominal.top_had_m/1e3 < 300)")

    # NOTE: SEEMS THAT MASS CONTSTRAINTS DON'T HELP TOO MUCH
    truth_cuts = ["(particleLevel.mujets_particle || particleLevel.ejets_particle)"]
    truth_cuts.append("(Length$(particleLevel.jet_pt)==5)")
    truth_cuts.append("(particleLevel.g_pt[0]/1e3 > 25)")
    truth_cuts.append("(particleLevel.g_pt[0]/particleLevel.top_had_pt > 0.05)")
    truth_cuts.append("(particleLevel.top_lep_pt/1000 < 110)")
    truth_cuts.append("(particleLevel.theta_d < 1.0)")
    truth_cuts.append("(particleLevel.top_had_m/1e3 > 150 && particleLevel.top_had_m/1e3 < 300)")
    truth_cuts.append("(particleLevel.theta > particleLevel.theta_had)")

    return Unfolder(
                input_ttbar,        #MC that we base the unfolding on
                c.input_data_tuple,      #data tuple that we want to unfold
                c.output_folder,         #Where all ROOT  and  eps will be put
                "(particleLevel.theta_had[0]-particleLevel.theta_d_had)/(particleLevel.theta_had[0]+particleLevel.theta_d_had)",                 #Truth level variable name that will be unfolded to
                "(nominal.theta_had[0]-nominal.theta_d_had)/(nominal.theta_had[0]+nominal.theta_d_had)",                 #Detector level variable name that will be unfolded from
                [-1,-0.25,0.0,0.25,0.5,0.7,1.0],
                method = "bayes",        # we're going to use iterative bayesian
                truth_tuple_name = "particleLevel",
                detector_tuple_name = "nominal",
                data_tuple_is_mc = True, #say that we are in test mode and therefiore the data tuple is infact MC
                save_output = True,       # we want everything to be shoved to a ROOT file
                x_axis_label = "Measured A_{#theta}",
                y_axis_label = "Truth A_{#theta}",
                unfolded_x_axis_title = "Hadronic A_{#theta}",
                unfolded_y_axis_title = "#frac{1}{#sigma_{0}} #frac{d#sigma}{dA_{#theta}}",
                messages = ["Fiducial phase space", "Control Region"],
                truth_cuts = truth_cuts,
                detector_cuts = reco_cuts,
                name=name,
                n_iterations = 8,
                verbosity=verbosity,
                set_log_y = True,
                load_unfolder=load_unfolder,
                additional_samples=additional_samples,
                background_samples=background_samples,
                luminosity=c.luminosity,
                truth_weight=truth_weight,
                detector_weight=detector_weight,
                np_background_samples=np_background_samples,
                do_bootstrap=False,
                signal_fastsim_sample=ttbar_fastsim_samples if "fastsim" in aux_settings.lower() else None
               )

def load_signal_region(load_unfolder,
               additional_samples,
               background_samples,
               np_background_samples=[],
               aux_settings="",
               verbosity=1):
    """

    """

    name = "Leptonic_Asymmetry_SR"
    truth_weight, detector_weight = None, None
    input_mc = c.input_mc_tuple
    from TopAnalysis.Backgrounds import ttbar_samples, ttbar_pdf_samples, ttbar_fastsim_samples,ttbar_debug_samples, ttbar_debug_fastsim_samples
    input_ttbar = ttbar_samples
    if "pdf" in aux_settings.lower():
        background_samples = []
        input_ttbar = ttbar_pdf_samples
        truth_weight = "mc_generator_weights[11]"
        detector_weight = "mc_generator_weights[11]*weight_leptonSF*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup"
        name += "_pdf"

    if 'debug' in aux_settings:
        input_ttbar = ttbar_debug_samples
        ttbar_fastsim_samples = ttbar_debug_fastsim_samples
        c.input_data_tuple =[c.input_data_tuple[0][:1]]
        additional_samples = []
        name+="_debug"

    #LepSR_topPt100_minTopM150_mTopW150_thetaD100_gPT25
    reco_cuts  = ["(nominal.ejets_2015 || nominal.ejets_2016 || nominal.mujets_2015 || nominal.mujets_2016)"]
    reco_cuts.append("(Length$(nominal.jet_pt)==5)")
    reco_cuts.append("(nominal.g_pt[0]/1e3 >  25)")
    reco_cuts.append("(nominal.g_pt[0]/nominal.top_lep_pt > 0.05)")
    reco_cuts.append("(nominal.top_lep_pt/1000 > 100)")
    reco_cuts.append("(nominal.theta < nominal.theta_had)")
    reco_cuts.append("(nominal.top_lep_m/1e3 > 150  && nominal.top_lep_m/1e3 < 300)")

    truth_cuts = ["(particleLevel.mujets_particle || particleLevel.ejets_particle)"]
    truth_cuts.append("(Length$(particleLevel.jet_pt)==5)")
    truth_cuts.append("(particleLevel.g_pt[0]/1e3 > 25)")
    truth_cuts.append("(particleLevel.g_pt[0]/particleLevel.top_lep_pt > 0.05)")
    truth_cuts.append("(particleLevel.top_lep_pt/1000 > 100)")
    truth_cuts.append("(particleLevel.top_lep_m/1e3 > 150 && particleLevel.top_lep_m/1e3 < 300)")
    truth_cuts.append("(particleLevel.theta < particleLevel.theta_had)")


    return Unfolder(
                input_ttbar,        #MC that we base the unfolding on
                c.input_data_tuple,      #data tuple that we want to unfold
                c.output_folder,         #Where all ROOT  and  eps will be put
                "(particleLevel.theta[0]-particleLevel.theta_d)/(particleLevel.theta[0]+particleLevel.theta_d)",                 #Truth level variable name that will be unfolded to
                "(nominal.theta[0]-nominal.theta_d)/(nominal.theta[0]+nominal.theta_d)",                 #Detector level variable name that will be unfolded from
                [-1.0, 0.0508475, 0.322034, 0.457627, 0.59322, 0.661017, 0.728814, 0.79661, 0.864407, 1.0],
                method="bayes",        # we're going to use iterative bayesian
                truth_tuple_name = "particleLevel",
                detector_tuple_name = "nominal",
                data_tuple_is_mc = True, # say that we are in test mode and therefiore the data tuple is infact MC
                save_output = True,       # we want everything to be shoved to a ROOT file
                x_axis_label="Measured A_{#theta}",
                y_axis_label="Truth A_{#theta}",
                unfolded_x_axis_title = "Leptonic A_{#theta}",
                unfolded_y_axis_title = "#frac{1}{#sigma_{0}} #frac{d#sigma}{dA_{#theta}}",
                messages=["Fiducial phase space", "Signal region"],
                truth_cuts=truth_cuts,
                detector_cuts=reco_cuts,
                name=name,
                n_iterations=8,
                set_log_y=True,
                verbosity=verbosity,
                load_unfolder=load_unfolder,
                additional_samples=additional_samples,
                background_samples=background_samples,
                luminosity=c.luminosity,
                truth_weight=truth_weight,
                detector_weight=detector_weight,
                np_background_samples=np_background_samples,
                do_bootstrap=False,
                signal_fastsim_sample=ttbar_fastsim_samples if "fastsim" in aux_settings.lower() else None
               )

def load_hadronic_signal_region(load_unfolder,
               additional_samples,
               background_samples,
               np_background_samples=[],
               aux_settings="",
               verbosity=1):
    """

    """


    name = "Hadronic_Asymmetry_SR"
    truth_weight, detector_weight = None, None
    input_mc = c.input_mc_tuple
    from TopAnalysis.Backgrounds import ttbar_samples, ttbar_pdf_samples, ttbar_fastsim_samples,ttbar_debug_samples, ttbar_debug_fastsim_samples
    input_ttbar = ttbar_samples
    if "pdf" in aux_settings.lower():
        background_samples = []
        input_ttbar = ttbar_pdf_samples
        truth_weight = "mc_generator_weights[11]"
        detector_weight = "mc_generator_weights[11]*weight_leptonSF*weight_jvt*weight_bTagSF_MV2c10_85*weight_pileup"
        name += "_pdf"

    if 'debug' in aux_settings:
        input_ttbar = ttbar_debug_samples
        ttbar_fastsim_samples = ttbar_debug_fastsim_samples
        c.input_data_tuple =[c.input_data_tuple[0][0:]]
        name+="_debug"


    # HadSR_topPt110_minTopM150_mTopW150_thetaD100_gPT25
    reco_cuts  = ["(nominal.ejets_2015 || nominal.ejets_2016 || nominal.mujets_2015 || nominal.mujets_2016)"]
    reco_cuts.append("(Length$(nominal.jet_pt)==5)")
    reco_cuts.append("(nominal.g_pt[0]/1e3 >  25)")
    reco_cuts.append("(nominal.g_pt[0]/nominal.top_had_pt > 0.05)")
    reco_cuts.append("(nominal.top_lep_pt/1000 > 110)")
    reco_cuts.append("(nominal.theta_d < 1.0)")
    reco_cuts.append("(nominal.theta > nominal.theta_had)")
    reco_cuts.append("(nominal.top_had_m/1e3 > 150  && nominal.top_had_m/1e3 < 300)")

    # NOTE: SEEMS THAT MASS CONTSTRAINTS DON'T HELP TOO MUCH
    truth_cuts = ["(particleLevel.mujets_particle || particleLevel.ejets_particle)"]
    truth_cuts.append("(Length$(particleLevel.jet_pt)==5)")
    truth_cuts.append("(particleLevel.g_pt[0]/1e3 > 25)")
    truth_cuts.append("(particleLevel.g_pt[0]/particleLevel.top_had_pt > 0.05)")
    truth_cuts.append("(particleLevel.top_lep_pt/1000 > 110)")
    truth_cuts.append("(particleLevel.theta_d < 1.0)")
    truth_cuts.append("(particleLevel.top_had_m/1e3 > 150 && particleLevel.top_had_m/1e3 < 300)")
    truth_cuts.append("(particleLevel.theta > particleLevel.theta_had)")


    return Unfolder(
                input_ttbar,        #MC that we base the unfolding on
                c.input_data_tuple,      #data tuple that we want to unfold
                c.output_folder,         #Where all ROOT  and  eps will be put
                "(particleLevel.theta_had[0]-particleLevel.theta_d_had)/(particleLevel.theta_had[0]+particleLevel.theta_d_had)",                 #Truth level variable name that will be unfolded to
                "(nominal.theta_had[0]-nominal.theta_d_had)/(nominal.theta_had[0]+nominal.theta_d_had)",                 #Detector level variable name that will be unfolded from
                [-1, 0.0508475, 0.186441, 0.322034, 0.389831, 0.457627, 0.525424, 0.559322, 0.59322, 0.627119, 0.661017, 0.694915, 0.728814, 0.762712, 0.79661, 0.864407, 1.0],#,10,15],
                method="bayes",        # we're going to use iterative bayesian
                truth_tuple_name = "particleLevel",
                detector_tuple_name = "nominal",
                data_tuple_is_mc = True, # say that we are in test mode and therefiore the data tuple is infact MC
                save_output = True,       # we want everything to be shoved to a ROOT file
                x_axis_label="Measured A_{#theta}",
                y_axis_label="Truth A_{#theta}",
                unfolded_x_axis_title = "Hadronic A_{#theta}",
                unfolded_y_axis_title = "#frac{1}{#sigma_{0}} #frac{d#sigma}{dA_{#theta}}",
                messages=["Fiducial phase space", "Signal Region"],
                truth_cuts=truth_cuts,
                detector_cuts=reco_cuts,
                name=name,
                n_iterations=8,
                set_log_y=True,
                verbosity=verbosity,
                load_unfolder=load_unfolder,
                additional_samples=additional_samples,
                background_samples=background_samples,
                luminosity=c.luminosity,
                truth_weight=truth_weight,
                detector_weight=detector_weight,
                np_background_samples=np_background_samples,
                do_bootstrap=False,
                signal_fastsim_sample=ttbar_fastsim_samples if "fastsim" in aux_settings.lower() else None
                )

def load_unfolders(unfolder_names=None, load_unfolder=None,
    additional_samples=None, aux_settings="",
    verbosity=1,
    background_samples=[],np_background_samples=[ttbar_allhad_samples]
    ):
    """
    Returns a list of unfolders based on the name 
    """

    # Make this a clear list 
    if unfolder_names is None:
        unfolder_names = []

    unfolders = []
    if any("el_phi" == name  for name in unfolder_names):
        unfolders.append(
            load_el_phi(load_unfolder,
                        additional_samples,
                        background_samples,
                        aux_settings= aux_settings,
                        verbosity=verbosity,
                        np_background_samples=np_background_samples
                        )
            )

    if any("mu_pt" == name  for name in unfolder_names):
        unfolders.append(
            load_mu_pt(load_unfolder,
                        additional_samples,
                        background_samples,
                        aux_settings= aux_settings,
                        verbosity=verbosity,
                        np_background_samples=np_background_samples
                        )
            )

        
    if any("control_region" == name  for name in unfolder_names):
        unfolders.append(
            load_control_region(load_unfolder,
                        additional_samples,
                        background_samples,
                        aux_settings= aux_settings,
                        verbosity=verbosity,
                        np_background_samples=np_background_samples
                        )
            )

    if any("signal_region" == name  for name in unfolder_names):
        unfolders.append(
            load_signal_region(load_unfolder,
                        additional_samples,
                        background_samples,
                        aux_settings= aux_settings,
                        verbosity=verbosity,
                        np_background_samples=np_background_samples
                        )
            )

    if any("hadronic_control_region" == name  for name in unfolder_names):
        unfolders.append(
            load_hadronic_control_region(load_unfolder,
                        additional_samples,
                        background_samples,
                        aux_settings= aux_settings,
                        verbosity=verbosity,
                        np_background_samples=np_background_samples
                        )
            )

    if any("hadronic_signal_region" == name  for name in unfolder_names):
        unfolders.append(
            load_hadronic_signal_region(load_unfolder,
                        additional_samples,
                        background_samples,
                        aux_settings= aux_settings,
                        verbosity=verbosity,
                        np_background_samples=np_background_samples
                        )
            )
    return unfolders
    
def print_unfolder_names():
    unfolders = ["el_phi", "mu_pt", "control_region", "signal_region",  "hadronic_control_region", "hadronic_signal_region" ]
    for unf in unfolders:
        print "            " + unf