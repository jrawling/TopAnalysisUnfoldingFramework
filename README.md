# Deadcone Unfolding Analysis

This package contains a set of scripts to perform an unfolded measurement of the collinear radiative properties of the top quark. It assumes that you have access to ntuples generated in AnalysisTop with the DeadconeAnalysis package checked out and implemented - see this twiki and this git.

## Contents
  1. [Quickstart Guide](#quickstart)
  2. [Performing the nominal unfolding](#nominal)
  3. [Running the systematics](#systematics)
  4. [Finalizing the plots and tables](#finalizing)
  

## <a name="quickstart">1. Quickstart guide</a> 

Download your ntuples and update the paths in the following files

  * TopAnalysis/Backgrounds 
  * config.py

 Then run the following scripts

  * `python perform_nominal_unfolding.py signal_region`
  * `python run_systematic.py signal_region all` ( or if you have access to a many cored macheine run `python run_all_systematics_multicore signal_region`)
  * `python finalize_plots.py` 

You will ( and should! ) verify that your ntuples behave in expected ways. This can be done by the scripts found in `ValidaitonScripts`. In the first instances it is suggested to look at basic kinematics and the amount of fake events in the selection. 

  *  ValidationScripts/EvaluateFakes.py 
  *  ValidationScripts/GenerateControlPlots.py 

## <a name="nominal">2. Performing the nominal unfolding</a> 



## <a name="systematics">3. Evaluating Systematics</a> 
### <a name="systematics">3.1 Evaluating the PDF uncertainties</a> 

The PDF variations are perfomred using the PDF4LHC variation scheeme. As such each variation is found with respect to the central value of the PDF4LHC h


## <a name="finalizing">4. Finalizing plots and tables</a> 

