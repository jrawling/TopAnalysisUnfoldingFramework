'''
    Author: Jacob Rawling
    Date:

    Utilizes the TopAnalysis.Unfolding package ( ) to create the migration matrix,
    perform the desired type of unfolding, run some tests, save the ouptut in ROOT
    and Journal ready eps fomrat
'''
from TopAnalysis.Unfolding.Unfolder import Unfolder
from TopAnalysis.Unfolding.IdentityTest import IdentityTest
from TopAnalysis.Unfolding.StressTest import StressTest
from TopAnalysis.Unfolding.ClosureTest import ClosureTest
from TopAnalysis.Unfolding.IterationConvergenceTest import IterationConvergenceTest

from TopAnalysis.FakeEvaluator import FakeEvalautor
from TopAnalysis.Unfolding.Systematic import Systematic
from TopAnalysis.SystematicsManager import SystematicsManager
from TopAnalysis.SystematicsManager.all_systs_grouped import combinations

import numpy as np
import config as c
import ROOT as r
from collections import OrderedDict
from TopAnalysis.Backgrounds import ttV_samples, background_samples
from unfolder_configs import load_unfolders, print_unfolder_names
import sys 

def main():
    # Read in the unfolding region
    unfolder_names = None
    if len(sys.argv) >= 1:
        unfolder_names = sys.argv[1]

    # Read in auxillary settings such as if we are running on AFII or if we're
    # doing  
    if len(sys.argv) >= 2:
        aux_settings = ' '.join(sys.argv[2:])
    else:
        aux_settings = ""

    debug_mode = 'debug' in aux_settings


    # Load the unfolder from the configurations we have pre-defined 
    from TopAnalysis.Backgrounds import background_samples, background_debug_samples, ttbar_allhad_samples
    if debug_mode:
        bkg_samples =[] #]background_debug_samples  
        np_bkg_samples = []# [ttbar_allhad_samples ]
        additional_samples=c.additional_debug_samples
    else:
        bkg_samples = background_samples
        additional_samples=c.additional_samples
        np_bkg_samples = [ttbar_allhad_samples ]


    unfolders = load_unfolders([unfolder_names], 
                               additional_samples=additional_samples,
                               background_samples=bkg_samples,
                               np_background_samples=np_bkg_samples,
                               aux_settings=aux_settings,
                               load_unfolder=None)
    if unfolders == []:
        print "USAGE: python2.7 perform_nominal_unfoldering <unfolder_name 1> (optional_Settings) ..."
        print "       where unfolder_name is one of the following: "
        print_unfolder_names()
        sys.exit()

    for unfolder in unfolders:

        # Choose which systematics to run over, configured correctly (hopefully) behind the
        # Scenes.
        # See SysteamaticsManager.load_systematics and Unfolding.Systematic for details
        # on whats managed. It's nice to run the closure systematic to validate everything is 
        # working as intended. 
        # unfolder.systematics =SystematicsManager.load_systematics(unfolder, selected_systematics = ["closure"])
        # unfolder.add_test( IdentityTest(name = "IdentityTest") )

        #first thing is to construct the unfolder
        unfolder.construct()

        # Now we actually perform the unfolding, save any plots
        unfolder.evaluate()
        unfolder.evaluate_systematics()
        unfolder.evaluate_total_systematic()

        # and check that everything behaviours how it should
        # unfolder.run_tests()

        # Now draw everything, tests, systematics, unfolded data, etc.
        unfolder.draw_reco_plots()
        unfolder.draw_nominal_unfolding_plots()
        unfolder.draw_afii()
        unfolder.draw_systematics(combinations)
        unfolder.draw_tests()

        #clean up ROOT stuff
        unfolder.close()
        print "Forcing exit."
        sys.exit(-1)

if __name__ == "__main__":
    main()