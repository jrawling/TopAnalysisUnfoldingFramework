#setup the environment
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
lsetup root

echo "INPUT_PATH: ", $INPUT_PATH
echo "region: ", $region
echo "PYTHONPATH: ", $PYTHONPATH
echo "plot_type:", $plot_type 


### run the command
python $INPUT_PATH/CreateControlPlot.py $region $plot_type 