#setup the environment
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
lsetup root

echo "INPUT_PATH: ", $INPUT_PATH
echo "region: ", $region
echo "PYTHONPATH: ", $PYTHONPATH


### run the command
python $INPUT_PATH/perform_nominal_unfolding.py $region fastsim nononono 
