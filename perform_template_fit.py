"""
"""
import ROOT as r 
from TopAnalysis.TemplateFitter import TemplateFitter
import config as c
from TopAnalysis.Backgrounds import background_samples
import RootHelperFunctions.RootHelperFunctions as rhf
from unfolder_configs import load_unfolders, print_unfolder_names
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt 
from TopAnalysis.SystematicsManager import SystematicsManager
import copy


def draw_ATLAS_labels(ax, messages):
    """
    Draw the ATLAS label for a Matplotlib plot .
    """
    plt.rc('font', family='sans-serif',size=25)
    plt.text(0.05, 0.95, '\\textbf{ATLAS} Internal', fontsize=30, transform=ax.transAxes)
    plt.text(0.05, 0.93, '$\\sqrt{s}=13$ TeV, 79.8fb$^{-1}$', fontsize=22, transform=ax.transAxes)
    y = 0.91
    for msg in messages:
        plt.text(0.05, y, msg, fontsize=22, transform=ax.transAxes)
        y -= 0.02
    plt.rc('font', family='serif',size=25)

def get_templates(unfolder_name):
    # Load the previously ran unfolder 
    unfolder = load_unfolders([unfolder_name], 
                       additional_samples=c.additional_samples,
                       background_samples=background_samples,
                       load_unfolder=True,
                       verbosity=0)[0]

    # Get the histograms as bin centres 
    DC_on, DC_off, nominal,data = (rhf.grab_bin_centres(unfolder.additional_sample_hist["Pythia8 DC on"]),
                                 rhf.grab_bin_centres(unfolder.additional_sample_hist["Pythia8 DC off"]),
                                 rhf.grab_bin_centres(unfolder.corr_unfolded_mc),
                                 rhf.grab_bin_centres(unfolder.corr_unfolded_data))



    # load the systematics
    unfolder.systematics = SystematicsManager.load_systematics(unfolder, 
                                            selected_systematics="all",
                                            grouped=True, )

    unfolder.load_systematics(unfolder.output_folder + "/systematics/")

    systematics = {}

    # For each systematic evaluate the shift!
    nominal  = np.sum(np.array(data)/np.sum(nominal))*np.array(nominal)
    nominal_normed = (1.0/np.sum(nominal) )*np.array(nominal)
    # print(" Nominal normed: " + str( nominal_normed ))
    syst_combination = {}
    systematics["nominal"] = np.multiply( np.array(nominal), 
                                        np.array([ 1.0 for i in nominal ] )
                                        )
    syst_combination["nominal"] = "None"
    for syst_name in unfolder.systematics:
        try:
            syst_name_cleaned = syst_name.replace("_up","").replace("_down","")
            syst_name_cleaned = syst_name_cleaned.replace("_Up","").replace("_Down","")

            # Not working in the corret way! 
            systematics[syst_name_cleaned] = np.multiply(
                                          np.array(nominal),
                                          np.array([ 1.0 for i in nominal ] ) +
                                          0.01*np.array(rhf.grab_bin_centres(
                                            unfolder.systematics[syst_name].total_up_variation
                                            )
                                          )
                                          # 0.01*np.maximum(
                                          #   ), 
                                          #  -1.0*np.array(rhf.grab_bin_centres(
                                          #    unfolder.systematics[syst_name].total_down_variation
                                          # ))
                                        # )
                                    )

            syst_combination[syst_name_cleaned] = unfolder.systematics[syst_name].combination 
            systematics[syst_name_cleaned] = systematics[syst_name_cleaned].tolist()

            # print("Loaded systematic " + syst_name_cleaned + ":  " +str(systematics[syst_name_cleaned]))
        except AttributeError:
            print("WARNING: Failed to find systematic shift for ", syst_name_cleaned)
            continue

    # Calculate a modified DC_off scenario which is shifted by the amount the DC on is 
    # across of phase-space
    DC_off_normed = (1.0/np.sum(DC_off) )*np.array(DC_off)
    DC_on_normed = (1.0/np.sum(DC_on) )*np.array(DC_on)

    # 
    delta = np.array(nominal_normed) - np.array(DC_on_normed)
    DC_off_mod =  DC_off_normed + delta
    DC_on_mod =  DC_on + delta
    DC_on_closure = np.array(nominal_normed) - DC_on_mod

    # Unnormalize the deadcone-hypotheses   
    DC_off  = np.sum(np.array(data)/np.sum(DC_off))*np.array(DC_off)
    DC_off_mod  = np.sum(np.array(data)/np.sum(DC_off_mod))*np.array(DC_off_mod)
    DC_on  = np.sum(np.array(data)/np.sum(DC_on))*np.array(DC_on)

    # Convert to arrays
    nominal = nominal.tolist()
    DC_off_mod = DC_off_mod.tolist()
    DC_on = DC_on.tolist() 
    DC_off = DC_off.tolist() 

    syst_uncerts =  np.array(rhf.grab_bin_centres(unfolder.total_stat_syst_up))
    syst_uncerts = syst_uncerts.tolist()
    print(unfolder_name + ": Loaded systematic uncertainty sie:" +  str(syst_uncerts))

    return DC_on, DC_off, nominal,DC_off_mod, data, syst_uncerts, systematics , syst_combination


def create_mu_plot(
    had_SR_mu, had_SR_err_stat, had_SR_err_syst,
    lep_SR_mu, lep_SR_err_stat, lep_SR_err_syst,
    had_CR_mu, had_CR_err_stat, had_CR_err_syst,
    lep_CR_mu, lep_CR_err_stat, lep_CR_err_syst,
    global_mu, global_err_stat, global_err_syst,
    name,
    output_folder,
    messages ):

    plt.rc('text', usetex=True)
    plt.rc('font', family='serif',size=25)
    plt.figure(figsize=(20,20))
    ax=plt.gca()
    # plt.errorbar([lep_CR_mu], [1.],xerr=[max(lep_CR_err_stat, lep_CR_err_syst)], fmt='ro');
    plt.errorbar([had_CR_mu], [5.],xerr=[(had_CR_err_stat**2+ had_CR_err_syst**2)**0.5], fmt='ro',capsize=20,elinewidth=5,capthick=3,markersize=12)
    plt.errorbar([had_CR_mu], [5.],xerr=[had_CR_err_stat], fmt='bo',capsize=20,elinewidth=5,capthick=3,markersize=12)

    plt.errorbar([had_SR_mu], [4.],xerr=[(had_SR_err_stat**2+ had_SR_err_syst**2)**0.5], fmt='ro',capsize=20,elinewidth=5,capthick=3,markersize=12)
    plt.errorbar([had_SR_mu], [4.],xerr=[had_SR_err_stat], fmt='bo',capsize=20,elinewidth=5,capthick=3,markersize=12)

    plt.errorbar([lep_CR_mu], [3.],xerr=[(lep_CR_err_stat**2+ lep_CR_err_syst**2)**0.5], fmt='ro',capsize=20,elinewidth=5,capthick=3,markersize=12)
    plt.errorbar([lep_CR_mu], [3.],xerr=[lep_CR_err_stat], fmt='bo',capsize=20,elinewidth=5,capthick=3,markersize=12)
   
    plt.errorbar([lep_SR_mu], [2.],xerr=[(lep_SR_err_stat**2 + lep_SR_err_syst**2)**0.5], fmt='ro',capsize=20,elinewidth=5,capthick=3,markersize=12)
    plt.errorbar([lep_SR_mu], [2.],xerr=[lep_SR_err_stat], fmt='bo',capsize=20,elinewidth=5,capthick=3,markersize=12)
    
    plt.errorbar([global_mu], [1.],xerr=[(global_err_stat**2 + global_err_syst**2)**0.5], fmt='ro',capsize=20,elinewidth=5,capthick=3,markersize=12)
    plt.errorbar([global_mu], [1.],xerr=[global_err_stat], fmt='bo',capsize=20,elinewidth=5,capthick=3,markersize=12)
    xmin, xmax = plt.xlim()
    plt.xlim(xmax=xmax +0.5*(xmax-xmin))
    text_x_pos = 0.05*(xmax-xmin)+max(lep_SR_mu+(lep_SR_err_stat**2 + 
                                       lep_SR_err_syst**2)**0.5, 
                                      max(had_SR_mu+(had_SR_err_stat**2+ 
                                           had_SR_err_syst**2)**0.5,
                                           max(lep_CR_mu+(lep_CR_err_stat**2+
                                                lep_CR_err_syst**2)**0.5, 
                                               had_CR_mu+(had_CR_err_stat**2
                                                + had_CR_err_syst**2)**0.5
                                              ) 
                                          )
                                      )
    plt.text(text_x_pos, 5.5, "    $\\mu$ $\\pm$  stat $\\pm$ syst", fontsize=30)
    plt.text(text_x_pos, 5.0, "%.3f $\\pm$ %.3f $\\pm$ %.3f"%(had_CR_mu,had_CR_err_stat,had_CR_err_syst), fontsize=30)
    plt.text(text_x_pos, 4.0, "%.3f $\\pm$ %.3f $\\pm$ %.3f"%(had_SR_mu,had_SR_err_stat,had_SR_err_syst), fontsize=30)
    plt.text(text_x_pos, 3.0, "%.3f $\\pm$ %.3f $\\pm$ %.3f"%(lep_CR_mu,lep_CR_err_stat,lep_CR_err_syst), fontsize=30)
    plt.text(text_x_pos, 2.0, "%.3f $\\pm$ %.3f $\\pm$ %.3f"%(lep_SR_mu,lep_SR_err_stat,lep_SR_err_syst), fontsize=30)
    plt.text(text_x_pos, 1.0, "%.3f $\\pm$ %.3f $\\pm$ %.3f"%(global_mu,global_err_stat,global_err_syst), fontsize=30)
    

    plt.axvline(global_mu,color='k', ls='--')
    plt.grid()
    N = 7
    ind = np.arange(N)

    plt.xlabel('Deadcone effect signal strength, $f_{DC}$')
    plt.yticks(ind, ('', 'Global', 'Leptonic SR', 'Leptonic CR', 'Hadronic SR', 'Hadronic CR', ''))
    plt.yticks(ind, ('', 'Global', 'Leptonic SR', 'Leptonic CR', 'Hadronic SR', 'Hadronic CR', ''))
    draw_ATLAS_labels(plt.gca(), messages)
    rhf.create_folder(output_folder)
    plt.savefig(output_folder+name)
    plt.show()



def create_mu_plot(
    had_SR_mu, had_SR_err_stat, had_SR_err_syst,
    lep_SR_mu, lep_SR_err_stat, lep_SR_err_syst,
    had_CR_mu, had_CR_err_stat, had_CR_err_syst,
    lep_CR_mu, lep_CR_err_stat, lep_CR_err_syst,
    global_mu, global_err_stat, global_err_syst,
    name,
    output_folder,
    messages ):

    plt.rc('text', usetex=True)
    plt.rc('font', family='serif',size=25)
    plt.figure(figsize=(20,20))
    ax=plt.gca()
  
    plt.errorbar([lep_CR_mu], [3.],xerr=[(lep_CR_err_stat**2+ lep_CR_err_syst**2)**0.5], fmt='ro',capsize=20,elinewidth=5,capthick=3,markersize=12)
    plt.errorbar([lep_CR_mu], [3.],xerr=[lep_CR_err_stat], fmt='bo',capsize=20,elinewidth=5,capthick=3,markersize=12)
   
    plt.errorbar([lep_SR_mu], [2.],xerr=[(lep_SR_err_stat**2 + lep_SR_err_syst**2)**0.5], fmt='ro',capsize=20,elinewidth=5,capthick=3,markersize=12)
    plt.errorbar([lep_SR_mu], [2.],xerr=[lep_SR_err_stat], fmt='bo',capsize=20,elinewidth=5,capthick=3,markersize=12)
    
    plt.errorbar([global_mu], [1.],xerr=[(global_err_stat**2 + global_err_syst**2)**0.5], fmt='ro',capsize=20,elinewidth=5,capthick=3,markersize=12)
    plt.errorbar([global_mu], [1.],xerr=[global_err_stat], fmt='bo',capsize=20,elinewidth=5,capthick=3,markersize=12)
    xmin, xmax = plt.xlim()
    plt.xlim(xmax=xmax +0.5*(xmax-xmin))
    text_x_pos = 0.05*(xmax-xmin)+max(lep_SR_mu+(lep_SR_err_stat**2 + 
                                       lep_SR_err_syst**2)**0.5, 
                                      lep_CR_mu+(lep_CR_err_stat**2+
                                            lep_CR_err_syst**2)**0.5
                                      )
    plt.text(text_x_pos, 3.5, "    $\\mu$ $\\pm$  stat $\\pm$ syst", fontsize=30)
    plt.text(text_x_pos, 3.0, "%.3f $\\pm$ %.3f $\\pm$ %.3f"%(lep_CR_mu,lep_CR_err_stat,lep_CR_err_syst), fontsize=30)
    plt.text(text_x_pos, 2.0, "%.3f $\\pm$ %.3f $\\pm$ %.3f"%(lep_SR_mu,lep_SR_err_stat,lep_SR_err_syst), fontsize=30)
    plt.text(text_x_pos, 1.0, "%.3f $\\pm$ %.3f $\\pm$ %.3f"%(global_mu,global_err_stat,global_err_syst), fontsize=30)
    

    plt.axvline(global_mu,color='k', ls='--')
    plt.grid()
    N = 5
    ind = np.arange(N)

    plt.xlabel('Deadcone effect signal strength, $f_{DC}$')
    plt.yticks(ind, ('', 'Global', 'Leptonic SR', 'Leptonic CR', ''))
    plt.yticks(ind, ('', 'Global', 'Leptonic SR', 'Leptonic CR', ''))
    draw_ATLAS_labels(plt.gca(), messages)
    rhf.create_folder(output_folder)
    plt.savefig(output_folder+name)
    plt.show()

run_LO, run_NLO, run_reweighted = True, True, True
def main():
    # Get the templates for our four regions 
    print("Loading unfolders.")
    lep_SR_DC_on, lep_SR_DC_off, lep_SR_nom, lep_SR_DC_off_mod, lep_SR_data, lep_SR_syst, lep_SR_syst_shifts,lep_SR_syst_combs = get_templates("signal_region")
    had_SR_DC_on, had_SR_DC_off, had_SR_nom, had_SR_DC_off_mod, had_SR_data, had_SR_syst, had_SR_syst_shifts,had_SR_syst_combs = get_templates("hadronic_signal_region")
    had_CR_DC_on, had_CR_DC_off, had_CR_nom, had_CR_DC_off_mod, had_CR_data, had_CR_syst, had_CR_syst_shifts,had_CR_syst_combs = get_templates("hadronic_control_region")
    lep_CR_DC_on, lep_CR_DC_off, lep_CR_nom, lep_CR_DC_off_mod, lep_CR_data, lep_CR_syst, lep_CR_syst_shifts,lep_CR_syst_combs = get_templates("control_region")

    # Perform the template fits 
    had_SR_fitter = TemplateFitter(template_1=had_SR_DC_on, template_2=had_SR_DC_off, data=had_SR_data, syst_shifts=had_SR_syst_shifts, combinations=had_SR_nom)
    lep_SR_fitter = TemplateFitter(template_1=lep_SR_DC_on, template_2=lep_SR_DC_off, data=lep_SR_data, syst_shifts=lep_SR_syst_shifts, combinations=lep_SR_nom)
    had_CR_fitter = TemplateFitter(template_1=had_CR_DC_on, template_2=had_CR_DC_off, data=had_CR_data, syst_shifts=had_CR_syst_shifts, combinations=had_CR_nom)
    lep_CR_fitter = TemplateFitter(template_1=lep_CR_DC_on, template_2=lep_CR_DC_off, data=lep_CR_data, syst_shifts=lep_CR_syst_shifts, combinations=lep_CR_nom)

    #Create the fitters using the nominal NLO MC as the SM template
    had_SR_nom_fitter = TemplateFitter(template_1=had_SR_nom, template_2=had_SR_DC_off, data=had_SR_data,  syst_shifts=had_SR_syst_shifts, combinations=had_SR_nom)
    lep_SR_nom_fitter = TemplateFitter(template_1=lep_SR_nom, template_2=lep_SR_DC_off, data=lep_SR_data,  syst_shifts=lep_SR_syst_shifts, combinations=lep_SR_nom)
    had_CR_nom_fitter = TemplateFitter(template_1=had_CR_nom, template_2=had_CR_DC_off, data=had_CR_data,  syst_shifts=had_CR_syst_shifts, combinations=had_CR_nom)
    lep_CR_nom_fitter = TemplateFitter(template_1=lep_CR_nom, template_2=lep_CR_DC_off, data=lep_CR_data,  syst_shifts=lep_CR_syst_shifts, combinations=lep_CR_nom)

    #Create the fitters using the nominal NLO MC as the SM template
    had_SR_mod_fitter = TemplateFitter(template_1=had_SR_nom, template_2=had_SR_DC_off_mod, data=had_SR_data,  syst_shifts=had_SR_syst_shifts, combinations=had_SR_nom)
    lep_SR_mod_fitter = TemplateFitter(template_1=lep_SR_nom, template_2=lep_SR_DC_off_mod, data=lep_SR_data,  syst_shifts=lep_SR_syst_shifts, combinations=lep_SR_nom)
    had_CR_mod_fitter = TemplateFitter(template_1=had_CR_nom, template_2=had_CR_DC_off_mod, data=had_CR_data,  syst_shifts=had_CR_syst_shifts, combinations=had_CR_nom)
    lep_CR_mod_fitter = TemplateFitter(template_1=lep_CR_nom, template_2=lep_CR_DC_off_mod, data=lep_CR_data,  syst_shifts=lep_CR_syst_shifts, combinations=lep_CR_nom)

    # Create global fit by simply combining the bins 
    global_DC_on       = lep_SR_DC_on  + lep_CR_DC_on #+ had_SR_DC_on +had_CR_DC_on
    global_DC_off      = lep_SR_DC_off + lep_CR_DC_off  #+ had_SR_DC_off + had_CR_DC_off
    global_DC_off_mod  = lep_SR_DC_off_mod + lep_CR_DC_off_mod  #+ had_SR_DC_off_mod + had_CR_DC_off_mod
    global_nom         = lep_SR_nom  + lep_CR_nom #+ had_SR_nom +had_CR_nom
    global_data        = lep_SR_data + lep_CR_data # + had_SR_data +had_CR_data 
    global_syst_uncert = lep_SR_syst + lep_CR_syst #+ had_SR_syst  +had_CR_syst 
    global_syst_shifts, global_syst_combs = {}, {}
    for syst_name in lep_SR_syst_shifts:
        try:
            global_syst_shifts[syst_name] = list(lep_SR_syst_shifts[syst_name]) + list(lep_CR_syst_shifts[syst_name]) #+ list(had_SR_syst_shifts[syst_name]) + list(had_CR_syst_shifts[syst_name])
            global_syst_combs[syst_name] = list(lep_SR_syst_combs[syst_name]) + list(lep_CR_syst_combs[syst_name]) #+ list(had_SR_syst_combs[syst_name]) + list(had_CR_syst_combs[syst_name])
        except KeyError:
            print("WARNING: Failed to combine error " + syst_name  + " into "\
                  " global fit")
    print "Created global uncerties", global_syst_shifts.keys()

    # Create fitting tool for this global fit 
    global_fitter = TemplateFitter(template_1=global_DC_on, template_2=global_DC_off, data=global_data, combinations=global_syst_combs, syst_shifts=global_syst_shifts)
    global_nom_fitter = TemplateFitter(template_1=global_nom, template_2=global_DC_off, data=global_data, combinations=global_syst_combs, syst_shifts=global_syst_shifts)
    global_mod_fitter = TemplateFitter(template_1=global_nom, template_2=global_DC_off_mod, data=global_data, combinations=global_syst_combs, syst_shifts=global_syst_shifts)

    # Extract the paramaters 
    if run_LO:
        print("Performing the LO measurement.")

        had_SR_mu, had_SR_err_stat, had_SR_err_syst = had_SR_fitter.fit(stat_uncert=np.sqrt(had_SR_data)/had_SR_data)
        lep_SR_mu, lep_SR_err_stat, lep_SR_err_syst = lep_SR_fitter.fit(stat_uncert=np.sqrt(lep_SR_data)/lep_SR_data)
        had_CR_mu, had_CR_err_stat, had_CR_err_syst = had_CR_fitter.fit(stat_uncert=np.sqrt(had_CR_data)/had_CR_data)
        lep_CR_mu, lep_CR_err_stat, lep_CR_err_syst = lep_CR_fitter.fit(stat_uncert=np.sqrt(lep_CR_data)/lep_CR_data)
        global_mu, global_err_stat, global_err_syst = global_fitter.fit(stat_uncert=np.sqrt(global_data)/global_data)
        had_SR_fitter.plot_uncertainty_distributions(c.template_fit_output_folder, "had_SR_loglikelihood_profile.png", messages=["Pythia8 LO templates"])
        lep_SR_fitter.plot_uncertainty_distributions(c.template_fit_output_folder, "lep_SR_loglikelihood_profile.png",messages=["Pythia8 LO templates"])
        had_CR_fitter.plot_uncertainty_distributions(c.template_fit_output_folder, "had_CR_loglikelihood_profile.png", messages=["Pythia8 LO templates"])
        lep_CR_fitter.plot_uncertainty_distributions(c.template_fit_output_folder, "lep_CR_loglikelihood_profile.png", messages=["Pythia8 LO templates"])
        global_fitter.plot_uncertainty_distributions(c.template_fit_output_folder, "global_loglikelihood_profile.png",messages=["Pythia8 LO templates"])
        create_mu_plot(
            had_SR_mu, had_SR_err_stat, had_SR_err_syst,
            lep_SR_mu, lep_SR_err_stat, lep_SR_err_syst,
            had_CR_mu, had_CR_err_stat, had_CR_err_syst,
            lep_CR_mu, lep_CR_err_stat, lep_CR_err_syst,
            global_mu, global_err_stat, global_err_syst,
            name="LO_template_fit_resuls.png",
            output_folder=c.template_fit_output_folder,
            messages=["Pythia8 LO templates"]
            )
        print("Results saved to: " + c.template_fit_output_folder + "LO_template_fit_resuls.png")

    #Extract the paramaters 
    if run_NLO:
        print("Performing the NLO measurement.")
        print("FITTING HAD SR")
        had_SR_mu, had_SR_err_stat, had_SR_err_syst = had_SR_nom_fitter.fit(stat_uncert=np.sqrt(had_SR_data)/had_SR_data)

        print("\n\n\n")
        print("FITTING LEP SR")
        lep_SR_mu, lep_SR_err_stat, lep_SR_err_syst = lep_SR_nom_fitter.fit(stat_uncert=np.sqrt(lep_SR_data)/lep_SR_data)
        had_CR_mu, had_CR_err_stat, had_CR_err_syst = had_CR_nom_fitter.fit(stat_uncert=np.sqrt(had_CR_data)/had_CR_data)
        lep_CR_mu, lep_CR_err_stat, lep_CR_err_syst = lep_CR_nom_fitter.fit(stat_uncert=np.sqrt(lep_CR_data)/lep_CR_data)
        global_mu, global_err_stat, global_err_syst = global_nom_fitter.fit(stat_uncert=np.sqrt(global_data)/global_data)

        had_SR_nom_fitter.plot_uncertainty_distributions(c.template_fit_output_folder, "nom_had_SR_loglikelihood_profile.png",
            messages=["Powheg+Pythia8 NLO SM template"]) 
        lep_SR_nom_fitter.plot_uncertainty_distributions(c.template_fit_output_folder, "nom_lep_SR_loglikelihood_profile.png",
            messages=["Powheg+Pythia8 NLO SM template"])
        had_CR_nom_fitter.plot_uncertainty_distributions(c.template_fit_output_folder, "nom_had_CR_loglikelihood_profile.png",
            messages=["Powheg+Pythia8 NLO SM template"]) 
        lep_CR_nom_fitter.plot_uncertainty_distributions(c.template_fit_output_folder, "nom_lep_CR_loglikelihood_profile.png",
            messages=["Powheg+Pythia8 NLO SM template"])
        global_nom_fitter.plot_uncertainty_distributions(c.template_fit_output_folder, "nom_global_loglikelihood_profile.png",
            messages=["Powheg+Pythia8 NLO SM template"])

        create_mu_plot(
            had_SR_mu, had_SR_err_stat, had_SR_err_syst,
            lep_SR_mu, lep_SR_err_stat, lep_SR_err_syst,
            had_CR_mu, had_CR_err_stat, had_CR_err_syst,
            lep_CR_mu, lep_CR_err_stat, lep_CR_err_syst,
            global_mu, global_err_stat, global_err_syst,
            name="nLO_template_fit_resuls.png",
            output_folder=c.template_fit_output_folder,
            messages=["Powheg+Pythia8 NLO SM template"]
            )

        print("Results saved to: " + c.template_fit_output_folder + "nLO_template_fit_resuls.png")


    # # Extract the paramaters 
    if run_reweighted:
        print("Performing the NLOvs  reweighted LO measurement.")
        had_SR_mu, had_SR_err_stat, had_SR_err_syst = had_SR_mod_fitter.fit(stat_uncert=np.sqrt(had_SR_data)/had_SR_data)
        lep_SR_mu, lep_SR_err_stat, lep_SR_err_syst = lep_SR_mod_fitter.fit(stat_uncert=np.sqrt(lep_SR_data)/lep_SR_data)
        had_CR_mu, had_CR_err_stat, had_CR_err_syst = had_CR_mod_fitter.fit(stat_uncert=np.sqrt(had_CR_data)/had_CR_data)
        lep_CR_mu, lep_CR_err_stat, lep_CR_err_syst = lep_CR_mod_fitter.fit(stat_uncert=np.sqrt(lep_CR_data)/lep_CR_data)
        global_mu, global_err_stat, global_err_syst = global_mod_fitter.fit(stat_uncert=np.sqrt(global_data)/global_data)
        had_SR_mod_fitter.plot_uncertainty_distributions(c.template_fit_output_folder, "reweight_had_SR_loglikelihood_profile.png",
            messages=["Powheg+Pythia8 and Pythia8 Reweighted LO template"])
        lep_SR_mod_fitter.plot_uncertainty_distributions(c.template_fit_output_folder, "reweight_lep_SR_loglikelihood_profile.png",
            messages=["Powheg+Pythia8 and Pythia8 Reweighted LO template"])
        had_CR_mod_fitter.plot_uncertainty_distributions(c.template_fit_output_folder, "reweight_had_CR_loglikelihood_profile.png",
            messages=["Powheg+Pythia8 and Pythia8 Reweighted LO template"])
        lep_CR_mod_fitter.plot_uncertainty_distributions(c.template_fit_output_folder, "reweight_lep_CR_loglikelihood_profile.png",
            messages=["Powheg+Pythia8 and Pythia8 Reweighted LO template"])
        global_mod_fitter.plot_uncertainty_distributions(c.template_fit_output_folder, "reweight_global_loglikelihood_profile.png",
            messages=["Powheg+Pythia8 and Pythia8 Reweighted LO template"])

        create_mu_plot(
            had_SR_mu, had_SR_err_stat, had_SR_err_syst,
            lep_SR_mu, lep_SR_err_stat, lep_SR_err_syst,
            had_CR_mu, had_CR_err_stat, had_CR_err_syst,
            lep_CR_mu, lep_CR_err_stat, lep_CR_err_syst,
            global_mu, global_err_stat, global_err_syst,
            name="reweighted_LO_template_fit_resuls.png",
            output_folder=c.template_fit_output_folder,
            messages=["Powheg+Pythia8 and Pythia8 Reweighted LO template"]
            )
        print("Results saved to: " + c.template_fit_output_folder + "reweighted_LO_template_fit_resuls.png")


# 
main()