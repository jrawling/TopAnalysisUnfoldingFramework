'''
    Author: Jacob Rawling
    Date:

    Utilizes the TopAnalysis.Unfolding package ( ) to create the migration matrix,
    perform the desired type of unfolding, run some tests, save the ouptut in ROOT
    and Journal ready eps fomrat
'''
from TopAnalysis.Unfolding.Unfolder import Unfolder
from TopAnalysis.Unfolding.IdentityTest import IdentityTest
from TopAnalysis.Unfolding.StressTest import StressTest
from TopAnalysis.Unfolding.ClosureTest import ClosureTest
from TopAnalysis.Unfolding.IterationConvergenceTest import IterationConvergenceTest

from TopAnalysis.FakeEvaluator import FakeEvalautor
from TopAnalysis.Unfolding.Systematic import Systematic
from TopAnalysis.SystematicsManager import SystematicsManager
import numpy as np
import config as c
import ROOT as r
from collections import OrderedDict
from TopAnalysis.Backgrounds import background_samples
from unfolder_configs import load_unfolders, print_unfolder_names
from TopAnalysis.SystematicsManager.all_systs_grouped import combinations
import sys 

def main():
    # Read in the unfolding region
    unfolder_names = None
    if len(sys.argv) >= 1:
        unfolder_names = sys.argv[1:]
    r.gROOT.SetBatch()

    # Load the unfolder from the configurations we have pre-defined 
    unfolders = load_unfolders(unfolder_names, 
                               additional_samples=c.additional_samples,
                               background_samples=background_samples,
                               load_unfolder=True)
    if unfolders == []:
        print "USAGE: python2.7 perform_nominal_unfoldering <unfolder_name 1> <unfolder_name > ..."
        print "       where unfolder_name is one of the following: "
        print_unfolder_names()
        sys.exit()

    for unfolder in unfolders:

        # Choose which systematics to run over, configured correctly (hopefully) behind the
        # Scenes.
        # See SysteamaticsManager.load_systematics and Unfolding.Systematic for details
        # on these are managed
        unfolder.systematics = SystematicsManager.load_systematics(unfolder, 
                                                selected_systematics="all",
                                                grouped=True )

        # 
        unfolder.load_systematics(unfolder.output_folder + "/systematics/")

        # Now draw everything, tests, systematics, unfolded data, etc.
        r.gROOT.SetBatch()
        unfolder.draw_reco_plots()
        unfolder.draw_nominal_unfolding_plots()
        unfolder.double_ratio_sample_names = ["Pythia8 DC off","Pythia8 DC on"]
        unfolder.double_ratio_sample_name = "Pythia8 DC on"
        unfolder.draw_unfolded_distribution(additional_systs_to_draw=['Frag._and_Had.', 'Matrix_Element'])

        # Draw the unfolded systematics 
        unfolder.draw_systematics(combinations)
        unfolder.print_stat_uncertaintiy()  
        unfolder.evaluate_statistical_cov()      

        unfolder.evaluate_chi2()
        
        #clean up ROOT stuff
        unfolder.close()


if __name__ == "__main__":
    main()
